//
//  SmileMacro.h
//
//  Created by 。
//          /╭-----------—--—-—--—╮\
//          ||                    ||
//          ||    越努力，越幸运!     ||
//          ||                    ||
//          ||  NSLog(@"💗💗💗")   ||
//          ||                    ||
//          |╰--------------------╯|\
//          ╰.___________________.╯—\--、_
//                  //___\\           )    、
//        ___________________________/___   \
//       /  oooooooooooooooo  .o.  oooo /, ,—\---╮
//      / ==ooooooooooooooo==.o.  ooo= // /   ▓  /
//     /_==__==========__==_ooo__ooo=_/' /______/
//     "=============================“
//

#ifndef SmileMacro_h
#define SmileMacro_h

//是不是预发布环境
#define APIPrePublish [[NSBundle mainBundle].infoDictionary[@"APIPrePublish"] boolValue]
//是不是测试环境
#define APITEST [[NSBundle mainBundle].infoDictionary[@"APITEST"] boolValue]
//当前版本
#define BundleVersion [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]
//获取当前版本号
#define CurrentVersion [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]

//获取当前包名
#define BundleId [NSBundle mainBundle].infoDictionary[@"CFBundleIdentifier"]
//获取当前版本号 去掉小数点后的数字
#define CurrentVersionNum [NSBundle mainBundle].infoDictionary[@"VersionNum"]

#define K_GET_METHOD_RETURN_OBJC(objc) if (objc) return objc

//weak
#define WEAKSELF typeof(self) __weak weakSelf = self;
#define STRONGSELF typeof(self) __strong strongSelf = self;
#define STRONGSELFFor(object) typeof(object) __strong strongSelf = object;
//又拍上传头像的地址
#define UP_AVATAR_API APITEST ? @"http://image-ksoffice.test.upcdn.net" : @"http://pic.bbeitv.com"

//封面默认图
#define coverPlaceholder [UIImage imageNamed:@"cover_placeholder"]
//头像默认图
#define HeaderIconPlaceholder [UIImage imageNamed:@"icon_placeholder"]
//banner默认图
#define bannerPlaceholder [UIImage imageNamed:@"banner_placeholder"]
//视频默认图
#define shortVideoCoverPlaceholder [UIImage imageNamed:@"videoCover_placeholder"]
//网络错误提示
#define KNetworkError @"网络错误，请重试！"

#pragma mark - 💗💗💗【 获取屏幕宽度与高度】
//////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 获取屏幕宽度与高度】
 **/
//////////////////////////////////////////////////////////////////////////////

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenSize [UIScreen mainScreen].bounds.size

#define kScreenScale kScreenWidth/375

/**
 适配 给定4.7寸屏尺寸，适配4和5.5寸屏尺寸
 */
#define Suit55Inch           1.104
#define Suit4Inch            1.171875

/**
 适配 给定5.5寸屏尺寸，适配4和4.7寸屏尺寸
 */
//#define Suit47Inch           1.104
//#define Suit4Inch            1.29375

// 系统判定
#define IOS_VERSION    [[[UIDevice currentDevice]systemVersion]floatValue]
#define IS_IOS8        (IOS_VERSION>=8.0)
#define IS_IOS7        (IOS_VERSION>=7.0)
#define Is_IOS9        (IOS_VERSION>=9.0 && IOS_VERSION<10)

// 屏幕判定
#define IS_IPHONE35INCH  ([SDiPhoneVersion deviceSize] == iPhone35inch ? YES : NO)//4, 4S
#define IS_IPHONE4INCH  ([SDiPhoneVersion deviceSize] == iPhone4inch ? YES : NO)//5, 5C, 5S, SE
#define IS_IPHONE47INCH  ([SDiPhoneVersion deviceSize] == iPhone47inch ? YES : NO)//6, 6S, 7
#define IS_IPHONE55INCH ([SDiPhoneVersion deviceSize] == iPhone55inch ? YES : NO)//6P, 6SP, 7P


#pragma mark - 💗💗💗【 颜色相关 】
//////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 颜色相关 】
 **/
//////////////////////////////////////////////////////////////////////////////

//1.带有RGBA的颜色设置
#define kRGBColor(r, g, b)    [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define kRGBAColor(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(r)/255.0 blue:(r)/255.0 alpha:a]

#define kRandomColor    KRGBColor(arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0)        //随机色生成
//2.rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]

//2.清除背景色
#define CLEARCOLOR [UIColor clearColor]

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]


//💗【 系统颜色 】
#define COLOR_Clear           [UIColor clearColor]
#define COLOR_White           [UIColor whiteColor]
#define COLOR_Black           [UIColor blackColor]
#define COLOR_Red             [UIColor redColor]
#define COLOR_DarkGray        [UIColor darkGrayColor]
#define COLOR_LightGray       [UIColor lightGrayColor]
#define COLOR_GrayColor       [UIColor grayColor]
#define COLOR_Green           [UIColor greenColor]
#define COLOR_BlueColor       [UIColor blueColor]
#define COLOR_Cyan            [UIColor cyanColor]
#define COLOR_Yellow          [UIColor yellowColor]
#define COLOR_Magenta         [UIColor magentaColor]
#define COLOR_Orange          [UIColor orangeColor]
#define COLOR_Purple          [UIColor purpleColor]
#define COLOR_Brown           [UIColor brownColor]
#define color_hex(hex)        [UIColor colorWithHexString:(hex)]


//字体大小
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
/** 是否是4.0屏幕*/
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
/** 是否是4.7屏幕*/
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
/** 是否是5.5屏幕*/
#define iPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)


#ifdef iPhone6
#define fontScale 1.0
#elif iPhone5
#define fontScale 1.0
#elif iPhone6plus
#define fontScale 1.2
#else
#define fontScale 0.7
#endif

#define KBili_SPACE kScreenWidth/380 * fontScale
//字体
#define Font_sys(f) [UIFont systemFontOfSize:(f*SPACE)]

#pragma mark - 💗💗💗【 常用的缩写 】
//////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 常用的缩写 】
 **/
//////////////////////////////////////////////////////////////////////////////
#define kApplication        [UIApplication sharedApplication]
#define kKeyWindow          [UIApplication sharedApplication].keyWindow
#define kAppDelegate        [UIApplication sharedApplication].delegate
#define kUserDefaults      [NSUserDefaults standardUserDefaults]
#define kNotificationCenter [NSNotificationCenter defaultCenter]

#define coverPlaceholder [UIImage imageNamed:@"img_default.png"]

#pragma mark - 💗💗💗【 设备相关 】
//////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 设备相关 】
**/
//////////////////////////////////////////////////////////////////////////////

//💗判断是真机还是模拟器
#if TARGET_OS_IPHONE
//真机
#endif
#if TARGET_IPHONE_SIMULATOR
//模拟器
#endif

//1.APP版本号
#define kAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//2.系统版本号
#define kSystemVersion [[UIDevice currentDevice] systemVersion]

//3.获取当前语言
#define kCurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

//4.判断是否为iPhone
#define kISiPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

//5.判断是否为iPad
#define kISiPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//6.获取沙盒Document路径
#define kDocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]

//7.获取沙盒temp路径
 #define kTempPath NSTemporaryDirectory()

//8.获取沙盒Cache路径
#define kCachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]

//判断设备的操做系统是不是ios7
#define IOS7 (［[UIDevice currentDevice].systemVersion doubleValue] >= 7.0]

//💗设备判断补充
//判断是否为iPhone
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//判断是否为iPad
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//判断是否为ipod
#define IS_IPOD ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])
// 判断是否为 iPhone 5SE
#define iPhone5SE [[UIScreen mainScreen] bounds].size.width == 320.0f && [[UIScreen mainScreen] bounds].size.height == 568.0f
// 判断是否为iPhone 6/6s
#define iPhone6_6s [[UIScreen mainScreen] bounds].size.width == 375.0f && [[UIScreen mainScreen] bounds].size.height == 667.0f
// 判断是否为iPhone 6Plus/6sPlus
#define iPhone6Plus_6sPlus [[UIScreen mainScreen] bounds].size.width == 414.0f && [[UIScreen mainScreen] bounds].size.height == 736.0f
//获取系统版本
#define IOS_SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
//判断 iOS 8 或更高的系统版本
#define IOS_VERSION_8_OR_LATER (([[[UIDevice currentDevice] systemVersion] floatValue] >=8.0)? (YES):(NO))


#pragma mark - 💗💗💗【 开发相关 】
///////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 开发相关 】
 **/
//////////////////////////////////////////////////////////////////////////////

//弱引用/强引用
#define kWeakSelf(type)  __weak typeof(type) weak##type = type;
#define kStrongSelf(type) __strong typeof(type) type = weak##type;


//1.由角度转换弧度
#define kDegreesToRadian(x)      (M_PI * (x) / 180.0)

//2.由弧度转换角度
#define kRadianToDegrees(radian) (radian * 180.0) / (M_PI)

//3.获取一段时间间隔
#define kStartTime CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
#define kEndTime  NSLog(@"Time: %f", CFAbsoluteTimeGetCurrent() - start)

//4.设置View的tag属性
#define VIEWWITHTAG(_OBJECT, _TAG)    [_OBJECT viewWithTag : _TAG]

//5.G－C－D
//GCD - 一次性执行
#define GCD_kDISPATCH_ONCE_BLOCK(onceBlock) static dispatch_once_t onceToken; dispatch_once(&onceToken, onceBlock);
//GCD - 在Main线程上运行
#define GCD_kDISPATCH_MAIN_THREAD(mainQueueBlock) dispatch_async(dispatch_get_main_queue(), mainQueueBlock);
//GCD - 开启异步线程
#define GCD_kDISPATCH_GLOBAL_QUEUE_DEFAULT(globalQueueBlock) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), globalQueueBlocl);





//7.设置 view 圆角和边框
#define KViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

/// View 圆角
#define KViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

///  View加边框
#define KViewBorder(View, BorderColor, BorderWidth )\
\
View.layer.borderColor = BorderColor.CGColor;\
View.layer.borderWidth = BorderWidth;


//8.字符串是否为空
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )

//9.数组是否为空
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)

//10.字典是否为空
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)

//11.是否是空对象
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))


#pragma mark - 💗💗💗【 打印日志 】
///////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 打印日志 】
 **/
//////////////////////////////////////////////////////////////////////////////

//1.开发的时候打印，但是发布的时候不打印的NSLog
#ifdef DEBUG
#define YYLog(...) NSLog(@"%s 第%d行 \n %@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define YYLog(...)
#endif

//2.DEBUG 模式下打印日志,当前行
#ifdef DEBUG
#  define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#  define DLog(...)
#endif





// mark(NSString类型参数)为打印内容标题
#define NSLOG_Str(mark,str)       NSLog(@"🍌%@🍌🍌--str:%@--",(mark),(str))
#define NSLOG_Int(mark,int)       NSLog(@"🍌%@🍌🍌--int:%ld--",(mark),(int))
#define NSLOG_Float(mark,float)   NSLog(@"🍌%@🍌🍌--float:%f--",(mark),(float))
#define NSLOG_Bool(mark,bool)     NSLog(@"🍌%@🍌🍌--bool:%@--",(mark),(bool) ? @"YES" : @"NO")
#define NSLOG_Point(mark,point)   NSLog(@"🍌%@🍌🍌--x:%f--y:%f--",(mark),(point).x,(point).y)
#define NSLOG_Size(mark,size)     NSLog(@"🍌%@🍌🍌--width:%f--height:%f--",(mark),(size).width,(size).height)
#define NSLOG_Frame(mark,frame)   NSLog(@"🍌%@🍌🍌--x:%f--y:%f--width:%f--height:%f--",(mark),(frame).origin.x,(frame).origin.y,(frame).size.width,(frame).size.height)


#pragma mark - 💗💗💗【 图片相关 】
///////////////////////////////////////////////////////////////////////////////
/**
 *  💗【 图片相关 】
 **/
//////////////////////////////////////////////////////////////////////////////
//1.读取本地图片
#define LoadImageFrom(file,ext) [UIImage imageWithContentsOfFile:［NSBundle mainBundle]pathForResource:file ofType:ext］
//2.定义UIImage对象
#define ImageNamed(_pointer) [UIImage imageNamed:[UIUtil imageName:_pointer］
//3.获取图片宽高
#define IMG_ImgWidth(img)        ((img).size.width)
#define IMG_ImgHeight(img)       ((img).size.height)


#endif /* SmileMacro_h */

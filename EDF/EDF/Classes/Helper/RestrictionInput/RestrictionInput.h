//
//  RestrictionInput.h
//  DJ
//
//  Created by 微笑吧阳光 on 2019/6/10.
//  Copyright © 2019 DJ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RestrictionInput : NSObject

+ (void)restrictionInputTextField:(UITextField *)inputClass maxNumber:(NSInteger)maxNumber showView:(UIView *)showView showErrorMessage:(NSString *)errorMessage;

+ (void)restrictionInputTextView:(UITextView *)inputClass maxNumber:(NSInteger)maxNumber showView:(UIView *)showView showErrorMessage:(NSString *)errorMessage;

+ (BOOL)isInputRuleAndBlank:(NSString *)str;


@end

NS_ASSUME_NONNULL_END

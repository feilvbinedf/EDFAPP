//
//  PopCell.m
//  Calculated
//
//  Created by p on 2018/3/29.
//  Copyright © 2018年 p. All rights reserved.
//

#import "PopCell.h"

@implementation PopCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = RGBA(241, 241, 241, 1);
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
         [self setSeparatorInset:UIEdgeInsetsMake(0, 5, 0, 5)];
    }
    return self;
    
}
-(void)get_up
{
    [self.contentView addSubview:self.leftlable];
    [self.contentView addSubview:self.icon];
}
- (UILabel *)leftlable
{
    if (!_leftlable) {
        _leftlable = [[UILabel alloc ] initWithFrame:CGRectMake(RealValue_W(30),5, UIScreenWidth -40, 30)];
        _leftlable.font = AutoBoldFont(12);
        _leftlable.textColor = RGBA(61, 61, 61, 61);
        _leftlable.textAlignment = NSTextAlignmentLeft;
        
    }
    return _leftlable;
}
- (UIButton *)icon
{
    if (!_icon) {
        _icon = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth -40, 22 - RealValue_H(9), RealValue_W(26), RealValue_H(18))];
        [_icon setImage:[UIImage imageNamed:@"xuanzhong"] forState:(UIControlStateSelected)];
        [_icon setImage:[UIImage imageNamed:@""] forState:(UIControlStateNormal)];
    }
    return _icon;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  SmileNetWorkState.m
//  chengguaApp
//
//  Created by 微笑吧阳光 on 2018/4/4.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import "SmileNetWorkState.h"
#import "AFNetworkReachabilityManager.h"
@implementation SmileNetWorkState

+ (void)load {
    
    [SmileNetWorkState checkNetWorkState];
    
}
+ (instancetype)checkNetWorkState {
    
    
    SmileNetWorkState *state = [[SmileNetWorkState alloc] init];
    
    return state;
}
- (instancetype)init {
    if (self = [super init]) {
        
        self.isRightNetwork = YES;
        //1.创建网络监测者
        AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
        
        /*枚举里面四个状态  分别对应 未知 无网络 数据 WiFi
         typedef NS_ENUM(NSInteger, AFNetworkReachabilityStatus) {
         AFNetworkReachabilityStatusUnknown          = -1,      未知
         AFNetworkReachabilityStatusNotReachable     = 0,       无网络
         AFNetworkReachabilityStatusReachableViaWWAN = 1,       蜂窝数据网络
         AFNetworkReachabilityStatusReachableViaWiFi = 2,       WiFi
         };
         */
        __weak typeof(self) weakSelf = self;
        [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            //这里是监测到网络改变的block  可以写成switch方便
            //在里面可以随便写事件
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                    NSLog(@"未知网络状态");
                    strongSelf.isRightNetwork = NO;
                    //                [ONSTipsView ons_showTipsViewWithString:@"网络异常,请检查你的网络情况"];
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                    NSLog(@"无网络");
                    strongSelf.isRightNetwork = NO;
                    //                [ONSTipsView ons_showTipsViewWithString:@"网络异常,请检查你的网络情况"];
                    
                    break;
                    
                case AFNetworkReachabilityStatusReachableViaWWAN:
                    NSLog(@"蜂窝数据网");
                    strongSelf.isRightNetwork = YES;
                    //                [ONSTipsView hideTips];
                    
                    break;
                    
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    NSLog(@"WiFi网络");
                    strongSelf.isRightNetwork = YES;
                    //                self.networkState = YES;
                    break;
                    
                default:
                    break;
            }
            
        }] ;
        //开始监听
        [manager startMonitoring];
        
        
    }
    return self;
}

@end

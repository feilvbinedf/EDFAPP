//
//  UIAlignmentLabel.h
//  BaseApp
//
//  Created by  on 2018/7/3.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,

} VerticalAlignment;
@interface UIAlignmentLabel : UILabel
{
@private
    VerticalAlignment _verticalAlignment;
}

@property (nonatomic) VerticalAlignment verticalAlignment;
@end

//
//  SmileAlert.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/20.
//  Copyright © 2019 p. All rights reserved.
//

#import "SmileAlert.h"

#import "MessageAlertView.h"
#import "LoginMessageAlert.h"
#import "couponAlertShowView.h"
#import "CashCardCenterListPopView.h"
@implementation SmileAlert


//创建单例
+ (SmileAlert *) sharedInstance
{
    static  SmileAlert *sharedInstance = nil ;
    static  dispatch_once_t onceToken;
    dispatch_once (& onceToken, ^ {
        //初始化自己
        sharedInstance = [[self alloc] init];
        
    });
    return  sharedInstance;
}
-(void)mazi_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick{
    if (kStringIsEmpty(content)) {
        content = @"是否确认操作？";
    }
   
    MessageAlertView * contentView =[[NSBundle mainBundle]loadNibNamed:@"MessageAlertView" owner:self options:nil].lastObject;
    contentView.gk_size = CGSizeMake(kScreenWidth-100, (kScreenWidth-100)*0.61);
    [GKCover coverFrom:[UIApplication sharedApplication].keyWindow
           contentView:contentView
                 style:GKCoverStyleTranslucent
             showStyle:GKCoverShowStyleCenter
         showAnimStyle:GKCoverShowAnimStyleCenter
         hideAnimStyle:GKCoverHideAnimStyleCenter
              notClick:YES showBlock:^{
                  NSLog(@"遮罩显示了。。。");
              } hideBlock:^{
                  NSLog(@"遮罩消失了。。。");
              }];
    KViewRadius(contentView, 4);
    contentView.layer.shadowOpacity = 0.8;
    contentView.layer.shadowOffset = CGSizeMake(2, 2);
    contentView.layer.cornerRadius = 4;
    contentView.contentLab.text = content;
    contentView.contentLab.textColor = [UIColor blackColor];
    [contentView.sureBtn add_BtnClickHandler:^(NSInteger tag) {
        [GKCover hideCover];
        if (sureClick) {
            sureClick(YES);
        }
    }];
    
    [contentView.cancelBtn add_BtnClickHandler:^(NSInteger tag) {
        [GKCover hideCover];
        if (sureClick) {
            sureClick(NO);
        }
    }];
    
    
    
}


-(void)registered_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick{
    
    
    LoginMessageAlert * contentView =[[NSBundle mainBundle]loadNibNamed:@"LoginMessageAlert" owner:self options:nil].lastObject;
       contentView.gk_size = CGSizeMake(300, 340);
       [GKCover coverFrom:[UIApplication sharedApplication].keyWindow
              contentView:contentView
                    style:GKCoverStyleTranslucent
                showStyle:GKCoverShowStyleCenter
            showAnimStyle:GKCoverShowAnimStyleCenter
            hideAnimStyle:GKCoverHideAnimStyleCenter
                 notClick:YES showBlock:^{
                     NSLog(@"遮罩显示了。。。");
                 } hideBlock:^{
                     NSLog(@"遮罩消失了。。。");
                 }];

//       contentView.contentLab.text = content;
      contentView.backgroundColor = [UIColor clearColor];
       [contentView.sureBtn add_BtnClickHandler:^(NSInteger tag) {
           [GKCover hideCover];
           if (sureClick) {
               sureClick(YES);
           }
       }];

       [contentView.closeBtn add_BtnClickHandler:^(NSInteger tag) {
           [GKCover hideCover];
           if (sureClick) {
               sureClick(NO);
           }
       }];
       
    
    
}


-(void)couponGetMessage_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick{
    
    
    couponAlertShowView * contentView =[[NSBundle mainBundle]loadNibNamed:@"couponAlertShowView" owner:self options:nil].lastObject;
       contentView.gk_size = CGSizeMake(310, 250);
       [GKCover coverFrom:[UIApplication sharedApplication].keyWindow
              contentView:contentView
                    style:GKCoverStyleTranslucent
                showStyle:GKCoverShowStyleCenter
            showAnimStyle:GKCoverShowAnimStyleCenter
            hideAnimStyle:GKCoverHideAnimStyleCenter
                 notClick:YES showBlock:^{
                     NSLog(@"遮罩显示了。。。");
                 } hideBlock:^{
                     NSLog(@"遮罩消失了。。。");
                 }];

      contentView.backgroundColor = [UIColor clearColor];
       [contentView.sureBtn add_BtnClickHandler:^(NSInteger tag) {
           [GKCover hideCover];
           if (sureClick) {
               sureClick(YES);
           }
       }];

       [contentView.cancelBtn add_BtnClickHandler:^(NSInteger tag) {
           [GKCover hideCover];
           if (sureClick) {
               sureClick(NO);
           }
       }];
       
    [contentView.closeBtn add_BtnClickHandler:^(NSInteger tag) {
          [GKCover hideCover];
      }];
      
    
}

-(void)CashCardCenterListPopView_alertContent:(NSArray*)contentArry AndBlock:(void (^)(BOOL sure, NSDictionary * dataDicc))sureClick{
    
    
    CashCardCenterListPopView * contentView =[[NSBundle mainBundle]loadNibNamed:@"CashCardCenterListPopView" owner:self options:nil].lastObject;
       contentView.gk_size = CGSizeMake(KScreenW-80, (KScreenW-80)*1.1);
       [GKCover coverFrom:[UIApplication sharedApplication].keyWindow
              contentView:contentView
                    style:GKCoverStyleTranslucent
                showStyle:GKCoverShowStyleCenter
            showAnimStyle:GKCoverShowAnimStyleCenter
            hideAnimStyle:GKCoverHideAnimStyleCenter
                 notClick:YES showBlock:^{
                     NSLog(@"遮罩显示了。。。");
                 } hideBlock:^{
                     NSLog(@"遮罩消失了。。。");
                 }];
    contentView.dataArry = contentArry;
      contentView.backgroundColor = [UIColor clearColor];
      contentView.seclectBlock = ^(BOOL sure, NSDictionary * _Nonnull dataDicc) {
        [GKCover hideCover];
                     if (sureClick) {
                         sureClick(sure,dataDicc);
                     }
      };


       [contentView.otherBtn add_BtnClickHandler:^(NSInteger tag) {
           [GKCover hideCover];
           if (sureClick) {
               sureClick(NO,nil);
           }
       }];
       
    [contentView.closeBtn add_BtnClickHandler:^(NSInteger tag) {
          [GKCover hideCover];
      }];
      
    
}


@end

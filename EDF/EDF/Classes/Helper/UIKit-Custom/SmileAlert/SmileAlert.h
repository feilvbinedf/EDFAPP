//
//  SmileAlert.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/20.
//  Copyright © 2019 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmileAlert : NSObject
//创建单例
+ (SmileAlert *) sharedInstance;

//
-(void)mazi_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick;


-(void)registered_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick;
//优惠卡弹框
-(void)couponGetMessage_alertContent:(NSString*)content AndBlock:(void (^)(BOOL sure))sureClick;
//卡包选择
-(void)CashCardCenterListPopView_alertContent:(NSArray*)contentArry AndBlock:(void (^)(BOOL sure, NSDictionary * dataDicc))sureClick;

@end

NS_ASSUME_NONNULL_END

//  LoginViewController.m
//  Calculated
//
//  Created by p on 2018/3/21.
//  Copyright © 2018年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WJUnit : NSObject
/*
 iphoneX
 */
+ (BOOL)deviceIsIPhoneX;

+ (void)showMessage:(NSString *)message;

/**
 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
 
 @param color #开头字符串色值
 @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)color;

/**
 查找指定字符串的富文本
 
 @param str str
 @param attributedString attributedString
 @param color color
 @param font font
 @return 富文本
 */
+ (NSMutableAttributedString *)finderattributedString:(NSString *)str
                                     attributedString:(NSString *)attributedString
                                                color:(UIColor *)color
                                                 font:(UIFont*)font;

//设置行间距
+ (NSMutableAttributedString *)finderattributedLinesString:(NSString *)str
                                          attributedString:(NSString *)attributedString
                                                     color:(UIColor *)color
                                                      font:(UIFont *)font;

/**
 自定义图片大小·
 字符串与图片拼接为富文本字符串
 
 @param str 原始文字
 @param imageStr image名称
 @param index 位置
 @param imageframe  imageframe y轴偏移量 传0为默认(向上偏移约0.5),>0向上偏移，<0向下偏移
 @return 富文本
 */
+ (NSMutableAttributedString *)getcontent:(NSString *)str image:(NSString *)imageStr index:(NSInteger)index imageframe:(CGRect)imageframe;

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

+ (NSString *)getGameIcon:(NSString *)ID;
/**
 获取Window当前显示的ViewController
 
 @return Window当前显示的ViewController
 */
+ (UIViewController*)currentViewController;

//随机数
+ (NSString *)getarc4random;

/*
 那些游戏是否竖屏
 */
+ (BOOL)allowRotationWithGameid :(NSString *)gameid;
@end



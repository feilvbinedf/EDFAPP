//
//  ToastView.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "ToastView.h"

@interface ToastView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIImageView *imageView;
@property (nonatomic,strong)UILabel *titleLable;
@property (nonatomic,assign)CGFloat stayTime;
@property (nonatomic ,strong)NSTimer *backTimer;
@property (nonatomic , assign)NSInteger backCountDown;
@end
@implementation ToastView
- (instancetype)initWithtToastandStayTime:(CGFloat)setStayTime
                             andImageName:(NSString *)imageStr
                              andTitleStr:(NSString *)titleStr
                              andlightStr:(NSString *)andlightStr;
{
    if (self=[super init]) {
        self.frame=CGRectMake(UIScreenWidth/2 -190/2, UIScreenHeight/2 - 126/2, 190, 126);
        self.backgroundColor=[UIColor colorWithWhite:.0 alpha:0];
        [self addSubview:self.bgView];
        [self addSubview:self.imageView];
        [self addSubview:self.titleLable];
        _stayTime = setStayTime;
        _imageView.image = [UIImage imageNamed:imageStr];
        _titleLable.numberOfLines = 0;
        _titleLable.attributedText = [WJUnit finderattributedString:titleStr attributedString:andlightStr color:MAINCOLOR font:AutoFont(14)];
    }
    return self;
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(self.width/2 -230/2 , self.height - 93, 230, 95)];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.shadowColor = RGBA(90, 90, 90, 1).CGColor;
        _bgView.layer.shadowOpacity = 0.8;
        _bgView.layer.shadowOffset = CGSizeMake(2, 2);
        _bgView.layer.cornerRadius = 4;
     
    }
    return _bgView;
}
- (UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
       // _imageView.backgroundColor = [UIColor redColor];
        _imageView.centerX = _bgView.centerX;
    }
    return _imageView;
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, _imageView.bottom , 160, 50)];
        _titleLable.textColor = RGBA(90, 90, 90, 1);
        _titleLable.font = AutoFont(14);
        _titleLable.textAlignment = NSTextAlignmentCenter;
        _titleLable.centerX = _bgView.centerX;
    }
    return _titleLable;
}
-(void)showLXAlertView
{
     [kWindow addSubview:self];
      [self backTime];
}
- (void)backTime
{
    //设置倒计时总时长
    [self.backTimer invalidate];
    self.backCountDown = _stayTime;
    self.backTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(viewBackNSTimer) userInfo:nil repeats:YES];
    
}
//上升
- (void)setGoUpAnimation
{
    [self.backTimer invalidate];
    [UIView animateWithDuration:0.5 animations:^{
        self.hidden = YES;
    } completion:^(BOOL finished) {
        
        [self removeSelf];
        
    }];
}
//使用NSTimer实现倒计时
- (void)viewBackNSTimer
{
    self.backCountDown -- ;
    
    if (self.backCountDown == 0  || self.backCountDown < 0) {
        [self.backTimer invalidate];
        [self setGoUpAnimation];
    }
    
}
-(void)removeSelf
{
    [self removeFromSuperview];
}
@end

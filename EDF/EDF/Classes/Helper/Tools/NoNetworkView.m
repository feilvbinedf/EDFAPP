//
//  NoNetworkView.m
//  Exchange
//
//  Created by p on 2018/8/14.
//  Copyright © 2018年 p. All rights reserved.
//

#import "NoNetworkView.h"
@interface NoNetworkView()
{
    BOOL   _NoNetwork;
}
@end
@implementation NoNetworkView

/**
 构造方法
 @param frame 占位图的frame
 @param NoNetwork 如果有网络说明没有按钮
 */
- (instancetype)initWithFrame:(CGRect)frame
                    NoNetwork:(BOOL )NoNetwork
{
    if (self = [super initWithFrame:frame])
    {
        
        _NoNetwork = NoNetwork;
        [self setUI];
    }
    return self;
}
- (void)dissmiss
{
    [self removeFromSuperview];
}
#pragma mark - UI搭建
- (void)setUI
{
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.width/2 - RealValue_W(160), self.height/2 -  RealValue_W(100), RealValue_W(320), RealValue_W(140))];
    _titlelable = [[UILabel alloc] initWithFrame:CGRectMake(self.width/2 - 120, imageView.bottom+10, 240, 22)];
    _titlelable.adjustsFontSizeToFitWidth = YES;
    _titlelable.font = AutoFont(13);
    _titlelable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titlelable];
   
    if (_NoNetwork) {
        imageView.image = [UIImage imageNamed:@"noData"];
        
         _titlelable.text = @"暂无网络";
      
        
    }else
    {
        imageView.image = [UIImage imageNamed:@"noData"];
        _titlelable.text = @"暂无数据";
    }
    
    [self addSubview:imageView];
  
    
}
@end

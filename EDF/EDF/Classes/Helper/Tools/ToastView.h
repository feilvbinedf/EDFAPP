//
//  ToastView.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToastView : UIView
/**
 自定义图片大小·
 字符串与图片拼接为富文本字符串
 
 @param setStayTime 停留时间
 @param imageStr 成功或者失败图片
 @param titleStr 内容
 @param andlightStr  高亮色内容
 @return 富文本
 */
- (instancetype)initWithtToastandStayTime:(CGFloat)setStayTime
                             andImageName:(NSString *)imageStr
                              andTitleStr:(NSString *)titleStr
                              andlightStr:(NSString *)andlightStr;
-(void)showLXAlertView;
@end


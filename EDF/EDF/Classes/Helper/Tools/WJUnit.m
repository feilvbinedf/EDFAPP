//  LoginViewController.m
//  Calculated
//
//  Created by p on 2018/3/21.
//  Copyright © 2018年 p. All rights reserved.
//

#import "WJUnit.h"
#import "sys/utsname.h"
#import "AppDelegate.h"
@implementation WJUnit
/*
 iphoneX
 */
+ (BOOL)deviceIsIPhoneX {
    struct utsname systemInfo;
    
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    if ([platform isEqualToString:@"i386"] || [platform isEqualToString:@"x86_64"]) {
        // 模拟器下采用屏幕的高度来判断
        return [UIScreen mainScreen].bounds.size.height == 812;
    }
    // iPhone10,6是美版iPhoneX 感谢hegelsu指出：https://github.com/banchichen/TZImagePickerController/issues/635
    BOOL isIPhoneX = [platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"];
    return isIPhoneX;
}

+ (void)showMessage:(NSString *)message
{
    static BOOL isShow = NO;
    if (isShow) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        isShow = YES;
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = AutoFont(14);
        style.verticalPadding =10;
        style.cornerRadius = 0;
        [kWindow makeToast:message
                  duration:1.5
                  position:CSToastPositionCenter
                     style:style];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShow = NO;
        });
    });
}
// 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
+ (UIColor *)colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // 判断前缀并剪切掉
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}
+ (NSMutableAttributedString *)finderattributedString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    if (kStringIsEmpty(str)) {
        NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:@""];
        return muattributeString;
    }
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)finderattributedLinesString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    NSMutableParagraphStyle *paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:5];
    [muattributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)getcontent:(NSString *)str image:(NSString *)imageStr index:(NSInteger)index imageframe:(CGRect)imageframe
{
    CGFloat y = imageframe.origin.y;
    if (y == 0) {
        y = RealValue_H(1);
    }
    NSMutableAttributedString *attri       = [[NSMutableAttributedString alloc] initWithString:str];
    NSTextAttachment *attch                = [[NSTextAttachment alloc] init];
    attch.image                            = [UIImage imageNamed:imageStr];
    attch.bounds                           = CGRectMake(0,y, imageframe.size.width,imageframe.size.height);
    
    NSAttributedString *string             = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:string atIndex:index];
    return attri;
}
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
+ (NSString *)getGameIcon:(NSString *)ID;
{
    if ([ID isEqualToString:@"d"]) {
        
        return @"AG";
    }else if ([ID isEqualToString:@"j"])
    {
        return @"AGPY";
    }else if ([ID isEqualToString:@"f"])
    {
        return @"BNG";
    }else if ([ID isEqualToString:@"k"])
    {
        return @"CQ9";
    }else if ([ID isEqualToString:@"b"])
    {
        return @"ISB";
    }else if ([ID isEqualToString:@"5"])
    {
        return @"GGPY";
    }else if ([ID isEqualToString:@"h"])
    {
        return @"BBIN";
    }else if ([ID isEqualToString:@"g"])
    {
        return @"SkyWind";
    }else if ([ID isEqualToString:@"c"])
    {
        return @"PSN";
    }else if ([ID isEqualToString:@"i"])
    {
        return @"";
    }else if ([ID isEqualToString:@"6"])
    {
        return @"TTG";
    }else if ([ID isEqualToString:@"7"])
    {
        return @"PS";
    }else if ([ID isEqualToString:@"8"])
    {
        return @"MG";
    }else if ([ID isEqualToString:@"1"])
    {
        return @"PT";
    }else if ([ID isEqualToString:@"2"])
    {
        return @"PNG";
    }else if ([ID isEqualToString:@"a"])
    {
        return @"PP";
    }else if ([ID isEqualToString:@"ky"])
    {
        return @"ky";
    }else if ([ID isEqualToString:@"lc"])
    {
        return @"lc";
    }else if ([ID isEqualToString:@"m"])
    {
        return @"JDB";
    }else if ([ID isEqualToString:@"n"])
    {
        return @"SSG";
    }else if ([ID isEqualToString:@"o"])
    {
        return @"JDB1";
    }else if ([ID isEqualToString:@"4"])
    {
        return @"og";
    }else if ([ID isEqualToString:@"p"])
    {
        return @"ebet";
    }else if ([ID isEqualToString:@"q"])
    {
        return @"agicon";
    }else if ([ID isEqualToString:@"r"])
    {
        return @"pg";
    }else if ([ID isEqualToString:@"s"])
    {
        return @"dst";
    }
  
    
    return @"";
}
//获取Window当前显示的ViewController
+ (UIViewController*)currentViewController{
    AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *vc  =  appdele.window.rootViewController;
    
    //    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    //    UIViewController* vc = (UIViewController *)window.rootViewController;
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
        vc = ((UITabBarController*)vc).selectedViewController;
    }
    
    if ([vc isKindOfClass:[UINavigationController class]]) {
        vc = ((UINavigationController*)vc).visibleViewController;
    }
    
    if (vc.presentedViewController) {
        vc = vc.presentedViewController;
    }
    
    return vc;
}
+ (NSString *)getarc4random
{
    int last = (arc4random() % 90) + 10;
    NSString *timeNow = [[NSString alloc] initWithFormat:@"CK%@%i", [self getNowTimeTimestamp3],last];

    NSLog(@"%@", timeNow);
   
    return timeNow;
}
+ (NSString *)getNowTimeTimestamp3{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
}
+ (BOOL)allowRotationWithGameid:(NSString *)gameid;
{
    NSArray * gameids = @[@"5902",@"5908",@"5901",@"5912",@"5907",@"5054",@"5904",@"5119"];
    BOOL isbool = [gameids containsObject:gameid];
    return isbool;
}
@end

//
//  LLWebViewController.h
//  LLWebViewController
//
//  Created by liushaohua on 2017/1/19.
//  Copyright © 2017年 liushaohua. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface LLWebViewController :EDBaseViewController
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, copy) NSString *urlStr;
/**是否支持web下拉刷新 default is NO*/
@property (nonatomic, assign) BOOL isPullRefresh;
@property (nonatomic, assign) BOOL isShowGame;
@property (nonatomic, assign) NSInteger isType; //1 是基本的竖屏不隐藏导航栏 2是竖屏游戏隐藏导航栏 3是横屏隐藏导航栏
@property(nonatomic,assign)BOOL  isWebGo;

@end

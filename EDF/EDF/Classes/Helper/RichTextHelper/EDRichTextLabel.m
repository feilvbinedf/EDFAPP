//
//  EDRichTextLabel.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/21.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDRichTextLabel.h"

@implementation EDRichTextLabel
- (id)initWithFrame:(CGRect)frame rich_TextCofigWith:(NSString *)contentStr  blodStrArry:(NSArray*)boldArry  colortextArry:(NSArray*)colorTextArry;{
    if (self = [super initWithFrame:frame]) {
        [self rich_TextCofigWith:contentStr blodStrArry:boldArry colortextArry:colorTextArry];
    }
    return self;
}
-(void)rich_TextCofigWith:(NSString *)contentStr  blodStrArry:(NSArray*)boldArry  colortextArry:(NSArray*)colorTextArry{
    
    NSMutableAttributedString * strr1 =[NSAttributedString_Encapsulation changeAstringTextColorWithColor:EDColor_BaseBlue string:contentStr andSubString:colorTextArry changeFontAndColor:[UIFont boldSystemFontOfSize:14] Color:kRGBColor(68, 68, 68) blodSubStringArray:boldArry];


    self.numberOfLines = 0;
    self.textColor = kRGBColor(68, 68, 68);
    self.font = [UIFont systemFontOfSize:14];
    self.attributedText = strr1;
    
    
    
    
}
@end

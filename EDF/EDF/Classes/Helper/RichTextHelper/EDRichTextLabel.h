//
//  EDRichTextLabel.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/21.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSAttributedString_Encapsulation.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDRichTextLabel : UILabel

-(void)rich_TextCofigWith:(NSString *)contentStr  blodStrArry:(NSArray*)boldArry  colortextArry:(NSArray*)colorTextArry;

@end

NS_ASSUME_NONNULL_END

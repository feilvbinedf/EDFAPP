//
//  UIBarButtonItem+CABarButton.m
//  Calculated
//
//  Created by p on 2018/7/4.
//  Copyright © 2018年 p. All rights reserved.
//

#import "UIBarButtonItem+CABarButton.h"

@implementation UIBarButtonItem (CABarButton)
/**
 系统导航条按钮UIBarButtonItem封装
 
 @param imageName 图片名字
 @param target 当前Controller
 @param action 方法名
 @return 返回根据图片可定制UIBarButtonItem
 */
+(instancetype)LeftitemWithImageNamed:(NSString *)imageName
                           title:(NSString *)title
                               target:(id)target
                               action:(SEL)action{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
    view.backgroundColor = [UIColor clearColor];
    UIButton * Barbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    Barbutton.frame = CGRectMake(0, 0, 80,25);
    Barbutton.titleLabel.font = AutoFont(16);
    [Barbutton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [Barbutton setTitle:title forState:(UIControlStateNormal)];
   
    [Barbutton setTitleColor:BLACKCOLOR forState:UIControlStateNormal];
    [Barbutton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
     Barbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [Barbutton sizeToFit];
    Barbutton.centerY = view.centerY;
    [view addSubview:Barbutton];
   
    return [[UIBarButtonItem alloc]initWithCustomView:view];
    
    
}
+(instancetype)RightitemWithImageNamed:(NSString *)imageName
                                 title:(NSString *)title
                                target:(id)target
                                action:(SEL)action;
{

    UIButton * Barbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    Barbutton.titleLabel.font = AutoFont(13);
    [Barbutton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [Barbutton setTitle:title forState:(UIControlStateNormal)];
    
    [Barbutton setTitleColor:WHITECOLOR forState:UIControlStateNormal];
    [Barbutton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    Barbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [Barbutton sizeToFit];
   
    
    
    return [[UIBarButtonItem alloc]initWithCustomView:Barbutton];
}
@end

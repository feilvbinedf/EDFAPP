//
//  UIButton+BMExtension.m
//  BMDeliverySpecialists
//
//  Created by fenglh on 15/10/29.
//  Copyright (c) 2015年 BlueMoon. All rights reserved.
//

#import "UIButton+BMExtension.h"


#pragma mark 内部类 BMExButton
@interface BMExButton : UIButton
@property (copy, nonatomic) TapButtonActionBlock action;
@end


@implementation BMExButton

- (instancetype)init
{
    if(self = [super init])
    {
        [self addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        [self addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)btnClick:(UIButton *)button
{
    if(self.action)
    {
        self.action(self);
    }
}
@end




@implementation UIButton (BMExtension)


//  创建普通按钮
+ (instancetype)dd_buttonCustomButtonWithFrame:(CGRect)frame
                                         title:(NSString *)title
                               backgroundColor:(UIColor *)backgroundColor
                                    titleColor:(UIColor *)titleColor
                                     tapAction:(TapButtonActionBlock)tapAction
{
    BMExButton *btn = [BMExButton buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn setTitleColor:titleColor forState:UIControlStateHighlighted];
    btn.backgroundColor = backgroundColor;
    btn.clipsToBounds = YES;
    btn.action = tapAction;
    
    return btn;
}

//  创建图片按钮
+ (instancetype)dd_buttonSystemButtonWithFrame:(CGRect)frame
                   NormalBackgroundImageString:(NSString *)imageString
                                     tapAction:(TapButtonActionBlock)tapAction
{
    BMExButton *btn = [BMExButton buttonWithType:UIButtonTypeSystem];
    btn.frame = frame;
    [btn setBackgroundImage:[UIImage imageNamed:imageString] forState:UIControlStateNormal];
    btn.clipsToBounds = YES;
    btn.action = tapAction;
    return btn;
}


- (void)centerVerticallyImageAndTextWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
    
}


- (void)centerVerticallyImageAndText
{
    const CGFloat kDefaultPadding = 6.0f;
    
    [self centerVerticallyImageAndTextWithPadding:kDefaultPadding];
}

- (void)centerHorizontallyImageAndTextWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGFloat totalWitdh = (imageSize.width + titleSize.width + padding);
    self.imageEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - (totalWitdh - imageSize.width),
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            -imageSize.width,
                                            0.0f,
                                            - (totalWitdh - titleSize.width));
}

- (void)centerHorizontalImageAndText
{
    const CGFloat kDefaultPadding = 6.0f;
    [self centerHorizontallyImageAndTextWithPadding:kDefaultPadding];
}

@end

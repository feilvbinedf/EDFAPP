//
//  UIButton+BMExtension.h
//  BMDeliverySpecialists
//
//  Created by fenglh on 15/10/29.
//  Copyright (c) 2015年 BlueMoon. All rights reserved.

#import <UIKit/UIKit.h>


typedef void(^TapButtonActionBlock) (UIButton *button) ;



@interface UIButton (BMExtension)

/**
 *  快速创建文字Button
 *
 *  @param frame           frame
 *  @param title           title
 *  @param backgroundColor 背景颜色
 *  @param titleColor      文字颜色
 *  @param tapAction       回调
 */
+ (instancetype)dd_buttonCustomButtonWithFrame:(CGRect)frame
                                         title:(NSString *)title
                               backgroundColor:(UIColor *)backgroundColor
                                    titleColor:(UIColor *)titleColor
                                     tapAction:(TapButtonActionBlock)tapAction;
/**
 *   快速创建图片Button
 *
 *  @param frame       frame
 *  @param imageString 按钮的背景图片
 *  @param tapAction   回调
 */
+ (instancetype)dd_buttonSystemButtonWithFrame:(CGRect)frame
                   NormalBackgroundImageString:(NSString *)imageString
                                     tapAction:(TapButtonActionBlock)tapAction;

/**
 *  图片与文字垂直对齐
 *
 *  @param padding 间距
 */
- (void)centerVerticallyImageAndTextWithPadding:(float)padding;

- (void)centerVerticallyImageAndText;

/**
 *  图片与文字水平对齐
 *
 *  @param padding 间距
 */
- (void)centerHorizontallyImageAndTextWithPadding:(float)padding;

- (void)centerHorizontalImageAndText;

@end

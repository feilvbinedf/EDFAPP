//
//  UIBarButtonItem+CABarButton.h
//  Calculated
//
//  Created by p on 2018/7/4.
//  Copyright © 2018年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (CABarButton)
/**
 系统导航条按钮UIBarButtonItem封装
 
 @param imageName 图片名字
 @param target 当前Controller
 @param action 方法名
 @return 返回根据图片可定制UIBarButtonItem
 */
+(instancetype)LeftitemWithImageNamed:(NSString *)imageName
                                title:(NSString *)title
                               target:(id)target
                               action:(SEL)action;

/**
 系统导航条按钮UIBarButtonItem封装
 
 @param imageName 图片名字
 @param title 标题
 @param target 当前Controller
 
 @param action 方法名
 @return 返回根据图片可定制UIBarButtonItem
 */
+(instancetype)RightitemWithImageNamed:(NSString *)imageName
                                 title:(NSString *)title
                                 target:(id)target
                                 action:(SEL)action;
@end



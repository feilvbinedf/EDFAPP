//
//  EDMainTabBarController.m
//  EDF
//
//  Created by  on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDMainTabBarController.h"
#import "EDHomeViewController.h"
#import "EDWalletCenterViewController.h"
#import "EDSearchViewController.h"
#import "EDAttentionViewController.h"
#import "EDUserCenterViewController.h"
#import "JPUSHService.h"
@interface EDMainTabBarController ()<UITabBarDelegate,UITabBarControllerDelegate>

@end

@implementation EDMainTabBarController

- (void)viewDidLoad {
    
    [super viewDidLoad];
     self.delegate =self;
    //初始化子控制器
    EDHomeViewController*vc1 = [[EDHomeViewController alloc] init];
    EDWalletCenterViewController *vc2 = [[EDWalletCenterViewController alloc] init];
    EDSearchViewController *vc3 = [[EDSearchViewController alloc] init];
    EDAttentionViewController *vc4 = [[EDAttentionViewController alloc] init];
    EDUserCenterViewController *vc5 = [[EDUserCenterViewController alloc] init];
    
    [self addChildVc:vc1 title:@"首页" image:@"Home_unSel" selectedImage:@"Home_Sel" vcTag:0];
    [self addChildVc:vc2 title:@"钱包" image:@"Wall_unSel" selectedImage:@"Wall_Sel" vcTag:1];
    [self addChildVc:vc3 title:@"搜索" image:@"Search_unSel" selectedImage:@"Search_Sel" vcTag:2];
    [self addChildVc:vc4 title:@"资讯" image:@"News_unSel" selectedImage:@"News_Sel" vcTag:3];
    [self addChildVc:vc5 title:@"我的" image:@"Center_unSel" selectedImage:@"Center_Sel" vcTag:4];
    
    self.selectedIndex = 0;
  
    
    if ([DJLoginHelper sharedInstance].is_Login ==YES) {
        
        [JPUSHService setAlias:[DJLoginHelper sharedInstance].cj_UserAlias completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            if (iResCode ==0) {
                  NSLog(@"💗极光注册add_setAlias成功");
             }else{
                 NSLog(@"💣极光注册add_setAlias失败");
             }
        } seq:10086];
        
    }
    
    
}

/**
 *  添加一个子控制器
 *
 *  @param childVc       子控制器
 *  @param title         标题
 *  @param image         图片
 *  @param selectedImage 选中的图片
 */
- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage vcTag:(NSInteger)vcTag{
    
    // 设置子控制器的文字
    childVc.title = title; // 同时设置tabbar和navigationBar的文字
    childVc.tabBarItem.title = title; // 设置tabbar的文字
    childVc.navigationItem.title = title; // 设置navigationBar的文字
    
    // 设置子控制器的图片
    UIImage *img=[UIImage imageNamed:image];
    img=[img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]; //不让图片渲染
    childVc.tabBarItem.image=img;

    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.tag = vcTag;
    // 设置文字的样式
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = kRGBAColor(63, 63, 63, 1);
    textAttrs[NSFontAttributeName] = AutoFont(12);

    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] =kRGBColor(44, 143, 219);
    //kRGBAColor(44, 143, 219, 1);;
    [childVc.tabBarItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [childVc.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
        
    // 先给外面传进来的小控制器 包装 一个导航控制器
    EDBaseNavigationController *nav = [[EDBaseNavigationController alloc] initWithRootViewController:childVc];
    // 添加为子控制器
    [self addChildViewController:nav];
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController NS_AVAILABLE_IOS(3_0){
    
    if (viewController.tabBarItem.tag == 4 || viewController.tabBarItem.tag == 3||viewController.tabBarItem.tag == 1 )
    {
        
        if (![DJLoginHelper sharedInstance].is_Login)
        {
            [[DJLoginHelper sharedInstance] dj_showLoginVC:self];
            return NO;
        }else
        {
            return YES;
        }
        
    }
    
    return YES;
}

@end

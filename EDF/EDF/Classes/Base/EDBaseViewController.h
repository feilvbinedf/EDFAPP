//
//  EDBaseViewController.h
//  EDF
//
//  Created by  on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingTableView.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDBaseViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)TPKeyboardAvoidingTableView * ks_TableView;
@property(nonatomic,strong) NSMutableArray * modelsArry;


-(void)ks_tableAddHeaderRequstRefush;
-(void)ks_tableAddFootRequst;

-(void)getData_Refush;
-(void)getData_LoadingMore;

/**
 *  禁止左滑
 *  @param enabled 禁止左滑
 */
- (void)SetTheLoadProperties:(BOOL)enabled;
/*
 开始动画
 */
- (void)showLoadingAnimation;
/*
 结束加载
 */
- (void)stopLoadingAnimation;
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark;

@end

NS_ASSUME_NONNULL_END

//
//  EDBaseNavigationController.h
//  EDF
//
//  Created by  on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDBaseNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END

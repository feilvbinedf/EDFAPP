//
//  EDBaseNavigationController.m
//  EDF
//
//  Created by  on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseNavigationController.h"

@interface EDBaseNavigationController ()

@end

@implementation EDBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置导航栏的颜色显示
    self.navigationBar.barTintColor = WHITECOLOR;
    [self.navigationBar setTitleTextAttributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:18],
                                                 NSForegroundColorAttributeName : WHITECOLOR}];
    

//    [ self.navigationBar setBackgroundImage:[self createImageWithColor:CLEARCOLOR frame:CGRectMake(0, 0, UIScreenWidth,kStatusBarAndNavigationBarHeight)] forBarMetrics:UIBarMetricsDefault];
//    [ self.navigationBar setShadowImage:[UIImage new]];
//
//    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"barImage"] forBarMetrics:UIBarMetricsDefault];
//
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kStatusBarAndNavigationBarHeight)];
    UIImageView *backViewimage = [BBControl createImageViewFrame:CGRectMake(0, 0, kScreenWidth, kStatusBarAndNavigationBarHeight) imageName:@"barImage"];
    backViewimage.contentMode = UIViewContentModeScaleToFill;
    [backView addSubview:backViewimage];

    [self.navigationBar setBackgroundImage:[self convertViewToImage:backView] forBarMetrics:UIBarMetricsDefault];

}
-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需  要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, YES, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

//根据颜色值生成纯色图片
- (UIImage *)createImageWithColor:(UIColor *)color frame:(CGRect)frame {
    CGRect rect = CGRectMake(0, 0, frame.size.width, frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
/**
 *  重写这个方法目的：能够拦截所有push进来的控制器
 *
 *  @param viewController 即将push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if (self.viewControllers.count > 0) { // 这时push进来的控制器viewController，不是第一个子控制器（不是根控制器）
        /* 自动显示和隐藏tabbar */
        viewController.hidesBottomBarWhenPushed = YES;
        
        /* 设置导航栏上面的内容 */
        //设置导航栏标题字体颜色
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.6],NSFontAttributeName:[UIFont systemFontOfSize:18]} forState:UIControlStateNormal];
        
        //回到上一页
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = CGRectMake(0, 0, 25, 25);
        backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);
        [backBtn setImage:[UIImage imageNamed:@"return"] forState:0];
        backBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [backBtn addTarget:self action:@selector(backToFormerpage) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        viewController.navigationItem.leftBarButtonItem = leftBtn;
        
        
        //开启iOS7的滑动返回效果
        if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            //只有在二级页面生效
            if ([self.viewControllers count] >= 1) {
                self.interactivePopGestureRecognizer.delegate = (id) self;
            }
        }
    }
    
    [super pushViewController:viewController animated:animated];
}
- (void)backToFormerpage{
    
    [self popViewControllerAnimated:YES];
}

- (void)backToHomepage{
    
    [self popToRootViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

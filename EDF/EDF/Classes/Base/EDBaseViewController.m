//
//  EDBaseViewController.m
//  EDF
//
//  Created by  on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "DGActivityIndicatorView.h"
#import "MJRefresh.h"
@interface EDBaseViewController ()

@property (nonatomic,strong) DGActivityIndicatorView * activityIndicatorView;


@end

@implementation EDBaseViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.view endEditing:YES];
    [self statusBarStyleIsDark:NO];
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    if (@available(iOS 11.0, *)) {
    self.ks_TableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    } else {
  self.automaticallyAdjustsScrollViewInsets = NO;
        
    }

    
}
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark
{
    if (statusBarStyleIsDark)
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }else
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
      //self.view.backgroundColor =kRGBColor(245, 245, 245);
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.modelsArry = [NSMutableArray array];
    //基类基本设置
    [self  commonSeting];
    _activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeCookieTerminator tintColor:MAINCOLOR];

        

    
}
-(void)commonSeting{
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.view endEditing:YES];
}
-(void)ks_tableAddHeaderRequstRefush{
    WEAKSELF
//    self.ks_TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        //调用刷新方法
//        STRONGSELFFor(weakSelf);
//        [strongSelf getData_Refush];
//    }];
//    [self.ks_TableView.mj_header beginRefreshing];
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData_Refush)];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    header.automaticallyChangeAlpha = YES;
    
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    
    // 马上进入刷新状态
    [header beginRefreshing];
    
    // 设置header
    self.ks_TableView.mj_header = header;
    
}
-(void)ks_tableAddFootRequst{
    WEAKSELF
    self.ks_TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        STRONGSELFFor(weakSelf)
        [strongSelf getData_LoadingMore];
    }];
}

-(void)getData_Refush{
    
}
-(void)getData_LoadingMore{
    
}
- (void)showLoadingAnimation
{
    [_activityIndicatorView startAnimating];
}
- (void)stopLoadingAnimation
{
    
    [_activityIndicatorView stopAnimating];
}


#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UITableViewCell new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
- (void)SetTheLoadProperties:(BOOL)enabled{
    if (enabled) {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }else
    {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
#pragma mark ------setter   ---getter
-(TPKeyboardAvoidingTableView*)ks_TableView
{
    if (_ks_TableView==nil) {
        _ks_TableView=[[TPKeyboardAvoidingTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _ks_TableView.separatorColor=[UIColor clearColor];
        _ks_TableView.delegate=self;
        _ks_TableView.dataSource=self;
        _ks_TableView.sectionFooterHeight=0.01;
        _ks_TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _ks_TableView.estimatedRowHeight =0;
            _ks_TableView.estimatedSectionHeaderHeight =0;
            _ks_TableView.estimatedSectionFooterHeight =0;
            _ks_TableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        
    }
    return _ks_TableView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

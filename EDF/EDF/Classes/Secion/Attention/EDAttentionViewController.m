//
//  EDAttentionViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDAttentionViewController.h"
#import "AttentionPagView.h"
@interface EDAttentionViewController ()
@property(nonatomic,strong)AttentionPagView * pagView;
@end

@implementation EDAttentionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    UIImageView * bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight +RealValue_H(88), UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight - RealValue_H(88))];
    bgImage.image = [UIImage imageNamed:@"ic_infor_bg"];
//    bgImage
    [self.view addSubview:bgImage];
    [self.view addSubview:[self test1]];
   

}
- (AttentionPagView *)test1 {
    _pagView = [[AttentionPagView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, ContentViewHeight)
                                       withTitles:@[@"游戏可视化",@"平台可视化",@"玩家财富榜"]
                              withViewControllers:@[@"GameViewController",@"PlatformViewController",@"WealthViewController"]
                                   withParameters:nil];
    
    
    return _pagView;
}
@end

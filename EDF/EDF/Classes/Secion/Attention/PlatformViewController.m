//
//  PlatformViewController.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PlatformViewController.h"
#import "EDAttentionHeadView.h"
#import "AttentionModel.h"
#import "ZFChart.h"
@interface PlatformViewController ()<ZFGenericChartDataSource, ZFBarChartDelegate,ZFLineChartDelegate>
@property (nonatomic,strong)EDAttentionHeadView * headView;
@property (nonatomic, strong) NSMutableArray * lineArray;//
@property (nonatomic, strong) NSMutableArray * dataArray;//
@property (nonatomic, strong) NSArray * Xarray;//
@property (nonatomic, strong) ZFBarChart * barChart;
@property (nonatomic, strong) ZFLineChart * lineChart;
@property (nonatomic, assign)  NSInteger index;//
@property(nonatomic,strong)UIScrollView * bgSc;
@end

@implementation PlatformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    self.index = 0;
    
    [self.view addSubview:self.headView];
    [self loadTodayPlatViewe];
     _dataArray = [NSMutableArray new];
    _lineArray = [NSMutableArray new];
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, self.headView.bottom, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor =  [UIColor colorWithWhite:0 alpha:0];
    [self.view addSubview:self.bgSc];
    
    [self.bgSc addSubview:self.barChart];
    [self.bgSc addSubview:self.lineChart];
    
    
    WEAKSELF
    self.bgSc.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //调用刷新方法
        STRONGSELFFor(weakSelf);
        if (strongSelf.index==0) {
              [strongSelf loadTodayPlatViewe];
        }else if (strongSelf.index==1){
                [strongSelf loadHalfMonthPlatViewe];
        }else{
              [strongSelf loadJackpotViewe];
        }
    }];
    [self.bgSc.mj_header beginRefreshing];
    
    
}
//JACKPOT
- (void)loadTodayPlatViewe
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadTodayPlatViewe"];
    [self.dataArray removeAllObjects];
    WeakSelf
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            self.dataArray = [YlineModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
             [weakSelf inititles:self.dataArray];
             [weakSelf.barChart strokePath];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
         [self.bgSc.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
         [self.bgSc.mj_header endRefreshing];
        
    }];
}
//JACKPOT
- (void)loadHalfMonthPlatViewe
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadHalfMonthPlatViewe"];
   
    [self.lineArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
        WeakSelf
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            NSDictionary * data = dic[@"data"];
            self.Xarray = data[@"date"];
            
            self.lineArray = [YlineModel mj_objectArrayWithKeyValuesArray:data[@"lines"]];
            [weakSelf inititles:self.lineArray];
            weakSelf.lineChart.isshowLable = NO;
            weakSelf.lineChart.isshowRed = NO;
             weakSelf.lineChart.unit = @"%";
            [weakSelf.lineChart strokePath];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
         [self.bgSc.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
         [self.bgSc.mj_header endRefreshing];
        
    }];
}
//JACKPOT
- (void)loadJackpotViewe
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadJackpotViewe"];
    [self.lineArray removeAllObjects];
    WeakSelf
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            NSDictionary * data = dic[@"data"];
            self.Xarray = data[@"date"];
            self.lineArray = [YlineModel mj_objectArrayWithKeyValuesArray:data[@"lines"]];
            
            [weakSelf inititles:self.lineArray];
            weakSelf.lineChart.isshowLable = YES;
            weakSelf.lineChart.isshowRed = YES;
             weakSelf.lineChart.unit = @"¥";
            [weakSelf.lineChart strokePath];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
        
        [self.bgSc.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
         [self.bgSc.mj_header endRefreshing];
        
    }];
}
- (EDAttentionHeadView *)headView
{
    if (!_headView) {
        _headView = [[EDAttentionHeadView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 70)];
        //        _headView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        [_headView getClassButton:@[@"本日平台发财指数",@"半月平台财富线",@"JACKPOT"]];
        WeakSelf
        [_headView setButtonblock:^(NSInteger index) {
            _index = index;
            switch (index) {
                case 0:
                    [weakSelf loadTodayPlatViewe];
                     weakSelf.barChart.hidden = NO;
                    weakSelf.lineChart.hidden = YES;
                    break;
                case 1:
                    [weakSelf loadHalfMonthPlatViewe];
                    weakSelf.barChart.hidden = YES;
                    weakSelf.lineChart.hidden = NO;
                    break;
                case 2:
                    [weakSelf loadJackpotViewe];
                    weakSelf.barChart.hidden = YES;
                    weakSelf.lineChart.hidden = NO;
                    break;
                    
                default:
                    break;
            }
        }];
    }
    return _headView;
}
- (void)inititles:(NSArray *)array
{
    for (UIView *view in self.view.subviews) {
        if (view.tag>10085) {
            [view removeFromSuperview];
        }
        
    }
    for (int i =0; i<array.count; i++)
    {
        YlineModel * model = array[i];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setBackgroundColor:ColorStr(model.color) forState:UIControlStateNormal];
        [button setTitleColor:WHITECOLOR forState:UIControlStateNormal];
        if (kStringIsEmpty(model.value))
        {
             [button setTitle:kFormat(@"%@",model.name ) forState:UIControlStateNormal];
        }else
        {
             [button setTitle:kFormat(@"%@\n%@",model.value,model.name ) forState:UIControlStateNormal];
        }
       
        button.titleLabel.lineBreakMode = 0;//这句话很重要，不加这句话加上换行符也没用
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.titleLabel.font = AutoFont(11);
        KViewRadius(button, 2);
        button.tag = i+10086;
//        if (_index) {
//            <#statements#>
//        }
        NSInteger index = i % 4;
        NSInteger page = i / 4;
        button.frame = CGRectMake(20+ index * (80 + 8),UIScreenHeight - _headView.bottom -230 + page  * (30 + 10)+10, 70, 30);//重设button的frame
        [self.view addSubview:button];
    }
    
}
- (ZFBarChart *)barChart
{
    if (!_barChart) {
         _barChart = [[ZFBarChart alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, UIScreenHeight - _headView.bottom -230-kStatusBarAndNavigationBarHeight)];
        _barChart.dataSource = self;
        _barChart.delegate = self;
        _barChart.unit = @"%";
        _barChart.isShowXLineSeparate = NO;
        _barChart.isShowYLineSeparate = YES;
        _barChart.unitColor = ZFWhite;
        _barChart.xAxisColor = ZFWhite;
        _barChart.yAxisColor = ZFWhite;
        _barChart.xAxisColor = RGBA(255, 255, 255, 0.5);
        _barChart.yAxisColor = RGBA(255, 255, 255, 0.5);
        _barChart.axisLineNameColor = ZFWhite;
        _barChart.axisLineValueColor = ZFWhite;
        _barChart.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        _barChart.isShowAxisArrows = NO;
        _barChart.separateLineStyle = kLineStyleRealLine;
       _barChart.separateLineDashPhase = 0.f;
        _barChart.separateLineDashPattern = @[@(5), @(5)];
        _barChart.isMultipleColorInSingleBarChart = YES;
       
    }
    return _barChart;
}
- (ZFLineChart *)lineChart
{
    if (!_lineChart) {
        _lineChart = [[ZFLineChart alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, UIScreenHeight - _headView.bottom -230-kStatusBarAndNavigationBarHeight)];
        _lineChart.dataSource = self;
        _lineChart.delegate = self;
       
        _lineChart.isShowXLineSeparate = NO;
        _lineChart.isShowYLineSeparate = YES;
        _lineChart.unitColor = ZFWhite;
        _lineChart.xAxisColor = ZFWhite;
        _lineChart.yAxisColor = ZFWhite;
        _lineChart.xAxisColor = RGBA(255, 255, 255, 0.5);
        _lineChart.yAxisColor = RGBA(255, 255, 255, 0.5);
        _lineChart.axisLineNameColor = ZFWhite;
        _lineChart.axisLineValueColor = ZFWhite;
        _lineChart.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        _lineChart.isShowAxisArrows = NO;
        _lineChart.separateLineStyle = kLineStyleRealLine;
        _lineChart.separateLineDashPhase = 0.f;
        _lineChart.separateLineDashPattern = @[@(5), @(5)];
        _lineChart.hidden = YES;
    }
    return _lineChart;
}

#pragma mark - ZFGenericChartDataSource

- (NSArray *)valueArrayInGenericChart:(ZFGenericChart *)chart{
    NSMutableArray * data = [[NSMutableArray alloc] init];
    if (chart == _barChart) {
        for (YlineModel * model in self.dataArray)
        {
            [data addObject:model.value];
        }
        return data;
    }else
    {
       
        for (YlineModel * model in self.lineArray)
        {
            if (model.values.count>0)
            {
                NSMutableArray * numbers = [[NSMutableArray alloc] init];
                NSArray *array = model.values;
                for (NSNumber * number in  array)
                {
                    [numbers addObject:kFormat(@"%@", number)];
                }
                [data addObject:numbers];
            }else
            {
                [data addObject:model.value];
            }
           
        }
        return data;
    }
    
}

- (NSArray *)nameArrayInGenericChart:(ZFGenericChart *)chart{
    NSMutableArray * data = [[NSMutableArray alloc] init];
     if (chart == _barChart) {
         
         for (YlineModel * model in self.dataArray)
         {
             [data addObject:model.name];
         }
         return data;
     }else
     {
         return _Xarray;
     }
    
}

- (NSArray *)colorArrayInGenericChart:(ZFGenericChart *)chart{
    NSMutableArray * data = [[NSMutableArray alloc] init];
     if (chart == _barChart) {
         
         for (dataModel * model in self.dataArray)
         {
             UIColor * color = ColorStr(model.color);
             [data addObject: color];
         }
         return data;
     }else
     {
         for (YlineModel * model in self.lineArray)
         {
             UIColor * color = ColorStr(model.color);
             [data addObject: color];
         }
         return data;
     }
    
}

- (CGFloat)axisLineMaxValueInGenericChart:(ZFGenericChart *)chart{
    return 500;
}
/**
 *  bar宽度(若不设置，默认为25.f)
 */
- (CGFloat)barWidthInBarChart:(ZFBarChart *)barChart;
{
    return 40;
}
/**
 *  组与组之间的间距(若不设置,默认为20.f)
 */
- (CGFloat)paddingForGroupsInBarChart:(ZFBarChart *)barChart;
{
    return 10;
}
- (NSUInteger)axisLineSectionCountInGenericChart:(ZFGenericChart *)chart{
    return 10;
}
- (CGFloat)lineWidthInLineChart:(ZFLineChart *)lineChart{
    return 4.f;
}
- (id)valueTextColorArrayInBarChart:(ZFBarChart *)barChart;
{
    return WHITECOLOR;
}

@end

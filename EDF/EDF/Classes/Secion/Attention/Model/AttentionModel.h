//
//  AttentionModel.h
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttentionModel : NSObject
//名字
@property (nonatomic,copy) NSString *dep;
@property (nonatomic,copy) NSString *rn;
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *lv_name;
@property (nonatomic,copy) NSString *viplevel;
@property (nonatomic,copy) NSString *wid;
@property (nonatomic,copy) NSString *win;
@end
@interface PlatformModel : NSObject
@property (nonatomic,copy) NSString *bet;
@property (nonatomic,copy) NSString *gameid;
@property (nonatomic,copy) NSString *game_name;
@property (nonatomic,copy) NSString *winlose;
@property (nonatomic,copy) NSString *betdate;
@property (nonatomic,copy) NSString *plat_name;
@property (nonatomic,copy) NSString *plat_id;
@property (nonatomic,copy) NSString *sessionid;
@property (nonatomic,copy) NSString *rn;
@property (nonatomic,copy) NSString *validbet;
@property (nonatomic,copy) NSString *multiple;
@property (nonatomic,copy) NSString *user_count;

@end
@interface dataModel : NSObject
//名字
@property (nonatomic,copy) NSString *color;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *value;
@end
@interface XlineModel : NSObject
@property (nonatomic,copy) NSArray *date; //横坐标数据
@end
@interface YlineModel : NSObject
@property (nonatomic,copy) NSArray *values; //横坐标数据
@property (nonatomic,copy) NSString *color;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *value;
@end
NS_ASSUME_NONNULL_END

//
//  WealthViewController.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "WealthViewController.h"
#import "EDAttentionHeadView.h"
#import "WealthCell.h"
#import "AttentionModel.h"
@interface WealthViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)EDAttentionHeadView * headView;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;//
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation WealthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.index = 0;
    _dataArray = [NSMutableArray new];
    self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    [self.view addSubview:self.headView];
    [self.view addSubview:self.tableView];
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //调用刷新方法
        STRONGSELFFor(weakSelf);
        if (strongSelf.index ==0) {
            [strongSelf getTodayData];
        }else{
            [strongSelf getWeekData];
        }
        
    }];
    [self.tableView.mj_header beginRefreshing];
   // [self getTodayData];
    
}
- (void)getTodayData
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadTodayTopWinList"];
    [self.dataArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
         [self.tableView.mj_header endRefreshing];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            self.dataArray = [AttentionModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
          [self.tableView reloadData];
          [self placeholderViewWithFrame:self.tableView.frame NoNetwork:NO];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
         [self.tableView.mj_header endRefreshing];
    }];
}
- (void)getWeekData
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadWeekTopWinList"];
    [self.dataArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
         [self.tableView.mj_header endRefreshing];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
       
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
             self.dataArray = [AttentionModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
          
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
          [self.tableView reloadData];
          [self placeholderViewWithFrame:self.tableView.frame NoNetwork:NO];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
         [self.tableView.mj_header endRefreshing];
        [WJUnit showMessage:@"请求错误"];
        
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, self.tableView.height -100) NoNetwork:NoNetwork];
        _workView.titlelable.textColor = [UIColor whiteColor];
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
- (EDAttentionHeadView *)headView
{
    if (!_headView) {
        _headView = [[EDAttentionHeadView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 70)];
        //        _headView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        [_headView getClassButton:@[@"今日盈利榜",@"本周盈利榜"]];
        WeakSelf
        [_headView setButtonblock:^(NSInteger index) {
            weakSelf.index = index;
            if (index ==0)
            {
                [weakSelf getTodayData];
            }else if (index ==1)
            {
                [weakSelf getWeekData];
            }
        }];
    }
    return _headView;
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _headView.bottom -20, UIScreenWidth, UIScreenHeight - _headView.bottom +20 - TABBAR_HEIGHT - kStatusBarAndNavigationBarHeight)style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        _tableView.showsVerticalScrollIndicator = NO;
        
        [_tableView registerNib:[UINib nibWithNibName:@"WealthCell" bundle:nil]  forCellReuseIdentifier:@"WealthCell"];
    }
    return _tableView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     WealthCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"WealthCell" forIndexPath:indexPath];
    cell.model = _dataArray[indexPath.row];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
@end

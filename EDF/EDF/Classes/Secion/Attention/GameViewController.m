//
//  GameViewController.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "GameViewController.h"
#import "AttentionModel.h"
#import "PlatformViewCell.h"
#import "EDAttentionHeadView.h"
@interface GameViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;//
@property (nonatomic,strong)EDAttentionHeadView * headView;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.index = 0;
     self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    [self.view addSubview:self.headView];
    [self.view addSubview:self.tableView];
    _dataArray = [NSMutableArray new];
    
    WEAKSELF
     self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
          //调用刷新方法
          STRONGSELFFor(weakSelf);
         if (strongSelf.index ==0) {
                [strongSelf loadTopBonusGameList];
         }else if (strongSelf.index ==1){
                 [strongSelf loadTopMultipleGameList];
         }else{
                  [strongSelf loadTopPlayerCountGameList];
         }
     
       }];
      [self.tableView.mj_header beginRefreshing];

}
- (EDAttentionHeadView *)headView
{
    if (!_headView) {
        WeakSelf
        _headView = [[EDAttentionHeadView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 70)];
//        _headView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        [_headView getClassButton:@[@"本日巨奖榜",@"本日奇迹榜",@"本日流行榜"]];
        [_headView setButtonblock:^(NSInteger index) {
            weakSelf.index = index;
            switch (index) {
                case 0:
                    [weakSelf loadTopBonusGameList];
                    break;
                case 1:
                    [weakSelf loadTopMultipleGameList];
                    break;
                case 2:
                    [weakSelf loadTopPlayerCountGameList];
                    break;
                    
                default:
                    break;
            }
            
        }];
    }
    return _headView;
}
//本日巨奖榜
- (void)loadTopBonusGameList
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadTopBonusGameList"];
    [self.dataArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
        [self stopLoadingAnimation];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            self.dataArray = [PlatformModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.tableView reloadData];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
         [self placeholderViewWithFrame:self.tableView.frame NoNetwork:NO];
        [self.tableView.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
        
    }];
}
//本日奇迹榜
- (void)loadTopMultipleGameList
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadTopMultipleGameList"];
    [self.dataArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
            [self.tableView.mj_header endRefreshing];
        [self stopLoadingAnimation];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            self.dataArray = [PlatformModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.tableView reloadData];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
         [self placeholderViewWithFrame:self.tableView.frame NoNetwork:NO];
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
            [self.tableView.mj_header endRefreshing];
        
    }];
}
//本日流行榜
- (void)loadTopPlayerCountGameList
{
    [self showLoadingAnimation];
    NSString  * game = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadTopPlayerCountGameList"];
    [self.dataArray removeAllObjects];
    [[SmileHttpTool sharedInstance] GET :game parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject){
            [self.tableView.mj_header endRefreshing];
        [self stopLoadingAnimation];
        NSDictionary * dic = [self objectFromJSONString:responseObject];
        if ([dic[@"statusCode"] isEqual:@"SUCCESS"]) //请求成功
        {
            self.dataArray = [PlatformModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.tableView reloadData];
        }else
        {
            [JMNotifyView showNotify:[dic jsonString:@"message"]  isSuccessful:NO];
        }
         [self placeholderViewWithFrame:self.tableView.frame NoNetwork:NO];
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
            [self.tableView.mj_header endRefreshing];
        [WJUnit showMessage:@"请求错误"];
        
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, self.tableView.height -60) NoNetwork:NoNetwork];
        _workView.titlelable.textColor = [UIColor whiteColor];
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _headView.bottom -20, UIScreenWidth, UIScreenHeight - _headView.bottom +20 - TABBAR_HEIGHT - kStatusBarAndNavigationBarHeight)style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        _tableView.showsVerticalScrollIndicator = NO;
        
        [_tableView registerNib:[UINib nibWithNibName:@"PlatformViewCell" bundle:nil]  forCellReuseIdentifier:@"PlatformViewCell"];
    }
    return _tableView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlatformViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PlatformViewCell" forIndexPath:indexPath];
    PlatformModel * model = _dataArray[indexPath.row];
    [cell getData:model type:self.index];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
@end

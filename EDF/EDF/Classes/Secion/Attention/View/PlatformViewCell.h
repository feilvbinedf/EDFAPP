//
//  PlatformViewCell.h
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttentionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PlatformViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gradeIcon;
@property (weak, nonatomic) IBOutlet UILabel *PTLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UILabel *feeLab;
@property (weak, nonatomic) IBOutlet UIImageView *gradeBgImage;
@property(nonatomic,strong)PlatformModel * model;
- (void)getData:(PlatformModel *)model type:(NSInteger )type;
@end

NS_ASSUME_NONNULL_END

//
//  EDAttentionHeadView.h
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDAttentionHeadView : UIView
@property (nonatomic,strong)NSMutableArray * buttons;
@property (copy, nonatomic) void(^buttonblock)(NSInteger index);
- (void)getClassButton:(NSArray *)titles;

@end


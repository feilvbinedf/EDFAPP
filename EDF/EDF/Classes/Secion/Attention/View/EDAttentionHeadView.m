//
//  EDAttentionHeadView.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDAttentionHeadView.h"

@implementation EDAttentionHeadView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.buttons = [NSMutableArray new];
    }
    return self;
    
}
- (void)getClassButton:(NSArray *)titles;
{
    CGFloat buttonW = (UIScreenWidth -20)/titles.count -8;
    for (int i=0; i<titles.count; i++)
    {
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(10 + (buttonW+8)*i , 20 ,buttonW, 30)];
        [button setTitle:titles[i] forState:0];
        [button setTitleColor:RGBA(68, 68, 68, 1) forState:(UIControlStateNormal)];
        [button setTitleColor:WHITECOLOR forState:(UIControlStateSelected)];
        KViewRadius(button, 15);
        button.tag = i;
        button.titleLabel.font = AutoFont(13);
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = WHITECOLOR;
        [self.buttons addObject:button];
        if (i ==0) {
            [self selectedWithindex:0];
        }
        [self addSubview:button];
    }
}
- (void)selectedWithindex:(NSInteger)index
{
    for (UIButton *button in self.buttons) {
        if (button.tag == index) {
            
            button.selected = YES;
            
            [button setBackgroundImage:[UIImage imageNamed:@"tab active"] forState:0];
        }else{
            button.selected = NO;
            [button setBackgroundImage:[UIImage imageNamed:@""] forState:0];
        }
    }
    if (self.buttonblock) {
        self.buttonblock(index);
    }
}
- (void)buttonClick:(UIButton *)sender
{
    [self selectedWithindex:sender.tag];
}
@end

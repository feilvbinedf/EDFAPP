//
//  WealthCell.h
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttentionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WealthCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *grade;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *bgimageView;
@property (strong, nonatomic) AttentionModel *model;
@end

NS_ASSUME_NONNULL_END

//
//  WealthCell.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "WealthCell.h"

@implementation WealthCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(AttentionModel *)model
{
    _number.text = model.username;
    _grade.text = model.lv_name;
    _price.text = kFormat(@"盈利:%@元", model.win);
    switch (model.rn.integerValue) {
        case 1:
            _bgimageView.image = [UIImage imageNamed:@"guanjun"];
            break;
        case 2:
            _bgimageView.image = [UIImage imageNamed:@"yajun"];
            break;
        case 3:
            _bgimageView.image = [UIImage imageNamed:@"jijun"];
            break;
        default:
            break;
    }
    
}
@end

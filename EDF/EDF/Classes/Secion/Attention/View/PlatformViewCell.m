//
//  PlatformViewCell.m
//  EDF
//
//  Created by p on 2019/7/27.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PlatformViewCell.h"

@implementation PlatformViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)getData:(PlatformModel *)model type:(NSInteger )type;
{
    _PTLab.text = model.plat_name;
    _nameLab.text = model.game_name;
    _orderLab.hidden = YES;
    if (type==0)
    {
        _feeLab.text = kFormat(@"%@ ¥", model.winlose);
        _orderLab.hidden = NO;
    }else if (type==1)
    {
        _feeLab.text = kFormat(@"倍率：%@", model.multiple);
         _orderLab.hidden = NO;
    }else
    {
        _feeLab.text = kFormat(@"人数：%@", model.user_count);
    }
    NSString * orderId =  @"----";
    if (!kStringIsEmpty(model.sessionid)) {
        if (model.sessionid.length>4) {
            orderId = [model.sessionid substringFromIndex:model.sessionid.length- 4];
        }else
        {
            orderId = model.sessionid;
        }
    }
    
    _orderLab.text = kFormat(@"订单尾号：%@",orderId);
    
    switch (model.rn.integerValue) {
        case 1:
            _gradeBgImage.image = [UIImage imageNamed:@"guanjun1"];
            _gradeIcon.image = [UIImage imageNamed:@"guanjun_icon"];
            break;
        case 2:
            _gradeBgImage.image = [UIImage imageNamed:@"yajun1"];
             _gradeIcon.image = [UIImage imageNamed:@"yajun_icon"];
            break;
        case 3:
            _gradeBgImage.image = [UIImage imageNamed:@"jijun1"];
             _gradeIcon.image = [UIImage imageNamed:@"jijun_icon"];
            break;
        default:
            break;
    }
}

@end

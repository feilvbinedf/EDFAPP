//
//  EDUserCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDUserCenterViewController.h"
#import "EDUserCenterHeaderView.h"
#import "EDDUserItemTableViewCell.h"
#import "UIScrollView+ScalableCover.h"
#import "SDImageCache.h"
#import "EDWelfareCenterViewController.h"
#import "EDSettingInfoViewController.h"
#import "MailMainViewController.h"
#import "EDUserGoldDetailViewController.h"
#import "EDHistoricalRecordCenterVC.h"
#import "EDSecuritySetingViewController.h"
#import "EDUserVipAboutViewController.h"
#import "EDDBankeCardListViewController.h"
#import "EDSignInDetailViewController.h"
#import "EDActivityCenterViewController.h"
#import "EDBangdingPhoneOrEmailViewCobntroller.h"
#import "EDSelfServiceListViewController.h"
#import "TGWebViewController.h"
#import "EDLoveDayListViewController.h"
#import "AuthenticityViewController.h"
#import "LLWebViewController.h"

@interface EDUserCenterViewController ()<UINavigationControllerDelegate>
@property(nonatomic,strong)EDUserCenterHeaderView* userHeader;
@property(nonatomic,assign)BOOL new_reg;
@property(nonatomic,copy)NSString *  collectionUrlStr;
@property(nonatomic,copy)NSString *  kefuTextStr;
@end

@implementation EDUserCenterViewController
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self refushMyInfoData];
    [self request_UserInfoBangDing];
    [self User_getActivityAll];
    self.kefuTextStr = @"2990539864";

}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
     self.userHeader.frame = CGRectMake(0, 0, kScreenWidth, 295);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 设置导航控制器的代理为self
    self.navigationController.delegate = self;
    
   //
    [self creatUI];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LoginSusess) name:LoginSusessNotificationse object:nil];
    

}
-(void)LoginSusess{
    
    [self refushMyInfoData];
    [self request_UserInfoBangDing];
    [self User_getActivityAll];
    
}

-(void)creatUI{
      self.view.backgroundColor =kRGBColor(245, 245, 245);
    
    self.userHeader = [[NSBundle mainBundle]loadNibNamed:@"EDUserCenterHeaderView" owner:self options:nil].lastObject;
    self.userHeader.frame = CGRectMake(0, 0, kScreenWidth, 295);

    self.ks_TableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-TABBAR_HEIGHT-20);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
    self.ks_TableView.tableHeaderView = self.userHeader;
    
    UIView  * footerView = [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, 85)];
    footerView.backgroundColor =kRGBColor(245, 245, 245);
    
    UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, 20, kScreenWidth-120, 40) target:self SEL:@selector(loginOutBtnClick:) title:@"退出登录"];
    [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
    [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
    [footerView addSubview:bottomBtn];
    self.ks_TableView.tableFooterView =footerView;
    [self.ks_TableView addScalableCoverWithImage:[UIImage imageNamed:@"Rectangle"]];
    

    
    WEAKSELF
    [self.userHeader.signInBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        EDSignInDetailViewController * signVC = [EDSignInDetailViewController new];
        signVC.title = @"每日签到";
        [strongSelf.navigationController pushViewController:signVC animated:YES];
    }];
//    

    [self.userHeader.refushBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        CABasicAnimation* rotationAnimation;rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
         rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
        rotationAnimation.duration = 2;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = 10;[self.userHeader.refushBtn.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        [strongSelf refushMyInfoData];
    }];
    
    [self.userHeader.activityBtn01 add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary * dataDicc =self.userHeader.dataArry[0];
            NSString* encodedString = [NSString stringWithFormat:@"%@%@&uid=%@&wb=true",EDFactivityURL22, [dataDicc jsonString:@"url"],[DJLoginHelper sharedInstance].cj_UserUuid];
            TGWebViewController *web = [[TGWebViewController alloc] init];
            web.url =encodedString;
            web.webTitle =[dataDicc jsonString:@"name"];
            web.progressColor = EDColor_BaseBlue;
            [strongSelf.navigationController pushViewController:web animated:YES];
            
        });
    

    }];
    
    [self.userHeader.activityBtn02 add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        NSDictionary * dataDicc =self.userHeader.dataArry[1];
         NSString* encodedString = [NSString stringWithFormat:@"%@%@&uid=%@&wb=true",EDFactivityURL22, [dataDicc jsonString:@"url"],[DJLoginHelper sharedInstance].cj_UserUuid];
        TGWebViewController *web = [[TGWebViewController alloc] init];
        web.url =encodedString;
        web.webTitle =[dataDicc jsonString:@"name"];
        web.progressColor = EDColor_BaseBlue;
        [strongSelf.navigationController pushViewController:web animated:YES];
        
    }];
    
    
    [self.userHeader.activityBtn03 add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        NSDictionary * dataDicc =self.userHeader.dataArry[2];
         NSString* encodedString = [NSString stringWithFormat:@"%@%@&uid=%@&wb=true",EDFactivityURL22, [dataDicc jsonString:@"url"],[DJLoginHelper sharedInstance].cj_UserUuid];
        TGWebViewController *web = [[TGWebViewController alloc] init];
        web.url =encodedString;
        web.webTitle =[dataDicc jsonString:@"name"];
        web.progressColor = EDColor_BaseBlue;
        [strongSelf.navigationController pushViewController:web animated:YES];
        
    }];
    
    
    [self.userHeader.colectBtn add_BtnClickHandler:^(NSInteger tag) {
       STRONGSELFFor(weakSelf);
        YYLog(@"收藏网址点击");
//            LLWebViewController * webview = [[LLWebViewController alloc] init];
//            webview.urlStr = strongSelf.collectionUrlStr;
//            webview.isType = 1;
//            webview.titleStr = @"收藏网址";
//            [[WJUnit currentViewController].navigationController pushViewController:webview animated:YES];
        [strongSelf gotoSafari:strongSelf.collectionUrlStr];
    }];
    
    
//    self.userHeader.actionBtnClickBlock22 = ^(NSDictionary * _Nonnull dataDicc) {
//        STRONGSELFFor(weakSelf);
//        YYLog(@"--actionBtnClickBlock--%@",dataDicc);
//        NSString* encodedString = [NSString stringWithFormat:@"http://test.ydfbet.net:8080%@&uid=%@&wb=true", [dataDicc jsonString:@"url"],[DJLoginHelper sharedInstance].cj_UserUuid];
//        TGWebViewController *web = [[TGWebViewController alloc] init];
//        web.url =encodedString;
//        //[encodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];;
//        web.webTitle =[dataDicc jsonString:@"name"];
//        web.progressColor = EDColor_BaseBlue;
//        [strongSelf.navigationController pushViewController:web animated:YES];
//    };
    
    
    [self.userHeader.fizhiBtn add_BtnClickHandler:^(NSInteger tag) {
        [self writeToPasteBoard:self.kefuTextStr];
        [WJUnit showMessage:@"复制成功" ];
    }];
  
}
-(void)loginOutBtnClick:(UIButton*)sender{
    
    //User_LoginOut
    [self showLoadingAnimation];
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_LoginOut"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            
            
            self.navigationController.tabBarController.selectedIndex =0;
            [[DJLoginHelper sharedInstance] loginOut];
            [[NSNotificationCenter defaultCenter]postNotificationName:ExitLogonNotificationse object:nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"ExitLogonNotificationse22" object:nil];
            [self.ks_TableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
   
}

-(void)refushMyInfoData{
    //myUserNavigationDatd
  //  [self showLoadingAnimation];
    if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
        self.userHeader.nameLab.text =[NSString stringWithFormat:@"账号:%@",[dict jsonString:@"username"]];
        self.userHeader.cashNum.text =[NSString stringWithFormat:@"总资产:%@",[dict  jsonString:@"property"]];
        self.userHeader.dengjiText.text =[NSString stringWithFormat:@"%@",[dict jsonString:@"vip"]];
         self.new_reg =[dict  jsonBool:@"new_reg"];
    }
    [self.ks_TableView reloadData];
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [[DJLoginHelper sharedInstance]save_UserInfo:[dict jsonDict:@"data"]];
            NSLog(@"User_getUserInfos---%@",dict);
            strongSelf.userHeader.nameLab.text =[NSString stringWithFormat:@"账号:%@",[[dict jsonDict:@"data"]  jsonString:@"username"]];
            strongSelf.userHeader.cashNum.text =[NSString stringWithFormat:@"总资产:%@",[[dict jsonDict:@"data"]  jsonString:@"property"]];
             strongSelf.userHeader.dengjiText.text =[NSString stringWithFormat:@"%@",[[dict jsonDict:@"data"]  jsonString:@"vip"]];
            strongSelf.new_reg =[[dict jsonDict:@"data"]  jsonBool:@"new_reg"];
            strongSelf.collectionUrlStr =[[dict jsonDict:@"data"]  jsonString:@"domain_url"];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
          [self performSelector:@selector(delayMethod) withObject:nil/*可传任意类型参数*/ afterDelay:4.0];
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [self stopLoadingAnimation];
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    //    [self refushMyInfoData];
          [self performSelector:@selector(delayMethod) withObject:nil/*可传任意类型参数*/ afterDelay:4.0];
    }];
    
    
    
    NSString  * url2 = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_getUserExclusiveCustomer"];
    [[SmileHttpTool sharedInstance] GET :url2 parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_getUserExclusiveCustomer----%@",dict001);
            NSDictionary * dicc = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.userHeader.qqText.text = [NSString stringWithFormat:@"%@ %@",[dicc jsonString:@"txt"],[dicc jsonString:@"remark"]];
               
                 AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                if (appDelegate.is_RegisteredAlert ==YES) {
                     NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithString:self.userHeader.qqText.text];
                    NSRange textRange = [self.userHeader.qqText.text rangeOfString:[dicc jsonString:@"remark"]];
                    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:textRange];
                    self.userHeader.qqText.attributedText = string;
                    
                }else{
                     self.userHeader.qqText.text = [NSString stringWithFormat:@"%@ %@",[dicc jsonString:@"txt"],[dicc jsonString:@"remark"]];
                    
                }
                
                self.kefuTextStr =[dicc jsonString:@"remark"];
                
                
                if ([[dicc jsonString:@"type"] isEqualToString:@"QQ"]) {
                    self.userHeader.kefuImage.image = [UIImage imageNamed:@"invalidNameQQ.png"];
                }else if ([[dicc jsonString:@"type"] isEqualToString:@"微客服"]){
                     self.userHeader.kefuImage.image = [UIImage imageNamed:@"weixin-2.png"];
                }
            });
        }else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
//                [toast showLXAlertView];
//            });
        }
        
        
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        //[JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
-(void)delayMethod{
    [self.userHeader.refushBtn.layer removeAnimationForKey:@"rotationAnimation"];
    // [JMNotifyView showNotify:@"已更新信息~"];
}
#pragma mark 粘贴板 : 写入系统粘贴板
-(NSString *)writeToPasteBoard:(NSString *)content{
   
   UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
   [pasteboard setString:content];
   return 0;
}
-(void)request_UserInfoBangDing{
    //User_GetgetUserBandInfo

    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_GetgetUserBandInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary *dict = [dict001 jsonDict:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (!kDictIsEmpty(dict )) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault  setValue:dict forKey:@"userBangDingInfo"];
                [userDefault synchronize ];
            }
        }else{

        }
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
    }];
    
    
}

- (void)User_getActivityAll
{
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getActivityAll"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:@{@"type":@"常规,特殊"} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
           //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            YYLog(@"User_getActivityAll---%@",dict);
            NSArray * dataArry = [dict jsonArray:@"data"];
            strongSelf.modelsArry = [NSMutableArray arrayWithArray:dataArry];
            [strongSelf.userHeader refushActivityBtnWith:self.modelsArry];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        
        [WJUnit showMessage:@"获取活动请求失败"];
    //[self getBalance];
        
    }];
}

#pragma mark ----,UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        if (self.new_reg ==YES) {
            return 4;
        }else{
            return 3;
        }
        
    }else if (section==1){
         return 4;
    }else if (section==2){
        return 2;
    }else if (section==3){
         return 2;
    }
    else {
         return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDDUserItemTableViewCell";
    EDDUserItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDDUserItemTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDDUserItemTableViewCell" owner:self options:nil] lastObject];
    }
    cell.contentView.backgroundColor = [UIColor whiteColor];
    [self config_CellWith:cell atIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView  * ooterView = [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    ooterView.backgroundColor =kRGBColor(245, 245, 245);
    return ooterView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(void)config_CellWith:(EDDUserItemTableViewCell *)cell  atIndexPath:(NSIndexPath *)indexPath{
    cell.right_Icon.hidden = NO;
    cell.otherLab.hidden = YES;
    
    if (indexPath.section==0) {
        cell.redCo.hidden = YES;
        cell.messageCount.hidden = YES;
        if (self.new_reg==YES) {
            NSArray  * titleArry  = @[@"客户真伪验证",@"日久生情",@"自助服务",@"福利领取中心",@"站内信"];
            NSArray  * iconArry  = @[@"Authenticity",@"center_rijiushengqing",@"Center_服务",@"Center_Gift3",@"Center_Message"];
            cell.iconimage.image = [UIImage imageNamed:iconArry[indexPath.row]];
            cell.itemLab.text = titleArry[indexPath.row];
            if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
                NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
                if ( [cell.itemLab.text isEqualToString:@"福利领取中心"]) {
                    if ([dict jsonInteger:@"bonus_count"]>0) {
                        cell.redCo.hidden = NO;
                        cell.messageCount.hidden = NO;
                          cell.messageCount.text = [dict jsonString:@"bonus_count"];
                    }else{
                        cell.redCo.hidden = YES;
                        cell.messageCount.hidden = YES;
                    }
                }
                if ([cell.itemLab.text isEqualToString:@"站内信"]){
                    if ([dict jsonInteger:@"msg_count"]>0) {
                        cell.redCo.hidden = NO;
                        cell.messageCount.hidden = NO;
                        cell.messageCount.text = [dict jsonString:@"msg_count"];
                    }else{
                        cell.redCo.hidden = YES;
                        cell.messageCount.hidden = YES;
                    }
                }
                  if ([cell.itemLab.text isEqualToString:@"日久生情"] ||[cell.itemLab.text isEqualToString:@"客户真伪验证"] || [cell.itemLab.text isEqualToString:@"自助服务"]){
                      cell.redCo.hidden = YES;
                      cell.messageCount.hidden = YES;
                  }
                
            }else{
                cell.redCo.hidden = YES;
                cell.messageCount.hidden = YES;
            }

        }else{
            NSArray  * titleArry  = @[@"客户真伪验证",@"自助服务",@"福利领取中心",@"站内信"];
            NSArray  * iconArry  = @[@"Authenticity",@"Center_服务",@"Center_Gift3",@"Center_Message"];
            cell.iconimage.image = [UIImage imageNamed:iconArry[indexPath.row]];
            cell.itemLab.text = titleArry[indexPath.row];
        }
    
        if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
//            if (indexPath.row==1) {
//                if ([dict jsonInteger:@"bonus_count"]>0) {
//                    cell.redCo.hidden = NO;
//                    cell.messageCount.hidden = NO;
//                    cell.messageCount.text = [dict jsonString:@"bonus_count"];
//                }else{
//                    cell.redCo.hidden = YES;
//                    cell.messageCount.hidden = YES;
//                }
//            }else if (indexPath.row==2){
//                if ([dict jsonInteger:@"msg_count"]>0) {
//                    cell.redCo.hidden = NO;
//                    cell.messageCount.hidden = NO;
//                      cell.messageCount.text = [dict jsonString:@"msg_count"];
//                }else{
//                    cell.redCo.hidden = YES;
//                    cell.messageCount.hidden = YES;
//                }
//            }else{
//                cell.redCo.hidden = YES;
//                cell.messageCount.hidden = YES;
//            }
            if ( [cell.itemLab.text isEqualToString:@"福利领取中心"]) {
                if ([dict jsonInteger:@"bonus_count"]>0) {
                    cell.redCo.hidden = NO;
                    cell.messageCount.hidden = NO;
                    cell.messageCount.text = [dict jsonString:@"bonus_count"];
                }else{
                    cell.redCo.hidden = YES;
                    cell.messageCount.hidden = YES;
                }
            }
            if ([cell.itemLab.text isEqualToString:@"站内信"]){
                if ([dict jsonInteger:@"msg_count"]>0) {
                    cell.redCo.hidden = NO;
                    cell.messageCount.hidden = NO;
                    cell.messageCount.text = [dict jsonString:@"msg_count"];
                }else{
                    cell.redCo.hidden = YES;
                    cell.messageCount.hidden = YES;
                }
            }
            if ([cell.itemLab.text isEqualToString:@"日久生情"] || [cell.itemLab.text isEqualToString:@"自助服务"]){
                cell.redCo.hidden = YES;
                cell.messageCount.hidden = YES;
            }
        }else{
            cell.redCo.hidden = YES;
            cell.messageCount.hidden = YES;
        }
        
    }else if (indexPath.section==1){
        NSArray  * titleArry  = @[@"个人信息",@"个人资产",@"VIP信息",@"历史记录"];
        NSArray  * iconArry  = @[@"Center_Me",@"Center_资产",@"Center_VIP",@"Center_历史记录"];
        cell.iconimage.image = [UIImage imageNamed:iconArry[indexPath.row]];
        cell.itemLab.text = titleArry[indexPath.row];
    }else if (indexPath.section==2){
        NSArray  * titleArry  = @[@"安全设置",@"银行卡绑定"];
        NSArray  * iconArry  = @[@"Center_安全",@"Center_Card"];
        cell.iconimage.image = [UIImage imageNamed:iconArry[indexPath.row]];
        cell.itemLab.text = titleArry[indexPath.row];
    }
    else {
        cell.right_Icon.hidden = YES;
        cell.otherLab.hidden = NO;
        
        NSArray  * titleArry  = @[@"清除缓存",@"版本号"];
        NSArray  * iconArry  = @[@"Center_Clear",@"Center_banben"];
        cell.iconimage.image = [UIImage imageNamed:iconArry[indexPath.row]];
        cell.itemLab.text = titleArry[indexPath.row];
        
        if (indexPath.row==0) {
            NSUInteger intg = [[SDImageCache sharedImageCache] totalDiskSize];
            //
            NSString * currentVolum = [NSString stringWithFormat:@"%@",[self fileSizeWithInterge:intg]];
            cell.otherLab.text =currentVolum;
            
        }else{
        cell.otherLab.text =    [NSString stringWithFormat:@"%@",kAppVersion];
        }
        
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (self.new_reg ==YES) {
            switch (indexPath.row) {
                case 0:
                {
                    AuthenticityViewController * authenticityVC = [AuthenticityViewController new];
                    authenticityVC.title = @"客户真伪验证";
                    [self.navigationController pushViewController:authenticityVC animated:YES];
                    
                }
                     break;
                case 1:
                {
                    EDLoveDayListViewController * loveVC = [EDLoveDayListViewController new];
                    loveVC.title = @"日久生情";
                    [self.navigationController pushViewController:loveVC animated:YES];
                    
                }
                    break;
                case 2:
                {
                    EDSelfServiceListViewController * selSerVC = [EDSelfServiceListViewController new];
                    selSerVC.title = @"自助服务区";
                    [self.navigationController pushViewController:selSerVC animated:YES];

                    
                }
                    break;
                case 3:
                {
                    
                    EDWelfareCenterViewController * welfareVC = [EDWelfareCenterViewController new];
                    welfareVC.title = @"福利领取区";
                    [self.navigationController pushViewController:welfareVC animated:YES];

                }
                    break;
                case 4:
                {
                    MailMainViewController * MailMainVC = [[MailMainViewController alloc] init];
                    MailMainVC.title = @"站内信";
                    [self.navigationController pushViewController:MailMainVC animated:YES];
                }
                    break;
                default:
                    break;
            }
            
        }else{
            switch (indexPath.row) {
                case 0:
                {
                    AuthenticityViewController * authenticityVC = [AuthenticityViewController new];
                    authenticityVC.title = @"客户真伪验证";
                    [self.navigationController pushViewController:authenticityVC animated:YES];
                    
                }
                    break;
                case 1:
                {
                    EDSelfServiceListViewController * selSerVC = [EDSelfServiceListViewController new];
                    selSerVC.title = @"自助服务区";
                    [self.navigationController pushViewController:selSerVC animated:YES];
                    
                }
                    break;
                case 2:
                {
                    EDWelfareCenterViewController * welfareVC = [EDWelfareCenterViewController new];
                    welfareVC.title = @"福利领取区";
                    [self.navigationController pushViewController:welfareVC animated:YES];
                    
                }
                    break;
                case 3:
                {
                    MailMainViewController * MailMainVC = [[MailMainViewController alloc] init];
                    MailMainVC.title = @"站内信";
                    [self.navigationController pushViewController:MailMainVC animated:YES];
                    
                    
                    
                }
                    break;
                case 4:
                {
                    
                }
                    break;
                default:
                    break;
            }
        }
      
    }else if (indexPath.section==1){

        switch (indexPath.row) {
            case 0:
            {
                EDSettingInfoViewController * welfareVC = [EDSettingInfoViewController new];
                welfareVC.title = @"个人信息";
                [self.navigationController pushViewController:welfareVC animated:YES];
                
            }
                break;
            case 1:
            {
                EDUserGoldDetailViewController * welfareVC = [EDUserGoldDetailViewController new];
                welfareVC.title = @"个人资产";
                welfareVC.infoDicc = [DJLoginHelper sharedInstance].cj_UserDicc;
                [self.navigationController pushViewController:welfareVC animated:YES];
                
            }
                break;
            case 2:
            {
                
                EDUserVipAboutViewController * vipVC = [EDUserVipAboutViewController new];
                vipVC.title = @"VIP信息";
                [self.navigationController pushViewController:vipVC animated:YES];
            }
                break;
                
            case 3:
            {
                EDHistoricalRecordCenterVC * welfareVC = [EDHistoricalRecordCenterVC new];
                welfareVC.title = @"历史记录";
                [self.navigationController pushViewController:welfareVC animated:YES];
                
            }
                break;
            default:
                break;
        }
        
        
        
    }else if (indexPath.section==2){
 
        if (indexPath.row==0) {
            
            if (!kStringIsEmpty([[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"]) && ![[[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"] isEqualToString:@"false"]) {
                
                EDSecuritySetingViewController  * SecurityVC = [EDSecuritySetingViewController new];
                SecurityVC.title = @"安全设置";
                [self.navigationController pushViewController:SecurityVC animated:YES];
                
            }else{
                WEAKSELF
                [[SmileAlert sharedInstance] mazi_alertContent:@"请先手机认证\n再进行安全设置" AndBlock:^(BOOL sure) {
                    STRONGSELFFor(weakSelf);
                    if (sure==YES) {
                        EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
                        bangdingVC.title = @"绑定手机";
                        bangdingVC.showType = 0;
                        WEAKSELF
                        bangdingVC.dismiss = ^(BOOL isSend) {
                            STRONGSELFFor(weakSelf);
                            [strongSelf refushMyInfoData];
                        };
                        [self.navigationController pushViewController:bangdingVC animated:YES];
                    }
                }];
                YYLog(@"先去绑定给手机号才能安全设置~");
            }
        }else{
            
            if (!kStringIsEmpty([[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"]) && ![[[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"] isEqualToString:@"false"]) {
                EDDBankeCardListViewController  * SecurityVC = [EDDBankeCardListViewController new];
                SecurityVC.title = @"我的银行卡";
                [self.navigationController pushViewController:SecurityVC animated:YES];
            }else{
                YYLog(@"先去绑定给手机号才能设置银行卡绑定~");
                WEAKSELF
                [[SmileAlert sharedInstance] mazi_alertContent:@"请先手机认证\n再绑定银行卡" AndBlock:^(BOOL sure) {
                    STRONGSELFFor(weakSelf);
                    if (sure==YES) {
                        EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
                        bangdingVC.title = @"绑定手机";
                        bangdingVC.showType = 0;
                        WEAKSELF
                        bangdingVC.dismiss = ^(BOOL isSend) {
                            STRONGSELFFor(weakSelf);
                            [strongSelf refushMyInfoData];
                        };
                        [self.navigationController pushViewController:bangdingVC animated:YES];
                    }
                }];
            }
            
        }
    }
    else {
        if (indexPath.row==0) {
            NSUInteger intg = [[SDImageCache sharedImageCache] totalDiskSize];
            [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                [JMNotifyView showNotify:@"缓存已清理" isSuccessful:YES];
                [self.ks_TableView reloadData];
            }];
          
        }else{
     
        }
        
    }
    
    
    
}
//计算出大小
- (NSString *)fileSizeWithInterge:(NSInteger)size{
        // 1k = 1024, 1m = 1024k
 if (size < 1024) {// 小于1k
 return [NSString stringWithFormat:@"%ldB",(long)size];
}else if (size < 1024 * 1024){// 小于1m
CGFloat aFloat = size/1024;
 return [NSString stringWithFormat:@"%.0fK",aFloat];
 }else if (size < 1024 * 1024 * 1024){// 小于1G
CGFloat aFloat = size/(1024 * 1024);
return [NSString stringWithFormat:@"%.1fM",aFloat];
 }else{
 CGFloat aFloat = size/(1024*1024*1024);
  return [NSString stringWithFormat:@"%.1fG",aFloat];
 }
}


#pragma mark - UINavigationControllerDelegate
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
    
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
}

-(void)dealloc
{
    self.navigationController.delegate = nil;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
//跳转到浏览器
-(void)gotoSafari:(NSString *)webUrl{
    
    if (![webUrl hasPrefix:@"http"]) {//是否具有http前缀
        webUrl = [NSString stringWithFormat:@"http://%@",webUrl];
    }
    NSURL *url = [NSURL URLWithString:webUrl];
    if([[UIDevice currentDevice].systemVersion floatValue] >= 10.0){
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{}
                                     completionHandler:^(BOOL success) {
                                         NSLog(@"Open %d",success);
                                     }];
        } else {
            BOOL success = [[UIApplication sharedApplication] openURL:url];
            NSLog(@"Open  %d",success);
        }
    } else{
        bool can = [[UIApplication sharedApplication] canOpenURL:url];
        if(can){
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//int64_t delayInSeconds = 1.0; // 延迟的时间
//__weak typeof(self)weakSelf = self;
//dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//    CJUserInfoSettingSubViewController * subVC = [CJUserInfoSettingSubViewController new];
//    subVC.typeStr = @"提款密码2";
//    subVC.answerStr = answer;
//    subVC.qusID = questionID;
//    WEAKSELF
//    subVC.dismiss = ^(BOOL isSend) {
//        STRONGSELFFor(weakSelf);
//        [strongSelf queryPersonalBaseInfo];
//    };
//
//    [self.navigationController pushViewController:subVC animated:YES];
//});
@end

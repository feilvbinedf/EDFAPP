//
//  AuthenticityViewController.h
//  EDF
//
//  Created by Kk on 2020/7/15.
//  Copyright © 2020 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthenticityViewController : EDBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)verificationBtn:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END

//
//  AuthenticityViewController.m
//  EDF
//
//  Created by Kk on 2020/7/15.
//  Copyright © 2020 p. All rights reserved.
//

#import "AuthenticityViewController.h"

@interface AuthenticityViewController ()

@end

@implementation AuthenticityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    KViewRadius(self.textField, 4);
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */
-(BOOL)isQQ:(NSString*)qqStr {
    
    NSString *pattern = @"^[1-9]\\d{5,10}$";
    return [self matchWithPattern:pattern andWillPatternStr:qqStr];
    
}
- (BOOL) matchWithPattern:(NSString*) pattern  andWillPatternStr:(NSString*)patternStr
{
    
    NSError*error =nil;
    NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    if(error) {
            NSLog(@"创建正则表达式失败%@",error);
        return NO;
          
    }
    //  匹配
        NSTextCheckingResult*results  = [regularExpression firstMatchInString:patternStr options:0 range:NSMakeRange(0, patternStr.length)];
    
        if(results)
    {
         return YES;
    }else
    {
          return NO;
    }
    
}


- (IBAction)verificationBtn:(UIButton *)sender {
    //Game_loadNewPlayerPromotion
    if (kStringIsEmpty(_textField.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:@"请输入QQ号" andlightStr:@""];
                      
        [toast showLXAlertView];
        return;
    }
    [self showLoadingAnimation];
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"checkCustomerServiceDoesItExist"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:@{@"type":@"QQ",@"code":_textField.text} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
           
            dispatch_async(dispatch_get_main_queue(), ^{
                           ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:@"验证成功" andlightStr:@""];
                           [toast showLXAlertView];
                       });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [self stopLoadingAnimation];
    } failure:^(id responseObject, NSError *error) {
       [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求失败"];
      
        
    }];
    
}
@end

//
//  EDTopBtnsView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDTopBtnsView.h"
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          10.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   35.0f    // 高
//#define Button_Width    75.0f    // 宽
@interface EDTopBtnsView ()

@property(nonatomic,strong)UIViewController * superVC;

@property(nonatomic,strong)NSArray  * titlesArry;
@property(nonatomic,strong)NSMutableArray * listBtnArry;
@end
@implementation EDTopBtnsView

-(instancetype)initWith:(NSArray*)titleArry andSuperVC:(UIViewController *)suoerVC{
    
    self = [super init];
    if(self) {
        self.backgroundColor =kRGBColor(245, 245, 245);
        self.listBtnArry = [NSMutableArray array];
        _titlesArry = titleArry;
        _superVC =suoerVC;
        [self creatUI];
    }
    return self;
}
-(void)creatUI{
    int count = 4; // 每行按钮的数量为 4
    CGFloat btnWidth = (kScreenWidth-60) / count;  //宽
    for (int i = 0 ; i < _titlesArry.count; i++) {
        NSInteger index = i % count;
         NSInteger page = i / count;
        UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        mapBtn.backgroundColor = [UIColor blueColor];
        mapBtn.tag = i+1000;
        mapBtn.frame = CGRectMake(index * (btnWidth + Width_Space) + Start_X, page  * (Button_Height + Height_Space)+Start_Y, btnWidth, Button_Height);
        if (i==_titlesArry.count-1) {
            self.frame = CGRectMake(0, 0, kScreenWidth, mapBtn.bottom+10);
        }
        if (i==0) {
            mapBtn.selected = YES;
        }
        KViewRadius(mapBtn, 4);
        [mapBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [mapBtn setTitle:_titlesArry[i] forState:UIControlStateNormal];
        [mapBtn setBackgroundColor:[UIColor whiteColor] forState:0];
        [mapBtn setBackgroundColor:kRGBColor(190, 227, 255) forState:UIControlStateSelected];
        [mapBtn setTitleColor:kRGBColor(91, 91, 91)  forState:0];
        [mapBtn setTitleColor:EDColor_BaseBlue forState:UIControlStateSelected];
        [self.listBtnArry addObject:mapBtn];
        //按钮点击方法
        [mapBtn addTarget:self action:@selector(itemBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:mapBtn];

    }

}
-(void)setIsCun:(BOOL)isCun{
    _isCun = isCun;
}
-(void)itemBtnClick:(UIButton*)sender{
    if (_isCun==NO) {
        sender.selected = YES;
        for (UIButton * button  in self.listBtnArry) {
            if (sender.tag !=button.tag) {
                button.selected = NO;
            }
        }
        if (self.seclectTypeBlcok) {
            self.seclectTypeBlcok(sender.tag-1000);
        }
    }else{
       // 1004  1005
        if (sender.tag<1004) {
            sender.selected = YES;
            for (UIButton * button  in self.listBtnArry) {
                if (sender.tag !=button.tag && button.tag<1004) {
                    button.selected = NO;
                }
            }
            UIButton * button1005 = [self viewWithTag:1005];
            UIButton * button1004 = [self viewWithTag:1004];
            if (button1004.selected==YES) {
                if (self.seclectTypeBlcok2) {
                    self.seclectTypeBlcok2(sender.tag-1000, 4);
                }
            }else if (button1005.selected ==YES){
                if (self.seclectTypeBlcok2) {
                    self.seclectTypeBlcok2(sender.tag-1000, 5);
                }
            }
            else{
                if (self.seclectTypeBlcok2) {
                    self.seclectTypeBlcok2(sender.tag-1000, 0);
                }
            }
        }else{
             sender.selected = YES;
            if (sender.tag==1004) {
                UIButton * button1005 = [self viewWithTag:1005];
                button1005.selected = NO;
            }else{
                UIButton * button1004 = [self viewWithTag:1004];
                button1004.selected = NO;
            }
            [self.listBtnArry enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.selected  ==YES && obj.tag<1004) {
                    if (self.seclectTypeBlcok2) {
                        self.seclectTypeBlcok2(obj.tag-1000, sender.tag-1000);
                        
                    }
                      *stop = YES;
                }
             
            }];
        }
        
    }

}


-(void)selectBtnsTag:(NSInteger )daysTag  typeTag:(NSInteger)typeTag{
    
//    for (UIButton * button  in self.listBtnArry) {
//        if (button.tag !=(daysTag +1000) && button.tag !=(typeTag+1003)) {
//            button.selected = NO;
//        }else{
//            button.selected = YES;
//        }
//    }
    
    
    
}
@end

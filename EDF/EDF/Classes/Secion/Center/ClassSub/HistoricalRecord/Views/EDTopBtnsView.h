//
//  EDTopBtnsView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDTopBtnsView : UIView

-(instancetype)initWith:(NSArray*)titleArry andSuperVC:(UIViewController *)suoerVC;
@property(nonatomic,assign)BOOL  isCun;
@property (nonatomic, copy) void (^seclectTypeBlcok)(NSInteger btnTag);
@property (nonatomic, copy) void (^seclectTypeBlcok2)(NSInteger btnTag,NSInteger type);
-(void)selectBtnsTag:(NSInteger )daysTag  typeTag:(NSInteger)typeTag;

@end

NS_ASSUME_NONNULL_END

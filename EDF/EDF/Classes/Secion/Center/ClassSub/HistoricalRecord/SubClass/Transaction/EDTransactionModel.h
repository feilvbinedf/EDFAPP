//
//  EDTransactionModel.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/18.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDTransactionModel : EDBaseModel

@property(nonatomic,copy)NSString<Optional> * no;
@property(nonatomic,copy)NSString<Optional> * amount;
@property(nonatomic,copy)NSString<Optional> * account_id;
@property(nonatomic,copy)NSString<Optional> * user_id;
@property(nonatomic,copy)NSString<Optional> * show_date;
@property(nonatomic,copy)NSString<Optional> * bonus;
@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * create_date;
@property(nonatomic,copy)NSString<Optional> * type;
@property(nonatomic,copy)NSString<Optional> * a_name;
@property(nonatomic,copy)NSString<Optional> * refund;
@property(nonatomic,copy)NSString<Optional> * status;


@property(nonatomic,copy)NSString<Optional> * bet;
@property(nonatomic,copy)NSString<Optional> * betdate;
@property(nonatomic,copy)NSString<Optional> * winlose;
//@property(nonatomic,copy)NSString<Optional> * account_id;
//@property(nonatomic,copy)NSString<Optional> * user_id;
//@property(nonatomic,copy)NSString<Optional> * show_date;
@property(nonatomic,copy)NSString<Optional> * p_name;
@property(nonatomic,copy)NSString<Optional> * plat_id;
//@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * validbet;
//@property(nonatomic,copy)NSString<Optional> * a_name;


//@property(nonatomic,copy)NSString<Optional> * bonus;
@property(nonatomic,copy)NSString<Optional> * name;
///@property(nonatomic,copy)NSString<Optional> * create_date;
@property(nonatomic,copy)NSString<Optional> * update_date;
//@property(nonatomic,copy)NSString<Optional> * status;


@property(nonatomic,copy)NSString<Optional> * detail;

//
//"amount": "10",
//"id": "ONJFirEYSKOdhBtULl4LjC",
//"detail": "系统派送",
//"create_date": "2019-07-10%2016:43:43",
//"update_date": "2019-07-10%2017:13:30",
//"status": "已领取"






//"bonus": "10",
//"name": "测试3",
//"id": "SrUpnogoR9CCQiIPyszxxQ",
//"create_date": "2019-07-10%2016:42:40",
//"update_date": "2019-07-10%2016:54:50",
//"status": "已领取"







//"bet": "604",
//"betdate": "2019-06-19",
//"winlose": "-78",
//"account_id": "PoEte7edT0uXB3dF1iPt_w",
//"user_id": "yAeXkl3hQxKY1EWN-9ViKA",
//"show_date": "06/19",
//"p_name": "PS平台",
//"plat_id": "7",
//"id": "RlTIfldVTjGE2nzm6yyKeQ",
//"validbet": "604",
//"a_name": "基本账户"
//},



@end

NS_ASSUME_NONNULL_END

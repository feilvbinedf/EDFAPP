//
//  EDBettingListViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBettingListViewController.h"
#import "EDTopBtnsView.h"
#import "EDBettingTableViewCell.h"
#import "EDFaGoldListTableViewCell.h"
#import "EDHongLiListTableViewCell.h"

#import "EDTransactionModel.h"

@interface EDBettingListViewController ()
{
    NSInteger _pageNum;
}
@property(nonatomic,strong)EDTopBtnsView * topBtnsView;
@property (nonatomic,assign) NSInteger   date_TimeType;
@property (nonatomic,strong) NoNetworkView * workView;
@end
@implementation EDBettingListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
        self.date_TimeType = 1;
    [self creatUI];
}

-(void)creatUI{
    self.topBtnsView = [[EDTopBtnsView alloc]initWith:@[@"今天",@"昨天",@"最近一周",@"最近一月"] andSuperVC:self];
    [self.view addSubview:self.topBtnsView];
    WEAKSELF
    self.topBtnsView.seclectTypeBlcok = ^(NSInteger btnTag) {
        YYLog(@"seclectTypeBlcok---%ld",btnTag);
        STRONGSELFFor(weakSelf);
        strongSelf.date_TimeType = btnTag+1;
        [strongSelf.ks_TableView.mj_header beginRefreshing];
    };
    
    self.ks_TableView.frame = CGRectMake(0, self.topBtnsView.bottom, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-self.topBtnsView.height-50);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
    [self ks_tableAddHeaderRequstRefush];
    [self ks_tableAddFootRequst];
    
    
}
-(void)getData_Refush{
    _pageNum = 0;
    [self refushData:YES];
    
    
}
-(void)getData_LoadingMore{
    _pageNum ++;
    [self refushData:NO];
}
-(void)refushData:(BOOL)isRefush{
    
    NSString  * url =@"";
    if (self.showTypeT ==0) {
        url=     [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getBettingHistory"];
    }else if (self.showTypeT==1){
       // User_getRedMoneyHistory
            url=     [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getRedMoneyHistory"];
    }else{
       // User_getCoinHistory
            url=     [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getCoinHistory"];
    }
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:@(_pageNum) forKey:@"curpage"];
    [diccc setValue:@(self.date_TimeType) forKey:@"date"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        
        if (isRefush ==YES) {
            [strongSelf.modelsArry removeAllObjects];
        }
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary * dict = [dict001 jsonDict:@"data"];
        
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            NSArray *  dataArr = [dict jsonArray:@"date"];
            NSInteger allPage = [dict jsonInteger:@"pages"];
            NSInteger curpage = [dict jsonInteger:@"pageNumber"];
            if (isRefush==NO) {
                if (allPage == curpage+1) {
                    //没数据了
                    [strongSelf.ks_TableView.mj_footer endRefreshingWithNoMoreData];
                    return;
                }
            }
            
            for (NSDictionary * modelDicc in dataArr) {
                EDTransactionModel * modell = [[EDTransactionModel alloc]initWithDictionary:modelDicc error:nil];
                [strongSelf.modelsArry addObject:modell];
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self placeholderViewWithFrame:self.ks_TableView.frame NoNetwork:NO];
        });
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [strongSelf.ks_TableView reloadData];
        NSLog(@"User_welfareMobileActionWelfareCore---%@",dict001);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [WJUnit showMessage:@"请求错误"];

    }];
    
    
}



#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.showTypeT ==0) {
        static NSString *identifier = @"EDBettingTableViewCell";
        EDBettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = (EDBettingTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDBettingTableViewCell" owner:self options:nil] lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        EDTransactionModel * modell = self.modelsArry[indexPath.row];
        cell.showTime.text = modell.show_date;
        cell.pintaiLab.text = modell.p_name;
        cell.numLab.text = modell.winlose;
        
        return cell;
    }else if (self.showTypeT==1){
        static NSString *identifier = @"EDHongLiListTableViewCell";
        EDHongLiListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = (EDHongLiListTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDHongLiListTableViewCell" owner:self options:nil] lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        EDTransactionModel * modell = self.modelsArry[indexPath.row];
        cell.nameLab.text = modell.name;
        cell.numberLab.text = modell.bonus;
        
        cell.liTimeLab.text =[NSString stringWithFormat:@"领取时间:%@", [[modell.create_date substringFromIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
        cell.paiTimeLab.text  = [NSString stringWithFormat:@"派发时间:%@", [[modell.update_date substringFromIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
        cell.typeLab.text = modell.status;
        
        return cell;
    }else{
        static NSString *identifier = @"EDFaGoldListTableViewCell";
        EDFaGoldListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = (EDFaGoldListTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDFaGoldListTableViewCell" owner:self options:nil] lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        EDTransactionModel * modell = self.modelsArry[indexPath.row];
        cell.numLab.text = [NSString stringWithFormat:@"%@:%@",modell.detail,modell.amount];
        cell.liTimeLab.text =[NSString stringWithFormat:@"领取时间:%@", [[modell.create_date substringFromIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
        
        if (kStringIsEmpty(modell.update_date)) {
            cell.paiLab.hidden = YES;
        }else{
            if (modell.update_date.length >5) {
                cell.paiLab.text  = [NSString stringWithFormat:@"派发时间:%@", [[modell.update_date substringFromIndex:5] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
            }else{
                cell.paiLab.hidden = YES;
            }
        }

  
        cell.typeLab.text = modell.status;
        
        return cell;
    }
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.showTypeT==0) {
            return 40;
    }else if (self.showTypeT==1){
        return 105;
    }else{
        return 80;
    }

}

-(void)stepCell:(EDBettingTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
  
    switch (self.showTypeT) {
        case 0:
        {
  
            
        }
            break;
        case 1:
        {
            
            
            
        }
            break;
        case 2:
        {
            
        }
            break;
            
        default:
            break;
    }
    
}

- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.modelsArry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        [self.ks_TableView addSubview:_workView];
        _workView.centerY = self.ks_TableView.centerY-100;
    }else
    {
        [_workView dissmiss];
    }
}


@end

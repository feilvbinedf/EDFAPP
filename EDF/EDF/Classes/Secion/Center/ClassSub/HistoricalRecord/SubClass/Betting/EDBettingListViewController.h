//
//  EDBettingListViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDBettingListViewController : EDBaseViewController

@property(nonatomic,assign)NSInteger  showTypeT; // 0 投住  1 红利  2 发币

@end

NS_ASSUME_NONNULL_END

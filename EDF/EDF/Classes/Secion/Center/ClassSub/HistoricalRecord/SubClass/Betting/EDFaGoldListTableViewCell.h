//
//  EDFaGoldListTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDFaGoldListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UILabel *liTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *paiLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@end

NS_ASSUME_NONNULL_END

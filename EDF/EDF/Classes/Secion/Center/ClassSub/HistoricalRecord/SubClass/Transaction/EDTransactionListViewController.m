//
//  EDTransactionListViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDTransactionListViewController.h"
#import "EDTopBtnsView.h"
#import "EDTransactionTableViewCell.h"
#import "EDTransactionModel.h"
@interface EDTransactionListViewController ()
{
    NSInteger _pageNum;
}
@property(nonatomic,strong)EDTopBtnsView * topBtnsView;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,assign) NSInteger   dataType;
@property (nonatomic,assign) NSInteger   date_TimeType;
@end
@implementation EDTransactionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageNum = 0;
    self.dataType = 0;
    self.date_TimeType = 1;
    [self creatUI];
}

-(void)creatUI{
    self.modelsArry = [NSMutableArray array];
    self.topBtnsView = [[EDTopBtnsView alloc]initWith:@[@"今天",@"昨天",@"最近一周",@"最近一月",@"存款",@"提款"] andSuperVC:self];
    self.topBtnsView.isCun = YES;
    [self.view addSubview:self.topBtnsView];
    [self.topBtnsView selectBtnsTag:self.date_TimeType-1 typeTag:self.dataType];
    
    
    WEAKSELF
    self.topBtnsView.seclectTypeBlcok2 = ^(NSInteger btnTag,NSInteger type) {
        STRONGSELFFor(weakSelf);
        YYLog(@"seclectTypeBlcok---%ld---type--%ld",btnTag,type);
        if (type==4) {
            strongSelf.dataType =1;
            strongSelf.date_TimeType = btnTag+1;
        }else if (type ==5){
            strongSelf.dataType=2;
            strongSelf.date_TimeType = btnTag+1;
        }else{
             strongSelf.dataType=0;
            strongSelf.date_TimeType = btnTag+1;
        }
        [strongSelf.ks_TableView.mj_header beginRefreshing];
    };
    
    self.ks_TableView.frame = CGRectMake(0, self.topBtnsView.bottom, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-self.topBtnsView.height-50);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
    [self ks_tableAddHeaderRequstRefush];
    [self ks_tableAddFootRequst];
    
    
}
-(void)getData_Refush{
    _pageNum = 0;
    [self refushData:YES];
    
    
}
-(void)getData_LoadingMore{
    _pageNum ++;
    [self refushData:NO];
}
-(void)refushData:(BOOL)isRefush{
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getTransactionHistory"];
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:@(_pageNum) forKey:@"curpage"];
    if (self.dataType==0) {
            [diccc setValue:@"" forKey:@"type"];
    }else{
            [diccc setValue:@(self.dataType) forKey:@"type"];
    }

     [diccc setValue:@(self.date_TimeType) forKey:@"date"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        
        if (isRefush ==YES) {
            [strongSelf.modelsArry removeAllObjects];
        }
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *  dataArr = [dict001 jsonArray:@"data"];
//        NSInteger allPage = [dict jsonInteger:@"pages"];
//        NSInteger curpage = [dict jsonInteger:@"pageNumber"];
//        if (isRefush==NO) {
//            if (allPage == curpage+1) {
//                //没数据了
//                [strongSelf.ks_TableView.mj_footer endRefreshingWithNoMoreData];
//                return;
//            }
//        }
        
        for (NSDictionary * modelDicc in dataArr) {
            EDTransactionModel * modell = [[EDTransactionModel alloc]initWithDictionary:modelDicc error:nil];
            [strongSelf.modelsArry addObject:modell];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self placeholderViewWithFrame:self.ks_TableView.frame NoNetwork:NO];
        });
        [self.ks_TableView.mj_footer endRefreshing];
        [self.ks_TableView.mj_header endRefreshing];
        [strongSelf.ks_TableView reloadData];
        NSLog(@"User_welfareMobileActionWelfareCore---%@",dict001);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        //  [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDTransactionTableViewCell";
    EDTransactionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDTransactionTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDTransactionTableViewCell" owner:self options:nil] lastObject];
    }
    
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 105;
}

-(void)stepCell:(EDTransactionTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    EDTransactionModel * modell = self.modelsArry[indexPath.row];
    cell.orderLab.text = [NSString stringWithFormat:@"订单号:%@",modell.no];
    cell.showTime.text =  [NSString stringWithFormat:@"时间:%@",[modell.show_date stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
    cell.numLab.text =[NSString stringWithFormat:@"金额:%@",modell.amount];
    cell.typeLab.text =[NSString stringWithFormat:@"类型:%@",modell.type];
    cell.staseLab.text =[NSString stringWithFormat:@"%@",modell.status];
    if ([modell.status isEqualToString:@"已通过"]) {
        cell.staseLab.textColor = EDColor_BaseBlue;
    }else{
        cell.staseLab.textColor = kRGBColor(68, 68, 68);
    }
}

- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.modelsArry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        [self.ks_TableView addSubview:_workView];
        _workView.centerY = self.ks_TableView.centerY-100;
    }else
    {
        [_workView dissmiss];
    }
}


@end

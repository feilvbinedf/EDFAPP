//
//  EDHongLiListTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDHongLiListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *liTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *paiTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@end

NS_ASSUME_NONNULL_END

//
//  EDHistoricalRecordCenterVC.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDHistoricalRecordCenterVC.h"
#import "XXPageTabView.h"
#import "EDTransactionTableViewCell.h"
#import "EDTransactionListViewController.h"
#import "EDBettingListViewController.h"
@interface EDHistoricalRecordCenterVC ()
<XXPageTabViewDelegate>

{
    XXPageTabView *_pageTabView;
}
@property(nonatomic,strong) EDTransactionListViewController* vc001;
@property(nonatomic,strong)EDBettingListViewController * vc002;
@property(nonatomic,strong)EDBettingListViewController * vc003;
@property(nonatomic,strong)EDBettingListViewController * vc004;
@end

@implementation EDHistoricalRecordCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSegmentType_0View];
    
}


- (void)setupSegmentType_0View {
     self.view.backgroundColor =kRGBColor(245, 245, 245);
    self.vc001 = [EDTransactionListViewController new];
    self.vc002 = [EDBettingListViewController new];
    self.vc002.showTypeT =0;
    self.vc003 = [EDBettingListViewController new];
    self.vc003.showTypeT =1;
    self.vc004 = [EDBettingListViewController new];
    self.vc004.showTypeT =2;
    [self addChildViewController:self.vc001];
    [self addChildViewController:self.vc002];
    [self addChildViewController:self.vc003];
    [self addChildViewController:self.vc004];
    
    _pageTabView = [[XXPageTabView alloc] initWithChildControllers:self.childViewControllers childTitles:@[@"交易查询", @"投注查询",@"红 利", @"发 币"]];
    _pageTabView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    _pageTabView.tabSize = CGSizeMake(kScreenWidth, 50);
    _pageTabView.tabItemFont = AppFont(15);
    _pageTabView.unSelectedColor = kRGBColor(161, 161, 161);
    _pageTabView.selectedColor = kRGBColor(44, 143, 219);
    _pageTabView.bodyBounces = NO;
    _pageTabView.tabBackgroundColor = [UIColor whiteColor];
    _pageTabView.titleStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.indicatorStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.delegate = self;
    _pageTabView.indicatorWidth = 27;
    _pageTabView.indicatorHeight = 3;
    _pageTabView.backgroundColor = [UIColor whiteColor];
    _pageTabView.bodyBackgroundColor= [UIColor whiteColor];
    [self.view addSubview:_pageTabView];
    
    
    
}
#pragma mark - XXPageTabViewDelegate
- (void)pageTabViewDidEndChange {
    NSInteger selectedTabIndex = _pageTabView.selectedTabIndex;
    
}



@end

//
//  EDDBankeCardListViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/20.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDDBankeCardListViewController.h"
#import "EDBankCardTableViewCell.h"
#import "EDUserAddCardActionViewController.h"
#import "CJBankCardModel.h"
#import "EDBangdingPhoneOrEmailViewCobntroller.h"

@interface EDDBankeCardListViewController ()
@end

@implementation EDDBankeCardListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self User_getAllBankCards];
    [self ks_tableAddHeaderRequstRefush];
}
-(void)getData_Refush{
    [self User_getAllBankCards];
}
-(void)creatUI{
    
    self.ks_TableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
}
-(void)User_getAllBankCards{
     [self showLoadingAnimation];
    //User_getAllBankCards
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getAllBankCards"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *dictArry = [dict001 jsonArray:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (dictArry.count>0) {
                [strongSelf.modelsArry removeAllObjects];
                
                for (NSDictionary * diccdata in dictArry) {
                    CJBankCardModel *  queModel = [[CJBankCardModel alloc]initWithDictionary:diccdata error:nil];
                    [strongSelf.modelsArry addObject:queModel];
                }
                
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
        [strongSelf.ks_TableView.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        //NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        
    }];
}
#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDBankCardTableViewCell";
    EDBankCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDBankCardTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDBankCardTableViewCell" owner:self options:nil] lastObject];
    }
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160;
}

-(void)stepCell:(EDBankCardTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    
    cell.numbLab.text = [NSString stringWithFormat:@"银行卡%ld",indexPath.row+1];
    
    NSInteger dataCount = self.modelsArry.count;
    
    switch (dataCount) {
        case 0:
        {
            cell.infoBgView.hidden = YES;
            
        }
            break;
        case 1:
        {
            if (indexPath.row>0) {
                cell.infoBgView.hidden = YES;
                cell.is_DataConfig = NO;
            }else{
                cell.infoBgView.hidden = NO;
                CJBankCardModel * cardModel = self.modelsArry.firstObject;
                if (!kStringIsEmpty(cardModel.card)) {
                    cell.cardNumLab.text =[NSString stringWithFormat:@"尾号:%@", [cardModel.card substringFromIndex:cardModel.card.length-4]];
                    cell.bankName.text = [cardModel.card substringToIndex:4];
                }else{
                    cell.cardNumLab.text = @"";
                    cell.bankName.text = @"";
                }
                cell.is_DataConfig = YES;
            }
        }
            break;
            
        case 2:
        {
            if (indexPath.row>1) {
                cell.infoBgView.hidden = YES;
                cell.is_DataConfig = NO;
            }else{
                cell.infoBgView.hidden = NO;
                CJBankCardModel * cardModel = self.modelsArry[indexPath.row];
                if (!kStringIsEmpty(cardModel.card)) {
                    cell.cardNumLab.text =[NSString stringWithFormat:@"尾号:%@", [cardModel.card substringFromIndex:cardModel.card.length-4]];
                    cell.bankName.text = [cardModel.card substringToIndex:4];
                }else{
                    cell.cardNumLab.text = @"";
                    cell.bankName.text = @"";
                    
                }
                cell.is_DataConfig = YES;
            }
        }
            break;
        case 3:
        {
            cell.infoBgView.hidden = NO;
            CJBankCardModel * cardModel = self.modelsArry[indexPath.row];
            
            if (!kStringIsEmpty(cardModel.card)) {
                cell.cardNumLab.text =[NSString stringWithFormat:@"尾号:%@", [cardModel.card substringFromIndex:cardModel.card.length-4]];
                cell.bankName.text = [cardModel.card substringToIndex:4];
            }else{
                cell.cardNumLab.text = @"";
                cell.bankName.text = @"";
                
            }
            cell.is_DataConfig = YES;
        }
            break;
            
            
        default:
            break;
    }

  
    
    WEAKSELF
    [cell.addBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        EDUserAddCardActionViewController * addVC = [EDUserAddCardActionViewController new];
        WEAKSELF
        addVC.dismiss = ^(BOOL isSend) {
            STRONGSELFFor(weakSelf);
            [strongSelf User_getAllBankCards];
        };
        addVC.cellIndex = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        addVC.title = @"绑定银行卡";
        [strongSelf.navigationController pushViewController:addVC animated:YES];
    }];
//
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EDBankCardTableViewCell * itemCell = [self.ks_TableView cellForRowAtIndexPath:indexPath];
    
    if (itemCell.is_DataConfig ==YES) {
        //数据已配置，需要先去验证手机号
            CJBankCardModel * cardModel = self.modelsArry[indexPath.row];
        EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
        bangdingVC.title = @"验证手机号";
        bangdingVC.showType = 1;
        WEAKSELF
        bangdingVC.dismissWithPhoneCode = ^(NSString * _Nonnull phone, NSString * _Nonnull code) {
            STRONGSELFFor(weakSelf);
            dispatch_async(dispatch_get_main_queue(), ^{
                int64_t delayInSeconds = 1.0; // 延迟的时间
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    EDUserAddCardActionViewController * addVC = [EDUserAddCardActionViewController new];
                    WEAKSELF
                    addVC.dismiss = ^(BOOL isSend) {
                        STRONGSELFFor(weakSelf);
                        [strongSelf User_getAllBankCards];
                    };
                    addVC.cellIndex = [NSString stringWithFormat:@"%ld",indexPath.row+1];
                    addVC.title = @"修改银行卡";
                    addVC.phoneStr = phone;
                    addVC.codeStr = code;
                    addVC.is_Edit = YES;
                    addVC.bankModal = cardModel;
                    [self.navigationController pushViewController:addVC animated:YES];
                });
                
            });
        };
        [self.navigationController pushViewController:bangdingVC animated:YES];
        
    }else{
        //数据没配置，需要点击中间的加号按钮
        
        
    }
    
    
    
}








@end

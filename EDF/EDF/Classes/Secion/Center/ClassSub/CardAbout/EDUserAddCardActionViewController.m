//
//  EDUserAddCardActionViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/21.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDUserAddCardActionViewController.h"
#import "EDInfoSettingView.h"
#import "CGXPickerView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "EDQuestionModell.h"
#import "RestrictionInput.h"

@interface EDUserAddCardActionViewController ()
@property(nonatomic,strong)EDInfoSettingView * infoView;
@property(nonatomic,strong)EDInfoSettingView * infoView2;
@property(nonatomic,strong)TPKeyboardAvoidingScrollView * bgSC;
@property(nonatomic,strong)NSArray * seletAdressArry;
@property(nonatomic,strong)NSArray * seletAdressRowArry;
@property(nonatomic,strong)EDQuestionModell * fristModel;

@end

@implementation EDUserAddCardActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self User_getAllBankInfos];
    self.seletAdressRowArry =@[@4, @0,@0];
    
}

-(void)creatUI{
    
    self.bgSC = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight)];
    self.bgSC.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSC];
    
    
    EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
    infoView.enterTF.placeholder = @"";
    infoView.btnBg.hidden = NO;
    [infoView.daysBtn setTitle:@"选择开户银行 " forState:0];
    infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
    [self.bgSC addSubview:infoView];
    self.infoView = infoView;
    WEAKSELF
    [infoView.daysBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf choseBank:infoView.daysBtn];
    }];
    
    EDInfoSettingView * infoView2 = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
    infoView2.enterTF.placeholder = @"输入姓名";
    infoView2.enterTF.tag = 10086;
    infoView2.enterTF.keyboardType = UIKeyboardTypeDefault;
         [infoView2.enterTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    infoView2.frame =CGRectMake(0, infoView.bottom+1, kScreenWidth, 70);
    [self.bgSC addSubview:infoView2];
    
    EDInfoSettingView * infoView3 = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
    infoView3.enterTF.placeholder = @"输入卡号";
    infoView3.enterTF.keyboardType = UIKeyboardTypeNumberPad;
    infoView3.enterTF.tag = 10087;
        [infoView3.enterTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    infoView3.frame =CGRectMake(0, infoView2.bottom+1, kScreenWidth, 70);
    [self.bgSC addSubview:infoView3];
    
    EDInfoSettingView * infoView4 = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
    infoView4.enterTF.placeholder = @"确认卡号";
    infoView4.enterTF.tag = 10088;
       [infoView4.enterTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    infoView4.enterTF.keyboardType = UIKeyboardTypeNumberPad;
    infoView4.frame =CGRectMake(0, infoView3.bottom+1, kScreenWidth, 70);
    [self.bgSC addSubview:infoView4];
    
    
    EDInfoSettingView * infoView5 = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
    infoView5.enterTF.placeholder = @"";
    infoView5.btnBg.hidden = NO;
    [infoView5.daysBtn setTitle:@"省 市 区 " forState:0];
    infoView5.frame =CGRectMake(0, infoView4.bottom+1, kScreenWidth, 70);
    [self.bgSC addSubview:infoView5];
    self.infoView2 = infoView5;
    
    [infoView5.daysBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
         [strongSelf choseAdress:infoView.daysBtn];
    }];
    
    
    UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, infoView5.bottom+40, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick:) title:@"保存设置"];
    [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
    [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
    [self.bgSC addSubview:bottomBtn];
    
    if (self.is_Edit ==YES) {
        
        
    }
    
    
//    bankcode = 1;
//    bankno = 123123124231512412;
//    card = "\U5de5\U5546\U94f6\U884c/\U5c3e\U53f7\Uff1a2412";
//    city = "\U5929\U6d25\U5e02";
//    id = BODoUz3YT8WXEnv8a0sPdQ;
//    province = "\U5929\U6d25\U5e02";
//    whichone = 0;

}
-(void)config_EditData{
   // self.bankModal ?
}
-(void)User_getAllBankInfos{
    [self showLoadingAnimation];
    //User_getAllBankLists
    NSArray * localArry = [kUserDefaults objectForKey:@"UserAllbankList"];
    NSDictionary * friDicc = localArry[0];
    
    self.fristModel = [[EDQuestionModell alloc]initWithDictionary:friDicc error:nil];
    [self.infoView.daysBtn setTitle:self.fristModel.bankName forState:0];
    
    for (NSDictionary * diccdata in localArry) {
        EDQuestionModell *  queModel = [[EDQuestionModell alloc]initWithDictionary:diccdata error:nil];
        [self.modelsArry addObject:queModel];
    }
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getAllBankLists"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
          [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *dictArry = [dict001 jsonArray:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (dictArry.count>0) {
                [strongSelf.modelsArry removeAllObjects];
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault  setValue:dictArry forKey:@"UserAllbankList"];
                [userDefault synchronize ];
                NSDictionary * friDicc = dictArry[0];
                
                strongSelf.fristModel = [[EDQuestionModell alloc]initWithDictionary:friDicc error:nil];
                [strongSelf.infoView.daysBtn setTitle:strongSelf.fristModel.bankName forState:0];
                
                for (NSDictionary * diccdata in dictArry) {
                    EDQuestionModell *  queModel = [[EDQuestionModell alloc]initWithDictionary:diccdata error:nil];
                    [strongSelf.modelsArry addObject:queModel];
                }
                
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        //NSLog(@"error----%@",error);
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
        
    }];
    

}

-(void)choseBank:(UIButton*)sender{
    //选择了银行卡
    if (self.modelsArry.count==0) {
        
        [self User_getAllBankInfos];
        return;
    }
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    NSMutableArray * dataArry = [NSMutableArray array];
    for (EDQuestionModell * modell in self.modelsArry) {
        [dataArry addObject:modell.bankName];
    }
    WEAKSELF
    [CGXPickerView showStringPickerWithTitle:@"选择开户行" DataSource:dataArry DefaultSelValue:self.fristModel.bankName IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
        NSLog(@"CGXPickerView--%@",selectValue);
        [self.modelsArry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            STRONGSELFFor(weakSelf);
            NSLog(@"%zi %@", idx, obj);
            EDQuestionModell * modell2 = (EDQuestionModell *)obj;
            if ([modell2.bankName isEqualToString:selectValue]) {
                NSLog(@"------ %zi %@", idx, obj);
                strongSelf.fristModel = modell2;
                [strongSelf.infoView.daysBtn setTitle:modell2.bankName forState:0];
                *stop = YES;
            }
        }];
        
    }];
    
    
    
}
-(void)choseAdress:(UIButton*)sender{
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitle= @"";
    manager.leftBtnTitleSize=0;
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    WEAKSELF
    [CGXPickerView showAddressPickerWithTitle:@"请选择省市区" DefaultSelected:self.seletAdressRowArry IsAutoSelect:YES Manager:manager ResultBlock:^(NSArray *selectAddressArr, NSArray *selectAddressRow) {
        STRONGSELFFor(weakSelf);
        NSLog(@"%@-%@",selectAddressArr,selectAddressRow);
        strongSelf.seletAdressRowArry = selectAddressRow;
         strongSelf.seletAdressArry = selectAddressArr;
           [strongSelf.infoView2.daysBtn setTitle:[NSString stringWithFormat:@"%@%@%@", selectAddressArr[0], selectAddressArr[1],selectAddressArr[2]] forState:0];
    }];
    
}

-(void)textFieldDidChange:(UITextField *)textField {
    
    if (textField.tag == 10086) {
        [RestrictionInput restrictionInputTextField:textField maxNumber:10 showView:self.view showErrorMessage:@""];
    }else{
           [RestrictionInput restrictionInputTextField:textField maxNumber:21 showView:self.view showErrorMessage:@""];
        
    }
    
}

-(void)sureBtnClick:(UIButton *)sender{

    WEAKSELF
    [[SmileAlert sharedInstance] mazi_alertContent:@" 请确保银行卡信息正确\n否则因信息不准确导致出款问题\n由本人负责" AndBlock:^(BOOL sure) {
        STRONGSELFFor(weakSelf);
        if (sure==YES) {
            [strongSelf add_bankCardAction];
        }
    }];

}

-(void)add_bankCardAction{
    
    if (!self.fristModel || kStringIsEmpty(self.fristModel.bankName)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入开户行"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    UITextField * enterText = [self.view viewWithTag:10086];
    if (kStringIsEmpty(enterText.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入姓名"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    if ([self isBlank:enterText.text]==YES) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"姓名不能包含空格"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    UITextField * enterText10087 = [self.view viewWithTag:10087];
    if (kStringIsEmpty(enterText10087.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入银行卡号"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    if ([self isNum:enterText10087.text]==NO) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"银行卡号必须为数字"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    
    if ([self checkBankCardNumber:enterText10087.text]==NO) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入正确的银行卡号"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    UITextField * enterText10088 = [self.view viewWithTag:10088];
    if (kStringIsEmpty(enterText10088.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请确认银行卡号"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    if (![enterText10087.text isEqualToString:enterText10088.text]) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"两次输入的卡号不一致"] andlightStr:@""];
        [toast showLXAlertView];
        return;
        
    }
    
    
    if (self.seletAdressArry.count==0 || [self.infoView2.daysBtn.titleLabel.text isEqualToString:@"省 市 区 "]) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请选择省市区"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_SaveBankNumber"];
    WEAKSELF
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:self.fristModel.id forKey:@"bankCode"];
    [diccc setValue:enterText10087.text forKey:@"cardNum"];
    [diccc setValue:self.fristModel.bankName forKey:@"opener"];
    [diccc setValue:self.cellIndex forKey:@"which"];
    [diccc setValue:self.seletAdressArry[0] forKey:@"province"];
    [diccc setValue:self.seletAdressArry[1] forKey:@"city"];
    [diccc setValue:self.seletAdressArry[2] forKey:@"county"];
    
    if (self.is_Edit ==YES) {
        [diccc setValue:self.phoneStr forKey:@"tel"];
        [diccc setValue:self.codeStr forKey:@"code"];
    }
    
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        NSLog(@"responseObjectStr----%@",responseObjectStr);
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"绑定成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDUserAddCardActionViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
    
}

/**
 * 判断字符串是否包含空格
 */
-(BOOL)isBlank:(NSString *)str{
    NSRange _range = [str rangeOfString:@" "];
    if (_range.location != NSNotFound) {
        //有空格
        // NSLog(@"有空格");
        return YES;
    }else {
        //没有空格
        // NSLog(@"没有空格");
        return NO;
    }
}

- (BOOL)isNum:(NSString *)checkedNumString {
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}

-(BOOL)checkBankCardNumber:(NSString*)cardNumber

{
    
    int oddSum =0;    // 奇数和
    
    int evenSum =0;    // 偶数和
    
    int allSum =0;    // 总和
    
    
    
    // 循环加和
    
    for(NSInteger i =1; i <= cardNumber.length; i++)
        
    {
        
        NSString *theNumber = [cardNumber substringWithRange:NSMakeRange(cardNumber.length-i,1)];
        
        int lastNumber = [theNumber intValue];
        
        if(i%2==0)
            
        {
            
            // 偶数位
            
            lastNumber *=2;
            
            if(lastNumber >9)
                
            {
                
                lastNumber -=9;
                
            }
            
            evenSum += lastNumber;
            
        }
        
        else
            
        {
            
            // 奇数位
            
            oddSum += lastNumber;
            
        }
        
    }
    
    allSum = oddSum + evenSum;
    
    // 是否合法
    
    if(allSum%10==0)
        
    {
        
        return YES;
        
    }
    
    else
        
    {
        
        return NO;
        
    }
    
}


@end

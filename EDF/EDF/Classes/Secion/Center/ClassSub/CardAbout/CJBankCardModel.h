//
//  CJBankCardModel.h
//  DJ
//
//  Created by 微笑吧阳光 on 2019/6/16.
//  Copyright © 2019 DJ. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CJBankCardModel : EDBaseModel

//银行卡
@property(nonatomic,copy)NSString<Optional> * bankcode;
@property(nonatomic,copy)NSString<Optional> * bankno;
@property(nonatomic,copy)NSString<Optional> * county;
@property(nonatomic,copy)NSString<Optional> * city;
@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * province;
@property(nonatomic,copy)NSString<Optional> * card;

//提款账户

@property(nonatomic,copy)NSString<Optional> * zeroline;
@property(nonatomic,copy)NSString<Optional> * flowtimes;
@property(nonatomic,copy)NSString<Optional> * bonus;
@property(nonatomic,copy)NSString<Optional> * rebate;
@property(nonatomic,copy)NSString<Optional> * maxflow;
@property(nonatomic,copy)NSString<Optional> * promotion_id;
@property(nonatomic,copy)NSString<Optional> * account_gameplatform;
@property(nonatomic,copy)NSString<Optional> * type;
@property(nonatomic,copy)NSString<Optional> * update_date;
@property(nonatomic,copy)NSString<Optional> * rebateable;
@property(nonatomic,copy)NSString<Optional> * order_date;
@property(nonatomic,copy)NSString<Optional> * balance;
@property(nonatomic,copy)NSString<Optional> * user_id;
@property(nonatomic,copy)NSString<Optional> * validflow;
@property(nonatomic,copy)NSString<Optional> * name;
//@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * create_date;
@property(nonatomic,copy)NSString<Optional> * withdrawlimit;
@property(nonatomic,copy)NSString<Optional> * cash;
@property(nonatomic,copy)NSString<Optional> * refund;
@property(nonatomic,copy)NSString<Optional> * withdraw;
@property(nonatomic,copy)NSString<Optional> * status;


//{
//    "zeroline": "5",
//    "flowtimes": "20",
//    "bonus": "0",
//    "rebate": "0",
//    "maxflow": "0",
//    "promotion_id": "0",
//    "account_gameplatform": "lc",
//    "type": "调整账户",
//    "update_date": "2019-05-01%2018:50:36",
//    "rebateable": "0",
//    "order_date": "2018-12-03%2018:19:16",
//    "balance": "110",
//    "user_id": "yAeXkl3hQxKY1EWN-9ViKA",
//    "validflow": "0",
//    "name": "棋牌测试",
//    "id": "vv3vO4BlTjGaHhK5EsgaOA",
//    "create_date": "2018-12-03%2018:19:16",
//    "withdrawlimit": "",
//    "cash": 110,
//    "refund": "0",
//    "withdraw": "0",
//    "status": "正常"
//}


//"province":"天津市","city":"天津市",
//"bankno":"1888888888888888888",
//"id":"ki8t05gsRjOdskQV7Nt30Q",
//"bankcode":"2",
//"card":"农业银行/尾号：8888"


@end

NS_ASSUME_NONNULL_END

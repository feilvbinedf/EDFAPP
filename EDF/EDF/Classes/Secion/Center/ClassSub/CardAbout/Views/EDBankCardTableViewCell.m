//
//  EDBankCardTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/20.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBankCardTableViewCell.h"

@implementation EDBankCardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.infoBgView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

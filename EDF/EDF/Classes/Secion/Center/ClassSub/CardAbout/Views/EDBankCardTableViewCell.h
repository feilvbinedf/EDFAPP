//
//  EDBankCardTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/20.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDBankCardTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *infoBgView;

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *cardNumLab;

@property (weak, nonatomic) IBOutlet UILabel *numbLab;

@property(nonatomic,assign)BOOL is_DataConfig;

@end

NS_ASSUME_NONNULL_END

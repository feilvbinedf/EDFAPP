//
//  EDUserAddCardActionViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/21.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "CJBankCardModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDUserAddCardActionViewController : EDBaseViewController


@property(nonatomic,strong)CJBankCardModel * bankModal;
@property(nonatomic,copy)NSString * cellIndex;;


@property(nonatomic,assign)BOOL is_Edit; //是否是编辑银行卡号码
@property(nonatomic,copy)NSString * phoneStr; //编辑银行卡信息之前需要验证手机号码
@property(nonatomic,copy)NSString * codeStr;


@property (nonatomic, copy) void (^dismiss)(BOOL isSend);
@end

NS_ASSUME_NONNULL_END

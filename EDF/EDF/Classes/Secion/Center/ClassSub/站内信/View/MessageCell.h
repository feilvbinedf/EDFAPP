//
//  MessageCell.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *messageIcon;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (nonatomic, strong) UITextView * textview;
@property (nonatomic, strong) UIImageView * contentImage;
@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, strong) UIButton * readButton;
@property (nonatomic, strong) UIButton * zanButton;
@property (nonatomic, strong) MessageModel * model;
@end

NS_ASSUME_NONNULL_END

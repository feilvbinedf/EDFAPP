//
//  PersonaCell.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PersonaCell.h"

@implementation PersonaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellAccessoryNone;
    
    [self.contentView addSubview:self.content];
}
- (void)setModel:(PersonaModel *)model
{
    _model = model;
    _content.hidden = YES;
    _contentLab.text = model.title;
    NSString *timestr = [model.create_date stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    _timeLab.text = timestr;
  
}
- (UILabel *)content
{
    if (!_content) {
        _content = [[UILabel alloc] initWithFrame:CGRectMake(20, _headView.bottom, UIScreenWidth -40, 0)];
        _content.numberOfLines = 0;
        _content.textColor = RGBA(68, 68, 68, 1);
        _content.font = AutoFont(12);
        _content.textAlignment = NSTextAlignmentCenter;
    }
    return _content;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
@implementation ChatCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        self.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return self;
    
}
- (void)get_up
{
    [self.contentView addSubview:self.leftIcon];
    [self.contentView addSubview:self.leftLab];
    [self.contentView addSubview:self.rightIcon];
    [self.contentView addSubview:self.rightab];
}
- (UILabel *)leftIcon
{
    if (!_leftIcon) {
        _leftIcon = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 30, 30)];
        _leftIcon.backgroundColor = MAINCOLOR;
        _leftIcon.textColor = WHITECOLOR;
        _leftIcon.textAlignment = NSTextAlignmentCenter;
        _leftIcon.font = AutoFont(12);
        _leftIcon.adjustsFontSizeToFitWidth = YES;
        KViewRadius(_leftIcon, 15);
    }
    return _leftIcon;
}
- (UILabel *)leftLab
{
    if (!_leftLab) {
        _leftLab = [[UILabel alloc] initWithFrame:CGRectMake(_leftIcon.right +7, 0, 0, 0)];
        [_leftLab sizeToFit];
        _leftLab.font = AutoFont(12);
        _leftLab.textColor = RGBA(68, 68, 68, 1);
        _leftLab.backgroundColor = WHITECOLOR;
        _leftLab.textAlignment = NSTextAlignmentCenter;
        KViewRadius(_leftLab, 2);
    }
    return _leftLab;
}
- (UIImageView *)rightIcon
{
    if (!_rightIcon) {
        _rightIcon = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth - 30 - 20, 10, 30, 30)];
        _rightIcon.image = [UIImage imageNamed:@"userIcon"];
   
    }
    return _rightIcon;
}
- (UILabel *)rightab
{
    if (!_rightab) {
        _rightab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_rightab sizeToFit];
        _rightab.textAlignment = NSTextAlignmentCenter;
        _rightab.font = AutoFont(12);
        _rightab.textColor = WHITECOLOR;
        _rightab.backgroundColor = MAINCOLOR;
        KViewRadius(_rightab, 2);
    }
    return _rightab;
}
- (void)setModel:(CatModel *)model
{
    if ([model.type isEqualToString:@"a2u"])
    {
        _rightab.hidden = _rightIcon.hidden = YES;
        _leftLab.hidden = _leftIcon.hidden =NO;
        _leftIcon.text = model.admin_name ;
        _leftLab.text =  model.content;
        [_leftLab sizeToFit];
        
        _leftLab.width = _leftLab.width +10;
        _leftLab.height = _leftLab.height +10;
        _leftLab.centerY = _leftIcon.centerY;
        
    }else
    {
        _rightab.hidden = _rightIcon.hidden = NO;
        _leftLab.hidden = _leftIcon.hidden =YES;
        _rightab.text = model.content;
        [_rightab sizeToFit];
        
        _rightab.width = _rightab.width +10;
        _rightab.height = _rightab.height +10;
        _rightab.mj_x = _rightIcon.mj_x - 7 - _rightab.width;
        _rightab.centerY = _rightIcon.centerY;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

//
//  SystemTableHeadView.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMCustomSegment.h"
NS_ASSUME_NONNULL_BEGIN

@interface SystemTableHeadView : UIView
@property(nonatomic,strong) SMCustomSegment *segment;
@property (nonatomic, strong) UILabel * redlable;//
@property (copy, nonatomic) void(^indexBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END

//
//  MessageCell.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellAccessoryNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
//    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    [self.contentView addSubview:self.contentImage];
    [self.contentView addSubview:self.textview];
 
    [self.textview addSubview:self.deleteButton];
    [self.textview addSubview:self.readButton];
    [self.textview addSubview:self.zanButton];
}
- (void)setModel:(MessageModel *)model
{
    _model = model;
    
    _contentLab.text = model.title;
     NSString *timestr = [model.create_date stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    _timeLab.text = timestr;
    _textview.attributedText= model.mucontent;
    _textview.textColor = RGBA(68, 68, 68, 1);
    _textview.textAlignment = NSTextAlignmentCenter;
    
    if ([model.already_praise isEqualToString:@"是"]) {
        _zanButton.selected = YES;
    }else
    {
         _zanButton.selected = NO;
    }
    if ([model.already_read isEqualToString:@"是"]) {
        _readButton.selected = YES;
    }else
    {
        _readButton.selected = NO;
    }
    if (kStringIsEmpty(model.content_id)) {
        _contentImage.top = 64;
        _textview.height = model.cellHeight -64 +60;
        _contentImage.height = 0;
        _deleteButton.mj_y = _textview.height -40 ;
        _textview.top =_contentImage.bottom;
        _readButton.mj_y = _deleteButton.mj_y -80;
        _zanButton.centerY = _readButton.centerY;
    }else{
        _contentImage.top = 64;
          _contentImage.hidden = NO;
        _contentImage.height = (kScreenWidth -30)*0.4;
        if (kStringIsEmpty(model.imageData)) {
            _contentImage.hidden = YES;
        }else{
            NSData *ImageData = [[NSData alloc] initWithBase64EncodedString:[model.imageData stringByReplacingOccurrencesOfString:@"data:image/jpeg;base64," withString:@""] options:NSDataBase64DecodingIgnoreUnknownCharacters];
            UIImage *testImage = [UIImage imageWithData:ImageData];
            _contentImage.image = testImage;
            
        }
        _textview.height = model.cellHeight -64-_contentImage.height +60;
        _deleteButton.mj_y = _textview.height -40;
        _readButton.mj_y = _deleteButton.mj_y -80;
        _zanButton.centerY = _readButton.centerY;
        _textview.top =_contentImage.bottom;
    }
  //
}

- (UITextView *)textview
{
    if (!_textview) {
        _textview = [[UITextView alloc] initWithFrame:CGRectMake(15, 64 ,UIScreenWidth -30,0)];
        _textview.editable = NO;
        _textview.showsVerticalScrollIndicator = NO;
        _textview.textAlignment = NSTextAlignmentCenter;
    }
    return _textview;
}
-(UIImageView *)contentImage{
    
    if (!_contentImage) {
       
        _contentImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 64, kScreenWidth-30, 0)];
        _contentImage.backgroundColor = kRGBColor(245, 245, 245);
        _contentImage.hidden = YES;
    }
    return _contentImage;
    
}
- (UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake((UIScreenWidth-30)/2 -50 , _textview.height -30, 100, 30)];
        [_deleteButton setTitle:@"删除消息" forState:0];
        KViewRadius(_deleteButton, 2);
        _deleteButton.backgroundColor = MAINCOLOR;
        _deleteButton.titleLabel.font = AutoFont(14);
        
    }
    return _deleteButton;
}
- (UIButton *)readButton
{
    if (!_readButton) {
        _readButton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth -80 ,_deleteButton.mj_y -80, 30, 70)];
        [_readButton setTitle:@"阅读" forState:0];
        [_readButton setTitleColor:RGBA(68, 68, 68, 1) forState:(UIControlStateNormal)];
        [_readButton setTitleColor:MAINCOLOR forState:(UIControlStateSelected)];
        [_readButton setImage:[UIImage imageNamed:@"yuedu_normal"] forState:UIControlStateNormal];
        [_readButton setImage:[UIImage imageNamed:@"yuedu_active"] forState:UIControlStateSelected]; 
        [_readButton centerHorizontallyImageAndTextWithPadding:10];
        _readButton.titleLabel.font = AutoFont(14);
        
    }
    return _readButton;
}
- (UIButton *)zanButton
{
    if (!_zanButton) {
        _zanButton = [[UIButton alloc] initWithFrame:CGRectMake(_readButton.left -90 ,0, 30, 70)];
        [_zanButton setTitle:@"点赞" forState:0];
        [_zanButton setTitleColor:RGBA(68, 68, 68, 1) forState:(UIControlStateNormal)];
        [_zanButton setTitleColor:MAINCOLOR forState:(UIControlStateSelected)];
        [_zanButton setImage:[UIImage imageNamed:@"zan_normal"] forState:UIControlStateNormal];
        [_zanButton setImage:[UIImage imageNamed:@"zan_active"] forState:UIControlStateSelected];
        _zanButton.titleLabel.font = AutoFont(14);
        [_zanButton centerHorizontallyImageAndTextWithPadding:10];
        _zanButton.centerY = _readButton.centerY;
    }
    return _zanButton;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

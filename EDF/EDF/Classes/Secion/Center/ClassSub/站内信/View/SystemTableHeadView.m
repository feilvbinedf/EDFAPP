//
//  SystemTableHeadView.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "SystemTableHeadView.h"
@interface SystemTableHeadView ()<SMCustomSegmentDelegate>
//@property (nonatomic,strong)UILabel * redLable
@end
@implementation SystemTableHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGBA(245, 245, 245, 1);
        [self get_up];
        [self addSubview:self.redlable];
    }
    return self;
    
}
- (UILabel *)redlable
{
    if (!_redlable) {
        _redlable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth/2 -34 , _segment.mj_y , 20, 20)];
        KViewRadius(_redlable, 10);
        _redlable.textColor = WHITECOLOR;
        _redlable.font = AutoFont(12);
        _redlable.textAlignment = NSTextAlignmentCenter;
        _redlable.backgroundColor = [UIColor redColor];
        _redlable.hidden = YES;
    }
    return _redlable;
}
- (void)get_up
{
    _segment =  [[SMCustomSegment alloc] initWithFrame:CGRectMake(0, 10, 223 , 35) titleArray:@[@"未读消息",@"已读消息"]];
    _segment.selectIndex = 0;
    _segment.delegate = self;
    _segment.normalBackgroundColor = WHITECOLOR;
    _segment.selectBackgroundColor = MAINCOLOR;
    _segment.titleNormalColor = RGBA(90, 90, 90, 1);
    _segment.titleSelectColor = [UIColor whiteColor];
    _segment.normalTitleFont = 14;
    _segment.selectTitleFont = 14;
    _segment.borderWidth = 0.5;
    _segment.centerX = self.centerX;
    [self addSubview:_segment];
}
- (void)customSegmentSelectIndex:(NSInteger)selectIndex
{
    if (self.indexBlock) {
        self.indexBlock(selectIndex);
    }
}
@end

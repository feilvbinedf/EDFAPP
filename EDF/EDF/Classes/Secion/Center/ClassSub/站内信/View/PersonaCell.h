//
//  PersonaCell.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonaCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *messageIcon;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (nonatomic, strong) PersonaModel * model;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ns_x;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *img_h;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *img_w;
@property (nonatomic,strong)UILabel * content;
@end

@interface ChatCell : UITableViewCell
@property (strong, nonatomic)  UILabel *leftIcon;
@property (strong, nonatomic)  UILabel *leftLab;
@property (strong, nonatomic)  UIImageView *rightIcon;
@property (strong, nonatomic)  UILabel *rightab;
@property (nonatomic, strong)  CatModel * model;
@end
NS_ASSUME_NONNULL_END

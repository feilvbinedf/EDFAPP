//
//  NoticeMessageVC.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoticeMessageVC : EDBaseViewController

@property (nonatomic,assign) NSInteger index;
@property(nonatomic,assign) BOOL  is_HomeMessage;
@end

NS_ASSUME_NONNULL_END

//
//  PersonalInfoViewController.m
//  EDF
//
//  Created by p on 2019/7/15.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "PersonaCell.h"
@interface PersonalInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UILabel * messageLable;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic,strong)UIView * footView;
@property (nonatomic,strong)UIView * chatView;
@property (nonatomic,strong)UIButton * deleteBut;
@property (nonatomic,strong)UITextField * chatField;
@property (nonatomic,strong)UIButton * addChatBut;
@end

@implementation PersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    [self.view addSubview:self.messageLable];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.footView];
    [self.footView addSubview:self.chatView];
    [self.footView addSubview:self.deleteBut];
    [_chatView addSubview:self.chatField];
    [_chatView addSubview:self.addChatBut];
    // 上拉加载
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self getData_Refush];
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
}
-(void)getData_Refush
{
    WeakSelf
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadReplyInfo"];
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:@{@"msg_id":_model.id} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
    
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * dic = [self objectFromJSONString:responseObject];
            weakSelf.dataArray  = [CatModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
           
            CatModel * mode = weakSelf.dataArray.lastObject;
            if ([mode.type isEqualToString:@"a2u"]) {
                weakSelf.chatView.hidden = NO;
            }else if ([mode.type isEqualToString:@"u2a"])
            {
                weakSelf.chatView.hidden = YES;
            }
            
           
            [weakSelf.tableView reloadData];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
         [self.tableView.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
         [self.tableView.mj_header endRefreshing];
        [WJUnit showMessage:@"请求错误"];
    }];
}
- (UILabel *)messageLable
{
    if (!_messageLable) {
        _messageLable = [[UILabel alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, 0)];
       
        _messageLable.numberOfLines = 0;
        _messageLable.textColor = RGBA(68, 68, 68, 1);
        _messageLable.font = AutoFont(14);
        _messageLable.text = _model.title;
      
        _messageLable.textAlignment = NSTextAlignmentCenter;
        [_messageLable sizeToFit];
        _messageLable.width = UIScreenWidth;
        _messageLable.height = _messageLable.height+10;
    }
    return _messageLable;
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _messageLable.bottom, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight -  _messageLable.height - 140)style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = CLEARCOLOR;
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = RGBA(245, 245, 245, 1);
        _tableView.showsVerticalScrollIndicator = NO;
         [_tableView registerClass:[ChatCell class] forCellReuseIdentifier:@"ChatCell"];
    }
    return _tableView;
}
- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] initWithFrame:CGRectMake(0, UIScreenHeight - 140, UIScreenWidth, 140)];
        _footView.backgroundColor = WHITECOLOR;
        
        
    }
    return _footView;
}
- (UIView *)chatView
{
    if (!_chatView) {
        _chatView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 70)];
        _chatView.backgroundColor = RGBA(245, 245, 245, 1);
        _chatView.hidden = YES;
    }
    return _chatView;
}
- (UITextField *)chatField
{
    if (!_chatField) {
        _chatField = [[UITextField alloc] initWithFrame:CGRectMake(20,20, RealValue_H(470), 30)];
        _chatField.placeholder = @"请输入回复文字";
        _chatField.font = AutoFont(12);
        _chatField.textColor = RGBA(68, 68, 68, 1);
        _chatField.tintColor = RGBA(68, 68, 68, 1);
      //  [_chatField setValue:RGBA(161, 161, 161, 1) forKeyPath:@"_placeholderLabel.textColor"];
        [_chatField setValue:AutoFont(12)forKeyPath:@"_placeholderLabel.font"];
        _chatField.backgroundColor = WHITECOLOR;
        _chatField.textAlignment = NSTextAlignmentCenter;
    }
    return _chatField;
}
- (UIButton *)addChatBut
{
    if (!_addChatBut) {
          WeakSelf
        _addChatBut = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(_chatField.right +22, 20, 78, 30) title:@"回复" backgroundColor:RGBA(78, 178, 237, 1) titleColor:RGBA(68, 68, 68, 1) tapAction:^(UIButton *button) {
            
            if (kStringIsEmpty(weakSelf.chatField.text)) {
               [JMNotifyView showNotify:@"请输入内容"  isSuccessful:NO];
                return ;
            }
            NSString  * replyMsg = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"replyMsg"];
            NSDictionary * dic = @{@"msg_id":weakSelf.model.id,@"content":weakSelf.chatField.text};
            [[SmileHttpTool sharedInstance] GET :replyMsg parameters:dic origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
                NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
                
                if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
                    CatModel * model = [[CatModel alloc] init];
                    model.content = weakSelf.chatField.text;
                    model.type = @"u2a";
                    [self.dataArray addObject:model];
                    [weakSelf.tableView reloadData];
                    
                }else{
                    [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
                }
                [self.tableView.mj_header endRefreshing];
                
            } failure:^(id responseObject, NSError *error) {
                [self.tableView.mj_header endRefreshing];
                [WJUnit showMessage:@"请求错误"];
            }];
            
        }];
        _addChatBut.titleLabel.font =AutoFont(14);
        KViewRadius(_addChatBut, 2);
    }
    return _addChatBut;
}
- (UIButton *)deleteBut
{
    if (!_deleteBut) {
        WeakSelf
        _deleteBut = [UIButton dd_buttonCustomButtonWithFrame:CGRectMake(0,_chatView.bottom + 20, 100, 30) title:@"删除消息" backgroundColor:MAINCOLOR titleColor:WHITECOLOR tapAction:^(UIButton *button) {
            NSString  * replyMsg = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"deleteUserInfoMsg"];
            NSDictionary * dic = @{@"msg_id":weakSelf.model.id};
            [[SmileHttpTool sharedInstance] GET :replyMsg parameters:dic origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
                NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
                
                if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                    
                }else{
                    [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
                }
                [self.tableView.mj_header endRefreshing];
                
            } failure:^(id responseObject, NSError *error) {
                [self.tableView.mj_header endRefreshing];
                [WJUnit showMessage:@"请求错误"];
            }];
            
            
        }];
        _deleteBut.centerX = _footView.centerX;
        _deleteBut.titleLabel.font =AutoFont(14);
        KViewRadius(_deleteBut, 2);
    }
    return _deleteBut;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatCell * cell = (ChatCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    CatModel * model = _dataArray[indexPath.row];
    if ([model.status isEqualToString:@"a2u"]) {
        return cell.leftLab.height +50;
    }else
    {
         return cell.rightab.height +50;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     ChatCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    CatModel * model = _dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
@end

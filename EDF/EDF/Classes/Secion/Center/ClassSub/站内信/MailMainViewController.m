//
//  MailMainViewController.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "MailMainViewController.h"
#import "MailPageView.h"
#import "SendMessageViewController.h"
@interface MailMainViewController ()
@property(nonatomic,strong) MailPageView *pageView;
@end

@implementation MailMainViewController
- (void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PersonalNotificationse object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"站内信";
    [self.view addSubview:self.pageView];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem RightitemWithImageNamed:@"" title:@"新建" target:self action:@selector(build)];
}
// 新建
- (void)build
{
    SendMessageViewController * sendMessageVC = [[SendMessageViewController alloc] init];
    sendMessageVC.title=@"发送新消息";
    [self.navigationController pushViewController:sendMessageVC animated:YES];
}
- (MailPageView *)pageView {
    _pageView = [[MailPageView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight)
                                       withTitles:@[@"系统消息",@"个人消息",@"系统公告"]
                              withViewControllers:@[@"SystemMessageVC",@"PersonalMessageVC",@"NoticeMessageVC"]
                                   withParameters:nil];
    
    
    return _pageView;
}

@end

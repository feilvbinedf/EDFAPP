//
//  SendMessageViewController.m
//  EDF
//
//  Created by p on 2019/7/15.
//  Copyright © 2019年 p. All rights reserved.
//

#import "SendMessageViewController.h"
#import "UITextView+Category.h"
#import "CGXPickerView.h"
@interface SendMessageViewController ()
@property (nonatomic,strong)UITextView * textView;
@property (nonatomic,strong)NSString * selectValue;
@property (nonatomic,strong)NSString * selectindex;
@property(nonatomic,copy)    NSString  * imageKey;
@end

@implementation SendMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self getCode];
}
- (void)getCode
{
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"LogincOnLoadOCode"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET_Data :url parameters:nil origin:YES success:^(NSInteger statusCode,NSURLSessionDataTask *  task,id responseObject) {
        STRONGSELFFor(weakSelf)
        YYLog(@"responseObject----%@",responseObject);
        NSData *imageDataStr = [NSData dataWithData:responseObject];
        NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse*)task.response;
        NSDictionary *allHeaderFieldsDict = httpURLResponse.allHeaderFields;
        NSLog(@"allHeaderFieldsDict---%@",allHeaderFieldsDict);
        strongSelf.imageKey = [allHeaderFieldsDict jsonString:@"cpkey"];
        strongSelf.codeImage.image =  [UIImage imageWithData:imageDataStr];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
}
-(void)chack_ImageCodeRequest{
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Code_verificationRandomCode"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.codeField.text forKey:@"code"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET_DataDChackImageCode:url imageKey:self.imageKey parameters:dicc origin:YES success:^(NSInteger statusCode, NSURLSessionDataTask *task, id responseObject) {
        STRONGSELFFor(weakSelf);
        // NSLog(@"LoginccCheckImageCodeAction----%@",responseObject);
        NSString* strdata = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData * data =[[strdata URLDecodedString] dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
             [weakSelf sendMessage];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        [JMNotifyView showNotify:@"校验图形验证码，网络错误，请重试~" isSuccessful:NO];
        [self stopLoadingAnimation];
    }];
    
}
- (void)setUI
{
    _view1.backgroundColor = _view2.backgroundColor = RGBA(240, 240, 240, 1);
    [self.openButton add_BtnClickHandler:^(NSInteger tag) { //下拉类型
        
        
        CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
        manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
        manager.leftBtnBGColor= [UIColor whiteColor];
        manager.rightBtnBGColor= [UIColor whiteColor];
        manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
        manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
        manager.leftBtnborderColor= [UIColor whiteColor];
        manager.rightBtnborderColor= [UIColor whiteColor];
        NSArray * array = @[@"投诉",@"建议",@"其它问题",@"游戏问题",@"优惠活动",@"存取款问题"];
        NSArray *dataArray = @[@"-1102",@"-1101",@"-1010",@"-1009",@"-1007",@"-1004"];
        WeakSelf
        [CGXPickerView showStringPickerWithTitle:@"消息类型" DataSource:array DefaultSelValue:weakSelf.selectValue IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
            weakSelf.selectValue = selectValue;
           
            weakSelf.selectindex = dataArray[[selectRow integerValue]];
            [weakSelf.openButton setTitle:weakSelf.selectValue forState:0];
            
        }];
        
    }];
    _titleField.backgroundColor =_codeField.backgroundColor = _bgView.backgroundColor = RGBA(241, 241, 241, 1);
    
    KViewRadius(_titleField, 2);
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
    _titleField.leftView =view;
    _titleField.leftViewMode = UITextFieldViewModeAlways;
    _titleField.textColor = RGBA(61, 61, 61, 1);
    
    [_bgView addSubview:self.textView];
    
    UIView * codeview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
    _codeField.leftView =codeview;
    _codeField.leftViewMode = UITextFieldViewModeAlways;
    _codeField.textColor = RGBA(61, 61, 61, 1);
    
    KViewRadius(_sendMessageButton, 2);
    WeakSelf
    [_sendMessageButton add_BtnClickHandler:^(NSInteger tag) { //发送消息
        
        if (kStringIsEmpty(weakSelf.selectindex)) {
            [WJUnit showMessage:@"请选择消息类型"];
            return ;
        }
        if (kStringIsEmpty(weakSelf.titleField.text)) {
            [WJUnit showMessage:@"请输入标题"];
            return ;
        }
        if (kStringIsEmpty(weakSelf.textView.text)) {
            [WJUnit showMessage:@"请输入内容"];
            return ;
        }
        if (kStringIsEmpty(weakSelf.textView.text)) {
            [WJUnit showMessage:@"请输入随机码"];
            return ;
        }
        [self chack_ImageCodeRequest];
       
       
        
        
        
    }];

}
- (void)sendMessage
{
    WeakSelf
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"sendSystemMsg"];
    NSDictionary * dic = @{@"admin_id":weakSelf.selectindex,@"title":weakSelf.titleField.text,@"code":weakSelf.codeField.text,@"content":weakSelf. textView.text,@"cpkey":self.imageKey};
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:dic origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        [self stopLoadingAnimation];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:2 andImageName:@"success" andTitleStr:@"发送成功" andlightStr:@""];
            [toast showLXAlertView];
            [weakSelf.navigationController popViewControllerAnimated:YES];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
    }];
}
- (UITextView *)textView
{
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 15,_bgView.width-40 , _bgView.height -30)];
        [_textView add_PlaceHolder:@"请输入内容"];
        _textView.backgroundColor = RGBA(241, 241, 241, 1);
    }
    return _textView;
}
@end

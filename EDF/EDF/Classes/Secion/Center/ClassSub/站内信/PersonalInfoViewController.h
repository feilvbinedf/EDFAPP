//
//  PersonalInfoViewController.h
//  EDF
//
//  Created by p on 2019/7/15.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "MessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonalInfoViewController : EDBaseViewController
@property (nonatomic,strong)PersonaModel * model;
@end

NS_ASSUME_NONNULL_END

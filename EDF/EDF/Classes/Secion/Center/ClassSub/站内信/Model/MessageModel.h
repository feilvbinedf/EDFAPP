//
//  MessageModel.h
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageModel : NSObject
//名字
@property (nonatomic,strong) NSString *content_id;
@property (nonatomic,strong) NSString *revertible;
@property (nonatomic,strong) NSString *imageData;
@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *create_date;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *isRead;
@property (nonatomic,strong) NSString *zan;
@property (nonatomic,strong) NSString *already_praise;
@property (nonatomic,strong) NSString *already_read;
@property (nonatomic,assign)  CGFloat  cellHeight;
@property (nonatomic,strong) NSMutableAttributedString *mucontent;
@property (nonatomic,assign) BOOL isOpen;
@end
@interface PersonaModel : NSObject
//名字

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *create_date;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,assign) BOOL isOpen;

@end

@interface CatModel : NSObject
//名字

@property (nonatomic,strong) NSString *admin_name;
@property (nonatomic,strong) NSString *create_date;
@property (nonatomic,strong) NSString *type; // a2u  最后一条是最新消息显示 u2a是我自己
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,assign) CGFloat cellHeight;

@end

NS_ASSUME_NONNULL_END

//
//  NoticeMessageVC.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "NoticeMessageVC.h"
#import "PersonaCell.h"
#import "MessageModel.h"
#import "SystemTableHeadView.h"
#import "NSString+handle.h"
#define headViewHeight RealValue_H(130)
@interface NoticeMessageVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,strong) SystemTableHeadView * tableviewHeadView;

@end

@implementation NoticeMessageVC
-(void)setIndex:(NSInteger)index{
    _index = index;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    [self.view addSubview:self.tableView];
    _index =10086;
    // 上拉加载
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self getData_Refush];
    }];
    [header beginRefreshing];
    self.tableView.mj_header = header;
}
#pragma mark----Action数据
//获取个人消息
-(void)getData_Refush
{
    [self showLoadingAnimation];
    WeakSelf
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"getNotice"];
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        [self stopLoadingAnimation];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * dic = [self objectFromJSONString:responseObject];
            weakSelf.dataArray  = [PersonaModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"date"]];
            [self placeholderViewWithFrame:CGRectMake(0, 100, UIScreenWidth, weakSelf.tableView.height -100) NoNetwork:NO];
            [weakSelf.tableView reloadData];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
         [self.tableView.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
         [self.tableView.mj_header endRefreshing];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}

-(void)setIs_HomeMessage:(BOOL)is_HomeMessage{
    _is_HomeMessage = is_HomeMessage;
    if (is_HomeMessage==YES) {
        _tableView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight);
    }else{
        _tableView.frame =CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight -  RealValue_H(100));
    }
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        if (self.is_HomeMessage ==YES) {
                _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight)style:UITableViewStyleGrouped];
        }else{
                _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight -  RealValue_H(100))style:UITableViewStyleGrouped];
        }
    
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = RGBA(210, 210, 210, 1);
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = WHITECOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        
        [_tableView registerNib:[UINib nibWithNibName:@"PersonaCell" bundle:nil]  forCellReuseIdentifier:@"PersonaCell"];

    }
    return _tableView;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     PersonaModel * model = _dataArray[indexPath.row];
    if (indexPath.row ==_index)
    {
        if (kStringIsEmpty(model.content))
        {
            return 64;
        }else
        {
            CGSize attSize = [model.content boundingRectWithSize:CGSizeMake(UIScreenWidth -30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:AutoFont(12)} context:nil].size;
            
            return 64 +attSize.height +10;
            
        }
    }
   
   
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PersonaCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PersonaCell" forIndexPath:indexPath];
    PersonaModel * model = _dataArray[indexPath.row];
    cell.model = model;
    cell.img_w.constant = 8;
    cell.img_h.constant =14;
    cell.messageIcon.image = [UIImage imageNamed:@"notice"];
    CGSize attSize = [model.content boundingRectWithSize:CGSizeMake(UIScreenWidth -30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:AutoFont(12)} context:nil].size;
    cell.content.height = attSize.height ;
    cell.content.text = model.content;
    if (_index ==indexPath.row) {
       cell.content.hidden = NO;
        cell.openBtn.selected = YES;
    }else
    {
        cell.content.hidden = YES;
        cell.openBtn.selected = NO;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _index = indexPath.row;
    [self.tableView reloadData];
}


@end

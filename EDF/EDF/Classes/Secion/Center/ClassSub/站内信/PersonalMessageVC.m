//
//  PersonalMessageVC.m
//  EDF
//
//  Created by p on 2019/7/11.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PersonalMessageVC.h"
#import "PersonaCell.h"
#import "MessageModel.h"
#import "SystemTableHeadView.h"
#import "PersonalInfoViewController.h"
#define headViewHeight RealValue_H(130)
@interface PersonalMessageVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic,strong) NoNetworkView * workView;
@property (nonatomic,strong) SystemTableHeadView * tableviewHeadView;
@property (nonatomic,strong) MJRefreshNormalHeader *header;
@end
@implementation PersonalMessageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    [self.view addSubview:self.tableView];
    // 上拉加载
    _header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self getData_Refush];
    }];
    [_header beginRefreshing];
    self.tableView.mj_header = _header;
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Personal) name:PersonalNotificationse object:nil];
}
- (void)Personal
{
    [_header beginRefreshing];
}
#pragma mark----Action数据
//获取个人消息
-(void)getData_Refush
{
    WeakSelf
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"getUserInfoMsg"];
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
       
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * dic = [self objectFromJSONString:responseObject];
            weakSelf.dataArray  = [PersonaModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"date"]];
            [self placeholderViewWithFrame:CGRectMake(0, 100, UIScreenWidth, weakSelf.tableView.height -100) NoNetwork:NO];
            [weakSelf.tableView reloadData];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
         [self.tableView.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
       
        [self.tableView.mj_header endRefreshing];
        [WJUnit showMessage:@"请求错误"];
    }];
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.dataArray.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        
        [_tableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}
#pragma  mark -----懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenHeight -kStatusBarAndNavigationBarHeight -  RealValue_H(100))style:UITableViewStyleGrouped];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorColor = RGBA(210, 210, 210, 1);
        _tableView.delegate = self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = WHITECOLOR;
        _tableView.showsVerticalScrollIndicator = NO;
        
        [_tableView registerNib:[UINib nibWithNibName:@"PersonaCell" bundle:nil]  forCellReuseIdentifier:@"PersonaCell"];
//        _tableView.tableHeaderView =self.tableviewHeadView;
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 64;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PersonaCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PersonaCell" forIndexPath:indexPath];
    PersonaModel * model = _dataArray[indexPath.row];
    cell.openBtn.hidden = YES;
    cell.ns_x.constant = -60;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; //显示最右边的箭头
    cell.model = model;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PersonalInfoViewController * PersonalInfoVC = [[PersonalInfoViewController alloc] init];
    PersonaModel * model = _dataArray[indexPath.row];
    PersonalInfoVC.model = model;
    PersonalInfoVC.title = @"个人消息详情";
    [self.navigationController pushViewController:PersonalInfoVC animated:YES];
}
#pragma mark ---- 侧滑删除
// 点击了“左滑出现的Delete按钮”会调用这个方法
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self queyDeletePath:indexPath];
    
}

//定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
}

// 修改Delete按钮文字为“删除”
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

//先要设Cell可编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
     return YES;
}

//设置进入编辑状态时，Cell不会缩进
- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark --- 删除数据的网络请求
- (void)queyDeletePath:(NSIndexPath *)indexPath{
     PersonaModel * model = _dataArray[indexPath.row];
    [self showLoadingAnimation];
    WeakSelf
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"deleteUserInfoMsg"];
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:@{@"id":model.id} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        [self stopLoadingAnimation];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:2 andImageName:@"success" andTitleStr:@"删除成功" andlightStr:@""];
            [toast showLXAlertView];
            // 删除模型
            [weakSelf.dataArray removeObjectAtIndex:indexPath.row];
            
            // 刷新
            [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
        
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
    }];
   
    
    
}

@end

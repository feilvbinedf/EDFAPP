//
//  EDCashCardListsViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/25.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDCashCardListsViewController.h"
#import "EDCashCardListTableViewCell.h"
#import "CJItemWelfareModel.h"

@interface EDCashCardListsViewController ()
@property (nonatomic,strong) NoNetworkView * workView;
@end

@implementation EDCashCardListsViewController
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
      self.ks_TableView.frame = CGRectMake(0, 10, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-60);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self creatUI];
   // [self ks_tableAddFootRequst];
    [self ks_tableAddHeaderRequstRefush];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userCouponActionget) name:@"User_welfareGetSingCurrency" object:nil];
      
}
-(void)userCouponActionget{
    
     [self.ks_TableView.mj_header beginRefreshing];
    
    
}
-(void)creatUI{
    
    self.modelsArry = [NSMutableArray array];
    self.view.backgroundColor =kRGBColor(245, 245, 245);
    self.ks_TableView.frame = CGRectMake(0, 10, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-60);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];


}
-(void)getData_Refush{

    [self refushData:YES];

    
}
-(void)refushData:(BOOL)isRefush{

    //userCouponActiongetUserCouponList
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"userCouponActiongetUserCouponList"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        
        if (isRefush ==YES) {
            [strongSelf.modelsArry removeAllObjects];
        }
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *  dataArr = [dict001 jsonArray:@"data"];
         NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
         if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
             
             for (NSDictionary * modelDicc in dataArr) {
                     CJItemWelfareModel * modell = [[CJItemWelfareModel alloc]initWithDictionary:modelDicc error:nil];
                     [strongSelf.modelsArry addObject:modell];
                 }
         }else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                          ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                          [toast showLXAlertView];
                      });
             
         }
    
        dispatch_async(dispatch_get_main_queue(), ^{
                  [self placeholderViewWithFrame:self.ks_TableView.frame NoNetwork:NO];
        });
  
        
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [strongSelf.ks_TableView reloadData];
        NSLog(@"userCouponActiongetUserCouponList---%@",dataArr);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        //  [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDCashCardListTableViewCell";
    EDCashCardListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDCashCardListTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDCashCardListTableViewCell" owner:self options:nil] lastObject];
    }
     CJItemWelfareModel * modell = self.modelsArry[indexPath.row];
    cell.itemModel = modell;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}
- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.modelsArry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        [self.ks_TableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

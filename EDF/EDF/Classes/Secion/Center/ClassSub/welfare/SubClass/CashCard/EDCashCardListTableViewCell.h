//
//  EDCashCardListTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJItemWelfareModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDCashCardListTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *titleLab;


@property (weak, nonatomic) IBOutlet UILabel *subTitle;

@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;







@property(nonatomic,strong)CJItemWelfareModel * itemModel;
@end

NS_ASSUME_NONNULL_END

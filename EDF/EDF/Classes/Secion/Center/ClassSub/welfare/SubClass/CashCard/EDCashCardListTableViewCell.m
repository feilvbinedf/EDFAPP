//
//  EDCashCardListTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDCashCardListTableViewCell.h"

@implementation EDCashCardListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemModel:(CJItemWelfareModel *)itemModel{
  _itemModel = itemModel;
    
    self.titleLab.text = itemModel.name;
    NSString * time = [itemModel.create_date stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    self.subTitle.text =  [NSString stringWithFormat:@"派发日期:%@",time];
    self.timeLab.hidden = YES;
    
    if ([itemModel.status isEqualToString:@"0"]) {
        self.timeLab.hidden = NO;
        self.typeLab.text = @"未使用";
        self.typeLab.textColor = RGBA(44, 143, 219, 1);
        self.timeLab.text =itemModel.expire_txt;
    }else if ([itemModel.status isEqualToString:@"1"]){
        self.typeLab.text = @"已使用";
        self.typeLab.textColor = RGBA(90, 90, 90, 1);
        
    }else if ([itemModel.status isEqualToString:@"2"]){
        self.typeLab.text = @"已过期";
        self.typeLab.textColor = RGBA(172, 172, 172, 1);
        
    }else{
        self.typeLab.text = @"";
        
    }
    
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

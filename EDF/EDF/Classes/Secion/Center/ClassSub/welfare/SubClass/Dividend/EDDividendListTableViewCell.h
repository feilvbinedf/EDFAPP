//
//  EDDividendListTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJItemWelfareModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDDividendListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLab;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftC;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property(nonatomic,assign)NSInteger  showType;
@property(nonatomic,strong)CJItemWelfareModel * itemModel;
@property (weak, nonatomic) IBOutlet UIImageView *tishiImage;


@end

NS_ASSUME_NONNULL_END

//
//  EDDividendListTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDDividendListTableViewCell.h"

@implementation EDDividendListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.tishiImage.hidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setItemModel:(CJItemWelfareModel *)itemModel{
    _itemModel = itemModel;
    if (_showType==1) {
        self.nameLab.text = itemModel.detail;
        self.numLab.text = [NSString stringWithFormat:@"%.0f",[itemModel.bonus floatValue]];
        NSString * time = [itemModel.create_date stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        self.timeLab.text =  [NSString stringWithFormat:@"派发日期:%@",time];
    }else{

        self.nameLab.text = [NSString stringWithFormat:@"%@  %.0f",itemModel.name,[itemModel.bonus floatValue]];
        //[itemModel.create_date stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSString * time = [itemModel.create_date stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        //Aug 1, 2019 12:10:14 AM
        fmt.dateFormat = @"MMM d, yyyy HH:mm:ss a";
        fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [fmt setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate * date = [fmt dateFromString:time];
        NSString * time22 = [date.description substringToIndex:19];
              self.timeLab.text =  [NSString stringWithFormat:@"派发日期:%@",time22];

    }
   
    
    if (kStringIsEmpty(itemModel.coupon_id)) {
       self.tishiImage.hidden = YES;
    }else{
        self.tishiImage.hidden = NO;
    }
    
    
    
    
}
-(void)setShowType:(NSInteger)showType{
    _showType = showType;
    
    if (showType==1) {
        //发币
        self.icon.hidden = NO;
        self.leftC.constant = 60;
        self.timeLab.hidden = NO;
        self.numLab.hidden = NO;
    }else{
        self.icon.hidden = NO;
        self.leftC.constant = 60;
        self.timeLab.hidden = NO;
        self.numLab.hidden = YES;
        
    }
}

@end

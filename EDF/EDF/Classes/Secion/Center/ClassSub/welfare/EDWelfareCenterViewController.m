//
//  EDWelfareCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDWelfareCenterViewController.h"
#import "EDDividendListViewController.h"
#import "XXPageTabView.h"
#import "EDCashCardListsViewController.h"

@interface EDWelfareCenterViewController ()
<XXPageTabViewDelegate>

{
    XXPageTabView *_pageTabView;
}
@property(nonatomic,strong)EDDividendListViewController * vc001;
@property(nonatomic,strong)EDDividendListViewController * vc002;
@property(nonatomic,strong)EDCashCardListsViewController * vc003;
@end


@implementation EDWelfareCenterViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatUI];
    [self setupSegmentType_0View];
    
}

-(void)creatUI{
    self.view.backgroundColor =kRGBColor(245, 245, 245);

}


- (void)setupSegmentType_0View {
    
    self.vc001 = [EDDividendListViewController new];
    self.vc001.showType =0;
    self.vc002 = [EDDividendListViewController new];
    self.vc002.showType =1;
    self.vc003 = [EDCashCardListsViewController new];
    
    [self addChildViewController:self.vc001];
    [self addChildViewController:self.vc002];
    [self addChildViewController:self.vc003];
    
    _pageTabView = [[XXPageTabView alloc] initWithChildControllers:self.childViewControllers childTitles:@[@"红利", @"发币",@"优惠卡"]];
    _pageTabView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    _pageTabView.tabSize = CGSizeMake(kScreenWidth, 50);
    _pageTabView.tabItemFont = AppFont(15);
    _pageTabView.unSelectedColor = kRGBColor(161, 161, 161);
    _pageTabView.selectedColor = kRGBColor(44, 143, 219);
    _pageTabView.bodyBounces = NO;
    _pageTabView.tabBackgroundColor = [UIColor whiteColor];
    _pageTabView.titleStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.indicatorStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.delegate = self;
    _pageTabView.indicatorWidth = 27;
    _pageTabView.indicatorHeight = 3;
    _pageTabView.backgroundColor = [UIColor whiteColor];
    _pageTabView.bodyBackgroundColor= [UIColor whiteColor];
    [self.view addSubview:_pageTabView];
    
    
}
#pragma mark - XXPageTabViewDelegate
- (void)pageTabViewDidEndChange {
    NSInteger selectedTabIndex = _pageTabView.selectedTabIndex;
    
    
    
    
    
}


@end

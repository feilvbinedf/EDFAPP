//
//  CJItemWelfareModel.h
//  DJ
//
//  Created by 微笑吧阳光 on 2019/6/12.
//  Copyright © 2019 DJ. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CJItemWelfareModel : EDBaseModel

@property(nonatomic,copy)NSString<Optional> * account_gameplatform;
@property(nonatomic,copy)NSString<Optional> * account_id;

@property(nonatomic,copy)NSString<Optional> * admin_id;

@property(nonatomic,copy)NSString<Optional> * allow_lottery;
@property(nonatomic,copy)NSString<Optional> * audit_admin;

@property(nonatomic,copy)NSString<Optional> * audit_date;

@property(nonatomic,copy)NSString<Optional> * big_type;
@property(nonatomic,copy)NSString<Optional> * create_date;
@property(nonatomic,copy)NSString<Optional> * create_date2;
@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * name;
@property(nonatomic,copy)NSString<Optional> * obj_id;
@property(nonatomic,copy)NSString<Optional> * type;
@property(nonatomic,copy)NSString<Optional> * update_date;
@property(nonatomic,copy)NSString<Optional> * user_id;
@property(nonatomic,copy)NSString<Optional> * withdrawlimit;

@property(nonatomic,strong)NSNumber *  bonus;
@property(nonatomic,strong)NSNumber *  flowtimes;
@property(nonatomic,strong)NSNumber *  promotion_id;
@property(nonatomic,strong)NSNumber *  rebateable;
@property(nonatomic,copy)NSString *  status;
@property(nonatomic,strong)NSNumber *  zeroline;


@property(nonatomic,copy)NSString<Optional> * detail;
@property(nonatomic,strong)NSNumber *  amount;
@property(nonatomic,strong)NSNumber *  extra_amount;

@property(nonatomic,strong)NSNumber *  valid_hour;

@property(nonatomic,strong)NSNumber *  multiple;

@property(nonatomic,copy)NSString<Optional> * invalid_date;
@property(nonatomic,copy)NSString<Optional> * expire_txt;

@property(nonatomic,strong)NSNumber *  amount_limit;
@property(nonatomic,copy)NSString<Optional> * coupon_id;

//
//{
//    "statusCode": "SUCCESS",
//    "message": "请求成功",
//    "data": [{
//        "valid_hour": "48",
//        "obj_id": "",
//        "multiple": "2",
//        "promotion_id": "oScsb_cpS8GnqvmttEkGCA_sub",
//        "update_date": "",
//        "invalid_date": "2019-12-25 23: 03: 21",
//        "expire_txt": "26小时后过期",
//        "user_id": "yAeXkl3hQxKY1EWN-9ViKA",
//        "amount_limit": "888",
//        "name": "双倍千倍百倍卡",
//        "id": "wZoBHnZtTn6t-lQYae3NTw",
//        "create_date": "2019-12-23 23: 03: 21",
//        "status": "0"
//    }]
//}


//{
//    "account_gameplatform" = rb;
//    "account_id" = "";
//    "admin_id" = "WvyZr1qsRK-HPsw4-vIbwQ";
//    "allow_lottery" = "";
//    "audit_admin" = "";
//    "audit_date" = "";
//    "big_type" = "";
//    bonus = 88;
//    "create_date" = "2019-06-10%2021:56:05";
//    flowtimes = 1;
//    id = Oj0BfPFhQJuc2KHPTyo0kg;
//    name = "\U6d4b\U8bd5\U91d1";
//    "obj_id" = "";
//    "promotion_id" = 1;
//    rebateable = 0;
//    status = "\U672a\U9886\U53d6";
//    type = "\U7cfb\U7edf\U6d3e\U9001";
//    "update_date" = "";
//    "user_id" = w3x1TSShQoahbu42PwB0Sg;
//    withdrawlimit = "";
//    zeroline = 0;
//}



@end

NS_ASSUME_NONNULL_END

//
//  EDChangeWithdrawalsCodeViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDChangeWithdrawalsCodeViewController : EDBaseViewController

@property(nonatomic,assign)NSInteger  showType;
@property(nonatomic,copy)NSString * phone;
@property(nonatomic,copy)NSString * codeStr;

@property (nonatomic, copy) void (^dismiss)(BOOL isSend);
@end

NS_ASSUME_NONNULL_END

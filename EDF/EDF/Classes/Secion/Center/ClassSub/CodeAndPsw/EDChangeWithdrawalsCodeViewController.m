//
//  EDChangeWithdrawalsCodeViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDChangeWithdrawalsCodeViewController.h"
#import "EDInfoSettingView.h"
#import "BangDingPhoneInfoView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "RestrictionInput.h"
#import "UIButton+CountDown.h"

@interface EDChangeWithdrawalsCodeViewController ()
@property(nonatomic,strong)BangDingPhoneInfoView * infoView;
@property(nonatomic,strong)TPKeyboardAvoidingScrollView * bgSC;
@property(nonatomic,strong) NSData  * imageDataStr;
@property(nonatomic,copy)    NSString  * imageKey;
@end

@implementation EDChangeWithdrawalsCodeViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.bgSC.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self creat_UI];
    if (self.showType!=10086) {
        [self requestImageCodeData];
        [self  refushMyInfoData];
    }
    
 
}

-(void)refushMyInfoData{
    //myUserNavigationDatd
    //  [self showLoadingAnimation];
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [[DJLoginHelper sharedInstance]save_UserInfo:[dict jsonDict:@"data"]];
            NSLog(@"User_getUserInfos---%@",dict);
           [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [self creat_UI];
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }

        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [self stopLoadingAnimation];
//        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
        [self refushMyInfoData];
    }];
    
}
-(void)creat_UI{
    
    self.bgSC = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight)];
    self.bgSC.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSC];

    EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView.enterTF.placeholder = @"请输入密码";
            infoView.enterTF.tag = 10010;
    if (self.showType==0) {
        infoView.enterTF.keyboardType = UIKeyboardTypeNumberPad;
    }else{
        infoView.enterTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
            infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
            [ infoView.enterTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [self.bgSC addSubview:infoView];
            
            
    EDInfoSettingView * infoView2 = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView2.enterTF.placeholder = @"确认密码";
            infoView2.enterTF.tag = 10011;
    if (self.showType==0) {
        infoView2.enterTF.keyboardType = UIKeyboardTypeNumberPad;
    }else{
        infoView2.enterTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
            infoView2.frame =CGRectMake(0, infoView.bottom, kScreenWidth, 70);
            [ infoView2.enterTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
  [self.bgSC addSubview:infoView2];
    
    if (self.showType==10086) {
        UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, infoView2.bottom+30, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick22:) title:@"保存设置"];
        [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
        [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
        [self.view addSubview:bottomBtn];
        
    }else{
        
        NSString  * phoneStr = [[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"];
        if (!kStringIsEmpty(phoneStr) && ![phoneStr isEqualToString:@"false"]) {
            phoneStr = [self changeTelephone:phoneStr];
        }
        UILabel  * tishiLabb = [BBControl createLabelWithFrame:CGRectMake(10,infoView2.bottom , kScreenWidth-20, 40) Font:14 Text:[NSString stringWithFormat:@"请先验证您当前绑定的手机号:%@",phoneStr]];
        tishiLabb.textAlignment = NSTextAlignmentCenter;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"请先验证您当前绑定的手机号:%@",phoneStr] attributes:@{
                                                                                                                                                                              NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14.0f],
                                                                                                                                                                              NSForegroundColorAttributeName: kRGBColor(90, 90, 90),
                                                                                                                                                                              NSKernAttributeName: @(0.0)
                                                                                                                                                                              }];
        [attributedString addAttribute:NSForegroundColorAttributeName value:EDColor_BaseBlue range:NSMakeRange(14, phoneStr.length)];
        tishiLabb.attributedText = attributedString;
        [self.bgSC addSubview:tishiLabb];
        
        self.infoView= [[NSBundle mainBundle]loadNibNamed:@"BangDingPhoneInfoView" owner:self options:nil].lastObject;
        self.infoView.frame =CGRectMake(0, tishiLabb.bottom, kScreenWidth, 300);
        self.infoView.fristHC.constant = 0;
        self.infoView.bottomHtop.constant =30;
        self.infoView.lab1.hidden = YES;
        self.infoView.lab2.hidden = YES;
        [self.bgSC addSubview:self.infoView];
        
        WEAKSELF
        [self.infoView.sureBtn add_BtnClickHandler:^(NSInteger tag) {
            STRONGSELFFor(weakSelf);
            [strongSelf sureBtnClick:strongSelf.infoView.sureBtn];
        }];
        
        [self.infoView.codeActionBtn add_BtnClickHandler:^(NSInteger tag) {
            STRONGSELFFor(weakSelf);
            [strongSelf phoneCodeBtnClick:strongSelf.infoView.codeActionBtn];
        }];
        
        [self.infoView.codeRefushBtn add_BtnClickHandler:^(NSInteger tag) {
            STRONGSELFFor(weakSelf);
            [strongSelf requestImageCodeData];
        }];
        
        
    }
    
            
            
//
//            UIView * bgView = [BBControl createViewWithFrame:CGRectMake(0, infoView.bottom, kScreenWidth, 50)];
//            bgView.backgroundColor= [UIColor whiteColor];
//            [self.view addSubview:bgView];

}
-(void)sureBtnClick22:(UIButton *)sender{
    [self.view endEditing:YES];
    
    UITextField * enterText = [self.view viewWithTag:10010];
    UITextField * enterText2 = [self.view viewWithTag:10011];
    
    if (kStringIsEmpty(enterText.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入密码"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    if ([self isBlank:enterText.text]==YES || [enterText.text containsString:@"."]) {
        [JMNotifyView showNotify:@"密码不能包含空格和小数点" isSuccessful:NO];
        return;
    }
    
    if ([self judgePassWordLegal:enterText.text] ==NO ) {
        [JMNotifyView showNotify:@"密码长度为6~16个字符，不能含有空格或小数点，不能是9位以下纯数字" isSuccessful:NO];
        return;
    }
    
    if (![enterText.text isEqualToString:enterText2.text]) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"两次密码输入不一致"] andlightStr:@""];
        [toast showLXAlertView];
        return;
        
    }
  
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_updateUserLoginPassWrod"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.codeStr forKey:@"telCode"];
    [dicc setValue:self.phone forKey:@"tel"];
    [dicc setValue:enterText.text forKey:@"newPassWord"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"修改密码成功"] andlightStr:@""];
                [toast showLXAlertView];
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    
}
-(void)requestImageCodeData{
    // LogincOnLoadOCode
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"LogincOnLoadOCode"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET_Data :url parameters:nil origin:YES success:^(NSInteger statusCode,NSURLSessionDataTask *  task,id responseObject) {
        STRONGSELFFor(weakSelf)
        YYLog(@"responseObject----%@",responseObject);
        strongSelf.imageDataStr = [NSData dataWithData:responseObject];
        NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse*)task.response;
        NSDictionary *allHeaderFieldsDict = httpURLResponse.allHeaderFields;
        NSLog(@"allHeaderFieldsDict---%@",allHeaderFieldsDict);
        strongSelf.imageKey = [allHeaderFieldsDict jsonString:@"cpkey"];
        strongSelf.infoView.codeImage.image =  [UIImage imageWithData:strongSelf.imageDataStr];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
    
}


-(void)sureBtnClick:(UIButton *)sender{
    [self.view endEditing:YES];
        UITextField * enterText = [self.view viewWithTag:10010];
         UITextField * enterText2 = [self.view viewWithTag:10011];
    
    
    if (self.showType==0) {
        if (kStringIsEmpty(enterText.text)) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入提款密码"] andlightStr:@""];
            [toast showLXAlertView];
            return;
        }
        
        if ([self isNum:enterText.text]==NO) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"提款密码只能为6位数字"] andlightStr:@""];
            [toast showLXAlertView];
            return;
        }
        
        if (![enterText.text isEqualToString:enterText2.text]) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"两次密码输入不一致"] andlightStr:@""];
            [toast showLXAlertView];
            return;
            
        }
        [self cheack_RomCode];
 
    }else{
        //修改登陆密码
        if (kStringIsEmpty(enterText.text)) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入密码"] andlightStr:@""];
            [toast showLXAlertView];
            return;
        }
        if ([self isBlank:enterText.text]==YES || [enterText.text containsString:@"."]) {
            [JMNotifyView showNotify:@"密码不能包含空格和小数点" isSuccessful:NO];
            return;
        }
        
        if ([self judgePassWordLegal:enterText.text] ==NO ) {
            [JMNotifyView showNotify:@"密码长度为6~16个字符，不能含有空格或小数点，不能是9位以下纯数字" isSuccessful:NO];
            return;
        }
        
        if (![enterText.text isEqualToString:enterText2.text]) {
            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"两次密码输入不一致"] andlightStr:@""];
            [toast showLXAlertView];
            return;
            
        }
        
        [self cheack_RomCode];

    }

}
-(void)cheack_RomCode{
    
    if (kStringIsEmpty(self.infoView.codeImageTF.text)) {
        [JMNotifyView showNotify:@"请输入随机图形码" isSuccessful:NO];
        return;
    }
    
    [self cheack_PhoneCode];
    
}
-(void)cheack_PhoneCode{
    
    if (kStringIsEmpty(self.infoView.codeNumTF.text)) {
        [JMNotifyView showNotify:@"请输入手机验证码" isSuccessful:NO];
        return;
    }
    
    [self chack_ImageCodeRequest];
    
}
-(void)chack_ImageCodeRequest{
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Code_verificationRandomCode"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.infoView.codeImageTF.text forKey:@"code"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET_DataDChackImageCode:url imageKey:self.imageKey parameters:dicc origin:YES success:^(NSInteger statusCode, NSURLSessionDataTask *task, id responseObject) {
        STRONGSELFFor(weakSelf);
        // NSLog(@"LoginccCheckImageCodeAction----%@",responseObject);
        NSString* strdata = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData * data =[[strdata URLDecodedString] dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            if (strongSelf.showType==0) {
                YYLog(@"--校验完毕-，可以去设置新的密码---SUCCESS");
                [strongSelf bangding_TiKuanCodeRequest];
            }else{
                   [strongSelf updatePassWordRequest];
            }

        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        [JMNotifyView showNotify:@"校验图形验证码，网络错误，请重试~" isSuccessful:NO];
        [self stopLoadingAnimation];
    }];
    
}
-(void)updatePassWordRequest{
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_updateUserLoginPassWrod"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.infoView.codeNumTF.text forKey:@"telCode"];
    NSString  * phoneStr = [[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"];
    [dicc setValue:phoneStr forKey:@"tel"];
    UITextField * enterText = [self.view viewWithTag:10010];
    [dicc setValue:enterText.text forKey:@"newPassWord"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"修改密码成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDChangeWithdrawalsCodeViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
}
-(void)bangding_TiKuanCodeRequest{
   // userCardMobileAction!setUserCode.action
    //User_setUserCode
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_setUserCode"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.infoView.codeNumTF.text forKey:@"telCode"];
    NSString  * phoneStr = [[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"];
    [dicc setValue:phoneStr forKey:@"tel"];
    UITextField * enterText = [self.view viewWithTag:10010];
    UITextField * enterText2 = [self.view viewWithTag:10011];
      [dicc setValue:enterText.text forKey:@"code"];
      [dicc setValue:enterText2.text forKey:@"repeatCode"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"设置提款密码成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDChangeWithdrawalsCodeViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

-(void)cheak_PhoneCode{
    // Code_verificationPhoneCode
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Code_verificationPhoneCode"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:self.infoView.codeNumTF.text forKey:@"code"];
    [dicc setValue:self.infoView.numTF.text forKey:@"tel"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
           
     
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}
-(void)phoneCodeBtnClick:(UIButton *)sender{

    if (kStringIsEmpty(self.infoView.codeImageTF.text)) {
        [JMNotifyView showNotify:@"请输入随机图形码" isSuccessful:NO];
        return;
    }
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"UserCode_SendMsgMobileAction"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    NSString  * phoneStr = [[DJLoginHelper sharedInstance].cj_UserDicc jsonString:@"phone"];
    [dicc setValue:phoneStr forKey:@"tel"];
    [dicc setValue:self.infoView.codeImageTF.text forKey:@"code"];
    [dicc setValue:self.imageKey forKey:@"cpkey"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf)
        [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender scheduledTimerWithTimeInterval:60.0f title:@"重新获取" countDownTitle:@"s" titleBackgroundColor:EDColor_BaseBlue countDownTitleBackgroundColor:EDColor_BaseBlue];
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"手机验证码已发送,注意查收"] andlightStr:@""];
                [toast showLXAlertView];
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
           [self stopLoadingAnimation];
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

-(void)textFieldDidChange:(UITextField *)textField {
    
    if (self.showType==0) {
            [RestrictionInput restrictionInputTextField:textField maxNumber:6 showView:self.view showErrorMessage:@""];
    }else{
            [RestrictionInput restrictionInputTextField:textField maxNumber:16 showView:self.view showErrorMessage:@""];
        
    }
    
}

/**
 * 判断字符串是否包含空格
 */
-(BOOL)isBlank:(NSString *)str{
    NSRange _range = [str rangeOfString:@" "];
    if (_range.location != NSNotFound) {
        //有空格
        // NSLog(@"有空格");
        return YES;
    }else {
        //没有空格
        // NSLog(@"没有空格");
        return NO;
    }
}
- (BOOL)isNum:(NSString *)checkedNumString {
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}
- (BOOL)judgePassWordLegal:(NSString *)pass{
    BOOL result = false;
    if ([pass length] >= PassworldLastlength){
        // 判断长度大于6位后再接着判断是否同时包含数字和字符
        NSString * regex =@"^(?=.*[a-zA-Z])(.{6,16})$";
        //@"^(?=.*[0-9])(?=.*[a-zA-Z])(.{6,16})$";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        result = [pred evaluateWithObject:pass];
        if (result ==NO) {
            return NO;
        }
    }
    NSRange range = [pass rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }else {
        
        NSRange range = [pass rangeOfString:@"."];
        if (range.location != NSNotFound) {
            return NO; //yes
        }else {
            
            return YES; //反之
        }
    }
    return result;
    
}

-(NSString*)changeTelephone:(NSString*)teleStr{

        NSString *string=[teleStr stringByReplacingOccurrencesOfString:[teleStr substringWithRange:NSMakeRange(3,4)]withString:@"****"];
 return string;

}


@end

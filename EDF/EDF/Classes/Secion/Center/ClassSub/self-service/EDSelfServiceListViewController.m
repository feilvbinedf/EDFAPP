//
//  EDSelfServiceListViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/25.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDSelfServiceListViewController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "ExchangeLevelCollectionViewCell.h"
#import "ServiceItemCollectionViewCell.h"
#import "LLWebViewController.h"
#import "EDSignInDetailViewController.h"
#import "EDExchangeCenterViewController.h"
#import "TGWebViewController.h"
#import "EDTodayGamesVC.h"
#import "TwoBtnsView.h"
#import "EDRebateViewController.h"
#import "EDLuckCashViewController.h"
#import "EDBirthdayLuckViewController.h"
#import "EDHundredTimesVC.h"
#import "EDTournamentViewController.h"
#import "EDRecommendFriendsViewController.h"
#import "EDTigerFitViewController.h"
#import "EDAdviceInfoViewController.h"
@interface EDSelfServiceListViewController ()
<UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout>
@property(nonatomic,strong)UIScrollView * bgSc;
@property (nonatomic, strong)  UICollectionView *collectionView;

@end

@implementation EDSelfServiceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self configData];
    [self User_getActivityAll];
    
}
-(void)configData{
    
    self.modelsArry = @[
                        @{@"name":@"每日签到",
                          @"icon_src":@"签到001"
                            },
                        @{@"name":@"每日游戏",
                          @"icon_src":@"形状001"
                          },
                        @{@"name":@"申请反水",
                          @"icon_src":@"形状002"
                          },
                        @{@"name":@"申请转运金",
                          @"icon_src":@"形状003"
                          },
                        @{@"name":@"发币兑换",
                          @"icon_src":@"形状004"
                          },
                        @{@"name":@"千倍百倍",
                          @"icon_src":@"形状005"
                          },
                        @{@"name":@"组队打虎",
                          @"icon_src":@"形状006"
                          },
                        @{@"name":@"个人锦标赛",
                          @"icon_src":@"形状007"
                          },
                        @{@"name":@"生日礼金",
                          @"icon_src":@"形状008"
                          },
                        @{@"name":@"推荐好友",
                          @"icon_src":@"形状009"
                          },
                        @{@"name":@"APP建议奖",
                          @"icon_src":@"形状0010"
                          },
                        ].mutableCopy;
//    @{@"name":@"推广任务",
//      @"icon_src":@"形状0010"
//      },
    [self.collectionView reloadData];
    
    
}
- (void)User_getActivityAll
{
    [self showLoadingAnimation];
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getActivityAll"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:@{@"type":@"特殊,常规"} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            YYLog(@"User_getActivityAll---%@",dict);
            NSArray * dataArry = [dict jsonArray:@"data"];
            for (NSDictionary * dataDicc in dataArry) {
                [strongSelf.modelsArry addObject:dataDicc];
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.collectionView reloadData];
    } failure:^(id responseObject, NSError *error) {
         [self stopLoadingAnimation];
        [WJUnit showMessage:@"获取活动请求失败"];
        //[self getBalance];
        
    }];
    

//    [[SmileHttpTool sharedInstance] GET :balance parameters:@{@"type":@"特殊"} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
//        [self stopLoadingAnimation];
//        STRONGSELFFor(weakSelf);
//        //NSLog(@"User_getActivityAll---%@",responseObject);
//        NSError *errorData;
//        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
//        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
//            YYLog(@"User_getActivityAll---%@",dict);
//            NSArray * dataArry = [dict jsonArray:@"data"];
//            for (NSDictionary * dataDicc in dataArry) {
//                [strongSelf.modelsArry addObject:dataDicc];
//            }
//        }else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
//                [toast showLXAlertView];
//            });
//        }
//        [strongSelf.collectionView reloadData];
//    } failure:^(id responseObject, NSError *error) {
//        [self stopLoadingAnimation];
//        [WJUnit showMessage:@"获取活动请求失败"];
//        //[self getBalance];
//        
//    }];
//    
    
}


-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    UILabel * toplab = [BBControl createLabelWithFrame:CGRectMake(0, 0, kScreenWidth, 50) Font:16 Text:@"EDF活动自助服务"];
    toplab.textAlignment = NSTextAlignmentCenter;
    toplab.backgroundColor = kRGBColor(245, 245, 245);
    toplab.textColor = EDColor_BaseBlue;
    [self.bgSc addSubview:toplab];
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.columnCount = 4;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, toplab.bottom, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-50) collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServiceItemCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ServiceItemCollectionViewCell"];
    
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.backgroundColor =kRGBColor(245, 245, 245);
    [self .bgSc addSubview:self.collectionView];
    

    
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.modelsArry.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ServiceItemCollectionViewCell *cell = (ServiceItemCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ServiceItemCollectionViewCell" forIndexPath:indexPath];
    NSDictionary * dataDicc = self.modelsArry[indexPath.item];
    if (indexPath.item<11) {
        [cell.iconImage setBackgroundImage:[UIImage imageNamed:[dataDicc jsonString:@"icon_src"]] forState:0];
        cell.namelab.text = [dataDicc jsonString:@"name"];
        
    }else{
        [cell.iconImage sd_setBackgroundImageWithURL:[NSURL URLWithString:[dataDicc jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
        cell.namelab.text =[dataDicc jsonString:@"name"];
        
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
        NSDictionary * dataDicc = self.modelsArry[indexPath.item];
    if (indexPath.item<12) {

        switch (indexPath.item) {
            case 0:
            {
                EDSignInDetailViewController * signVC = [EDSignInDetailViewController new];
                signVC.title = @"每日签到";
                [self.navigationController pushViewController:signVC animated:YES];
            }
                break;
            case 1:
            {
                EDTodayGamesVC * gameVC = [EDTodayGamesVC new];
                gameVC.title = @"今日特色游戏";
                [self.navigationController pushViewController:gameVC animated:YES];
            }
                break;
            case 2:
            {
                EDRebateViewController * gameVC = [EDRebateViewController new];
                gameVC.title = @"申请返水";
                [self.navigationController pushViewController:gameVC animated:YES];
            }
                break;
            case 3:
            {
                
                EDLuckCashViewController * gameVC = [EDLuckCashViewController new];
                gameVC.title = @"申请转运金";
                [self.navigationController pushViewController:gameVC animated:YES];
            }
                break;
            case 4:
            {
                EDExchangeCenterViewController * exchangeVC = [EDExchangeCenterViewController new];
                exchangeVC.title = @"发币兑换";
                [self.navigationController pushViewController:exchangeVC animated:YES];
            }
                break;
            case 5:
            {
                
                EDHundredTimesVC * gameVC = [EDHundredTimesVC new];
                gameVC.title = @"千倍百倍（PT、PS平台）";
                [self.navigationController pushViewController:gameVC animated:YES];
            }
                break;
            case 6:
            {
                EDTigerFitViewController * tigerVC = [EDTigerFitViewController new];
                tigerVC.title = @"组队打虎";
                [self.navigationController pushViewController:tigerVC animated:YES];
            }
                break;
            case 7:
            {
                EDTournamentViewController * exchangeVC = [EDTournamentViewController new];
                exchangeVC.title = @"个人锦标赛";
                [self.navigationController pushViewController:exchangeVC animated:YES];
            }
                break;
            case 8:
            {
     
                EDBirthdayLuckViewController * exchangeVC = [EDBirthdayLuckViewController new];
                exchangeVC.title = @"生日礼金";
                [self.navigationController pushViewController:exchangeVC animated:YES];
            }
                break;
            case 9:
            {
                EDRecommendFriendsViewController * recomdVC = [EDRecommendFriendsViewController new];
                recomdVC.title =@"推荐好友";
                [self.navigationController pushViewController:recomdVC animated:YES];
            }
                break;
            case 10:
            {
              //  [JMNotifyView showNotify:@"活动暂未开始，敬请期待！" isSuccessful:NO];
                EDAdviceInfoViewController * adiveVC = [EDAdviceInfoViewController new];
                adiveVC.title =@"APP建议奖";
                [self.navigationController pushViewController:adiveVC animated:YES];
            }
                break;
            case 11:
            {
      
                
            }
                break;
                
            default:
                break;
        }
    }else{
       
//        LLWebViewController * webVC = [LLWebViewController new];
//        webVC.title = [dataDicc jsonString:@"name"];
//
//
//     // encodedString =  [encodedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        webVC.urlStr =encodedString;
//        webVC.titleStr = [dataDicc jsonString:@"name"];
//        [self.navigationController pushViewController:webVC animated:YES];
            NSString* encodedString = [NSString stringWithFormat:@"%@%@&uid=%@&wb=true",EDFactivityURL22, [dataDicc jsonString:@"url"],[DJLoginHelper sharedInstance].cj_UserUuid];
        TGWebViewController *web = [[TGWebViewController alloc] init];
        web.url =encodedString;
        //[encodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];;
        web.webTitle =[dataDicc jsonString:@"name"];
        web.progressColor = EDColor_BaseBlue;
        [self.navigationController pushViewController:web animated:YES];
        
        
    }
    
    
    
    
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(kScreenWidth/4, kScreenWidth/4*0.8);
    
    
}


@end

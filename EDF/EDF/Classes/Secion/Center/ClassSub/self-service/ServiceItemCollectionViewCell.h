//
//  ServiceItemCollectionViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/25.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceItemCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *namelab;
@property (weak, nonatomic) IBOutlet UIButton *iconImage;


@end

NS_ASSUME_NONNULL_END

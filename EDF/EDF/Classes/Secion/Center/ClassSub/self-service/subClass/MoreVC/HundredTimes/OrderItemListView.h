//
//  OrderItemListView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemListView : UIView
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;

@property (weak, nonatomic) IBOutlet UILabel *beiNumLab;

@property (weak, nonatomic) IBOutlet UIButton *orderBtn;


@end

NS_ASSUME_NONNULL_END

//
//  EDGamefriendRecommendListVC.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDGamefriendRecommendListVC.h"
#import "FriendRecomdListTableViewCell.h"
#import "FriendHeaderView.h"
@interface EDGamefriendRecommendListVC ()
{
    NSInteger _pageNum;
}
@property(nonatomic,strong)FriendHeaderView * headerV;
@end
@implementation EDGamefriendRecommendListVC
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.headerV.frame = CGRectMake(0, 0, kScreenWidth, 45);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _pageNum =0;
    [self creatUI];
   // [self refushData];
    
    [self ks_tableAddFootRequst];
    [self ks_tableAddHeaderRequstRefush];
}
-(void)getData_LoadingMore{
    _pageNum ++;
     [self refushData:NO];
}
-(void)getData_Refush{
    _pageNum = 0;
    [self refushData:YES];
}
-(void)creatUI{
    
    self.ks_TableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];

    FriendHeaderView * headerV = [[NSBundle mainBundle]loadNibNamed:@"FriendHeaderView" owner:self options:nil].lastObject;
    self.headerV.frame = CGRectMake(0, 0, kScreenWidth, 45);
    
    self.ks_TableView.tableHeaderView = headerV;
    
    
}
-(void)refushData:(BOOL)isRefush{
    [self showLoadingAnimation];
    NSString  * url22 = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_friendRecommendList"];
    //Game_friendRecommendList
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:@(_pageNum) forKey:@"curpage"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url22 parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
         NSDictionary  *dict = [dict001 jsonDict:@"data"];
        //NSArray *  dataArr = [dict jsonArray:@"pagedata"];
        NSInteger allPage = [dict jsonInteger:@"totalpage"];
        NSInteger curpage = [dict jsonInteger:@"curpage"];
        
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (isRefush==YES) {
                [strongSelf.modelsArry removeAllObjects];
            }
            if (isRefush==NO) {
                if (allPage == curpage) {
                    //没数据了
                    [strongSelf.ks_TableView.mj_footer endRefreshingWithNoMoreData];
                    return;
                }
            }
            NSArray * dataArry = [dict jsonArray:@"pagedata"];
            for (NSDictionary * dataDicc in dataArry) {
                [strongSelf.modelsArry addObject:dataDicc];
            }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
           [self.ks_TableView.mj_header endRefreshing];
         [self.ks_TableView.mj_footer endRefreshing];
        [WJUnit showMessage:@"请求失败"];
        //[self getBalance];
        
    }];

}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FriendRecomdListTableViewCell";
    FriendRecomdListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (FriendRecomdListTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"FriendRecomdListTableViewCell" owner:self options:nil] lastObject];
    }
    
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(void)stepCell:(FriendRecomdListTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    NSDictionary * userDicc = self.modelsArry[indexPath.row];
    
    cell.typelab.text = [userDicc jsonString:@"status"];
    if ( [[userDicc jsonString:@"status"] isEqualToString:@"未注册"]) {
        cell.typelab.textColor = EDColor_BaseBlue;
    }else{
        cell.typelab.textColor = kRGBColor(63, 63, 63);
    }
    cell.nameLab.text =[userDicc jsonString:@"toname"];
    cell.phoneLab.text =[userDicc jsonString:@"tophone"];
    cell.codeLab.text =[userDicc jsonString:@"no"];
    
//    {
//        arrive = "";
//        content = "";
//        "create_date" = "2019-07-27%2014:56:59";
//        fromemail = "";
//        fromname = "";
//        fromphone = "";
//        id = Yku0bsHERSSKGCevrPpbPQ;
//        lv = "";
//        lvn = "";
//        maxlv = "";
//        no = 619863;
//        open = "";
//        "reg_id" = "";
//        remark = "";
//        status = "\U672a\U6ce8\U518c";
//        status2 = "";
//        template = "";
//        toemail = "";
//        toname = Simisi;
//        tophone = "15*****0950";
//        type = "\U597d\U53cb\U63a8\U8350";
//        "user_id" = "RE3-hjUeTIiUxWqxWOQOUA";
//    }
    
    
}
@end

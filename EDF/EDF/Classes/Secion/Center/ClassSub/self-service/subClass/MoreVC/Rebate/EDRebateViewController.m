//
//  EDRebateViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDRebateViewController.h"
#import "EDRichTextLabel.h"
#import "ThreeBtnsView.h"
#import "FiveBtnsView.h"

@interface EDRebateViewController ()
@property(nonatomic,strong)   UILabel * topLab;
@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)UIButton * enterbtn;
@property(nonatomic,strong)   NSDictionary * gameInfoDicc;
@end

@implementation EDRebateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.gameInfoDicc = [NSDictionary dictionary];
    [self creatUI];
    
    WEAKSELF
    self.bgSc.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //调用刷新方法
        STRONGSELFFor(weakSelf);
        [strongSelf getRebateInfo];
    }];
   [self.bgSc.mj_header beginRefreshing];
    
}

-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    self.topLab = [BBControl createLabelWithFrame:CGRectMake(10, 15, kScreenWidth, 100) Font:16 Text:@"申请返水"];
    self.topLab.backgroundColor = [UIColor clearColor];
    self.topLab.textAlignment = NSTextAlignmentCenter;
    [self.bgSc addSubview:self.topLab];
    
    self.enterbtn = [BBControl createButtonWithFrame:CGRectMake(50, self.topLab.bottom+10, kScreenWidth-100, 40) target:self SEL:@selector(enterbtnClick) title:@"申请"];
       [self.enterbtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
    [self.enterbtn setTitleColor:[UIColor whiteColor] forState:0];
    self.enterbtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.enterbtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [self.bgSc addSubview:self.enterbtn];

    NSString *str = @"活动时间\n\n2020年01月01日——31日\n\n活动适用平台\n\n所有老虎机平台\n\n活动内容\n\nEDF壹定发特色自助返水申请系统全新升级，引领行业返水领取方式，自助申请、实时结算、自动派发，让您告别排队等待和人工申请等繁琐流程！在EDF壹定发，您将体验到完全不一样的在线娱乐方式！其中，活动彩金、存送及捕鱼、棋牌类游戏不计算返水。各级别会员返水比例如下：";
    NSArray *arr = @[@"活动时间",@"活动适用平台",@"活动内容"];
    NSArray *arr2 = @[@"特色自助返水申请系统",@"活动彩金、存送及捕鱼、棋牌类游戏不计算返水。"];
    
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, self.enterbtn.bottom+20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.bgSc addSubview:leb01];
    
    UIView * numBgView = [BBControl createViewWithFrame:CGRectMake(10, leb01.bottom+20, kScreenWidth-20, 240)];
    KViewBorder(numBgView, kRGBColor(90, 90, 90), 1);
    [self.bgSc addSubview:numBgView];
    
    
    NSArray *dataArry = @[@[@"会员级别",@"老虎机",@"真人",@"PT,AG,MG",@"每日上限"],@[@"银冠会员",@"0.8%",@"0.4%",@"0.4%",@"3888"],@[@"金冠VIP会员",@"1.0%",@"0.5%",@"0.4%",@"5888"],@[@"王冠VIP会员",@"1.2%",@"0.6%",@"0.4%",@"8888"],@[@"黄冠VIP会员",@"1.3%",@"0.7%",@"0.4%",@"12888"],@[@"至尊VIP会员",@"1.5%",@"0.8%",@"0.4%",@"18888"]];
    CGFloat  space = 0;
    CGFloat  btnH = 40;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry[i];
        FiveBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"FiveBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.lab1.text = titles.firstObject;
         mapBtn.lab2.text = titles[1];
        mapBtn.lab3.text = titles[2];
        mapBtn.lab4.text = titles[3];
        mapBtn.lab5.text = titles.lastObject;
        [numBgView addSubview:mapBtn];
    }
    
    NSString *str22 = @"参加方式\n\n在【个人中心】——【自助服务区】——【申请返水】可以实时申请当前未结算的返水，返水累计满5元（所有平台累计）即可领取，如不足5元需要继续投注，待返水满5元后方可领取。申请成功后，请去【个人中心】——【福利领取中心】——【红利】中领取。 \n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n\n1、返水累计满5元（所有平台累计）即可申请领取，每周日23：59前必须把本周（周一00：00）至周日23：59）的返水金额全部领完，否则将清零。 ；\n2、返水金额每日最高达18888，返水将领取至基本账户1倍流水投注满100元即可申请提款，不限平台；\n3、捕鱼达人暂不参与返水活动；\n4、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；\n5、此活动必须遵守 EDF壹定发一般规则与条款 (请详细阅读)。EDF壹定发保留在任何时候对本活动的修改、停止及最终解释权。";
    NSArray *arr22 = @[@"参加方式",@"活动规则"];
    NSArray *arr222 = @[@"累计满5元",@"每周日23：59前",@"每日最高达18888，返水将领取至基本账户",];
    EDRichTextLabel * leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView.bottom+20, kScreenWidth-40, 500)];
    leb02.backgroundColor = kRGBColor(245, 245, 245);
    [leb02 rich_TextCofigWith:str22 blodStrArry:arr22 colortextArry:arr222];
    [leb02 sizeToFit];
    [self.bgSc addSubview:leb02];
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, leb02.bottom+50);

}

-(void)getRebateInfo{
    //Game_loadRebatePromotion
    [self showLoadingAnimation];
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadRebatePromotion"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * infoDicc = [dict jsonDict:@"data"];
            YYLog(@"Game_loadDailyGamePromotion---%@",dict);
            //PTimageGameURL
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                self.gameInfoDicc  = [NSDictionary dictionaryWithDictionary:infoDicc];
                NSString * str1 =[infoDicc jsonString:@"tips_txt"];
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>",  [str1 stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
                
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 5.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                self.topLab.attributedText = attributedString;
                self.topLab.textAlignment =NSTextAlignmentCenter;
                // [strongSelf.topLab sizeToFit];
                
                if ([[infoDicc jsonString:@"status"] isEqualToString:@"valid"]) {
                    [self.enterbtn setBackgroundColor:kRGBColor(44, 143, 219) forState:0];
                    self.enterbtn.enabled = YES;
                }else if ([[infoDicc jsonString:@"status"] isEqualToString:@"invalid"]){
                    [self.enterbtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                    self.enterbtn.enabled = NO;
                }else{
                    [self.enterbtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                    self.enterbtn.enabled = NO;
                };

            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [self.bgSc.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [self.bgSc.mj_header endRefreshing];
        [WJUnit showMessage:@"请求失败"];
        //[self getBalance];
        
    }];
    
    
    
}

-(void)enterbtnClick{
    
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_onGetRebateBonus"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf.bgSc.mj_header beginRefreshing];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

@end

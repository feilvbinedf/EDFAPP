//
//  RecomdFriendsView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "RecomdFriendsView.h"
#import "RestrictionInput.h"

@implementation RecomdFriendsView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    KViewRadius(self.sureBtn, 4);
     KViewRadius(self.moreBtn, 4);
    KViewRadius(self.nameTF, 4);
    KViewRadius(self.phoneTF, 4);
    
        [self.nameTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self.phoneTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
}
-(void)textFieldDidChange:(UITextField *)textField {
    if (textField==self.nameTF) {
            [RestrictionInput restrictionInputTextField:textField maxNumber:6 showView:self showErrorMessage:@""];
    }else{
            [RestrictionInput restrictionInputTextField:textField maxNumber:11 showView:self showErrorMessage:@""];
    }

}
@end

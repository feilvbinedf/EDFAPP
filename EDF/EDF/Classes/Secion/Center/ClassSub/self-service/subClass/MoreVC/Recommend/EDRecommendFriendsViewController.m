//
//  EDRecommendFriendsViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDRecommendFriendsViewController.h"
#import "RecomdFriendsView.h"
#import "EDRichTextLabel.h"
#import "EDGamefriendRecommendListVC.h"
@interface EDRecommendFriendsViewController ()

@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)RecomdFriendsView * infoView;
@property(nonatomic,assign)BOOL  is_Can_Recomd;
@end
@implementation EDRecommendFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self refushData];

}
-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    self.infoView = [[NSBundle mainBundle]loadNibNamed:@"RecomdFriendsView" owner:self options:nil].lastObject;
    self.infoView.frame = CGRectMake(0, 0, kScreenWidth, 370);
    
    [self.bgSc addSubview:self.infoView];

    WEAKSELF
    [self.infoView.sureBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf sureBtnClick:strongSelf.infoView.sureBtn];
    }];
    
    
    [self.infoView.moreBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        EDGamefriendRecommendListVC  * vc = [EDGamefriendRecommendListVC new];
        vc.title=@"推荐好友历史";
        [strongSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    
    NSString *str = @"活动时间\n\n2019年07月01日——31日\n\n活动内容\n\nEDF壹定发特色好友推荐系统再次升级！一次邀请——双向奖励、多重奖励、终生奖励！邀请您的朋友一起加入EDF壹定发，分享在线娱乐的刺激和财富！";
    NSArray *arr = @[@"活动时间",@"活动内容"];
    NSArray *arr2 = @[@"一次邀请——双向奖励、多重奖励、终生奖励！"];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, self.infoView.bottom+20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.bgSc addSubview:leb01];
    
    UIImageView * itext_Info = [BBControl createImageViewFrame:CGRectMake(10, leb01.bottom+15, kScreenWidth-20, kScreenWidth*0.54) imageName:@"itext_Info"];
    [self.bgSc addSubview:itext_Info];
    
    NSString *str22 = @"1、基础奖励为 双向奖励，被推荐者基本账户存款500元且累计投注额满5000，推荐者和被推荐者各得128元。。此奖励不受时间限制，达标即送。严禁注册多账号进行套利，一经发现，扣除所有红利并冻结账号。\n2、入门、进阶、精英奖励为单向奖励：只发给推荐者。 此奖金在被推荐者注册后一个月内有效，如果被推荐者在一个月内未达到累计存款金额的要求，推荐者不再享受此福利。\n3、累计存款只计算基本账户存款金额，笔笔送存款金额不计入。\n4、为了保护推荐者的隐私。通过“隐私保护”被推荐者按钮，被推荐者可以选择所有的奖金及1%发币是否可以继续派发给推荐者。\n理论上，您可获得奖金总共为：128 + 188 + 388 + 888  = 1592 元。此外，您还可以获得该好友每次存款您都可获得1%的发币回馈。\n当您推荐的好友足够多且累计存款金额足够大时，您每月收获的发币将为您带来巨大财富！发币将帮助您快速升级VIP，或者用来兑换筹码和实物礼品。\n\n参加方式\n\n在【个人中心】--【自助服务区】——【推荐好友】按钮进行推荐。\n推荐方式：您在填写好友的相关信息后，系统将自动生成推荐码，每次生成的推荐码都是唯一的。您的好友在注册时必须填写该推荐码，且绑定的真实信息（姓名、手机）必须与您推荐时填写的信息完全一致，您才可以获得推荐奖金。如果您有任何疑问，在推荐时请及时联系在线客服\n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n\n1、推荐人每月累计存款每满300元，即可获得1次推荐名额，累计存款越多，推荐名额也越多。例如存款达到2000元，则可以获得 2000 /  3 = 6次推荐名额;\n2、好友每次存款赠送的发币计算方式：好友每次存款金额的1%，向下取整，1发币起派发。例如好友存款120元时，赠送1个发币，好友存款1499时，赠送14个发币；\n3、推荐奖金领取：【个人中心】——【福利领取中心】——【红利】，赠送发币查询：【个人中心】——【福利领取中心】——【发币】。您所获得的奖金需要15倍流水投注于任意老虎机平台。\n4、被推荐人必须是第一次来本站存款并且游戏，只能通过官方网址进行注册，不能在代理合营商名下注册。如不能确认是否为官方网址，可在注册前咨询在线客服；\n5、被推荐人必须和推荐人使用的是不同的电脑，有不同的联系方式及IP地址，是真实的朋友而非借他人的身份信息进行虚假推荐和重复注册；\n6、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；";
    NSArray *arr22 = @[@"参加方式",@"活动规则"];
    NSArray *arr222 = @[@"双向奖励"];
    EDRichTextLabel * leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, itext_Info.bottom+20, kScreenWidth-40, 500)];
    leb02.backgroundColor = kRGBColor(245, 245, 245);
    [leb02 rich_TextCofigWith:str22 blodStrArry:arr22 colortextArry:arr222];
    [leb02 sizeToFit];
    [self.bgSc addSubview:leb02];
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, leb02.bottom+50);

}


-(void)sureBtnClick:(UIButton*)sender{
    
    if (self.is_Can_Recomd ==YES) {
        //推荐好友
        [self recomdFriends];
        
    }else{
      //立即存款
        [AppDelegate shareAppdelegate].index = 0;
        self.navigationController.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
}

-(void)recomdFriends{
    
    if (kStringIsEmpty(self.infoView.nameTF.text)) {
        
        [JMNotifyView showNotify:@"请输入朋友姓名" isSuccessful:NO];
        
        return;
    }
    
    if (kStringIsEmpty(self.infoView.phoneTF.text)) {
        
         [JMNotifyView showNotify:@"请输入朋友电话" isSuccessful:NO];
        return;
    }
    
    //Game_onSaveFriendInfo
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_onSaveFriendInfo"];
    NSMutableDictionary * jsonDicc = [NSMutableDictionary dictionary];
    [jsonDicc setValue:self.infoView.nameTF.text forKey:@"friend_name"];
    [jsonDicc setValue:self.infoView.phoneTF.text forKey:@"friend_phone"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:jsonDicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self refushData];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    
    
}
-(void)refushData{
    //Game_loadFriendPromotionInfo
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadFriendPromotionInfo"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_loadQbbBonusInfo----%@",dict001);
            NSDictionary * diccInfo = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [diccInfo jsonString:@"tips_text"]];
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 3.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                strongSelf.infoView.contentLab.attributedText = attributedString;
                strongSelf.infoView.contentLab.textAlignment =NSTextAlignmentCenter;
                strongSelf.infoView.contentLab.textColor = kRGBColor(68, 68, 68);
                strongSelf.infoView.peopleNum.text = [diccInfo jsonString:@"f_personal_count"];
                 strongSelf.infoView.cashNum.text = [diccInfo jsonString:@"f_bonus"];
                 strongSelf.infoView.fabiNum.text = [diccInfo jsonString:@"f_point"];
                if ([diccInfo jsonInteger:@"fcount"]>0) {
                    strongSelf.infoView.enterView.hidden = NO;
                    strongSelf.infoView.enterViewHC.constant = 100;
                    strongSelf.is_Can_Recomd = YES;
                    [strongSelf.infoView.sureBtn setTitle:@"保存" forState:0];
                    
                }else{
                     strongSelf.infoView.enterView.hidden = YES;
                     strongSelf.infoView.enterViewHC.constant = 0;
                     strongSelf.is_Can_Recomd = NO;
                    [strongSelf.infoView.sureBtn setTitle:@"立即存款" forState:0];
                }

            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];

    
    
}
@end

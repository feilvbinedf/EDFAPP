//
//  EDHundredTimesVC.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDHundredTimesVC.h"
#import "EDRichTextLabel.h"
#import "TwoBtnsView.h"
#import "HundredHeaderActionView.h"
#import "CGXPickerView.h"
#import "ThreeBtnsView.h"

@interface EDHundredTimesVC ()
@property(nonatomic,strong)   UILabel * topLab;
@property(nonatomic,strong)   HundredHeaderActionView * topActionView;

@property(nonatomic,strong)   UIView * textInfoView;
@property(nonatomic,copy)   NSString * typeSecltStr;

@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)   UIImageView * topImageV;
@end
@implementation EDHundredTimesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.typeSecltStr = @"";
    [self creatUI];
    [self refushData];
    [self refushData_yaBao];
}
-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    self.topActionView = [[NSBundle mainBundle]loadNibNamed:@"HundredHeaderActionView" owner:self options:nil].lastObject;
    self.topActionView.frame =CGRectMake(0, 0, kScreenWidth, 230);
    [self.bgSc addSubview:self.topActionView];
    
    
    self.textInfoView = [BBControl createViewWithFrame:CGRectMake(0, self.topActionView.bottom+20, kScreenWidth, 300)];
    self.textInfoView.backgroundColor = kRGBColor(245, 245, 245);
    [self.bgSc addSubview:self.textInfoView];
    
    NSString *str = @"活动时间\n\n2019年07月01日——31日\n\n活动适用平台\n\n老虎机PT、PS平台，其他平台陆续接入中\n\n活动内容\n\n认为我们之前的“千倍百倍自助申请系统”已经足够“狂拽炫酷diao炸天”了吗？精益求精的我们怎会就此停步！我们认为还可以再改进一点点，让您的娱乐更轻松、更有趣味！EDF壹定发“千倍百倍”活动第三次颠覆升级，这次我们新增了“押宝”的新玩法哦！\n\n一、千倍百倍基础奖励\n\n基础奖励分为两档：“喜上加喜”（当日中奖前存款满200元）、“奖上加奖”（当日中奖后存款满200元），两个奖励不能同时获得，且存款金额仅计算基本账户存款。活动具体规则如下：";
    NSArray *arr = @[@"活动时间",@"活动适用平台",@"活动内容"];
    NSArray *arr2 = @[@"“押宝”"];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, 20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.textInfoView addSubview:leb01];
    
    UIView * numBgView = [BBControl createViewWithFrame:CGRectMake(10, leb01.bottom+20, kScreenWidth-20, 160)];
    KViewBorder(numBgView, kRGBColor(90, 90, 90), 1);
    numBgView.backgroundColor = [UIColor clearColor];
    [self.textInfoView addSubview:numBgView];
    
    NSArray *dataArry = @[@[@"单局盈利倍数",@"喜上加喜",@"奖上加奖"],@[@"≥100倍",@"最高158元",@"最高58元"],@[@"≥500倍",@"最高588元",@"最高188元"],@[@"≥1000倍",@"最高1888元",@"最高388元"]];
    CGFloat  space = 0;
    CGFloat  btnH = 40;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry[i];
        ThreeBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"ThreeBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.lab01.text = titles.firstObject;
        mapBtn.lab02.text = titles[1];
        mapBtn.lab03.text = titles.lastObject;
        [numBgView addSubview:mapBtn];
    }
    
    NSString *str22 = @"\n二、千倍百倍特色奖励\n\n特色奖励为 押宝（又名：我赌我能中）,，玩家可以自愿参与。作为一家追求极致体验和娱乐精神的娱乐城，“押宝”是EDF首创的活动，主要针对自信心强、有娱乐精神的游戏玩家，押宝下注的金额是10元，押宝时间必须早于您中千倍百倍的时间和申请时间，如果押宝了，并且在当日押宝之后中了千倍百倍，我们将给您额外返奖。";
    NSArray *arr22 = @[];
    NSArray *arr222 = @[@"早于您中千倍百倍的时间和申请时间",@"押宝之后中了千倍百倍"];
    EDRichTextLabel * leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView.bottom+20, kScreenWidth-40, 500)];
    leb02.backgroundColor = kRGBColor(245, 245, 245);
    [leb02 rich_TextCofigWith:str22 blodStrArry:arr22 colortextArry:arr222];
    [leb02 sizeToFit];
    [self.textInfoView addSubview:leb02];
    
    UIView * numBgView3 = [BBControl createViewWithFrame:CGRectMake(10, leb02.bottom+20, kScreenWidth-20, 160)];
    KViewBorder(numBgView3, kRGBColor(90, 90, 90), 1);
    numBgView3.backgroundColor = [UIColor clearColor];
    [self.textInfoView addSubview:numBgView3];
    
    NSArray *dataArry3 = @[@[@"中奖倍数",@"押宝金额",@"返奖金额"],@[@"≥100倍",@"10",@"50"],@[@"≥500倍",@"10",@"100"],@[@"≥1000倍",@"10",@"150"]];
    for (int i = 0 ; i < dataArry3.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry3[i];
        ThreeBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"ThreeBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.lab01.text = titles.firstObject;
        mapBtn.lab02.text = titles[1];
        mapBtn.lab03.text = titles.lastObject;
        [numBgView3 addSubview:mapBtn];
    }
    
    
    NSString *str33 = @"\n参加方式\n\n该活动奖金只需在【个人中心】——【自助服务区】——【千倍百倍】按照步骤申请，无需填写任何信息。一般情况下在5到10分钟内您将自动获得对应奖金。活动仅限中奖当天申请，且一天只能申请一次。请您把握最佳时机。操作步骤如下：\n1、在【自助服务区】点击【千倍百倍】按钮，】按钮，弹出申请界面；\n2、在弹出界面点击“选择中奖牌局”，通过筛选功能，系统能自动臻选您当天符合要求的所有牌局；\n3、选择您需要提交的中奖牌局，相关游戏信息将自动带出，无需您填写任何信息；\n4、点击“提交”按钮，您将完成当天“千倍百倍”中奖申请。\n5、如果需要参加“押宝”活动，必须早于您中奖时间和申请时间之前下注。\n如果您对游戏结果有异议，您可以按照以下方法核实牌局记录和结果(以PT平台为例)：\n进入您下注的老虎机游戏，在游戏右上角或右下角的位置有“小扳手”图标，点击后会出现“游戏记录”，便可查询到当局代码和游戏结果。游戏结果以相应游戏平台提供的游戏记录为准，游戏界面截图无效\n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n1、玩家当天游戏中获得“千倍百倍”当天，需达到 最低存款200元，且该局游戏总投注至少1元人民币或以上，方可申请此奖金；\n2、该活动累计存款仅计算当日 基本账户的存款，，参加笔笔送及其他活动的存款金额不计；\n3、该活动奖金请在【个人中心】——【福利领取中心】——【红利】中领取，奖金所进行的投注不计算返水；\n4、游戏赢利倍数需要超过100倍（含100），倍数核算方法：该局游戏总赢利除以总投注所得数值。 单手盈利100倍以上（含100倍）500倍以下（不含500倍），EDF壹定发奖励该局投注额100倍；单手盈利500倍以上(含500)1000倍以下(不含1000)，EDF壹定发奖励该局投注额500倍；单手盈利1000倍以上（含1000倍）,EDF壹定发奖励该局投注额1000倍；\n5、必须投注于PT、PS老虎机游戏，所有所获免费游戏所产生盈利不计算本优惠； 例如：玩家在Cute and Fluffy投注中获得10次免费旋转机会产生盈利不计算本优惠； 非线注中奖的N个相同奖励金额不计算本优惠；\n6、“喜上加喜”和“奖上加奖”及“押宝”百倍最高奖金分别为：，五百倍最高奖金588元/188/100元，千倍最高奖金1888元/388/150元，所有奖金均需18倍流水即可提款；\n7、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；\n8、此活动必须遵守EDF壹定发一般规则与条款 (请详细阅读)。EDF壹定发保留在任何时候对本活动的修改、停止及最终解释权。";
    NSArray *arr33 = @[@"参加方式",@"活动规则"];
    NSArray *arr333 = @[@"活动仅限中奖当天申请，且一天只能申请一次。",@"“提交”",@"最低存款200元",@"至少1元人民币或以上",@"投注额100倍",@"投注额500倍",@"投注额1000倍",@"至少1元人民币或以上"];
    EDRichTextLabel * leb03 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView3.bottom+20, kScreenWidth-40, 500)];
    leb03.backgroundColor = kRGBColor(245, 245, 245);
    [leb03 rich_TextCofigWith:str33 blodStrArry:arr33 colortextArry:arr333];
    [leb03 sizeToFit];
    [self.textInfoView addSubview:leb03];
    
    self.textInfoView.height =leb03.bottom+20;
    

    
    WEAKSELF
    [self.topActionView.yaobaoBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf Game_onBetTreasure];
    }];
    [self.topActionView.selectBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf selectActionType];
    }];
    
    self.topActionView.actionBtnClickBlock = ^(NSDictionary * _Nonnull dataDicc) {
         STRONGSELFFor(weakSelf);
        [strongSelf action_ItemPopWith:dataDicc];
    };

    
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
    
}
-(void)action_ItemPopWith:(NSDictionary *)dataDicc{
    WEAKSELF
    [[SmileAlert sharedInstance] mazi_alertContent:@"您确定要申请此注单吗？\n(一天仅限申请一次)" AndBlock:^(BOOL sure) {
        STRONGSELFFor(weakSelf);
        if (sure==YES) {
         
            [strongSelf orderActionRequest:dataDicc];
        }
    }];
    
}
-(void)refushData{
    //Game_loadQbbBonusInfo  千倍百倍--获取信息
   // "status": "already_dep", //already_dep-存款达标可查询千百倍，notyet_dep-存款未达标，checking-千百倍奖金审核中，finish-已获得奖金

    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadQbbBonusInfo"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_loadQbbBonusInfo----%@",dict001);
            NSDictionary * diccInfo = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [diccInfo jsonString:@"tips_txt"]];
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 3.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                strongSelf.topActionView.contentLab.attributedText = attributedString;
                strongSelf.topActionView.contentLab.textAlignment =NSTextAlignmentCenter;
                strongSelf.topActionView.contentLab.textColor = kRGBColor(68, 68, 68);
                [strongSelf.topActionView.contentLab sizeToFit];
                ////already_dep-存款达标可查询千百倍，notyet_dep-存款未达标，checking-千百倍奖金审核中，finish-已获得奖金
                if ([[diccInfo jsonString:@"status"] isEqualToString:@"notyet_dep"]) {
                    strongSelf.topActionView.topView.hidden = YES;
                    strongSelf.topActionView.topViewHC.constant = 0;
                }else if ([[diccInfo jsonString:@"status"] isEqualToString:@"already_dep"]){
                    strongSelf.topActionView.topView.hidden = NO;
                    strongSelf.topActionView.topViewHC.constant = 50;
                }else if ([[diccInfo jsonString:@"status"] isEqualToString:@"checking"]){
                    strongSelf.topActionView.topView.hidden = YES;
                    strongSelf.topActionView.topViewHC.constant = 0;
                }
                else{
                    strongSelf.topActionView.topView.hidden = YES;
                    strongSelf.topActionView.topViewHC.constant = 0;
                }
                strongSelf.topActionView.height = strongSelf.topActionView.yaobaoBtn.bottom+20;
                 strongSelf.textInfoView.top = strongSelf.topActionView.bottom+20;
                 self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
                 [strongSelf change_ViewsHeightWith:0];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];

}

-(void)refushData_yaBao{
    
    //Game_loadTreasure 千倍百倍---获取押宝状态
    //  "status": "allow",//allow-可押宝，not_enough-余额不足，finish-已押宝，not_allow-不可押宝
    
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadTreasure"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_loadQbbBonusInfo----%@",dict001);
            NSDictionary * diccInfo = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [diccInfo jsonString:@"tips_txt"]];
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 3.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                strongSelf.topActionView.content02.attributedText = attributedString;
                strongSelf.topActionView.content02.textAlignment =NSTextAlignmentCenter;
                strongSelf.topActionView.content02.textColor = kRGBColor(68, 68, 68);
                 [strongSelf.topActionView.content02 sizeToFit];
                 // "status": "allow", //allow-可押宝，not_enough-余额不足，finish-已押宝，not_allow-不可押宝
                if ([[diccInfo jsonString:@"status"] isEqualToString:@"allow"]) {
                    strongSelf.topActionView.yaobaoBtn.hidden = NO;
                }else if ([[diccInfo jsonString:@"status"] isEqualToString:@"not_enough"]){
                    strongSelf.topActionView.yaobaoBtn.hidden = NO;
                }else if ([[diccInfo jsonString:@"status"] isEqualToString:@"finish"]){
                    strongSelf.topActionView.yaobaoBtn.hidden = YES;
                }
                else{
                    strongSelf.topActionView.yaobaoBtn.hidden = YES;
                }
               strongSelf.topActionView.height = strongSelf.topActionView.yaobaoBtn.bottom+20;
                 strongSelf.textInfoView.top = strongSelf.topActionView.bottom+20;
                 self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
                 [strongSelf change_ViewsHeightWith:0];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

-(void)Game_onBetTreasure{
    //Game_onBetTreasure
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_onBetTreasure"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf refushData_yaBao];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}

-(void)selectActionType{
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    NSMutableArray * dataArry = @[@"PT平台",@"PS平台"].mutableCopy;

    WEAKSELF
    [CGXPickerView showStringPickerWithTitle:@"选择平台" DataSource:dataArry DefaultSelValue:nil  IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
        STRONGSELFFor(weakSelf)
        NSLog(@"CGXPickerView--%@",selectValue);
        if (![strongSelf.typeSecltStr isEqualToString:selectValue]) {
            if ([selectValue isEqualToString:@"PT平台"]) {
                [strongSelf refushData_OrderList:@"1"];
            }else{
                [strongSelf refushData_OrderList:@"7"];
            }
            strongSelf.typeSecltStr = selectValue;
        }
 
    }];

}

-(void)refushData_OrderList:(NSString *)orderType{
    //Game_queryQbbOrderList
    
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_queryQbbOrderList"];
    NSMutableDictionary * dataDIcc = [NSMutableDictionary dictionary];
    [dataDIcc setValue:orderType forKey:@"plat_id"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dataDIcc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_queryQbbOrderList----%@",dict001);
            NSDictionary * infoDicc = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[infoDicc jsonString:@"status"] isEqualToString:@"has_order"]) {
                    [strongSelf.topActionView refushDataItemsWith:[infoDicc jsonArray:@"qbb_order_list"]];

                        [strongSelf change_ViewsHeightWith:[infoDicc jsonArray:@"qbb_order_list"].count*50];
                }else{
                   // no_order
                    [strongSelf.topActionView refushDataItemsWith:[NSArray array]];
                         [strongSelf change_ViewsHeightWith:0];
                }
           
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [infoDicc jsonString:@"tips_txt"]];
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 3.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                strongSelf.topActionView.contentLab.attributedText = attributedString;
                strongSelf.topActionView.contentLab.textAlignment =NSTextAlignmentCenter;
                strongSelf.topActionView.contentLab.textColor = kRGBColor(68, 68, 68);
                [strongSelf.topActionView.contentLab sizeToFit];
                strongSelf.topActionView.height = strongSelf.topActionView.yaobaoBtn.bottom+40;
                strongSelf.textInfoView.top = strongSelf.topActionView.bottom+20;
                 self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
            });
             [strongSelf change_ViewsHeightWith:0];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];

}

-(void)orderActionRequest:(NSDictionary *)dataDicc{
    //Game_queryQbbOrderListOnApplyQbbBonus
    
    [self showLoadingAnimation];
    //  NSDictionary * dicc = self.modelsArry[sender.tag];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_queryQbbOrderListOnApplyQbbBonus"];
    NSMutableDictionary * jsonDicc = [NSMutableDictionary dictionary];
    [jsonDicc setValue:[dataDicc jsonString:@"num"] forKey:@"num"];
     [jsonDicc setValue:[dataDicc jsonString:@"date"] forKey:@"date"];
     [jsonDicc setValue:[dataDicc jsonString:@"validbet"] forKey:@"validbet"];
     [jsonDicc setValue:[dataDicc jsonString:@"win"] forKey:@"win"];
     [jsonDicc setValue:[dataDicc jsonString:@"game_id"] forKey:@"game_id"];
     [jsonDicc setValue:[dataDicc jsonString:@"account_id"] forKey:@"account_id"];
       [jsonDicc setValue:[dataDicc jsonString:@"plat_id"] forKey:@"plat_id"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:jsonDicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self refushData];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];

    
}


-(void)change_ViewsHeightWith:(CGFloat )itemHeight{
    
    //self.topActionView.height = 230+itemHeight+20;
    self.textInfoView.top = self.topActionView.bottom+20;

   
}

@end

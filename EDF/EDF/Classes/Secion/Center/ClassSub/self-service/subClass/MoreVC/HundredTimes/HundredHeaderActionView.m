//
//  HundredHeaderActionView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "HundredHeaderActionView.h"
#import "OrderItemListView.h"
@implementation HundredHeaderActionView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    KViewRadius(self.yaobaoBtn, 4);
    self.topView.hidden = YES;
    
    self.orderListView.hidden = YES;
    self.orderHC.constant = 0;
    
}

-(void)refushDataItemsWith:(NSArray *)dataArry{
    
    
    if (dataArry.count>0) {
        self.orderListView.hidden = NO;
        self.orderHC.constant = dataArry.count*50;

    }else{
        self.orderListView.hidden = YES;
        self.orderHC.constant = 0;
    }
    [self.orderListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    CGFloat  space = 0;
    CGFloat  btnH = 50;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSDictionary * titles=dataArry[i];
        UIView * actionView = [BBControl createViewWithFrame:CGRectMake(0, i  * 50, kScreenWidth , 50)];
        UIView* lineView = [BBControl createViewWithFrame:CGRectMake(0, 49, kScreenWidth, 1)];
        lineView.backgroundColor = kRGBColor(245, 245, 245);
        [actionView addSubview:lineView];
        
        UILabel * timeLab = [BBControl createLabelWithFrame:CGRectMake(15, 10, 150, 15) Font:13 Text:[[titles jsonString:@"date"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
        [actionView addSubview:timeLab];
        
        UILabel * timeLab2 = [BBControl createLabelWithFrame:CGRectMake(15, timeLab.bottom+3, kScreenWidth-90, 15) Font:13 Text:[NSString stringWithFormat:@"注单号:%@,投注:%@",[titles jsonString:@"num"],[titles jsonString:@"validbet"]]];
        timeLab2.textColor =kRGBColor(68, 68, 68);
        [actionView addSubview:timeLab2];
        
        
        UIImageView * iconImage = [BBControl createImageViewFrame:CGRectMake(kScreenWidth-15-15, 16, 15, 18) imageName:@"right_icon"];
        iconImage.contentMode = UIViewContentModeCenter;
        [actionView addSubview:iconImage];
        
        
        UILabel * timeLab3 = [BBControl createLabelWithFrame:CGRectMake(iconImage.left-10-70, 16, 70, 17) Font:16 Text:[NSString stringWithFormat:@"%@倍",[titles jsonString:@"multiple"]]];
        timeLab3.textAlignment = NSTextAlignmentRight;
        timeLab3.textColor =EDColor_BaseBlue;
        [actionView addSubview:timeLab3];
        

        UIButton  * actionBtn = [BBControl createButtonWithFrame:CGRectMake(0, 0, kScreenWidth, 49) target:self SEL:nil title:@""];
          [actionView addSubview:actionBtn];
        WEAKSELF
        [actionBtn add_BtnClickHandler:^(NSInteger tag) {
            STRONGSELFFor(weakSelf);
            if (strongSelf.actionBtnClickBlock) {
                strongSelf.actionBtnClickBlock(titles);
            }
        }];
        
        
        
//        OrderItemListView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"OrderItemListView" owner:self options:nil].lastObject;
//        mapBtn.frame = CGRectMake(0, i  * 50, kScreenWidth , 50);
//        mapBtn.timeLab.text = [[titles jsonString:@"date"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
//        mapBtn.orderLab.text = [NSString stringWithFormat:@"注单号:%@,投注:%@",[titles jsonString:@"num"],[titles jsonString:@"validbet"]];
//        mapBtn.beiNumLab.text = [NSString stringWithFormat:@"%@倍",[titles jsonString:@"multiple"]];
//        mapBtn.height = 50;
//        [mapBtn sizeToFit];
        [self.orderListView addSubview:actionView];
        //self.orderHC.constant = actionView.bottom;
      //  [self layoutSubviews];
    }
    
    
    
    
    
    
    
    
}
@end

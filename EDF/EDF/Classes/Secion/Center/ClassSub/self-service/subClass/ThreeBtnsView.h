//
//  ThreeBtnsView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThreeBtnsView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lab01;
@property (weak, nonatomic) IBOutlet UILabel *lab02;
@property (weak, nonatomic) IBOutlet UILabel *lab03;

@end

NS_ASSUME_NONNULL_END

//
//  EDAdviceInfoViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/8/22.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDAdviceInfoViewController.h"
#import "EDRichTextLabel.h"
#import "TwoBtnsView.h"

@interface EDAdviceInfoViewController ()
@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)EDRichTextLabel * leb02 ;


@end

@implementation EDAdviceInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self config_UI];
    [self requestData];
    
}


-(void)config_UI{
    
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    NSString *str = @"APP建议奖\n\n没有完美的系统，只有更好的系统！\n\n我们的一小步，也许是行业的一大步！不断创新，为喜欢“EDF壹定发”的朋友，提供一个“有信誉、有实力、有情怀、更便捷”的娱乐平台始终是我们使命和核心价值观！如果您对于APP使用方面有好的建议和想法，或反馈新发现的使用BUG等，我们将按照如下方式派奖：";
    NSArray *arr = @[@"APP建议奖",@"对于建议奖，我们有如下说明：",@"",@"",@""];
    NSArray *arr2 = @[@"有信誉、有实力、有情怀、更便捷"];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, 20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.bgSc addSubview:leb01];
    
    
    UIView * numBgView = [BBControl createViewWithFrame:CGRectMake(10, leb01.bottom+20, kScreenWidth-20, 200)];
    KViewBorder(numBgView, kRGBColor(90, 90, 90), 1);
    [self.bgSc addSubview:numBgView];
    
    NSArray *dataArry = @[@[@"建议质量",@"建议奖励",],@[@"金玉良言奖",@"288发币",],@[@"奇思妙想奖",@"88元",],@[@"特别贡献奖",@"188元",],@[@"杰出贡献奖",@"1888元",]];
    CGFloat  space = 0;
    CGFloat  btnH = 40;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry[i];
        TwoBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"TwoBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.leftLab.text = titles.firstObject;
        mapBtn.rightLab.text = titles.lastObject;
        [numBgView addSubview:mapBtn];
    }
    
    NSString *str2 = @"对于建议奖，我们有如下说明：\n\n1、建议内容：\n对于言之无物、抄袭复制、重复提交等内容一律不予采用；\n2、提交方式：\n添加EDF建议专员QQ：345774200，提交您的建议或反馈。\n3、提交时间：\n建议专员QQ 工作时间：14：00-22：00。您随时可以添加，随时可以提交，我们将及时回复。对于不符合建议内容或者未采纳的除外；\n4、派奖时间：\n如果您的建议被采纳，相应的奖励将会及时派发，请您去【个人中心】——【福利领取中心】领取。";
    NSArray *arr1 = @[@"对于建议奖，我们有如下说明："];
    NSArray *arr22 = @[@"345774200"];
    self.leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView.bottom+20, kScreenWidth-40, 500)];
    self.leb02.backgroundColor = kRGBColor(245, 245, 245);
    [self.leb02 rich_TextCofigWith:str2 blodStrArry:arr1 colortextArry:arr22];
    [self.leb02 sizeToFit];
    [self.bgSc addSubview:self.leb02];
    
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.leb02.bottom+50);
    

}

-(void)requestData{
    
    NSString  * url2 = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"getAppProposalQQ"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url2 parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"getAppProposalQQ----%@",dict001);
            NSDictionary * dicc = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.leb02.text = [self.leb02.text stringByReplacingOccurrencesOfString:@"345774200" withString:[dicc jsonString:@"value"]];
                NSArray *arr1 = @[@"对于建议奖，我们有如下说明："];
                NSArray *arr22 = @[[dicc jsonString:@"value"]];
                [self.leb02 rich_TextCofigWith:self.leb02.text blodStrArry:arr1 colortextArry:arr22];
                [self.leb02 sizeToFit];
                [self.bgSc addSubview:self.leb02];
                
                self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.leb02.bottom+50);
            });
        }else{
        }
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        //[JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

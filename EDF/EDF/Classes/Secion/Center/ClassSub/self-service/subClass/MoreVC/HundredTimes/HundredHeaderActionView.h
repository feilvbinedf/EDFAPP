//
//  HundredHeaderActionView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HundredHeaderActionView : UIView
@property (weak, nonatomic) IBOutlet UIButton *yaobaoBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHC;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *content02;
@property (weak, nonatomic) IBOutlet UIView *orderListView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderHC;


-(void)refushDataItemsWith:(NSArray *)dataArry;
@property (nonatomic, copy) void (^actionBtnClickBlock)(NSDictionary *dataDicc);

@end

NS_ASSUME_NONNULL_END

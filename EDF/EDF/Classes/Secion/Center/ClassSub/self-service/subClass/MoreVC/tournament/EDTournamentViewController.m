//
//  EDTournamentViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDTournamentViewController.h"
#import "EDRichTextLabel.h"
#import "EDTournameDetailViewController.h"
#import "TGWebViewController.h"

@interface EDTournamentViewController ()

@property(nonatomic,strong)   UILabel * topLab;
@property(nonatomic,strong)   UIButton * fristBtn;
@property(nonatomic,strong)   UIButton * fristBtn02;
@property(nonatomic,strong)   UIButton * fristBtn03;
@property(nonatomic,strong)UIScrollView * bgSc;

@end
@implementation EDTournamentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self creatUI];
    [self refushData];
}


-(void)creatUI{
    
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    UIButton * fristBtn = [BBControl createButtonWithFrame:CGRectMake(kScreenWidth/2-100, 15, 200, 45) target:self SEL:@selector(fristBtnClick:) title:@"点我报名"];
    fristBtn.backgroundColor = EDColor_BaseBlue;
    [fristBtn setTitleColor:[UIColor whiteColor] forState:0];
    [self.bgSc addSubview:fristBtn];
    self.fristBtn = fristBtn;
    KViewRadius(fristBtn, 4);
    
    self.topLab = [BBControl createLabelWithFrame:CGRectMake(10, 15, kScreenWidth-20, 45) Font:14 Text:@""];
    self.topLab.textAlignment = NSTextAlignmentCenter;
    self.topLab.backgroundColor = kRGBColor(245, 245, 245);
    self.topLab .hidden = YES;
    
    self.topLab .numberOfLines = 0;
    [self.bgSc addSubview:self.topLab];

    UIButton * fristBtn02 = [BBControl createButtonWithFrame:CGRectMake(kScreenWidth/2-100, self.topLab.bottom+15, 200, 45) target:self SEL:@selector(fristBtnClick02:) title:@"查看个人排行"];
    fristBtn02.backgroundColor = EDColor_BaseBlue;
    [fristBtn02 setTitleColor:[UIColor whiteColor] forState:0];
    [self.bgSc addSubview:fristBtn02];
    self.fristBtn02 = fristBtn02;
     KViewRadius(fristBtn02, 4);
    
    UIButton * fristBtn03 = [BBControl createButtonWithFrame:CGRectMake(kScreenWidth/2-100, fristBtn02.bottom+15, 200, 45) target:self SEL:@selector(fristBtn03Click:) title:@"查看奖金明细"];
    fristBtn03.backgroundColor = EDColor_BaseBlue;
    [fristBtn03 setTitleColor:[UIColor whiteColor] forState:0];
    [self.bgSc addSubview:fristBtn03];
     self.fristBtn03 = fristBtn03;
  KViewRadius(fristBtn03, 4);
    
    NSString *str = @"活动时间\n\n2019年07月01日——31日，每周六0:00~每周日23:00\n\n活动适用平台\n\n所有老虎机平台\n\n活动内容\n\n是金子总是会发光的，是英雄总是会出名的!只要您够牛，EDF打虎英雄榜帮您上头条，同时还可以获得最高3888元打虎英雄奖金，让您名利双收！\n每周六0:00点至周日23:00，凡是已报名个人打虎锦标赛的会员，打虎积分满588即可获得28发币，排名前100名可获得相应奖金，最高3888元！具体奖金明细可以在【个人中心】——【自助服务区】——【个人锦标赛】中查看。\n如果说组队打虎是考验人缘，那么个人打虎锦标赛就是展现个人实力的时候！为了彩金和荣誉而战吧，各位打虎英雄！！\n\n参加方式\n\n每周六0:00通过【个人中心】——【自助服务区】——【个人锦标赛】自助报名。报名后开始统计打虎积分并于每周日23:00:00（GMT+8 北京时间，以系统服务器时间为准)截止统计并核算是否达标，如达标则在下周一下午18:00自动派发奖金，请在【个人中心】——【福利领取中心】——【红利】中领取。\n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n\n1、参加个人打虎前需完成新人认证活动，严禁一人多号参加，一经查出，取消一切奖金，并封号处理；\n2、活动每周都可参加一次，打虎积分自报名成功开始自动统计，并于每周日晚23:00 截止统计；\n3、个人打虎积分为平台的有效投注额乘以相关的打虎系数，其计算公式为：打虎积分= PT平台*1.0 + PNG平台*1.2+PS平台*1.2+TTG平台*1.2+BSG平台*1.1+PP平台*1.2+ISB平台*1.2+MG平台*1.2+OG平台*1.2+AG平台*1.2+捕鱼达人*1.2+PSN平台*1.2+BBIN平台*0.8+CQ9平台*1.2，该积分仅用于该活动考核，原有的返水和发币等将不受影响；\n4、打虎奖金可投注于全部老虎机平台(除BBIN)，15倍流水即可提款，投注不计返水；\n5、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；\n6、此活动必须遵守 EDF壹定发一般规则与条款 (请详细阅读)。EDF壹定发保留在任何时候对本活动的修改、停止及最终解释权。";
    NSArray *arr = @[@"活动时间",@"活动适用平台",@"活动内容",@"参加方式",@"活动规则",@""];
    NSArray *arr2 = @[@"3888元",@"每周日23:00:00",@"严禁一人多号参加",@"每周日晚23:00",@"(除BBIN)",@"投注不计返水"];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, self.fristBtn03.bottom+30, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.bgSc addSubview:leb01];
    
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, leb01.bottom+50);
    


}

-(void)refushData{
    //Game_loadPersonalMatchInfo
    [self showLoadingAnimation];
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadPersonalMatchInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * infoDicc = [dict jsonDict:@"data"];
            YYLog(@"Game_loadPersonalMatchInfo---%@",dict);
            //PTimageGameURL
            
            dispatch_async(dispatch_get_main_queue(), ^{

                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [infoDicc jsonString:@"tips_txt"] ];
                
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 1.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attributedString.length)];
                self.topLab.attributedText = attributedString;
                self.topLab.textAlignment =NSTextAlignmentCenter;
                self.topLab.textColor = kRGBColor(68, 68, 68);
               //  [self.topLab sizeToFit];
               // "status": "not_allow", //not_allow-不可报名，allow-可报名，started-已开始
                if ([[infoDicc jsonString:@"status"] isEqualToString:@"not_allow"]) {
                    self.topLab.hidden = NO;
                    
                }else if ([[infoDicc jsonString:@"status"] isEqualToString:@"allow"]){
                   self.topLab.hidden = YES;
                }else{
                   self.topLab.hidden = NO;
                };
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [self.bgSc.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [self.bgSc.mj_header endRefreshing];
        [WJUnit showMessage:@"请求失败"];
        //[self getBalance];
        
    }];
    
    
}


-(void)fristBtnClick:(UIButton*)sender{
    //Game_loadPersonalMatchInfosignUpUserMatch
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadPersonalMatchInfosignUpUserMatch"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf refushData];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}
-(void)fristBtnClick02:(UIButton*)sender{
    TGWebViewController *web = [[TGWebViewController alloc] init];
    web.url =@"https://edfvip333.com/richman/m/team_tigerlist_m.html";
    //[encodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];;
    web.webTitle =@"排行榜";
    web.progressColor = EDColor_BaseBlue;
    [self.navigationController pushViewController:web animated:YES];
}
-(void)fristBtn03Click:(UIButton*)sender{
    EDTournameDetailViewController * vcc = [EDTournameDetailViewController new];
    vcc.title = @"个人竞标赛奖金明细";
    [self.navigationController pushViewController:vcc animated:YES];
}

@end

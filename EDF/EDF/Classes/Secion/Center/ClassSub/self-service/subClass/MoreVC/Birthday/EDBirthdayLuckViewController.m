//
//  EDBirthdayLuckViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBirthdayLuckViewController.h"
#import "EDRichTextLabel.h"
#import "TwoBtnsView.h"

@interface EDBirthdayLuckViewController ()
@property(nonatomic,strong)   UILabel * topLab;
@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)   UIImageView * topImageV;
@end

@implementation EDBirthdayLuckViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self refushData];
}

-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    self.topImageV = [BBControl createImageViewFrame:CGRectMake(kScreenWidth/2-45,30, 90, 90) imageName:@""];
    [self.bgSc addSubview:self.topImageV];
    
    self.topLab = [BBControl createLabelWithFrame:CGRectMake(10, self.topImageV.bottom+15, kScreenWidth, 40) Font:16 Text:@"生日礼金"];
    self.topLab.backgroundColor = [UIColor clearColor];
    self.topLab.textColor = kRGBColor(63, 63, 63);
    self.topLab.textAlignment = NSTextAlignmentCenter;
    [self.bgSc addSubview:self.topLab];
    
    
    
    NSString *str = @"活动时间\n\n2019年07月01日——31日\n\n活动内容\n\n您的生日是属于您和家人的重要日子，作为我们尊敬的会员，请您接受我们的真诚祝福！凡在生日前30天内存款累计满2000元的EDF正式会员，即可申请专属您的生日礼金。礼金内容如下：";
    NSArray *arr = @[@"活动时间",@"活动内容"];
    NSArray *arr2 = @[];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, self.topLab.bottom+20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.bgSc addSubview:leb01];
    
    UIView * numBgView = [BBControl createViewWithFrame:CGRectMake(10, leb01.bottom+20, kScreenWidth-20, 240)];
    KViewBorder(numBgView, kRGBColor(90, 90, 90), 1);
    [self.bgSc addSubview:numBgView];
    
    NSArray *dataArry = @[@[@"会员级别",@"生日礼金",],@[@"银冠会员",@"28元",],@[@"金冠VIP",@"38元",],@[@"王冠VIP",@"58元",],@[@"皇冠VIP",@"88元",],@[@"至尊VIP",@"188元",]];
    CGFloat  space = 0;
    CGFloat  btnH = 40;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry[i];
        TwoBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"TwoBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.leftLab.text = titles.firstObject;
        mapBtn.rightLab.text = titles.lastObject;
        [numBgView addSubview:mapBtn];
    }
    
    NSString *str22 = @"参加方式\n\n生日前30天内存款累计满2000元，在生日当天即可在【个人中心】——【福利领取中心】——【红利】中申请专属生日礼金，我们在审核通过后会立即派发，请在【个人中心】——【福利领取中心】——【红利】中领取。\n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n1、申请生日礼金前必须完成姓名、手机、邮箱和生日信息的绑定；\n2、对于多账号者、提供虚假生日信息等不诚信玩家，一律不予派发，取消一切红利；\n3、所有生日礼金需15倍流水投注于全部老虎机平台(除BBIN)，投注不计返水；\n4、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；\n5、此活动必须遵守 EDF壹定发一般规则与条款  (请详细阅读)。EDF壹定发保留在任何时候对本活动的修改、停止及最终解释权。";
    NSArray *arr22 = @[@"参加方式",@"活动规则"];
    NSArray *arr222 = @[@"除BBIN"];
    EDRichTextLabel * leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView.bottom+20, kScreenWidth-40, 500)];
    leb02.backgroundColor = kRGBColor(245, 245, 245);
    [leb02 rich_TextCofigWith:str22 blodStrArry:arr22 colortextArry:arr222];
    [leb02 sizeToFit];
    [self.bgSc addSubview:leb02];
    self.bgSc.contentSize = CGSizeMake(kScreenWidth, leb02.bottom+50);
    
    
}

-(void)refushData{
    
    //Game_getUserExclusiveCustomer
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_getUserExclusiveCustomer"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"Game_getUserExclusiveCustomer----%@",dict001);
            NSDictionary * dicc = [dict001 jsonDict:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.topLab.text = [NSString stringWithFormat:@"%@%@",[dicc jsonString:@"txt"],[dicc jsonString:@"remark"]];
                [self.topImageV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://111.177.18.197:8880/richman/home/upload/images/%@",[dicc jsonString:@"obj"]]] placeholderImage:coverPlaceholder];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
}
@end

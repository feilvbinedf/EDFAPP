//
//  TwoBtnsView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TwoBtnsView : UIView
@property (weak, nonatomic) IBOutlet UILabel *leftLab;

@property (weak, nonatomic) IBOutlet UILabel *rightLab;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@end

NS_ASSUME_NONNULL_END

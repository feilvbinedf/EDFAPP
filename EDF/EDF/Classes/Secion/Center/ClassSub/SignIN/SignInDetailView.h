//
//  SignInDetailView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignInDetailView : UIView

@property (weak, nonatomic) IBOutlet UILabel *infoLab;

@property (weak, nonatomic) IBOutlet UIView *bgview1;

@property (weak, nonatomic) IBOutlet UIView *bgview2;
@property (weak, nonatomic) IBOutlet UIView *btnsBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnsHeight;

@property (weak, nonatomic) IBOutlet UILabel *content001;

@property (weak, nonatomic) IBOutlet UIButton *weekSignInBtn;


@end

NS_ASSUME_NONNULL_END

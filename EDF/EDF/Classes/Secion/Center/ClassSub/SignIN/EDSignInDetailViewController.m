//
//  EDSignInDetailViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDSignInDetailViewController.h"
#import "SignInDetailView.h"
@interface EDSignInDetailViewController ()
@property(nonatomic,strong)UIScrollView * bgScrollew;
@property(nonatomic,strong)SignInDetailView * infoView;
@end

@implementation EDSignInDetailViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
//    self.vipHeader.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
//
     self.infoView.frame = CGRectMake(0, 0, kScreenWidth, 2140);
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    [self requestQiandaoInfo];

}

-(void)creatUI{
    self.bgScrollew = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight)];
    self.bgScrollew.backgroundColor = kRGBColor(245, 245, 245);
        [self.view addSubview:self.bgScrollew];
    self.infoView = [[NSBundle mainBundle]loadNibNamed:@"SignInDetailView" owner:self options:nil].lastObject;
    self.infoView.frame = CGRectMake(0, 0, kScreenWidth, 2140);
    [self.bgScrollew addSubview:self.infoView];
    self.bgScrollew.contentSize = CGSizeMake(kScreenWidth, self.infoView.height);
    WEAKSELF
    [self.infoView.weekSignInBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf weekSignInBtn:strongSelf.infoView.weekSignInBtn];
    }];
}

-(void)weekSignInBtn:(UIButton*)sender{
  //  singin_onGetSignActiveBonus
     [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"singin_onGetSignActiveBonus"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
           
                dispatch_async(dispatch_get_main_queue(), ^{
                    ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                    [toast showLXAlertView];
                    [strongSelf.infoView.weekSignInBtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                    self.infoView.weekSignInBtn.enabled = NO;
                });
       
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
-(void)requestQiandaoInfo{
    //loadDailySignPromotion
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadDailySignPromotion"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                [strongSelf config_DataWith:dict001];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    NSString  * url22 = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"loadDailySignActivePromotion"];
    [[SmileHttpTool sharedInstance] GET :url22 parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary  * dataDicc = [dict001 jsonDict:@"data"];
                if ([[dataDicc jsonString:@"status"] isEqualToString:@"valid"]) {
                     [self.infoView.weekSignInBtn setBackgroundColor:kRGBColor(44, 143, 219) forState:0];
                    self.infoView.weekSignInBtn.enabled = YES;
                }else if ([[dataDicc jsonString:@"status"] isEqualToString:@"invalid"]){
                    [self.infoView.weekSignInBtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                    self.infoView.weekSignInBtn.enabled = NO;
                }else{
                    [self.infoView.weekSignInBtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                    self.infoView.weekSignInBtn.enabled = NO;
                };
                self.infoView.content001.text = [dataDicc jsonString:@"tips_txt"];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}
#define Start_X          30.0f      // 第一个按钮的X坐标
#define Start_Y          15.0f     // 第一个按钮的Y坐标
#define Width_Space      45.0f      // 2个按钮之间的横间距
#define Height_Space     15.0f     // 竖间距
#define Button_Height   35.0f    // 高
#define Button_Width    75.0f    // 宽

-(void)config_DataWith:(NSDictionary *)dataDicc{
    NSDictionary * infoDicc = [dataDicc jsonDict:@"data"];
    //self.infoView.infoLab.text = [infoDicc jsonString:@"tips_txt"];
    NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [infoDicc jsonString:@"tips_txt"] ];

    NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                              NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                              };
    NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
    paragraphStyle.lineSpacing = 5.0;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
    self.infoView.infoLab.attributedText = attributedString;
    self.infoView.infoLab.textAlignment =NSTextAlignmentCenter;
   // [self.infoView.infoLab sizeToFit];
    
    [self.infoView.btnsBg.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray * btnsArry = [infoDicc jsonArray:@"bonus_list"];
    
    self.modelsArry = [NSMutableArray arrayWithArray:btnsArry];
       CGFloat  space = 10;
       CGFloat  btnH = 35;
    CGFloat  btnW = (kScreenWidth-60-90)/3;
    
        for (int i = 0 ; i < btnsArry.count; i++) {
            NSDictionary * dicc = [btnsArry objectAtIndex:i ];
            NSInteger index = i % 3;
            NSInteger page = i / 3;
            // 圆角按钮
            UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            mapBtn.tag = i;//这句话不写等于废了
            mapBtn.frame = CGRectMake(index * (btnW + Width_Space) + Start_X, page  * (Button_Height + Height_Space)+Start_Y, btnW, Button_Height);
            [mapBtn setTitle:[dicc jsonString:@"bonus"] forState:0];
            [mapBtn setTitleColor:[UIColor whiteColor] forState:0];
            KViewRadius(mapBtn, 4);
            if ([[dicc jsonString:@"status"] isEqualToString:@"valid"]) {
                [mapBtn setBackgroundColor:kRGBColor(44, 143, 219) forState:0];
                 mapBtn.enabled = YES;
            }else if ([[dicc jsonString:@"status"] isEqualToString:@"invalid"]){
                  [mapBtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                //mapBtn.enabled = NO;
            }else{
               //finish
                  [mapBtn setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
               // mapBtn.enabled = NO;
            }
            if (i==btnsArry.count-1) {
                self.infoView.btnsHeight.constant = mapBtn.bottom+15;
            }
            [mapBtn addTarget:self action:@selector(mapBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            [self.listBtnArry  addObject:mapBtn];
            [self.infoView.btnsBg addSubview:mapBtn];

        }

}
-(void)mapBtnClick:(UIButton *)sender{
    
   // NSDictionary * dicc = self.modelsArry[sender.tag];
   // singin_onGetSignBonus
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"singin_onGetSignBonus"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [sender setBackgroundColor:kRGBColor(161, 161, 161) forState:0];
                sender.enabled = NO;
                [self requestQiandaoInfo]; 
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    
    
}
@end

//
//  UserVipInfoView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserVipInfoView : UIView

@property (weak, nonatomic) IBOutlet UILabel *vipText;
@property (weak, nonatomic) IBOutlet UILabel *cashNum;
@property (weak, nonatomic) IBOutlet UILabel *fabiNum;
@property (weak, nonatomic) IBOutlet UILabel *infoContent;
@property (weak, nonatomic) IBOutlet UILabel *rengLab;
@property (weak, nonatomic) IBOutlet UILabel *rengBiLab;
@property (weak, nonatomic) IBOutlet UILabel *uplab;
@property (weak, nonatomic) IBOutlet UILabel *upBiLab;

@end

NS_ASSUME_NONNULL_END

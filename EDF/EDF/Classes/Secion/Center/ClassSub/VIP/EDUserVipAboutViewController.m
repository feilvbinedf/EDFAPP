//
//  EDUserVipAboutViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDUserVipAboutViewController.h"
#import "UserVipInfoView.h"

@interface EDUserVipAboutViewController ()<UINavigationControllerDelegate>
@property(nonatomic,strong)UserVipInfoView* vipHeader;
@end

@implementation EDUserVipAboutViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.vipHeader.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    [self creatUI];
    [self refushVipRequest];
}

-(void)creatUI{
    
    self.vipHeader = [[NSBundle mainBundle]loadNibNamed:@"UserVipInfoView" owner:self options:nil].lastObject;
    self.vipHeader.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    [self.view addSubview:self.vipHeader];
    
}

-(void)refushVipRequest{
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_GetgetgetVipInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        NSDictionary * dict = [dict001 jsonDict:@"data"];
        NSLog(@"responseObjectStr----%@",responseObjectStr);
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            strongSelf.vipHeader.rengLab.text = [dict jsonString:@"up_dep"];
            strongSelf.vipHeader.rengBiLab.text = [dict jsonString:@"up_point"];
              strongSelf.vipHeader.uplab.text = [dict jsonString:@"keep_dep"];
            strongSelf.vipHeader.upBiLab.text = [dict jsonString:@"keep_point"];
              strongSelf.vipHeader.vipText.text = [dict jsonString:@"vipname"];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:2 andImageName:@"error" andTitleStr:@"领取失败" andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
    
    
}

@end

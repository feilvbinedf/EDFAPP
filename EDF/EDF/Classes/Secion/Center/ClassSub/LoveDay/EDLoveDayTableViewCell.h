//
//  EDLoveDayTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlignmentLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDLoveDayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIAlignmentLabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@end

NS_ASSUME_NONNULL_END

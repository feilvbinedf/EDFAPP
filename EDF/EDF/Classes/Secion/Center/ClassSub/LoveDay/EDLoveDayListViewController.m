//
//  EDLoveDayListViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDLoveDayListViewController.h"
#import "EDLoveDayTableViewCell.h"
@interface EDLoveDayListViewController ()
@end

@implementation EDLoveDayListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    
    [self ks_tableAddHeaderRequstRefush];
}
-(void)getData_Refush{
    [self Game_loadNewPlayerPromotion];
}
-(void)creatUI{
    
    UILabel * toplab = [BBControl createLabelWithFrame:CGRectMake(0, 0, kScreenWidth, 50) Font:16 Text:@"真情实意,真金白银,只送给懂EDF的你"];
    toplab.numberOfLines = 0;
    toplab.textAlignment = NSTextAlignmentCenter;
    toplab.backgroundColor = kRGBColor(245, 245, 245);
    toplab.textColor = EDColor_BaseBlue;
    
    self.ks_TableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    self.ks_TableView.tableHeaderView = toplab;
    
    
}

-(void)Game_loadNewPlayerPromotion{
    //Game_loadNewPlayerPromotion
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Game_loadNewPlayerPromotion"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [strongSelf.modelsArry removeAllObjects];
            YYLog(@"Game_loadNewPlayerPromotion---%@",dict);
            NSArray * dataArry = [dict jsonArray:@"data"];
            for (NSDictionary * dataDicc in dataArry) {
                [strongSelf.modelsArry addObject:dataDicc];
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
        [strongSelf.ks_TableView.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        [self.ks_TableView.mj_header endRefreshing];
        [WJUnit showMessage:@"获取活动请求失败"];
        //[self getBalance];
        
    }];

}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDLoveDayTableViewCell";
    EDLoveDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDLoveDayTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDLoveDayTableViewCell" owner:self options:nil] lastObject];
    }
    
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160;
}

-(void)stepCell:(EDLoveDayTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    NSDictionary * userDicc = self.modelsArry[indexPath.row];

    NSString * status = [userDicc jsonString:@"status"];
    if ([status isEqualToString:@"valid"]) {
        [cell.actionBtn setBackgroundColor:EDColor_BaseBlue forState:0];
        cell.actionBtn.enabled =YES;
    }else{
        [cell.actionBtn setBackgroundColor:[UIColor lightGrayColor] forState:0];
        cell.actionBtn.enabled =NO;
    }

    [cell.actionBtn setTitle:[NSString stringWithFormat:@"%@",[[userDicc jsonString:@"name"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]] forState:0];
    cell.contentLab.text = [NSString stringWithFormat:@"%@\n\n%@",[userDicc jsonString:@"tips_txt"],[userDicc jsonString:@"other"]];
    
    [cell.actionBtn add_BtnClickHandler:^(NSInteger tag) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate shareAppdelegate].index = 0;
            NSMutableDictionary * infoDicc = [NSMutableDictionary dictionary];
            [infoDicc setValue:[userDicc jsonString:@"id"] forKey:@"loveId"];
            [AppDelegate shareAppdelegate].loveHuodongID = [userDicc jsonString:@"id"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"loveHuodongAction" object:nil userInfo:infoDicc];
            self.navigationController.tabBarController.selectedIndex = 1;
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }];
}


@end

//
//  EDLoveDayTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDLoveDayTableViewCell.h"

@implementation EDLoveDayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    KViewRadius(self.actionBtn, 4);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

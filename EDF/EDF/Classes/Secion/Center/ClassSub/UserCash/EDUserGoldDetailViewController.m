//
//  EDUserGoldDetailViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDUserGoldDetailViewController.h"
#import "EDExchangeCenterViewController.h"
@interface EDUserGoldDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *goldLab;
@property (weak, nonatomic) IBOutlet UILabel *gonum;
@property (weak, nonatomic) IBOutlet UIButton *duihuanBtn;

@end

@implementation EDUserGoldDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refushMyInfoData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self refushMyInfoData];
    

    KViewRadius(self.duihuanBtn, 4);
}
-(void)refushMyInfoData{
    //myUserNavigationDatd
    //  [self showLoadingAnimation];
    if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
        self.infoDicc =dict;
        self.goldLab.text = [NSString stringWithFormat:@"您的总资产  %@元",[self.infoDicc jsonString:@"d_property"]];
        self.gonum.text = [NSString stringWithFormat:@"%@枚",[self.infoDicc jsonString:@"coin"]];
    }
    [self.ks_TableView reloadData];
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [[DJLoginHelper sharedInstance]save_UserInfo:[dict jsonDict:@"data"]];
            self.infoDicc =[dict jsonDict:@"data"];
            self.goldLab.text = [NSString stringWithFormat:@"您的总资产  %@元",[self.infoDicc jsonString:@"d_property"]];
            self.gonum.text = [NSString stringWithFormat:@"%@枚",[self.infoDicc jsonString:@"coin"]];
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [self stopLoadingAnimation];
    }];
    
}

- (IBAction)goldNumBtnClick:(UIButton *)sender {
    
    
    EDExchangeCenterViewController * exchangeVC = [EDExchangeCenterViewController new];
    exchangeVC.title = @"发币兑换";
    [self.navigationController pushViewController:exchangeVC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

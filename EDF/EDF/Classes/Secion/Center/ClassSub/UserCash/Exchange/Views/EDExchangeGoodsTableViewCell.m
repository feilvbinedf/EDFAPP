//
//  EDExchangeGoodsTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDExchangeGoodsTableViewCell.h"

@implementation EDExchangeGoodsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        [self.duihuanBtn setBackgroundColor:EDColor_BaseBlue forState:0];
}

-(void)setModell:(EDExchangeModel *)modell{
    _modell = modell;
    [self.coverimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://111.177.18.197:8880/richman/home/images/pointgoodspic/%@",modell.pic]] placeholderImage:coverPlaceholder];

    self.namelab.text =  [modell.name stringByReplacingOccurrencesOfString:@"%20" withString:@" "];;
    self.numberLab.text = [NSString stringWithFormat:@"%ld",[modell.point integerValue]];
    if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
//        if ([dict jsonInteger:@"coin"]<=[modell.point integerValue]) {
//            [self.duihuanBtn setBackgroundColor:[UIColor lightGrayColor] forState:0];
//            self.duihuanBtn.enabled =NO;
//        }else{
//            [self.duihuanBtn setBackgroundColor:EDColor_BaseBlue forState:0];
//            self.duihuanBtn.enabled =YES;
//        }
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

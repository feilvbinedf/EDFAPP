//
//  EDExchangeCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/15.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDExchangeCenterViewController.h"
#import "XXPageTabView.h"
#import "EDExchangeLevelViewController.h"
#import "EDExchangeGoodsViewController.h"
@interface EDExchangeCenterViewController ()
<XXPageTabViewDelegate>

{
    XXPageTabView *_pageTabView;
}
@property(nonatomic,strong)EDExchangeLevelViewController * vc001;
@property(nonatomic,strong)EDExchangeGoodsViewController * vc002;

@end

@implementation EDExchangeCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatUI];
    
}
-(void)creatUI{
      self.view.backgroundColor =kRGBColor(245, 245, 245);
    self.vc001 = [EDExchangeLevelViewController new];
    self.vc002 = [EDExchangeGoodsViewController new];
    [self addChildViewController:self.vc001];
    [self addChildViewController:self.vc002];
    
    _pageTabView = [[XXPageTabView alloc] initWithChildControllers:self.childViewControllers childTitles:@[@"筹码兑换区", @"礼品兑换区"]];
    _pageTabView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    _pageTabView.tabSize = CGSizeMake(kScreenWidth, 50);
    _pageTabView.tabItemFont = AppFont(15);
    _pageTabView.unSelectedColor = kRGBColor(161, 161, 161);
    _pageTabView.selectedColor = kRGBColor(44, 143, 219);
    _pageTabView.bodyBounces = NO;
    _pageTabView.tabBackgroundColor = [UIColor whiteColor];
    _pageTabView.titleStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.indicatorStyle = XXPageTabIndicatorStyleFollowText;
    _pageTabView.delegate = self;
    _pageTabView.indicatorHeight = 3;
    _pageTabView.backgroundColor = [UIColor whiteColor];
    _pageTabView.bodyBackgroundColor= [UIColor whiteColor];
    [self.view addSubview:_pageTabView];
    
    
}
#pragma mark - XXPageTabViewDelegate
- (void)pageTabViewDidEndChange {
    NSInteger selectedTabIndex = _pageTabView.selectedTabIndex;
    
}

@end

//
//  EDExchangeGoodsViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/15.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDExchangeGoodsViewController.h"
#import "EDExchangeGoodsTableViewCell.h"
#import "ExchangeHeaderView.h"

@interface EDExchangeGoodsViewController ()

@property(nonatomic,assign)NSInteger pageNum;
@property (nonatomic,strong) ExchangeHeaderView * topHeaderView;
@property (nonatomic,strong) NoNetworkView * workView;

@end
@implementation EDExchangeGoodsViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.topHeaderView.frame = CGRectMake(0, 0, kScreenWidth, 50);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.modelsArry = [NSMutableArray array];
    self.pageNum = 0;
    [self creatUI];
    [self ks_tableAddHeaderRequstRefush];
}
-(void)creatUI{
    self.topHeaderView = [[NSBundle mainBundle]loadNibNamed:@"ExchangeHeaderView" owner:self options:nil].lastObject;
    self.topHeaderView.frame = CGRectMake(0, 0, kScreenWidth, 50);
    [self.view addSubview:self.topHeaderView];
    self.ks_TableView.frame = CGRectMake(0, self.topHeaderView.bottom, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-100);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
}
-(void)getData_Refush{
    self.pageNum = 0;
    [self refushData:YES];
    [self refushMyInfoData];
}

-(void)refushMyInfoData{
    //myUserNavigationDatd
    //[self showLoadingAnimation];
    if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
        self.topHeaderView.vipText.text =[NSString stringWithFormat:@"%@",[dict jsonString:@"vip"]];
        self.topHeaderView.fabiNum.text =[NSString stringWithFormat:@"%@",[dict  jsonString:@"coin"]];
    }
    [self.ks_TableView reloadData];
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [[DJLoginHelper sharedInstance]save_UserInfo:[dict jsonDict:@"data"]];
            strongSelf.topHeaderView.fabiNum.text =[NSString stringWithFormat:@"%@",[[dict jsonDict:@"data"]  jsonString:@"coin"]];
            strongSelf.topHeaderView.vipText.text =[NSString stringWithFormat:@"%@",[[dict jsonDict:@"data"]  jsonString:@"vip"]];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [self stopLoadingAnimation];
    }];
    
}
-(void)refushData:(BOOL)isRefush{
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"UserCenter_getGoodsInfo22"];
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:@(self.pageNum) forKey:@"curpage"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        if (isRefush ==YES) {
            [strongSelf.modelsArry removeAllObjects];
        }
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [strongSelf.modelsArry removeAllObjects];
            NSArray *  dataArr = [dict001 jsonArray:@"data"];
            
            for (NSDictionary *modelDicc  in dataArr) {
                                    EDExchangeModel * modell = [[EDExchangeModel alloc]initWithDictionary:modelDicc error:nil];
                [strongSelf.modelsArry addObject:modell];

            }
            
        }else{
            [JMNotifyView showNotify:[dict001 jsonString:@"message"]  isSuccessful:NO];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self placeholderViewWithFrame:self.ks_TableView.frame NoNetwork:NO];
        });
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [strongSelf.ks_TableView reloadData];
        NSLog(@"UserCenter_getChipInfo22---%@",dict001);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [strongSelf.ks_TableView.mj_header endRefreshing];
        [WJUnit showMessage:@"网络错误"];
        //  [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return self.modelsArry.count;
    
}
-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDExchangeGoodsTableViewCell";
    EDExchangeGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDExchangeGoodsTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDExchangeGoodsTableViewCell" owner:self options:nil] lastObject];
    }

    cell.modell = self.modelsArry[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = kRGBColor(245, 245, 245);;
      cell.backgroundColor = kRGBColor(245, 245, 245);;
    WEAKSELF
    [cell.duihuanBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf duihuanClick:indexPath];
    }];
    return cell;
    
}
-(void)duihuanClick:(NSIndexPath*)indexptah{
    EDExchangeModel * model = self.modelsArry[indexptah.row];
    //  Center_exchangeChip
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Center_exchangeGoods"];
    NSMutableDictionary * jsonDicc = [NSMutableDictionary dictionary];
    [jsonDicc setValue:model.id forKey:@"id"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:jsonDicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self refushMyInfoData];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 350;
}

- (void)placeholderViewWithFrame:(CGRect)frame NoNetwork:(BOOL)NoNetwork
{
    if (self.modelsArry.count == 0)
    {
        [_workView removeFromSuperview];
        _workView = [[NoNetworkView alloc] initWithFrame:frame NoNetwork:NoNetwork];
        [self.ks_TableView addSubview:_workView];
    }else
    {
        [_workView dissmiss];
    }
}

@end

//
//  ExchangeHeaderView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import "ExchangeHeaderView.h"

@implementation ExchangeHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    KViewRadius(self.bgView1, 3);
    KViewRadius(self.bgView2, 3);
}

@end

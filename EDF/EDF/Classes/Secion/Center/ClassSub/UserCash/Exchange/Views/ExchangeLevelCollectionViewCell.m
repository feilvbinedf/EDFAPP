//
//  ExchangeLevelCollectionViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/15.
//  Copyright © 2019 p. All rights reserved.
//

#import "ExchangeLevelCollectionViewCell.h"

@implementation ExchangeLevelCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    KViewRadius(self.iconImage, 20);
}

-(void)setModell:(EDExchangeModel *)modell{
    _modell = modell;
    
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://111.177.18.197:8880/richman/home/images/pointgoodspic/%@",modell.pic]] placeholderImage:coverPlaceholder];
      self.nameLab.text =  [modell.name stringByReplacingOccurrencesOfString:@"%20" withString:@" "];;
    
    NSArray *array = [modell.priceStr componentsSeparatedByString:@"%"];
    if (array.count>0) {
        
        for (int i=0 ; i<array.count; i++) {
            UILabel * lab = [self viewWithTag:i+100];
            lab.text = array[i];
        }
    }
    

    
}
@end

//
//  EDExchangeGoodsTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDExchangeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDExchangeGoodsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *coverimage;
@property (weak, nonatomic) IBOutlet UIButton *duihuanBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *namelab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property(nonatomic,strong)EDExchangeModel * modell;
@end

NS_ASSUME_NONNULL_END

//
//  EDExchangeModel.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/16.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDExchangeModel : EDBaseModel

@property(nonatomic,copy)NSString<Optional> * discount;
@property(nonatomic,copy)NSString<Optional> * promotion_id;
@property(nonatomic,copy)NSString<Optional> * pic;
@property(nonatomic,copy)NSString<Optional> * type;
@property(nonatomic,copy)NSString<Optional> * priceStr;
@property(nonatomic,copy)NSString<Optional> * price;
@property(nonatomic,copy)NSString<Optional> * ample;
@property(nonatomic,copy)NSString<Optional> * name;
@property(nonatomic,copy)NSString<Optional> * goods_num;
@property(nonatomic,copy)NSString<Optional> * id;
@property(nonatomic,copy)NSString<Optional> * discount_end;
@property(nonatomic,copy)NSString<Optional> * discount_start;
@property(nonatomic,copy)NSString<Optional> * status;
@property(nonatomic,strong)NSNumber *  point;

//"discount": "",
//"promotion_id": "aJ7AWs9IS1K1POt7rC94Yg",
//"pic": "20chip.png",
//"type": "筹码",
//"point": "200",
//"priceStr": "银冠会员：200发币%金冠VIP：196发币%王冠VIP：192发币%皇冠VIP：184发币%至尊VIP：160发币",
//"price": "200%196%192%184%160",
//"ample": "",
//"name": "20元筹码",
//"goods_num": "",
//"id": "iBeqZ9zPTDK1eLFdioBBdg",
//"discount_end": "",
//"discount_start": "",
//"status": "上线"




    

@end

NS_ASSUME_NONNULL_END

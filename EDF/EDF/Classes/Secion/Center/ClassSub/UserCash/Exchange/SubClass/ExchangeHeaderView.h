//
//  ExchangeHeaderView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/27.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExchangeHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *vipText;

@property (weak, nonatomic) IBOutlet UILabel *fabiNum;
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@end

NS_ASSUME_NONNULL_END

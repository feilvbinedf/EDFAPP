//
//  ExchangeLevelCollectionViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/15.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDExchangeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ExchangeLevelCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property(nonatomic,strong)EDExchangeModel * modell;
@property (weak, nonatomic) IBOutlet UIButton *duihuanBtn;

@property (weak, nonatomic) IBOutlet UILabel *lab1;

@property (weak, nonatomic) IBOutlet UILabel *lab2;

@property (weak, nonatomic) IBOutlet UILabel *lab3;
@property (weak, nonatomic) IBOutlet UILabel *lab4;
@property (weak, nonatomic) IBOutlet UILabel *lab5;

@end

NS_ASSUME_NONNULL_END

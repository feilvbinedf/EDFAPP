//
//  EDUserGoldDetailViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDUserGoldDetailViewController : EDBaseViewController

@property(nonatomic,strong)NSDictionary * infoDicc;
@end

NS_ASSUME_NONNULL_END

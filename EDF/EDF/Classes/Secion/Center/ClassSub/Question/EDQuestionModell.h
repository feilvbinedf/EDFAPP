//
//  EDQuestionModell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDQuestionModell : EDBaseModel

@property(nonatomic,copy)NSString<Optional> * question;
@property(nonatomic,copy)NSString<Optional> * bankName;
@property(nonatomic,strong)NSNumber *  number;
@property(nonatomic,strong)NSNumber *  id;



@end

NS_ASSUME_NONNULL_END

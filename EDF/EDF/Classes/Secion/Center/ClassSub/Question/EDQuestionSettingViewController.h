//
//  EDQuestionSettingViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDQuestionSettingViewController : EDBaseViewController

@property(nonatomic,assign) NSInteger  showType;  //0 设置安全问题   1.验证安全问题

@property (nonatomic, copy) void (^dismiss)(BOOL isSend);


@end

NS_ASSUME_NONNULL_END

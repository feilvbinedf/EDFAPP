//
//  EDTigerFitViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/28.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDTigerFitViewController.h"
#import "TigerCreatActionView.h"
#import "TigerTeamMenmersInfoView.h"
#import "TigerTeamStaringView.h"
#import "EDRichTextLabel.h"
#import "ThreeBtnsView.h"

@interface EDTigerFitViewController ()
@property(nonatomic,strong)UIScrollView * bgSc;
@property(nonatomic,strong)   UILabel * topLab;
@property(nonatomic,strong)   UIButton  * more_CreatBtn;
@property(nonatomic,strong)   TigerCreatActionView * creat_View;
@property(nonatomic,strong)   TigerTeamMenmersInfoView * menmers_View;
@property(nonatomic,strong)   TigerTeamStaringView * staring_View;
@property(nonatomic,strong)   UIView * textInfoView;

@end

@implementation EDTigerFitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatUI];
    [self requestTypeInfo];
}
-(void)creatUI{
    self.bgSc = [[UIScrollView alloc]init];
    self.bgSc.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.bgSc.backgroundColor = kRGBColor(245, 245, 245);
    [self.view addSubview:self.bgSc];
    
    self.topLab = [BBControl createLabelWithFrame:CGRectMake(10, 15, kScreenWidth-20, 45) Font:14 Text:@""];
    self.topLab.textAlignment = NSTextAlignmentCenter;
    self.topLab.backgroundColor = kRGBColor(245, 245, 245);
    self.topLab .numberOfLines = 0;
    [self.bgSc addSubview:self.topLab];
    
    self.creat_View = [[NSBundle mainBundle]loadNibNamed:@"TigerCreatActionView" owner:self options:nil].lastObject;
    self.creat_View.frame = CGRectMake(0, self.topLab.bottom+5, kScreenWidth, 0);
    self.creat_View.hidden = YES;
    [self.bgSc addSubview:self.creat_View];

    WEAKSELF
    [self.creat_View.sureBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf creatTeamsRequest];
    }];
    
    
    self.menmers_View  = [[NSBundle mainBundle]loadNibNamed:@"TigerTeamMenmersInfoView" owner:self options:nil].lastObject;
    self.menmers_View.frame = CGRectMake(0, self.creat_View.bottom, kScreenWidth, 0);
    self.menmers_View.hidden = YES;
    [self.bgSc addSubview:self.menmers_View];
    
    [self.menmers_View.cancelBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf cancel_teamRequest];
    }];
    
    [self.menmers_View.sureBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf User_passTeamTiger];
    }];
    
    [self.menmers_View.dissBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf cancel_teamRequest];
    }];
    
    
    self.more_CreatBtn  = [BBControl createButtonWithFrame:CGRectMake(50, self.menmers_View.bottom, kScreenWidth-100, 0) target:self SEL:@selector(more_CreatBtnClick:) title:@"创建战队"];
    self.more_CreatBtn.hidden = YES;
    KViewRadius(self.more_CreatBtn, 4);
    [self.more_CreatBtn setTitleColor:[UIColor whiteColor] forState:0];
    [self.more_CreatBtn setBackgroundColor:EDColor_BaseBlue forState:0];
    [self.bgSc addSubview:self.more_CreatBtn];
    

    
    self.staring_View  = [[NSBundle mainBundle]loadNibNamed:@"TigerTeamStaringView" owner:self options:nil].lastObject;
    self.staring_View.frame = CGRectMake(0, self.creat_View.bottom, kScreenWidth, 0);
    self.staring_View.hidden = YES;
    [self.bgSc addSubview:self.staring_View];
    

    self.textInfoView = [BBControl createViewWithFrame:CGRectMake(0, self.creat_View.bottom+20, kScreenWidth, 300)];
    self.textInfoView.backgroundColor = kRGBColor(245, 245, 245);
    [self.bgSc addSubview:self.textInfoView];
    
    NSString *str = @"活动时间\n\n2019年07月01日——31日，每周一0:00至每周五23:00\n\n活动适用平台\n\n活动内容\n\n组队打虎再次升级，奖励更高、获奖更易！独乐乐不如众乐乐，快邀请您的亲朋好友一起来EDF组队打虎吧，享受与好友一起并肩战斗的乐趣！每周组队打虎积分达标，每人可获得高达1388元的打虎英雄奖励！\n\n活动奖励如下：";
    NSArray *arr = @[@"活动时间",@"活动适用平台",@"活动内容"];
    NSArray *arr2 = @[@"“1388元”"];
    EDRichTextLabel * leb01 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, 20, kScreenWidth-40, 500)];
    leb01.backgroundColor = kRGBColor(245, 245, 245);
    [leb01 rich_TextCofigWith:str blodStrArry:arr colortextArry:arr2];
    [leb01 sizeToFit];
    [self.textInfoView addSubview:leb01];
    
    UIView * numBgView = [BBControl createViewWithFrame:CGRectMake(10, leb01.bottom+20, kScreenWidth-20, 240)];
    KViewBorder(numBgView, kRGBColor(90, 90, 90), 1);
    numBgView.backgroundColor = [UIColor clearColor];
    [self.textInfoView addSubview:numBgView];
    
    NSArray *dataArry = @[@[@"组队级别",@"积分要求",@"打虎奖金"],@[@"入门级",@"8888*3",@"38*3"],@[@"新手级",@"38888*3",@"188*3"],@[@"挑战级",@"88888*3",@"588*3"],@[@"精英级",@"188888*3",@"888*3"],@[@"精英级",@"388888*3",@"1388*3"]];
    CGFloat  space = 0;
    CGFloat  btnH = 40;
    for (int i = 0 ; i < dataArry.count; i++) {
        NSInteger index = i % 1;
        NSInteger page = i / 1;
        NSArray * titles=dataArry[i];
        ThreeBtnsView*mapBtn =[[NSBundle mainBundle]loadNibNamed:@"ThreeBtnsView" owner:self options:nil].lastObject;
        mapBtn.frame = CGRectMake(index * (kScreenWidth-20 ), page  * (40 + space), kScreenWidth-20 , btnH);
        mapBtn.lab01.text = titles.firstObject;
        mapBtn.lab02.text = titles[1];
        mapBtn.lab03.text = titles.lastObject;
        [numBgView addSubview:mapBtn];
    }
    
    NSString *str22 = @"每周六下午14:00-18:00系统自动派发，如已达标无需申请。派奖时，按团队队员达标最低对应的级别派奖，如果团队三人，二人达到精英级，一人仅达到挑战级，那三人的奖金均为挑战级奖金588元。组队打虎考验的是团队作战，因此选择队友十分重要！\n\n参加方式\n\n每周一0:00开始报名，会员们在组队确定后，由队长在【个人中心】——【自助服务区】——【组队打虎】自助报名，在其他两名队友都同意接受邀请后，即算完成报名。报名后开始统计打虎积分并于 每周五23:00:00（GMT+8 北京时间)截止统计并核算是否达标，如达标每周六下午18:00系统自动派发，请在【个人中心】——【福利领取中心】——【红利】中领取。\n\n活动规则\n\nEDF壹定发会定期与不定期举办各种优惠活动，在您参与前，您必须完全同意以下规则和履行以下义务：\n\n1、参加组队打虎前需完成新人认证活动，且必须为真实好友，严禁一人多号参加，一经查出，取消一切奖金，并封号处理；\n\n2、组队由队长发起邀请，当两位好友全部接受邀请后，报名自动成功，团队积分自报名成功开始自动统计，并于；每周五晚23:00截止统计\n\n3、组队打虎活动获得的奖金根据所有队员达标的最低级别进行派奖，奖金可投注于全部老虎机平台(除BBIN)，15倍流水即可提款，投注不计返水；\n\n4、组队打虎积分为平台的有效投注额乘以相关的打虎系数，其计算公式为：打虎积分= PT平台*1.0 + PNG平台*1.2+PS平台*1.2+TTG平台*1.2+BSG平台*1.1+PP平台*1.2+ISB平台*1.2+MG平台*1.2+OG平台*1.2+AG平台*1.2+捕鱼达人*1.2+PSN平台*1.2+BBIN平台*0.8+CQ9平台*1.2，该积分仅用于该活动考核，原有的返水和发币等将不受影响；\n\n5、活动的报名、参赛、弃权等行为均为团队内部协商决定，EDF壹定发不介入相关纠纷调解，一切结果以系统记录的数据为准；\n\n6、本优惠是特别为EDF壹定发在线娱乐玩家而设，如发现任何滥用优惠活动的个人或团体等尝试或进行恶意的欺诈行为，以利用红利为获得利润为目的，公司将对违规帐户进行冻结并索回红利及所有盈利；\n\n7、此活动必须遵守 EDF壹定发一般规则与条款 (请详细阅读)。EDF壹定发保留在任何时候对本活动的修改、停止及最终解释权。\n";
    NSArray *arr22 = @[@"参加方式",@"活动规则"];
    NSArray *arr222 = @[@"【个人中心】——【自助服务区】——【组队打虎】",@"每周五23:00:00",@"严禁一人多号参加",@"每周五晚23:00",@"(除BBIN)",@"投注不计返水"];
    EDRichTextLabel * leb02 = [[EDRichTextLabel alloc]initWithFrame:CGRectMake(20, numBgView.bottom+20, kScreenWidth-40, 500)];
    leb02.backgroundColor = kRGBColor(245, 245, 245);
    [leb02 rich_TextCofigWith:str22 blodStrArry:arr22 colortextArry:arr222];
    [leb02 sizeToFit];
    [self.textInfoView addSubview:leb02];
    self.textInfoView.height =leb02.bottom+20;
   self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
    
    
}
-(void)more_CreatBtnClick:(UIButton*)sender{
    //User_confirmFailInfo
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_confirmFailInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            // YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                strongSelf.creat_View.teamname.text = @"";
                 strongSelf.creat_View.peo01.text = @"";
                 strongSelf.creat_View.peo02.text = @"";
                [self requestTypeInfo];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}

-(void)User_passTeamTiger{
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_passTeamTiger"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            // YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self requestTypeInfo];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}
-(void)cancel_teamRequest{
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_onrefuseTeamTiger"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            // YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self requestTypeInfo];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
-(void)creatTeamsRequest{
    [self.view endEditing:YES];
    if (kStringIsEmpty(self.creat_View.teamname.text)) {
        
        [JMNotifyView showNotify:@"请输入队名" isSuccessful:NO];
        return;
    }
    
    if (kStringIsEmpty(self.creat_View.peo01.text)) {
        
        [JMNotifyView showNotify:@"请输入成员账号1" isSuccessful:NO];
        return;
    }
    if (kStringIsEmpty(self.creat_View.peo02.text)) {
        
        [JMNotifyView showNotify:@"请输入成员账号2" isSuccessful:NO];
        return;
    }
    
    if ([self.creat_View.peo01.text isEqualToString:self.creat_View.peo02.text]) {
        [JMNotifyView showNotify:@"不能重复邀请同意账号的成员" isSuccessful:NO];
        return;
    }
    
    
    NSInteger chackType1 = [self checkIsHaveNumAndLetter:self.creat_View.peo01.text];
    
    if (chackType1 ==4 || chackType1==5 || chackType1==6) {
        [JMNotifyView showNotify:@"成员账号只能是数字字母组合，不能包含特殊字符" isSuccessful:NO];
        return;
    }
    
    
        NSInteger chackType = [self checkIsHaveNumAndLetter:self.creat_View.peo02.text];
    
    if (chackType ==4 || chackType==5 || chackType==6) {
        [JMNotifyView showNotify:@"成员账号只能是数字字母组合，不能包含特殊字符" isSuccessful:NO];
        return;
    }
    
   //User_onCreateTeam
    
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_onCreateTeam"];
    NSMutableDictionary * jsonDicc = [NSMutableDictionary dictionary];
    [jsonDicc setValue:self.creat_View.teamname.text forKey:@"team.name"];
    [jsonDicc setValue:[NSString stringWithFormat:@"%@,%@",self.creat_View.peo01.text,self.creat_View.peo02.text] forKey:@"usersStr"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:jsonDicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
           // YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"success" andTitleStr:[dict001 jsonString:@"data"] andlightStr:@""];
                [toast showLXAlertView];
                [self requestTypeInfo];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
-(void)requestTypeInfo{
    //User_loadTeamMatchInfo
    
    [self showLoadingAnimation];
    NSString  * balance = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_loadTeamMatchInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :balance parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        //NSLog(@"User_getActivityAll---%@",responseObject);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSDictionary * infoDicc = [dict jsonDict:@"data"];
            YYLog(@"User_loadTeamMatchInfo---%@",dict);
            //PTimageGameURL
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", [infoDicc jsonString:@"tips_txt"] ];
                
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                          NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                          };
                NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
                paragraphStyle.lineSpacing = 1.0;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, attributedString.length)];
                self.topLab.attributedText = attributedString;
                self.topLab.textAlignment =NSTextAlignmentCenter;
                self.topLab.textColor = kRGBColor(68, 68, 68);
               // [self.topLab sizeToFit];
 
                    //self.creat_View.frame = CGRectMake(0, self.topLab.bottom+10, kScreenWidth, 0);
                
                if ([[infoDicc jsonString:@"status"] isEqualToString:@"allow_create"]) {
                    strongSelf.creat_View.hidden = NO;
                    strongSelf.creat_View.height = 220;
                    strongSelf.staring_View.hidden = YES;
                    strongSelf.staring_View.height = 0;
                    strongSelf.more_CreatBtn.hidden = YES;
                    strongSelf.more_CreatBtn.height = 0;
                    strongSelf.menmers_View.hidden = YES;
                    strongSelf.menmers_View.height = 0;

                }else if ([[infoDicc jsonString:@"status"] isEqualToString:@"creating"]){
                    strongSelf.menmers_View.hidden = NO;
                    strongSelf.menmers_View.height = 350;
                    NSDictionary * team_data = [infoDicc jsonDict:@"team_data"];
                    strongSelf.menmers_View.teamname.text = [NSString stringWithFormat:@"队名:%@",[team_data jsonString:@"team_name"]];
                    strongSelf.menmers_View.teamLeader.text = [NSString stringWithFormat:@"队长:%@",[team_data jsonString:@"team_captain_name"]];
                    if ([[team_data jsonString:@"opt_power"] isEqualToString:@"allow_cancel"]) {
                          [strongSelf.menmers_View refushData:[team_data jsonArray:@"team_members"] isCreat:1];
                    }else if ([[team_data jsonString:@"opt_power"] isEqualToString:@"allow_refuse"]){
                          [strongSelf.menmers_View refushData:[team_data jsonArray:@"team_members"] isCreat:2];
                    }else{
                          [strongSelf.menmers_View refushData:[team_data jsonArray:@"team_members"] isCreat:3];
                        
                    }
                    strongSelf.more_CreatBtn.hidden = YES;
                    strongSelf.more_CreatBtn.height = 0;
                    strongSelf.creat_View.hidden = YES;
                    strongSelf.creat_View.height = 0;
                    strongSelf.staring_View.hidden = YES;
                    strongSelf.staring_View.height = 0;
                    
                }else if ([[infoDicc jsonString:@"status"] isEqualToString:@"create_fail"]){
                    //create_fail
                    strongSelf.more_CreatBtn.hidden = NO;
                    strongSelf.more_CreatBtn.height = 45;
                    strongSelf.menmers_View.hidden = YES;
                    strongSelf.menmers_View.height = 0;
                    strongSelf.creat_View.hidden = YES;
                    strongSelf.creat_View.height = 0;
                    strongSelf.staring_View.hidden = YES;
                    strongSelf.staring_View.height = 0;
                }else if ([[infoDicc jsonString:@"status"] isEqualToString:@"staring"]){
                    NSDictionary * team_data = [infoDicc jsonDict:@"team_data"];
                    //create_fail
                    strongSelf.staring_View.hidden = NO;
                    strongSelf.staring_View.height = 450;
                    strongSelf.more_CreatBtn.hidden = YES;
                    strongSelf.more_CreatBtn.height = 0;
                    strongSelf.menmers_View.hidden = YES;
                    strongSelf.menmers_View.height = 0;
                    strongSelf.creat_View.hidden = YES;
                    strongSelf.creat_View.height = 0;
                    [strongSelf.staring_View refushData:[team_data jsonArray:@"team_members"] andMoreDicc:team_data];
                }
                
                //
                else{
                    strongSelf.staring_View.hidden = YES;
                    strongSelf.staring_View.height = 0;
                    strongSelf.more_CreatBtn.hidden = YES;
                    strongSelf.more_CreatBtn.height = 0;
                    strongSelf.menmers_View.hidden = YES;
                    strongSelf.menmers_View.height = 0;
                    strongSelf.creat_View.hidden = YES;
                    strongSelf.creat_View.height = 0;
                }
 
                [self changeViewType:[infoDicc jsonString:@"status"]];

            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [self.bgSc.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        [self.bgSc.mj_header endRefreshing];
        [WJUnit showMessage:@"请求失败"];
        //[self getBalance];
        
    }];
    
    
    
}

-(void)changeViewType:(NSString *)typeStr{
    
    if ([typeStr isEqualToString:@"allow_create"]) {
        
        self.textInfoView.top =self.creat_View.bottom+10;
        self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
        
        
    }else if ([typeStr isEqualToString:@"creating"]){
        self.textInfoView.top =self.menmers_View.bottom+10;
        self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
        
    }else if ([typeStr isEqualToString:@"create_fail"]){
        self.textInfoView.top =self.more_CreatBtn.bottom+10;
        self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
        
    }else if ([typeStr isEqualToString:@"staring"]){
        self.textInfoView.top =self.staring_View.bottom+10;
        self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
        
    }else{
        self.textInfoView.top =self.topLab.bottom+10;
        self.bgSc.contentSize = CGSizeMake(kScreenWidth, self.textInfoView.bottom+50);
        
    }
    
    
    
}
-(int)checkIsHaveNumAndLetter:(NSString*)password{
    
    //数字条件
    
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]"options:NSRegularExpressionCaseInsensitive error:nil];
    //符合数字条件的有几个字节
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:password
                                 
                                                                       options:NSMatchingReportProgress
                                 
                                                                         range:NSMakeRange(0, password.length)];
    //英文字条件
    
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]"options:NSRegularExpressionCaseInsensitive error:nil];
    //符合英文字条件的有几个字节
    
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:password options:NSMatchingReportProgress range:NSMakeRange(0, password.length)];
    
    if (tNumMatchCount == password.length) {
        //全部符合数字，表示沒有英文
        return 1;
        
    } else if (tLetterMatchCount == password.length) {
        
        //全部符合英文，表示沒有数字
        return 2;
        
    } else if (tNumMatchCount + tLetterMatchCount == password.length) {
        
        //符合英文和符合数字条件的相加等于密码长度
        return 3;
        
    } else {
        return 4;
        //可能包含标点符号的情況，或是包含非英文的文字，这里再依照需求详细判断想呈现的错误
    }
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

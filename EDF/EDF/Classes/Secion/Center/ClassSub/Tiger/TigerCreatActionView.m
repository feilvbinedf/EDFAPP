//
//  TigerCreatActionView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/28.
//  Copyright © 2019 p. All rights reserved.
//

#import "TigerCreatActionView.h"
#import "RestrictionInput.h"

@implementation TigerCreatActionView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    KViewRadius(self.sureBtn, 4);
    
    [self.teamname addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.peo01 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.peo02 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}
-(void)textFieldDidChange:(UITextField *)textField {

       [RestrictionInput restrictionInputTextField:textField maxNumber:11 showView:self showErrorMessage:@""];
}
@end

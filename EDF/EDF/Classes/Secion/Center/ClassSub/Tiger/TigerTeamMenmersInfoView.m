//
//  TigerTeamMenmersInfoView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/28.
//  Copyright © 2019 p. All rights reserved.
//

#import "TigerTeamMenmersInfoView.h"

@implementation TigerTeamMenmersInfoView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    KViewRadius(self.cancelBtn, 4);

    self.cancelBtn.hidden = YES;
    self.sureBtn.hidden = YES;
    self.dissBtn.hidden = YES;
}

-(void)refushData:(NSArray *)dataArry  isCreat:(NSInteger  )isCreat{
    
    if (isCreat==1) {
        self.cancelBtn.hidden =NO;
        self.sureBtn.hidden = YES ;
        self.dissBtn.hidden = YES ;
    }else if (isCreat==2){
        self.cancelBtn.hidden =YES;
        self.sureBtn.hidden = NO ;
        self.dissBtn.hidden = NO ;
    }else{
        self.cancelBtn.hidden =YES;
        self.sureBtn.hidden = YES ;
        self.dissBtn.hidden = YES ;
    }
 

    
    NSDictionary * fristDicc = [dataArry firstObject];
    self.peo01.text = [NSString stringWithFormat:@"%@:%@",[fristDicc jsonString:@"role"],[fristDicc jsonString:@"username"]];
    self.type01.text = [fristDicc jsonString:@"status"];
    
    if ([[fristDicc jsonString:@"status"] isEqualToString:@"已通过"]) {
        self.type01.textColor = EDColor_BaseBlue;
        if ([[fristDicc jsonString:@"role"] isEqualToString:@"队长"]) {
            self.type01.hidden = YES;
        }else{
            self.type01.hidden =NO;
        }
    }else{
        self.type01.hidden =NO;
        self.type01.textColor = kRGBColor(153, 153, 153);
    }
    
      NSDictionary * fristDicc2 = [dataArry lastObject];
    
    self.peo02.text = [NSString stringWithFormat:@"%@:%@",[fristDicc2 jsonString:@"role"],[fristDicc2 jsonString:@"username"]];
    self.type02.text = [fristDicc2 jsonString:@"status"];
    if ([[fristDicc2 jsonString:@"status"] isEqualToString:@"已通过"]) {
        self.type02.textColor = EDColor_BaseBlue;
        if ([[fristDicc2 jsonString:@"role"] isEqualToString:@"队长"]) {
            self.type02.hidden = YES;
        }else{
             self.type02.hidden =NO;
        }
    }else{
        self.type02.hidden =NO;
        self.type02.textColor = kRGBColor(153, 153, 153);
    }
    
    
}
@end

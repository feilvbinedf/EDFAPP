//
//  TigerTeamMenmersInfoView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/28.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TigerTeamMenmersInfoView : UIView
@property (weak, nonatomic) IBOutlet UILabel *teamname;
@property (weak, nonatomic) IBOutlet UILabel *teamLeader;
@property (weak, nonatomic) IBOutlet UILabel *peo01;
@property (weak, nonatomic) IBOutlet UILabel *peo02;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UILabel *type01;
@property (weak, nonatomic) IBOutlet UILabel *type02;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIButton *dissBtn;

-(void)refushData:(NSArray *)dataArry  isCreat:(NSInteger)isCreat;
@end

NS_ASSUME_NONNULL_END

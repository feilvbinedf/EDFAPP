//
//  TigerCreatActionView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/28.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TigerCreatActionView : UIView
@property (weak, nonatomic) IBOutlet UITextField *teamname;
@property (weak, nonatomic) IBOutlet UITextField *peo01;
@property (weak, nonatomic) IBOutlet UITextField *peo02;

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@end

NS_ASSUME_NONNULL_END

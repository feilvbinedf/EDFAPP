//
//  TigerTeamStaringView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/29.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TigerTeamStaringView : UIView

@property (weak, nonatomic) IBOutlet UILabel *teamname;
@property (weak, nonatomic) IBOutlet UILabel *teamLeader;

@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *allnum;
@property (weak, nonatomic) IBOutlet UILabel *peo01;
@property (weak, nonatomic) IBOutlet UILabel *peo02;
@property (weak, nonatomic) IBOutlet UILabel *peo03;
@property (weak, nonatomic) IBOutlet UILabel *type01num;
@property (weak, nonatomic) IBOutlet UILabel *type02num;
@property (weak, nonatomic) IBOutlet UILabel *type03num;

-(void)refushData:(NSArray *)dataArry  andMoreDicc:(NSDictionary *)dataDicc;

@end

NS_ASSUME_NONNULL_END

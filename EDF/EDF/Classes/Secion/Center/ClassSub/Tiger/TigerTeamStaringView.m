//
//  TigerTeamStaringView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/29.
//  Copyright © 2019 p. All rights reserved.
//

#import "TigerTeamStaringView.h"

@implementation TigerTeamStaringView



-(void)refushData:(NSArray *)dataArry  andMoreDicc:(NSDictionary *)dataDicc{
    
    self.teamname.text = [NSString stringWithFormat:@"队名:%@",[dataDicc jsonString:@"team_name"]];
    self.teamLeader.text = [NSString stringWithFormat:@"队长:%@",[dataDicc jsonString:@"team_captain_name"]];
    
    self.startTime.text =[NSString stringWithFormat:@"开始时间:%@",[[dataDicc jsonString:@"start_date"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
    
        self.endTime.text =[NSString stringWithFormat:@"结束时间:%@",[[dataDicc jsonString:@"end_date"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
    
    self.allnum.text =[dataDicc jsonString:@"team_point"];
    
    NSDictionary * fristDicc = [dataArry firstObject];
    if ([fristDicc jsonBool:@"self"]==YES)
    {
        self.peo01.text = [NSString stringWithFormat:@"我的:%@",[fristDicc jsonString:@"username"]];
    }else{
          self.peo01.text = [NSString stringWithFormat:@"队员:%@",[fristDicc jsonString:@"username"]];
    }
     self.type01num.text = [fristDicc jsonString:@"point"];
    
    
    NSDictionary * fristDicc2 = [dataArry objectAtIndex:1];
    if ([fristDicc2 jsonBool:@"self"]==YES)
    {
        self.peo02.text = [NSString stringWithFormat:@"我的:%@",[fristDicc2 jsonString:@"username"]];
    }else{
        self.peo02.text = [NSString stringWithFormat:@"队员:%@",[fristDicc2 jsonString:@"username"]];
    }
    self.type02num.text = [fristDicc2 jsonString:@"point"];
    
    
    
    
    NSDictionary * fristDicc3 = [dataArry lastObject];
    if ([fristDicc3 jsonBool:@"self"]==YES)
    {
        self.peo03.text = [NSString stringWithFormat:@"我的:%@",[fristDicc3 jsonString:@"username"]];
    }else{
        self.peo03.text = [NSString stringWithFormat:@"队员:%@",[fristDicc3 jsonString:@"username"]];
    }
    self.type03num.text = [fristDicc3 jsonString:@"point"];
    

    
    
//    "team_data" =         {
//        "end_date" = "2019-08-02%2023:00:00";
//        "start_date" = "2019-07-29%2000:04:43";
//        "team_captain_name" = "\U5982*";
//        "team_members" =             (
//                                      {
//                                          point = "0.0";
//                                          self = false;
//                                          username = "y***01";
//                                      },
//                                      {
//                                          point = "0.0";
//                                          self = false;
//                                          username = "y***02";
//                                      },
//                                      {
//                                          point = "0.0";
//                                          self = true;
//                                          username = "C****56";
//                                      }
//                                      );
//        "team_name" = wakaka;
//        "team_point" = "0.0";
//    };
    
    
}
@end

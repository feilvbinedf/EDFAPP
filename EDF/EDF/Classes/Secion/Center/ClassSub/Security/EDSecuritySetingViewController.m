//
//  EDSecuritySetingViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/13.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDSecuritySetingViewController.h"
#import "EDUserInfoSetingTableViewCell.h"
#import "EDQuestionSettingViewController.h"
#import "EDChangeWithdrawalsCodeViewController.h"
@interface EDSecuritySetingViewController ()
@property(nonatomic,strong)NSDictionary * infoDicc;

@end

@implementation EDSecuritySetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infoDicc = [NSDictionary dictionary];
    [self creatUI];
    [self request_UserInfo:YES];
}

-(void)creatUI{
    
    self.ks_TableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
}
-(void)request_UserInfo:(BOOL)isLoding{
    //User_GetgetUserBandInfo
    if (isLoding==YES) {
        [self showLoadingAnimation];
    }
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_GetgetUserBandInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        if (isLoding==YES) {
            [self stopLoadingAnimation];
        }
        
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary *dict = [dict001 jsonDict:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (!kDictIsEmpty(dict )) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault  setValue:dict forKey:@"userBangDingInfo"];
                [userDefault synchronize ];
                strongSelf.infoDicc = [NSDictionary dictionaryWithDictionary:dict];
                [strongSelf.ks_TableView reloadData];
            }
        }else{
            // [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
        if (isLoding==YES) {
            [self stopLoadingAnimation];
        }
        [self request_UserInfo:NO];
    }];
    
    
}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDUserInfoSetingTableViewCell";
    EDUserInfoSetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDUserInfoSetingTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDUserInfoSetingTableViewCell" owner:self options:nil] lastObject];
    }
    
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(void)stepCell:(EDUserInfoSetingTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    NSArray * titleArry = @[@"提款密码",@"登陆密码"];
    cell.leftlab.text = titleArry[indexPath.row];
    cell.tishiLab.hidden = YES;
    cell.rightLab.hidden = NO;
    cell.tilabWC.constant = 100;
    if (indexPath.row==1) {
        cell.rightLab.hidden = YES;
    }else if (indexPath.row==0){
           cell.rightLab.hidden = NO;
        NSString * codeStr = [self.infoDicc jsonString:@"code"];
        if (kStringIsEmpty(codeStr)) {
            cell.rightLab.text = @"未设置";
            cell.rightLab.textColor =kRGBColor(255, 113, 112);
        }else{
            cell.rightLab.text = @"已设置";
            cell.tishiLab.textColor =kRGBColor(63, 63, 63);
        }
    }
    else{
         cell.rightLab.hidden = NO;
        BOOL  authentication = [self.infoDicc jsonBool:@"authentication"];
        if (authentication==NO) {
            cell.rightLab.text = @"未设置";
            cell.rightLab.textColor =kRGBColor(255, 113, 112);
        }else{
            cell.rightLab.text = @"已设置";
            cell.tishiLab.textColor =kRGBColor(63, 63, 63);
        }

    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==2) {
        BOOL  authentication = [self.infoDicc jsonBool:@"authentication"];
        if (authentication==NO) {
            
            EDQuestionSettingViewController * questVC = [EDQuestionSettingViewController new];
            questVC.title = @"设置安全问题";
            questVC.dismiss = ^(BOOL isSend) {
                if (isSend==YES) {
                    [self request_UserInfo:NO];
                }
            };
            [self.navigationController pushViewController:questVC animated:YES];
            
        }else{
            
        }
    }else if (indexPath.row==0){
     
        NSString * codeStr = [self.infoDicc jsonString:@"code"];
//        if (kStringIsEmpty(codeStr)) {
        EDChangeWithdrawalsCodeViewController *awalsCodeVC = [EDChangeWithdrawalsCodeViewController new];
        awalsCodeVC.showType =0;
        awalsCodeVC.title = @"设置提款密码";
        awalsCodeVC.dismiss = ^(BOOL isSend) {
            if (isSend==YES) {
                [self request_UserInfo:NO];
            }
        };
        [self.navigationController pushViewController:awalsCodeVC animated:YES];
   
    }
    else{
        NSString * codeStr = [self.infoDicc jsonString:@"code"];
        //        if (kStringIsEmpty(codeStr)) {
        EDChangeWithdrawalsCodeViewController *awalsCodeVC = [EDChangeWithdrawalsCodeViewController new];
        awalsCodeVC.showType =1;
        awalsCodeVC.title = @"修改登录密码";
//        awalsCodeVC.dismiss = ^(BOOL isSend) {
//            if (isSend==YES) {
//                [self request_UserInfo:NO];
//            }
//        };
        [self.navigationController pushViewController:awalsCodeVC animated:YES];
        
     
        
    }

    
}


@end

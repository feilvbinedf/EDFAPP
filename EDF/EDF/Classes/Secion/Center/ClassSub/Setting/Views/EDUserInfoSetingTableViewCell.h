//
//  EDUserInfoSetingTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/11.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDUserInfoSetingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rightLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tilabWC;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;

@property (weak, nonatomic) IBOutlet UILabel *leftlab;
@property (weak, nonatomic) IBOutlet UILabel *tishiLab;
@end

NS_ASSUME_NONNULL_END

//
//  EDBangdingPhoneOrEmailViewCobntroller.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDBangdingPhoneOrEmailViewCobntroller : EDBaseViewController
@property(nonatomic,assign) NSInteger  showType;
@property (nonatomic, copy) void (^dismiss)(BOOL isSend);
@property (nonatomic, copy) void (^dismissWithPhoneCode)(NSString *phone, NSString *code);

@end

NS_ASSUME_NONNULL_END

//
//  BangDingPhoneInfoView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "BangDingPhoneInfoView.h"
#import "RestrictionInput.h"
@implementation BangDingPhoneInfoView

-(void)awakeFromNib{
    [super awakeFromNib];
     KViewRadius(self.bg01, 4);
     KViewRadius(self.bg02, 4);
     KViewRadius(self.bg03, 4);
    [self.numTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeImageTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeNumTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.numTF.keyboardType = UIKeyboardTypeNumberPad;
    self.codeImageTF.keyboardType = UIKeyboardTypeNumberPad;
    self.codeNumTF.keyboardType = UIKeyboardTypeNumberPad;
}

-(void)textFieldDidChange:(UITextField *)textField {
    
    if (textField ==self.numTF) {
                [RestrictionInput restrictionInputTextField:textField maxNumber:11 showView:self showErrorMessage:@""];
    }else if (textField==self.codeImageTF){
               [RestrictionInput restrictionInputTextField:textField maxNumber:6 showView:self showErrorMessage:@""];
    }else{
              [RestrictionInput restrictionInputTextField:textField maxNumber:6 showView:self showErrorMessage:@""];
    }
    
}



@end

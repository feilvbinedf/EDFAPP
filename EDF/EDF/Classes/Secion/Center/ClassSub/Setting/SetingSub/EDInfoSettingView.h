//
//  EDInfoSettingView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDInfoSettingView : UIView
@property (weak, nonatomic) IBOutlet UITextField *enterTF;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *btnBg;
@property (weak, nonatomic) IBOutlet UIButton *daysBtn;

@end

NS_ASSUME_NONNULL_END

//
//  EDNameSettingViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDNameSettingViewController : EDBaseViewController

@property(nonatomic,assign) NSInteger  showType;  //0 名字  1 微信  2 QQ 
@property (nonatomic, copy) void (^dismiss)(BOOL isSend);
@end

NS_ASSUME_NONNULL_END

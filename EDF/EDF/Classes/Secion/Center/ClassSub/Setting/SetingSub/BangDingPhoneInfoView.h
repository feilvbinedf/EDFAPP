//
//  BangDingPhoneInfoView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BangDingPhoneInfoView : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fristHC;
@property (weak, nonatomic) IBOutlet UIView *bg01;
@property (weak, nonatomic) IBOutlet UITextField *numTF;
@property (weak, nonatomic) IBOutlet UIView *bg02;
@property (weak, nonatomic) IBOutlet UITextField *codeImageTF;
@property (weak, nonatomic) IBOutlet UIImageView *codeImage;

@property (weak, nonatomic) IBOutlet UIView *bg03;
@property (weak, nonatomic) IBOutlet UITextField *codeNumTF;
@property (weak, nonatomic) IBOutlet UIButton *codeActionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lab1;
@property (weak, nonatomic) IBOutlet UILabel *lab2;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHtop;
@property (weak, nonatomic) IBOutlet UIButton *codeRefushBtn;
@end

NS_ASSUME_NONNULL_END

//
//  EDNameSettingViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDNameSettingViewController.h"
#import "EDInfoSettingView.h"
#import "CGXPickerView.h"
@interface EDNameSettingViewController ()

@property(nonatomic,strong)EDInfoSettingView * infoView;

@property(nonatomic,strong)UIButton  * manBtn;
@property(nonatomic,copy)NSString   * daysString;
@property(nonatomic,strong)UIButton  * womanBtn;


@end
@implementation EDNameSettingViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.daysString = @"";
    [self creatUI];

}
-(void)creatUI{

    switch (self.showType) {
        case 0:
        {
            EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView.enterTF.placeholder = @"请输入姓名";
            infoView.enterTF.tag = 10010;
            infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
            [self.view addSubview:infoView];
            
            UIView * bgView = [BBControl createViewWithFrame:CGRectMake(0, infoView.bottom, kScreenWidth, 50)];
            bgView.backgroundColor= [UIColor whiteColor];
            [self.view addSubview:bgView];
            
            self.manBtn = [BBControl createButtonWithFrame:CGRectMake(30, 8, (kScreenWidth-90)/2, 35) target:self SEL:@selector(sexClickBtn:) title:@"男"];
            [self.manBtn setBackgroundColor:kRGBColor(244, 244, 244) forState:0  ];
            [self.manBtn setBackgroundColor:kRGBColor(190, 227, 255) forState:UIControlStateSelected  ];
            [self.manBtn setTitleColor:kRGBColor(44, 143, 219) forState:UIControlStateSelected ];
            [self.manBtn setTitleColor:kRGBColor(91, 91, 91) forState:0 ];
            [bgView addSubview:self.manBtn];
            self.manBtn.selected = YES;
              KViewRadius(self.manBtn, 4);
            self.womanBtn = [BBControl createButtonWithFrame:CGRectMake(self.manBtn.right+30, 8, (kScreenWidth-90)/2, 35) target:self SEL:@selector(sexClickBtn:) title:@"女"];
            [self.womanBtn setBackgroundColor:kRGBColor(244, 244, 244) forState:0  ];
            [self.womanBtn setBackgroundColor:kRGBColor(190, 227, 255) forState:UIControlStateSelected  ];
            [self.womanBtn setTitleColor:kRGBColor(44, 143, 219) forState:UIControlStateSelected ];
            [self.womanBtn setTitleColor:kRGBColor(91, 91, 91) forState:0 ];
            [bgView addSubview:self.womanBtn];
            self.womanBtn.selected = NO;
            KViewRadius(self.womanBtn, 4);
            
            UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, bgView.bottom+20, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick:) title:@"保存设置"];
            [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
            [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
            [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
            [self.view addSubview:bottomBtn];
            

        }
            break;
        case 1:
        {
            EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView.enterTF.placeholder = @"请输入微信号码";
            infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
            [self.view addSubview:infoView];
            infoView.enterTF.tag = 10011;
            UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, infoView.bottom+40, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick:) title:@"保存设置"];
            [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
            [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
            [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
            [self.view addSubview:bottomBtn];
        }
            break;
        case 2:
        {
            EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
             infoView.enterTF.placeholder = @"请输入QQ号码";
             infoView.enterTF.tag = 10011;
            infoView.enterTF.keyboardType = UIKeyboardTypeNumberPad;
            [self.view addSubview:infoView];
            UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, infoView.bottom+40, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick:) title:@"保存设置"];
            [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
            [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
            [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
            [self.view addSubview:bottomBtn];
        }
            break;
        case 3:
        {
            //生日
            EDInfoSettingView * infoView = [[NSBundle mainBundle]loadNibNamed:@"EDInfoSettingView" owner:self options:nil].lastObject;
            infoView.enterTF.placeholder = @"";
            infoView.btnBg.hidden = NO;
            infoView.frame =CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, 70);
            [self.view addSubview:infoView];
            self.infoView = infoView;
            WEAKSELF
            [infoView.daysBtn add_BtnClickHandler:^(NSInteger tag) {
                STRONGSELFFor(weakSelf);
                [strongSelf dasBtnsClick:infoView.daysBtn];
            }];
            self.infoView = infoView;
            UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, infoView.bottom+40, kScreenWidth-120, 50) target:self SEL:@selector(sureBtnClick:) title:@"保存设置"];
            [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
            [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
            [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
            [self.view addSubview:bottomBtn];
        }
            break;
            
        default:
            break;
    }

}
-(void)sexClickBtn:(UIButton *)sender{
      [self.view endEditing:YES];
    if (sender==self.manBtn) {
        self.manBtn.selected = YES;
       self.womanBtn.selected = NO;

    }else{
    self.manBtn.selected = NO;
     self.womanBtn.selected = YES;

    }

}
//时间选择器
-(void)dasBtnsClick:(UIButton *)sender{
    [self.view endEditing:YES];
    
    NSDate *now = [NSDate date];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *nowStr = [fmt stringFromDate:now];
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitle= @"";
    manager.leftBtnTitleSize=0;
     manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    WEAKSELF
    [CGXPickerView showDatePickerWithTitle:@"出生年月" DateType:UIDatePickerModeDate DefaultSelValue:@"1991-01-01" MinDateStr:@"1950-01-01" MaxDateStr:nowStr IsAutoSelect:YES Manager:manager ResultBlock:^(NSString *selectValue) {
        NSLog(@"%@",selectValue);
        STRONGSELFFor(weakSelf);
        strongSelf.daysString =selectValue;
        [strongSelf.infoView.daysBtn setTitle:selectValue forState:0];
    }];
    
}
-(void)sureBtnClick:(UIButton *)sender{
    [self.view endEditing:YES];
    switch (self.showType) {
        case 0:
        {
            [self user_BangDingNameAndSex];
        }
            break;
        case 1:
        {
            [self user_bangDingQQorWhchat:@"WX"];
        }
            break;
            case 2:
        {
            [self user_bangDingQQorWhchat:@"QQ"];
        }
               break;
        case 3:
        {
            [self user_bangDingBirthday];
        }
            break;
            
        default:
            break;
    }
    
}
#pragma mark - 绑定姓名和性别
-(void)user_BangDingNameAndSex{
    
    UITextField * enterText = [self.view viewWithTag:10010];
    
    if (kStringIsEmpty(enterText.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入姓名"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    if ([self isBlank:enterText.text]==YES) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"姓名不能包含空格"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    if (enterText.text.length> 10) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"姓名字符不能超过10"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_SetBindName"];
    WEAKSELF
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    if (self.manBtn.selected ==YES) {
          [diccc setValue:@"男" forKey:@"sex"];
    }else{
         [diccc setValue:@"女" forKey:@"sex"];
    }
  
    [diccc setValue:enterText.text forKey:@"u_name"];
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"设置成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDNameSettingViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];

}
#pragma mark - 绑定生日
-(void)user_bangDingBirthday{
    
    if (kStringIsEmpty(self.daysString)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请选择出生日期"] andlightStr:@""];
        [toast showLXAlertView];
        return;
        
    }

    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_SetBindBirthdayAndAddress"];
    WEAKSELF
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:self.daysString forKey:@"birthday"];
    [diccc setValue:@"" forKey:@"address"];
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"设置成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDNameSettingViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
    
}

#pragma mark - 绑定QQ  Whchat
-(void)user_bangDingQQorWhchat:(NSString *)type{
    
    UITextField * enterText = [self.view viewWithTag:10011];
    
    if (kStringIsEmpty(enterText.text)) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"请输入内容"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    if ([self isBlank:enterText.text]==YES) {
        ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"error" andTitleStr:[NSString stringWithFormat:@"内容不能包含空格"] andlightStr:@""];
        [toast showLXAlertView];
        return;
    }
    
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_SetBindQQAndWeChat"];
    WEAKSELF
    NSMutableDictionary *  diccc = [NSMutableDictionary dictionary];
    [diccc setValue:type forKey:@"type"];
    [diccc setValue:enterText.text forKey:@"number"];
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"设置成功"] andlightStr:@""];
                [toast showLXAlertView];
                
                __block EDNameSettingViewController *weakSelf = self;
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    if (weakSelf.dismiss) {
                        weakSelf.dismiss(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        [strongSelf.ks_TableView reloadData];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"请求错误"];
    }];
    
}
/**
 * 判断字符串是否包含空格
 */
-(BOOL)isBlank:(NSString *)str{
    NSRange _range = [str rangeOfString:@" "];
    if (_range.location != NSNotFound) {
        //有空格
        // NSLog(@"有空格");
        return YES;
    }else {
        //没有空格
        // NSLog(@"没有空格");
        return NO;
    }
}


@end

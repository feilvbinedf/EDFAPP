//
//  EDInfoSettingView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/12.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDInfoSettingView.h"

@implementation EDInfoSettingView

-(void)awakeFromNib{
    [super  awakeFromNib];
    
    KViewRadius(self.bgView, 4);
    self.btnBg.hidden = YES;
    
}

@end

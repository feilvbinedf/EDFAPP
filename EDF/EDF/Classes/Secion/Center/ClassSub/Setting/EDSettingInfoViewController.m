//
//  EDSettingInfoViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/11.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDSettingInfoViewController.h"
#import "EDUserInfoSetingTableViewCell.h"
#import "EDNameSettingViewController.h"
#import "EDBangdingPhoneOrEmailViewCobntroller.h"
#import "EDDBankeCardListViewController.h"
@interface EDSettingInfoViewController ()
@property(nonatomic,strong)NSDictionary * infoDicc;
@end
@implementation EDSettingInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infoDicc = [NSDictionary dictionary];
    [self creatUI];
    [self config_Data];
}

-(void)creatUI{
    
    self.ks_TableView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];

}

-(void)config_Data{
    NSDictionary * dicc = [kUserDefaults objectForKey:@"userBangDingInfo"];
    if (!kDictIsEmpty(dicc)) {
        self.infoDicc = [NSDictionary dictionaryWithDictionary:dicc];
        [self.ks_TableView reloadData];
        
    }
    [self request_UserInfo:YES];
    
}
-(void)request_UserInfo:(BOOL)isLoding{
    //User_GetgetUserBandInfo
    if (isLoding==YES) {
          [self showLoadingAnimation];
    }
  
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_GetgetUserBandInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        if (isLoding==YES) {
            [self stopLoadingAnimation];
        }
        
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary *dict = [dict001 jsonDict:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (!kDictIsEmpty(dict )) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault  setValue:dict forKey:@"userBangDingInfo"];
                [userDefault synchronize ];
                strongSelf.infoDicc = [NSDictionary dictionaryWithDictionary:dict];
                [strongSelf.ks_TableView reloadData];
            }
        }else{
           // [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
        if (isLoding==YES) {
            [self stopLoadingAnimation];
        }
        [self request_UserInfo:NO];
    }];
    
    
}
#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDUserInfoSetingTableViewCell";
    EDUserInfoSetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDUserInfoSetingTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDUserInfoSetingTableViewCell" owner:self options:nil] lastObject];
    }
    
    [self stepCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(void)stepCell:(EDUserInfoSetingTableViewCell *)cell  atIndex:(NSIndexPath *)indexPath{
    NSArray * titleArry = @[@"姓名",@"手机",@"微信",@"QQ",@"生日"];
    NSArray * titleSubArry = @[@"请实名认证",@"点击绑定，即得28发币",@"点击绑定",@"点击绑定",@"绑定有可能获得生日礼金哦~"];
    
    cell.leftlab.text = titleArry[indexPath.row];
    cell.tishiLab.text = titleSubArry[indexPath.row];
    cell.rightLab.hidden = YES;
     cell.rightIcon.hidden = NO;
    switch (indexPath.row) {
        case 0:
        {
            NSString * nameStr = [self.infoDicc jsonString:@"name"];
            if (kStringIsEmpty(nameStr)) {
                cell.rightIcon.hidden = NO;
                cell.tishiLab.text = titleSubArry[indexPath.row];
                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
            }else{
                 cell.rightIcon.hidden = YES;
                cell.tishiLab.text =nameStr;
                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
            }
        }
            break;
        case 1:
        {
            BOOL  binDingPhone = [self.infoDicc jsonBool:@"binDingPhone"];
            if (binDingPhone==NO) {
                cell.rightIcon.hidden = NO;
                cell.tishiLab.text = titleSubArry[indexPath.row];
                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
            }else{
                  cell.rightIcon.hidden = YES;
                cell.tishiLab.text =[self.infoDicc jsonString:@"phone"];
                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
            }
        }
            break;
//        case 2:
//        {
//            BOOL  binDingEmail = [self.infoDicc jsonBool:@"binDingEmail"];
//            if (binDingEmail==NO) {
//                cell.tishiLab.text = titleSubArry[indexPath.row];
//                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
//            }else{
//                cell.tishiLab.text =[self.infoDicc jsonString:@"email"];
//                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
//            }
//        }
//            break;
        case 2:
        {
            BOOL  binDingWechat = [self.infoDicc jsonBool:@"binDingWechat"];
            if (binDingWechat==NO) {
                cell.rightIcon.hidden = NO;
                cell.tishiLab.text = titleSubArry[indexPath.row];
                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
            }else{
                 cell.rightIcon.hidden = YES;
                cell.tishiLab.text =[self.infoDicc jsonString:@"wechat"];
                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
            }
        }
            break;
        case 3:
        {
            BOOL  binDingQq = [self.infoDicc jsonBool:@"binDingQq"];
            if (binDingQq==NO) {
                cell.rightIcon.hidden = NO;
                cell.tishiLab.text = titleSubArry[indexPath.row];
                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
            }else{
                   cell.rightIcon.hidden = YES;
                cell.tishiLab.text =[self.infoDicc jsonString:@"qq"];
                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
            }
        }
            break;
        case 4:
        {
            BOOL  binDingBirthday = [self.infoDicc jsonBool:@"binDingBirthday"];
            if (binDingBirthday==NO) {
                cell.rightIcon.hidden = NO;
                cell.tishiLab.text = titleSubArry[indexPath.row];
                cell.tishiLab.textColor =kRGBColor(255, 113, 112);
            }else{
                 cell.rightIcon.hidden = YES;
                cell.tishiLab.text =[self.infoDicc jsonString:@"birthday"];
                cell.tishiLab.textColor =kRGBColor(63, 63, 63);
            }
        }
            break;
            
        default:
            break;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
        {
            NSString * nameStr = [self.infoDicc jsonString:@"name"];
            if (kStringIsEmpty(nameStr)) {
                EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
                weichaatVC.title = @"绑定姓名";
                weichaatVC.showType = 0;
                WEAKSELF
                weichaatVC.dismiss = ^(BOOL isSend) {
                    STRONGSELFFor(weakSelf);
                    [strongSelf request_UserInfo:NO];
                };
                [self.navigationController pushViewController:weichaatVC animated:YES];
            }else{
    
            }
        }
            break;
        case 1:
        {
            BOOL  binDingPhone = [self.infoDicc jsonBool:@"binDingPhone"];
            if (binDingPhone==NO) {
                EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
                bangdingVC.title = @"绑定手机";
                bangdingVC.showType = 0;
                WEAKSELF
                bangdingVC.dismiss = ^(BOOL isSend) {
                    STRONGSELFFor(weakSelf);
                    [strongSelf request_UserInfo:NO];
                };
                [self.navigationController pushViewController:bangdingVC animated:YES];
            }else{

            }
        }
            break;
//        case 2:
//        {
//            BOOL  binDingEmail = [self.infoDicc jsonBool:@"binDingEmail"];
//            if (binDingEmail==NO) {
//                EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
//                bangdingVC.title = @"绑定邮箱";
//                bangdingVC.showType = 1;
//                [self.navigationController pushViewController:bangdingVC animated:YES];
//            }else{
//
//            }
//        }
            break;
        case 2:
        {
            BOOL  binDingWechat = [self.infoDicc jsonBool:@"binDingWechat"];
            if (binDingWechat==NO) {
                EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
                weichaatVC.title = @"绑定微信号";
                weichaatVC.showType = 1;
                WEAKSELF
                weichaatVC.dismiss = ^(BOOL isSend) {
                    STRONGSELFFor(weakSelf);
                    [strongSelf request_UserInfo:NO];
                };
                [self.navigationController pushViewController:weichaatVC animated:YES];
            }else{

            }
        }
            break;
        case 3:
        {
            BOOL  binDingQq = [self.infoDicc jsonBool:@"binDingQq"];
            if (binDingQq==NO) {
                EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
                weichaatVC.title = @"绑定QQ号";
                weichaatVC.showType = 2;
                WEAKSELF
                weichaatVC.dismiss = ^(BOOL isSend) {
                    STRONGSELFFor(weakSelf);
                    [strongSelf request_UserInfo:NO];
                };
                [self.navigationController pushViewController:weichaatVC animated:YES];
            }else{
        
            }
        }
            break;
        case 4:
        {
            BOOL  binDingBirthday = [self.infoDicc jsonBool:@"binDingBirthday"];
            
//            EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
//            weichaatVC.title = @"绑定生日";
//            weichaatVC.showType = 3;
//            [self.navigationController pushViewController:weichaatVC animated:YES];
//            
            if (binDingBirthday==NO) {
                EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
                weichaatVC.title = @"绑定生日";
                weichaatVC.showType = 3;
                [self.navigationController pushViewController:weichaatVC animated:YES];
            }else{
 
            }
        }
            break;
            
        default:
            break;
    }
    
}


@end

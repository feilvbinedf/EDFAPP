//
//  EDDUserItemTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDDUserItemTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconimage;
@property (weak, nonatomic) IBOutlet UILabel *itemLab;
@property (weak, nonatomic) IBOutlet UILabel *otherLab;
@property (weak, nonatomic) IBOutlet UIImageView *right_Icon;
@property (weak, nonatomic) IBOutlet UIView *redCo;
@property (weak, nonatomic) IBOutlet UILabel *messageCount;

@end

NS_ASSUME_NONNULL_END

//
//  EDUserCenterHeaderView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDUserCenterHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UIImageView *dengjiLab;
@property (weak, nonatomic) IBOutlet UILabel *dengjiText;
@property (weak, nonatomic) IBOutlet UILabel *cashNum;

@property (weak, nonatomic) IBOutlet UIView *imageBg;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIButton *activityBtn01;

@property (weak, nonatomic) IBOutlet UIButton *activityBtn02;

@property (weak, nonatomic) IBOutlet UIButton *activityBtn03;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (nonatomic, copy) void (^actionBtnClickBlock22)(NSDictionary *dataDicc);
@property(nonatomic,strong)NSArray *dataArry;
@property(nonatomic,strong)UIViewController *superVC;
@property (weak, nonatomic) IBOutlet UILabel *qqText;
@property (weak, nonatomic) IBOutlet UIButton *fizhiBtn;

@property (weak, nonatomic) IBOutlet UILabel *name01;

@property (weak, nonatomic) IBOutlet UILabel *name02;
@property (weak, nonatomic) IBOutlet UILabel *name03;
@property (weak, nonatomic) IBOutlet UIButton *colectBtn;

@property (weak, nonatomic) IBOutlet UIImageView *kefuImage;
@property (weak, nonatomic) IBOutlet UIButton *refushBtn;
-(void)refushActivityBtnWith:(NSArray *)dataArry;
@end

NS_ASSUME_NONNULL_END

//
//  EDDUserItemTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDDUserItemTableViewCell.h"

@implementation EDDUserItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    self.otherLab.hidden = YES;
    self.redCo.hidden = YES;
    self.messageCount.hidden = YES;
    KViewRadius(self.redCo, 5);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

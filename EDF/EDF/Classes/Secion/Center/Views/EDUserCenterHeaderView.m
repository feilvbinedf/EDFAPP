//
//  EDUserCenterHeaderView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDUserCenterHeaderView.h"
#import "SDWebImage.h"
#import "TGWebViewController.h"
#import "UIButton+Category.h"
@implementation EDUserCenterHeaderView

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
        KViewRadius(self.imageBg, 4);
    self.imageBg.layer.shadowColor = [UIColor blackColor].CGColor;//阴影颜色
    self.imageBg.layer.shadowOffset = CGSizeMake(1, 0);//偏移距离
    self.imageBg.layer.shadowOpacity = 0.2;//不透明度
    self.imageBg.layer.shadowRadius = 2;//半径
    self.imageBg.layer.masksToBounds = NO;
    KViewRadius(self.bgView, 23);

    self.activityBtn01.touchAreaInsets = UIEdgeInsetsMake(25, 25, 25, 25);
     self.activityBtn02.touchAreaInsets = UIEdgeInsetsMake(25, 25, 25, 25);
     self.activityBtn03.touchAreaInsets = UIEdgeInsetsMake(25, 25, 25, 25);
     self.signInBtn.touchAreaInsets = UIEdgeInsetsMake(25, 25, 25, 25);
    self.fizhiBtn.touchAreaInsets = UIEdgeInsetsMake(10, 10, 10, 10);
       
}
-(void)refushActivityBtnWith:(NSArray *)dataArry{
    _dataArry = dataArry;
    
    if (_dataArry.count>0) {
        if (_dataArry.count>3) {
            NSDictionary * fristDicc =[_dataArry firstObject];
            [self.activityBtn01 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
            self.name01.text =[fristDicc jsonString:@"name"];
            NSDictionary * fristDicc02 =[_dataArry objectAtIndex:1];
            [self.activityBtn02 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc02 jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
            self.name02.text =[fristDicc02 jsonString:@"name"];
            NSDictionary * fristDicc03 =[_dataArry objectAtIndex:2];
            [self.activityBtn03 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc03 jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
            self.name03.text =[fristDicc03 jsonString:@"name"];
            self.activityBtn03.hidden = NO;
            self.name03.hidden = NO;
            self.activityBtn02.hidden = NO;
            self.name02.hidden = NO;
            self.activityBtn01.hidden = NO;
            self.name01.hidden = NO;
        }else{
            switch (_dataArry.count) {
                case 1:
                {
                    NSDictionary * fristDicc =[_dataArry firstObject];
                    [self.activityBtn01 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name01.text =[fristDicc jsonString:@"name"];
                    
                    self.activityBtn03.hidden = YES;
                    self.name03.hidden = YES;
                    self.activityBtn02.hidden = YES;
                    self.name02.hidden = YES;
                    self.activityBtn01.hidden = NO;
                    self.name01.hidden = NO;
                    
                    
                }
                    break;
                case 2:
                {
                    NSDictionary * fristDicc =[_dataArry firstObject];
                    [self.activityBtn01 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name01.text =[fristDicc jsonString:@"name"];
                    NSDictionary * fristDicc02 =[_dataArry objectAtIndex:1];
                    [self.activityBtn02 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc02 jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name02.text =[fristDicc02 jsonString:@"name"];
                    
                    self.activityBtn03.hidden = YES;
                    self.name03.hidden = YES;
                    self.activityBtn02.hidden = NO;
                    self.name02.hidden = NO;
                    self.activityBtn01.hidden = NO;
                    self.name01.hidden = NO;
                    
                }
                    break;
                case 3:
                {
                    NSDictionary * fristDicc =[_dataArry firstObject];
                    [self.activityBtn01 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name01.text =[fristDicc jsonString:@"name"];
                    NSDictionary * fristDicc02 =[_dataArry objectAtIndex:1];
                    [self.activityBtn02 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc02 jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name02.text =[fristDicc02 jsonString:@"name"];
                    NSDictionary * fristDicc03 =[_dataArry objectAtIndex:2];
                    [self.activityBtn03 sd_setBackgroundImageWithURL:[NSURL URLWithString:[fristDicc03 jsonString:@"icon_src"]] forState:0 placeholderImage:[UIImage imageNamed:@"Center_Gift"]];
                    self.name03.text =[fristDicc03 jsonString:@"name"];
                    self.activityBtn03.hidden = NO;
                    self.name03.hidden = NO;
                    self.activityBtn02.hidden = NO;
                    self.name02.hidden = NO;
                    self.activityBtn01.hidden = NO;
                    self.name01.hidden = NO;
                }
                    break;
                    
                default:
                    break;
            }
            
        }
      
        
    }else{
        
        self.activityBtn01.hidden = YES;
        self.name01.hidden = YES;
        
        self.activityBtn02.hidden = YES;
        self.name02.hidden = YES;
        
        self.activityBtn03.hidden = YES;
        self.name03.hidden = YES;
        
    }
   
}

- (IBAction)activityBtnClick:(UIButton *)sender {
    
    if (_dataArry.count>0) {
//        if (self.actionBtnClickBlock22) {
//            self.actionBtnClickBlock22 = _dataArry[sender.tag-100];
//        }
        dispatch_async(dispatch_get_main_queue(), ^{

            
        });
        

        
    }
    
    
}




@end

//
//  EDWalletCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDWalletCenterViewController.h"
#import "XXPageTabView.h"
#import "EDDepositCenterViewController.h"
#import "EDTransferCenterViewController.h"
#import "EDWithdrawalCenterViewController.h"
#import "EDWalletModel.h"
#import "AppDelegate.h"
#import "UIDevice+TFDevice.h"
@interface EDWalletCenterViewController ()
<XXPageTabViewDelegate>

{
    XXPageTabView *_pageTabView;
}

@property(nonatomic,strong)EDDepositCenterViewController * deposit_VC;
@property(nonatomic,strong)EDTransferCenterViewController * transfer_VC;
@property(nonatomic,strong)EDWithdrawalCenterViewController * withdrawal_VC;
@end

@implementation EDWalletCenterViewController

- (void)viewDidAppear:(BOOL)animated
{
    _pageTabView.selectedTabIndex = [AppDelegate shareAppdelegate].index;
    [self.transfer_VC viewDidAppear:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
       [UIDevice switchNewOrientation:UIInterfaceOrientationPortrait];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSegmentType_0View];
    
}

- (void)setupSegmentType_0View {
    
    self.deposit_VC = [EDDepositCenterViewController new];
    self.transfer_VC = [EDTransferCenterViewController new];
    self.withdrawal_VC = [EDWithdrawalCenterViewController new];
    self.deposit_VC.isWebGo = self.isWebGo;
    self.transfer_VC.isWebGo = self.isWebGo;
    self.withdrawal_VC.isWebGo = self.isWebGo;
    [self addChildViewController:self.deposit_VC];
    [self addChildViewController:self.transfer_VC];
    [self addChildViewController:self.withdrawal_VC];
    
    
    
    _pageTabView = [[XXPageTabView alloc] initWithChildControllers:self.childViewControllers childTitles:@[@"存款", @"转账", @"取款"]];
    _pageTabView.frame = CGRectMake(0, kStatusBarAndNavigationBarHeight, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    _pageTabView.tabSize = CGSizeMake(kScreenWidth, 50);
    _pageTabView.tabItemFont = AppFont(15);
    _pageTabView.unSelectedColor = kRGBColor(161, 161, 161);
    _pageTabView.selectedColor = kRGBColor(44, 143, 219);
    _pageTabView.bodyBounces = NO;
    _pageTabView.tabBackgroundColor = [UIColor whiteColor];
    _pageTabView.titleStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.indicatorStyle = XXPageTabIndicatorStyleDefault;
    _pageTabView.delegate = self;
    _pageTabView.indicatorWidth = 27;
    _pageTabView.indicatorHeight = 3;
    _pageTabView.backgroundColor = [UIColor whiteColor];
    _pageTabView.bodyBackgroundColor= [UIColor whiteColor];
    [self.view addSubview:_pageTabView];
    
    if (self.isWebGo==YES) {
        _pageTabView.selectedTabIndex = self.selectIndex;
    }

}
#pragma mark - XXPageTabViewDelegate
- (void)pageTabViewDidEndChange {
    NSInteger selectedTabIndex = _pageTabView.selectedTabIndex;
    [AppDelegate shareAppdelegate].index = selectedTabIndex;
    if (_pageTabView.selectedTabIndex==1)
    {
         [self.transfer_VC viewDidAppear:YES];
    }else if (_pageTabView.selectedTabIndex==2){
         [self.withdrawal_VC viewDidAppear:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

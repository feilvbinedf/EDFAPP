//
//  EDWalletCenterViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDWalletCenterViewController : EDBaseViewController

@property(nonatomic,assign)BOOL  isWebGo;

@property(nonatomic,assign)NSInteger   selectIndex;

@end

NS_ASSUME_NONNULL_END

//
//  PayPalModel.h
//  EDF
//
//  Created by p on 2019/7/12.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayPalModel : NSObject
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *admin_id;
@property (nonatomic,copy) NSString *create_date;
@property (nonatomic,copy) NSString *update_date;
@property (nonatomic,copy) NSString *od;
@end

@interface PayModel : NSObject
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *suggest;
@property (nonatomic,copy) NSString *name;//最低
@property (nonatomic,copy) NSString *img_url;//充值数
@property (nonatomic,copy) NSArray *channel_list;//最大
@end

@interface PayClassModel : NSObject
@property (nonatomic,copy) NSString *allow_helppay;
@property (nonatomic,copy) NSString *page_min_amount;//最低
@property (nonatomic,copy) NSString *page_quck_amount;//充值数
@property (nonatomic,copy) NSString *page_max_amount;//最大
@property (nonatomic,copy) NSString *typename;//名字
@property (nonatomic,copy) NSString *bankcode_map;
@property (nonatomic,copy) NSString *bank_id;
@property (nonatomic,copy) NSString *pay_url;
@property (nonatomic,copy) NSString *suggest;
@property (nonatomic,copy) NSString *page_tips;
@property (nonatomic,copy) NSString *open_type;

@end

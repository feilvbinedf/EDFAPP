//
//  EDDepositTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/9.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlignmentLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDDepositTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIAlignmentLabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@end

NS_ASSUME_NONNULL_END

//
//  UIButton+WebCache.h
//  EDF
//
//  Created by p on 2019/12/14.
//  Copyright © 2019 p. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (WebCache)
- (void)xr_setButtonImageWithUrl:(NSString *)urlStr;
@end

NS_ASSUME_NONNULL_END

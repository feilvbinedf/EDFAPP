//
//  UIButton+WebCache.m
//  EDF
//
//  Created by p on 2019/12/14.
//  Copyright © 2019 p. All rights reserved.
//

#import "UIButton+WebCache.h"

@implementation UIButton (WebCache)
- (void)xr_setButtonImageWithUrl:(NSString *)urlStr {
    
    NSURL * url = [NSURL URLWithString:urlStr];
    // 根据图片的url下载图片数据
    dispatch_queue_t xrQueue = dispatch_queue_create("loadImage", NULL); // 创建GCD线程队列
    dispatch_async(xrQueue, ^{
        // 异步下载图片
        UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        // 主线程刷新UI
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImage:img forState:UIControlStateNormal];
        });
    });
    
}  
@end

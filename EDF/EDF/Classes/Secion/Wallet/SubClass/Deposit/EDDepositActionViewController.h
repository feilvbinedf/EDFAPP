//
//  EDDepositActionViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/9.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "EDWalletModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDDepositActionViewController : EDBaseViewController
@property(nonatomic,strong)EDWalletModel* modell;
@property(nonatomic,strong)NSString* price;
@property(nonatomic,assign)BOOL isEqual;
@property(nonatomic,assign)BOOL  isWebGo;
@end

NS_ASSUME_NONNULL_END

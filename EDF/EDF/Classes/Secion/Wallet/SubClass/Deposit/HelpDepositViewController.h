//
//  HelpDepositViewController.h
//  EDF
//
//  Created by p on 2019/7/25.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HelpDepositViewController : EDBaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *codeimage;
@property (weak, nonatomic) IBOutlet UILabel *amont;
@property (nonatomic,strong)NSString * urlStr;
@property (nonatomic,strong)NSString * amontStr;
@end

NS_ASSUME_NONNULL_END

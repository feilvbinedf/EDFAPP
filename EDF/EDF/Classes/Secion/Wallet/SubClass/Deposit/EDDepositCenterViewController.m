//
//  EDDepositCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/9.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDDepositCenterViewController.h"
#import "UIAlignmentLabel.h"
#import "EDDepositTableViewCell.h"
#import "EDDepositActionViewController.h"
#import "EDWalletModel.h"
#import "DepositStatusViewController.h"
@interface EDDepositCenterViewController ()
@property(nonatomic,strong)EDWalletModel* modell;
@property(nonatomic,strong)DepositStatusViewController* statusVC;
@property(nonatomic,copy)NSString * loveSeclectID;
@property(nonatomic,strong) UIButton * bottomBtn;

@end

@implementation EDDepositCenterViewController
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
      [self getMoneyMobileAction];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.modelsArry = [NSMutableArray array];
    [self setepUI];
    
    [self ks_tableAddHeaderRequstRefush];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loveHuodongSeclect:) name:@"loveHuodongAction" object:nil];

    
}
#pragma mark = 日久生情传值
-(void)loveHuodongSeclect:(NSNotification *)user
{
    NSDictionary *dic = user.userInfo;
    self.loveSeclectID= [dic jsonString:@"loveId"];
    [self refushData];
  //  [self.ks_TableView.mj_header beginRefreshing];
    
}
#pragma mark = 初始化
-(void)setepUI{

    UIView * topView = [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, 75)];
    topView.backgroundColor =kRGBColor(245, 245, 245);
    //[UIColor colorWithHexString:@"#F2F2F2"];
    
    UIAlignmentLabel * fristLab =[UIAlignmentLabel new];
    fristLab.frame =CGRectMake(0, 17, kScreenWidth, 17);
    fristLab.text =@"请先选择活动类型再进行充值";
    fristLab.font = AppFont(14);
    fristLab.text =@"请先选择活动类型再进行充值";
    fristLab.font = AppFont(14);
    UIAlignmentLabel * fristLab2 =[UIAlignmentLabel new];
    fristLab2.frame =CGRectMake(0, fristLab.bottom+10, kScreenWidth, 17);
    fristLab2.text =@"存款活动不与转运金、千倍百倍、每日签到等共享";
    fristLab2.font = AppFont(14);
    fristLab.textColor = EDColor_BaseBlue;
    fristLab2.textColor = EDColor_BaseBlue;
   // fristLab.verticalAlignment = VerticalAlignmentMiddle;
    fristLab.textAlignment = UITextAlignmentCenter;
    fristLab2.textAlignment = UITextAlignmentCenter;
   // fristLab2.verticalAlignment = VerticalAlignmentMiddle;
    [topView addSubview:fristLab];
    [topView addSubview:fristLab2];

    self.ks_TableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-TABBAR_HEIGHT-50);
    if (self.isWebGo==YES) {
           self.ks_TableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight-50);
    }
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    self.ks_TableView.tableHeaderView = topView;
    
    UIView * topView2 = [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, 75)];
     self.ks_TableView.tableFooterView = topView2;
    
    
    UIButton * bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, kScreenHeight-kStatusBarAndNavigationBarHeight-TABBAR_HEIGHT-70-50, kScreenWidth-120, 50) target:self SEL:@selector(cunKuanBtnClick:) title:@"点我存款"];
    if (self.isWebGo==YES) {
        bottomBtn.frame = CGRectMake(60, kScreenHeight-kStatusBarAndNavigationBarHeight-70-50, kScreenWidth-120, 50);
    }
    [bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
    [bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
    [self.view addSubview:bottomBtn];
    [self.ks_TableView reloadData];
    
    
    self.statusVC = [DepositStatusViewController new];
    self.statusVC.view.frame =CGRectMake(0, 0, kScreenWidth, self.view.height-50);
    if (self.isWebGo ==YES) {
          self.statusVC.view.frame =CGRectMake(0, 0, kScreenWidth, self.view.height);
    }
    [self.view addSubview:self.statusVC.view];
    self.statusVC.view.hidden = YES;
    WEAKSELF
    self.statusVC.actionBtnClickBlock = ^(NSInteger type) {
        STRONGSELFFor(weakSelf);
        [strongSelf.ks_TableView.mj_header beginRefreshing];
    };
    [self addChildViewController:self.statusVC];
    
    
}

-(void)getData_Refush{
    
    if (!kStringIsEmpty( [AppDelegate shareAppdelegate].loveHuodongID)) {
        self.loveSeclectID =[AppDelegate shareAppdelegate].loveHuodongID;
    }
    
    //self.loveSeclectID = @"";
    [self refushData];
    [self getMoneyMobileAction];

}
- (void)getMoneyMobileAction
{
    NSString  * AllGame = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"moneyMobileAction"];
    WeakSelf
    [self showLoadingAnimation];
  //  [self showLoadingAnimation];
    [[SmileHttpTool sharedInstance] GET :AllGame parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSArray * activeDeposit = dict[@"data"][@"activeDeposit"];
            NSArray * passOnceDeposit =  dict[@"data"][@"passOnceDeposit"];
            NSArray * refuseDeposit =  dict[@"data"][@"refuseDeposit"];
            if (activeDeposit.count>0) {
               // NSDictionary * dic = activeDeposit[0];
                weakSelf.statusVC.view.hidden = NO;
                [weakSelf.statusVC refushData];
            }else   if (passOnceDeposit.count>0) {
                weakSelf.statusVC.view.hidden = NO;
                [weakSelf.statusVC refushData];
            }else if (refuseDeposit.count >0)
            {
                weakSelf.statusVC.view.hidden = NO;
                [weakSelf.statusVC refushData];
                
            }else{
                weakSelf.statusVC.view.hidden = YES;
               // [weakSelf.statusVC refushData];
            }
            
        }else{
            weakSelf.statusVC.view.hidden = YES;
           // [weakSelf.statusVC refushData];
           // weakSelf.bottomBtn.hidden = NO;
         //   [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
        [self stopLoadingAnimation];
        
    } failure:^(id responseObject, NSError *error) {
        
        [self stopLoadingAnimation];
    }];
}
-(void)refushData{
    
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"promotionMobileAction_getAllPromotion"];

    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        //YYLog(@"promotionMobileAction_getAllPromotion=====%@",dict);
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [strongSelf.modelsArry removeAllObjects];
          
            NSArray * dataArry  = [NSArray arrayWithArray:[dict jsonArray:@"data"]];
            for (int i = 0; i<dataArry.count; i++)
            {
                NSDictionary * diccData = dataArry[i];
                EDWalletModel * modell = [[EDWalletModel alloc]initWithDictionary:diccData error:nil];
                if (kStringIsEmpty(strongSelf.loveSeclectID)) {
                    if (i ==0) {
                        weakSelf.modell = modell;
                        modell.is_Seclect = YES;
                    }
                }else{
                    if ([modell.id isEqualToString:strongSelf.loveSeclectID]) {
                        weakSelf.modell = modell;
                        modell.is_Seclect = YES;
                        [AppDelegate shareAppdelegate].loveHuodongID = @"";
                        self.loveSeclectID = @"";
                    }
                }
                [strongSelf.modelsArry addObject:modell];
            }
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"] isSuccessful:NO];
        }
         [self stopLoadingAnimation];
        [strongSelf.ks_TableView reloadData];
        [strongSelf.ks_TableView.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        [JMNotifyView showNotify:@"获取充值活动响应错误，请重试~" isSuccessful:NO];
        [strongSelf.ks_TableView.mj_header endRefreshing];
    }];
    
}

//

-(void)cunKuanBtnClick:(UIButton*)sender{
    
    EDDepositActionViewController * cunVC = [EDDepositActionViewController new];
    cunVC.title = @"存款";
    cunVC.modell = _modell;
    cunVC.isWebGo = self.isWebGo;
    NSArray *nameArray = [_modell.other componentsSeparatedByString:@":"];
    cunVC.price = nameArray[1];
    if ([_modell.other rangeOfString:@"等于"].location !=NSNotFound)
     {
         cunVC.isEqual = YES;
     }else
     {
         cunVC.isEqual = NO;
     }
    [self.navigationController pushViewController:cunVC animated:YES];
    
}
#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return self.modelsArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDDepositTableViewCell";
    EDDepositTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (EDDepositTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDDepositTableViewCell" owner:self options:nil] lastObject];
    }
     cell.contentView.backgroundColor = [UIColor whiteColor];
    EDWalletModel* modell = self.modelsArry[indexPath.row];
    cell.titleLab.text = modell.name;
    cell.contentLab.text = modell.deposittips;
    cell.selectBtn.selected = modell.is_Seclect;
    WEAKSELF
    [cell.selectBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        EDWalletModel* modell222 = self.modelsArry[indexPath.row];
        modell222.is_Seclect = YES;
        for ( EDWalletModel* modell2 in self.modelsArry) {
            if ([modell222.id isEqualToString:modell2.id]) {
                modell2.is_Seclect = YES;
            }else{
                modell2.is_Seclect = NO;
            }
        }
        _modell = modell222;
        [self.ks_TableView reloadData];
    }];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
    
}
#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EDWalletModel* modell = self.modelsArry[indexPath.row];
    modell.is_Seclect = YES;
    for ( EDWalletModel* modell2 in self.modelsArry) {
        if ([modell.id isEqualToString:modell2.id]) {
            modell2.is_Seclect = YES;
        }else{
            modell2.is_Seclect = NO;
        }
    }
    _modell = modell;
    if (![_modell.id isEqualToString:self.loveSeclectID]) {
        self.loveSeclectID = @"";
         [AppDelegate shareAppdelegate].loveHuodongID = @"";
    }
    [self.ks_TableView reloadData];
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

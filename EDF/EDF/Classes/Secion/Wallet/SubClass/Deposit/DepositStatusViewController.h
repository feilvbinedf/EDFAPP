//
//  DepositStatusViewController.h
//  EDF
//
//  Created by p on 2019/7/16.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DepositStatusViewController : EDBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *code;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
- (IBAction)copyCode:(UIButton *)sender;
- (IBAction)lookStatus:(id)sender;
- (IBAction)RePayment:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *lookButton;

@property (weak, nonatomic) IBOutlet UIButton *button;

@property (nonatomic, copy) void (^actionBtnClickBlock)(NSInteger type);

-(void)refushData;
@end

NS_ASSUME_NONNULL_END

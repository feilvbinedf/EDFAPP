//
//  EDTransferCenterViewController.h
//  EDF
//
//  Created by p on 2019/7/16.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDTransferCenterViewController : EDBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UILabel *PTTitle;

@property (weak, nonatomic) IBOutlet UIButton *lijiButton;
@property (weak, nonatomic) IBOutlet UIButton *yijianButton;
@property (weak, nonatomic) IBOutlet UIButton *activityButton;
@property (weak, nonatomic) IBOutlet UIButton *platformButton;
@property (weak, nonatomic) IBOutlet UIImageView *activityImage;
@property (weak, nonatomic) IBOutlet UIImageView *platformImage;
@property (weak, nonatomic) IBOutlet UILabel *banner;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headView;


@property(nonatomic,assign)BOOL  isWebGo;

@end

NS_ASSUME_NONNULL_END

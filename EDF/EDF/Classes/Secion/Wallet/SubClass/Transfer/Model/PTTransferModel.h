//
//  PTTransferModel.h
//  EDF
//
//  Created by p on 2019/7/17.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//平台账户
@interface PTTransferModel : NSObject
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *balance;
@property(nonatomic,strong)NSString *account_id;
@end
//活动账户
@interface PTActivityTransferModel : NSObject
@property(nonatomic,strong)NSString *bonus;
@property(nonatomic,strong)NSString *maxflow;
@property(nonatomic,strong)NSString *promotion_id;
@property(nonatomic,strong)NSString *type;

@property(nonatomic,strong)NSString *tips;
@property(nonatomic,strong)NSString *promotion_name;
@property(nonatomic,strong)NSString *balance;
@property(nonatomic,strong)NSString *refundable;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *create_date;
@property(nonatomic,strong)NSString *withdrawlimit;
@property(nonatomic,strong)NSString *zeroline;
@property(nonatomic,strong)NSString *flowtimes;
@property(nonatomic,strong)NSString *ptRistrict;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *update_date;
@property(nonatomic,strong)NSString *rebateable;
@property(nonatomic,strong)NSString *order_date;
@property(nonatomic,strong)NSString *user_id;
@property(nonatomic,strong)NSString *validflow;
@property(nonatomic,strong)NSString *ttBalance;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *refund;
@property(nonatomic,strong)NSString *gameplatform;
@property(nonatomic,strong)NSMutableAttributedString *mucontent;
@property(nonatomic,assign)float cellHeight;
@end
//平台账户
@interface ChooseModel : NSObject
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *balance;
@property(nonatomic,strong)NSString *gameplatform;
@property(nonatomic,strong)NSString *account_id;
@property(nonatomic,strong)NSString *ptRistrict;
@property(nonatomic,assign)BOOL iswallet;//是否是钱包
@end
NS_ASSUME_NONNULL_END

//
//  EDTransferTableViewCell.m
//  EDF
//
//  Created by p on 2019/7/17.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDTransferTableViewCell.h"
#define CELLWITH UIScreenWidth/4
@implementation EDTransferTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = WHITECOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
       
    }
    return self;
    
}
- (void)getPTTransferModelData:(PTTransferModel *) model index:(NSInteger)index;
{

    if (index ==0) {
        NSString * str1 = kFormat(@"%@\n%.2f", model.name,model.balance.floatValue);
        _PTLab1.attributedText = [WJUnit finderattributedLinesString:str1 attributedString:kFormat(@"%.2f", model.balance.floatValue) color:MAINCOLOR font:AutoFont(14)];
        _PTLab1.hidden = NO;
        _PTLab2.hidden =   _PTLab3.hidden = _PTLab4.hidden = _line1.hidden =_line2.hidden =  _line3.hidden = YES ;
    }else if (index ==1)
    {
        NSString * str1 = kFormat(@"%@\n%.2f", model.name,model.balance.floatValue);
        _PTLab2.attributedText = [WJUnit finderattributedLinesString:str1 attributedString:kFormat(@"%.2f", model.balance.floatValue) color:MAINCOLOR font:AutoFont(14)];
        _PTLab2.hidden =  _PTLab1.hidden =  _line1.hidden =NO;
        
        _PTLab3.hidden = _PTLab4.hidden = _line2.hidden =  _line3.hidden = YES;
    }else if (index ==2)
    {
        NSString * str1 = kFormat(@"%@\n%.2f", model.name,model.balance.floatValue);
        _PTLab3.attributedText = [WJUnit finderattributedLinesString:str1 attributedString:kFormat(@"%.2f", model.balance.floatValue) color:MAINCOLOR font:AutoFont(14)];
        _PTLab3.hidden =  _PTLab1.hidden = _PTLab2.hidden = _line1.hidden = _line2.hidden = NO;
        _PTLab4.hidden = _line3.hidden =YES;
    }else if (index ==3)
    {
        NSString * str1 = kFormat(@"%@\n%.2f", model.name,model.balance.floatValue);
        _PTLab4.attributedText = [WJUnit finderattributedLinesString:str1 attributedString:kFormat(@"%.2f", model.balance.floatValue) color:MAINCOLOR font:AutoFont(14)];
         _PTLab4.hidden = _PTLab3.hidden =  _PTLab2.hidden = _PTLab1.hidden =  _line1.hidden = _line2.hidden = _line3.hidden =NO;
    }
    _PTLab1.textAlignment = NSTextAlignmentCenter;
    _PTLab2.textAlignment = NSTextAlignmentCenter;
    _PTLab3.textAlignment = NSTextAlignmentCenter;
    _PTLab4.textAlignment = NSTextAlignmentCenter;
}
-(void)get_up
{
    [self.contentView addSubview:self.PTLab1];
    [self.contentView addSubview:self.PTLab2];
    [self.contentView addSubview:self.PTLab3];
    [self.contentView addSubview:self.PTLab4];
}
- (UILabel *)PTLab1
{
    if (!_PTLab1) {
        _PTLab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, CELLWITH-1, 45)];
        
        _PTLab1.font = AutoFont(14);
        _PTLab1.numberOfLines = 0;
        
        _line1 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab1.right, 0, 1, 17)];
        _line1.backgroundColor = RGBA(161, 161, 161, 1);
        _line1.centerY= _PTLab1.centerY;
        [self.contentView addSubview:_line1];
    }
    return _PTLab1;
}
- (UILabel *)PTLab2
{
    if (!_PTLab2) {
        _PTLab2 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab1.right+1, 10, CELLWITH-1, 45)];
//        _PTLab2.attributedText = [WJUnit finderattributedString:@"PT平台\n0.00" attributedString:@"0.00" color:MAINCOLOR font:AutoFont(14)];
        _PTLab2.font = AutoFont(14);
        _PTLab2.numberOfLines = 0;
        
        _line2 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab2.right, 0, 1, 17)];
        _line2.backgroundColor = RGBA(161, 161, 161, 1);
        _line2.centerY= _PTLab2.centerY;
        [self.contentView addSubview:_line2];
    }
    return _PTLab2;
}
- (UILabel *)PTLab3
{
    if (!_PTLab3) {
        _PTLab3 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab2.right+1, 10, CELLWITH-1, 45)];
//        _PTLab3.attributedText = [WJUnit finderattributedString:@"PT平台\n0.00" attributedString:@"0.00" color:MAINCOLOR font:AutoFont(14)];
        _PTLab3.font = AutoFont(14);
        _PTLab3.numberOfLines = 0;
        
        _line3 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab3.right, 0, 1, 17)];
        _line3.backgroundColor = RGBA(161, 161, 161, 1);
        _line3.centerY= _PTLab3.centerY;
        [self.contentView addSubview:_line3];
    }
    return _PTLab3;
}
- (UILabel *)PTLab4
{
    if (!_PTLab4) {
        _PTLab4 = [[UILabel alloc] initWithFrame:CGRectMake(_PTLab3.right+1, 10, CELLWITH, 45)];
//        _PTLab4.attributedText = [WJUnit finderattributedString:@"PT平台\n0.00" attributedString:@"0.00" color:MAINCOLOR font:AutoFont(14)];
        _PTLab4.font = AutoFont(14);
        _PTLab4.numberOfLines = 0;
       
    }
    return _PTLab4;
}
@end
@implementation EDTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = WHITECOLOR;
        self.selectionStyle = UITableViewCellAccessoryNone;
        [self get_up];
        
    }
    return self;
    
}
- (void)setModle:(PTActivityTransferModel *)modle
{
    _titleLable.text = modle.name;
    if ([modle.name isEqualToString:@"基本账户"]) {
        _contentLable.height = modle.cellHeight - 90;
         self.action_Btn.hidden = NO;
    }else{
        _contentLable.height = modle.cellHeight - 40;
         self.action_Btn.hidden = YES;
    }
    self.action_Btn.top = _contentLable.bottom+10;
    _contentLable.attributedText = modle.mucontent;
    _amontLable.text = kFormat(@"余额：%@", modle.balance);
    _contentLable.textColor = RGBA(207, 39, 39, 1);
}
- (void)get_up
{
    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.contentLable];
    [self.contentView addSubview:self.amontLable];
    
        self.action_Btn = [BBControl createButtonWithFrame:CGRectMake(kScreenWidth-100, self.contentLable.bottom+10, 85, 30) target:self SEL:nil title:@""];
    
   [self.action_Btn setBackgroundImage:[UIImage imageNamed:@"switch_Close.png"] forState:0];
    [self.action_Btn setBackgroundImage:[UIImage imageNamed:@"switch_Open"] forState:UIControlStateSelected];
    [self.contentView addSubview:self.action_Btn];
    self.action_Btn.hidden = YES;
    
    
    
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 120, 20)];
        _titleLable.font = AutoFont(14);
        _titleLable.textColor = RGBA(68, 68, 68, 1);
    }
    return _titleLable;
}
- (UILabel *)contentLable
{
    if (!_contentLable) {
        _contentLable = [[UILabel alloc] initWithFrame:CGRectMake(20,_titleLable.bottom + 10, UIScreenWidth -20, 0)];
        _contentLable.font = AutoFont(14);
    
        _contentLable.numberOfLines = 0;
    }
    return _contentLable;
}
- (UILabel *)amontLable
{
    if (!_amontLable) {
        _amontLable = [[UILabel alloc] initWithFrame:CGRectMake(UIScreenWidth - 20 -200, 10, 200, 20)];
        _amontLable.font = AutoFont(14);
        _amontLable.textColor = MAINCOLOR;
        _amontLable.textAlignment = NSTextAlignmentRight;
       
        
    }
    return _amontLable;
}
@end

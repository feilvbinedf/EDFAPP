//
//  EDTransferTableViewCell.h
//  EDF
//
//  Created by p on 2019/7/17.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTTransferModel.h"
@interface EDTransferTableViewCell : UITableViewCell
@property (nonatomic,strong)UILabel * PTLab1;
@property (nonatomic,strong)UILabel * PTLab2;
@property (nonatomic,strong)UILabel * PTLab3;
@property (nonatomic,strong)UILabel * PTLab4;
@property (nonatomic,strong)UILabel * line1;
@property (nonatomic,strong)UILabel * line2;
@property (nonatomic,strong)UILabel * line3;
- (void)getPTTransferModelData:(PTTransferModel *) model index:(NSInteger)index;
@end

@interface EDTableViewCell : UITableViewCell
@property (nonatomic,strong)UILabel * titleLable;
@property (nonatomic,strong)UILabel * contentLable;
@property (nonatomic,strong)UILabel * amontLable;
@property (nonatomic,strong)UIButton  * action_Btn;
@property (nonatomic,strong)PTActivityTransferModel * modle;
@end

//
//  EDTransferCenterViewController.m
//  EDF
//
//  Created by p on 2019/7/16.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDTransferCenterViewController.h"
#import "EDTransferTableViewCell.h"
#import "PTTransferModel.h"
#import "CGXPickerView.h"
#define MAINBLACKCOLOR ColorStr(@"#141443")
@interface EDTransferCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * activityArray; //钱包数据
@property (nonatomic,strong)NSMutableArray * dataArray;//平台数据
@property (nonatomic,strong)NSMutableArray * leftdataArray;//平台数据
@property (nonatomic,strong)NSMutableArray * rightdataArray;//平台数据
@property (nonatomic,strong)NSString * selectValue;
@property (nonatomic,assign)BOOL isActivity;//平台数据
@property (nonatomic,strong)ChooseModel * walletModel; //左边的数据
@property (nonatomic,strong)ChooseModel * gameModel;
@property (nonatomic,assign)BOOL isLeft;
@property (nonatomic,strong)NSString * type;
@property (nonatomic,assign)double  PTTotalprofit;
@property (nonatomic,assign)double  WatchTotalprofit;
@property(nonatomic,strong) MJRefreshNormalHeader *header;
@property(nonatomic,strong) UIButton * action_Btn;
@property(nonatomic,strong) NSDictionary * userIncoDicc;
@end

@implementation EDTransferCenterViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.headView.frame = CGRectMake(0, 0, UIScreenWidth , 350);

   
}
- (void)viewDidAppear:(BOOL)animated
{
    if ([AppDelegate shareAppdelegate].index==1) {
            [_header beginRefreshing];
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.backgroundColor = RGBA(241, 241, 241, 1);
    self.view.backgroundColor = [UIColor whiteColor];
    [self customUI];
    self.userIncoDicc = [NSDictionary dictionary];
    self.activityArray = [NSMutableArray new];
    self.dataArray = [NSMutableArray new];
    self.leftdataArray = [NSMutableArray new];
    self.rightdataArray = [NSMutableArray new];
    // 上拉加载
    _header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
     
        [self getMobileTransferAccounts];
    }];
    self.tableView.mj_header = _header;
}
// 获取可转账平台
- (void)getMobileTransferAccounts
{
    [self showLoadingAnimation];
    WeakSelf
    NSString  * transferAccounts = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"getMobileTransferAccounts"];
    [[SmileHttpTool sharedInstance] GET :transferAccounts parameters:@{} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [weakSelf.activityArray removeAllObjects];
            [weakSelf.dataArray removeAllObjects];
            [weakSelf.leftdataArray removeAllObjects];
            weakSelf.PTTotalprofit = 0.0;
            weakSelf.WatchTotalprofit = 0.0;
            NSDictionary * dic = [self objectFromJSONString:responseObject];
            self.dataArray  = [PTTransferModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"ptAccounts"]];
            self.activityArray  = [PTActivityTransferModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"accounts"]];
            for (PTActivityTransferModel * model in self.activityArray) {
                NSString *str = [model.tips stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSData *data = [str dataUsingEncoding:NSUnicodeStringEncoding];
                NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
                NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
                [attr addAttribute:NSFontAttributeName value:AutoFont(14) range:NSMakeRange(0, attr.length)];
                CGSize attSize = [attr boundingRectWithSize:CGSizeMake(UIScreenWidth -40, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
                
                if ([model.name isEqualToString:@"基本账户"]) {
                   model.cellHeight =attSize.height  +100;
                }else{
                   model.cellHeight =attSize.height  +50;
                }
                model.mucontent = attr;
            }
            
            for (PTActivityTransferModel * mode in self.activityArray) {//平台判断是否有余额
                
                if (mode.balance.floatValue>0.00)
                {
                    ChooseModel * M = [[ChooseModel alloc] init];
                    M.balance = mode.balance;
                    M.iswallet = YES;
                    M.id = mode.id;
                    M.gameplatform = mode.gameplatform;
                    M.account_id = @"";
                    M.name = mode.name;
                    M.ptRistrict = mode.ptRistrict;
                    [weakSelf.leftdataArray addObject:M];
                    weakSelf.WatchTotalprofit += M.balance.doubleValue;
                }
            }
            for (PTTransferModel * mode in self.dataArray) {//平台判断是否有余额
                
                if (mode.balance.floatValue>0.00)
                {
                    ChooseModel * M = [[ChooseModel alloc] init];
                    M.balance = mode.balance;
                    M.iswallet = NO;
                    M.id = mode.id;
                    M.gameplatform = @"";
                    M.account_id = mode.account_id;
                    M.name = mode.name;
                    M.ptRistrict = @"";
                    [weakSelf.leftdataArray addObject:M];
                    weakSelf.PTTotalprofit += M.balance.doubleValue;
                }
            }
            
            if (self.leftdataArray.count>0) {
               
                [self getLeftdata:weakSelf.leftdataArray[0]];
                
            }else
            {
                [self getbuttonTitleWithPTTransferWihtName:@"选择账户"];
                [self getbuttonTitleWithactivityWihtName:@"选择账户"];
            }
            [self activitySelected:weakSelf.isActivity];
            [self.tableView reloadData];
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
         [self.tableView.mj_header endRefreshing];
        [self stopLoadingAnimation];
    } failure:^(id responseObject, NSError *error) {
         [self.tableView.mj_header endRefreshing];
         [self stopLoadingAnimation];
    }];
    
    
    [self getuserInfoDicc];
    
    
    
}
-(void)getuserInfoDicc{
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        //  [self stopLoadingAnimation];
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            strongSelf.userIncoDicc = [NSDictionary dictionaryWithDictionary:[dict jsonDict:@"data"]];
            [strongSelf.tableView reloadData];
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        //[self stopLoadingAnimation];
    }];
}
- (CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font: (UIFont *)font
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    CGFloat height = ceil(rect.size.height) + 1;
    return height;
}
// 转账
- (void)getMoneyTransferWithparameters:(NSDictionary *)parameters
{
    if (kStringIsEmpty(_priceTextField.text)) {
        [WJUnit showMessage:@"请输入转账金额"];
        return;
    }
    [self showLoadingAnimation];
    WeakSelf
    NSString  * moneyTransfer = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"moneyTransfer"];
    
    [[SmileHttpTool sharedInstance] GET :moneyTransfer parameters:parameters origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [weakSelf getMobileTransferAccounts];

        }else{
            [self stopLoadingAnimation];
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
       
    } failure:^(id responseObject, NSError *error) {
        
    }];
}
- (void)getLeftdata:(ChooseModel *)model
{
    _priceTextField.text = model.balance;
    [self getbuttonTitleWithPTTransferWihtName:kFormat(@"%@(%@)", model.name,model.balance)];
   
    [_rightdataArray removeAllObjects];
    if (model.iswallet) {
         _walletModel = model;
        _type = @"转入";
        for (PTTransferModel * PTmodel in self.dataArray)
        {
             NSArray *nameArray = [model.ptRistrict componentsSeparatedByString:@","];
            BOOL isNSNotFound = NO;
            for (NSString * str in nameArray)
            {
                if ([PTmodel.id isEqualToString:str])
                {
                    isNSNotFound = YES;
                    break;
                }
            }
            if (isNSNotFound) {
                if (PTmodel.balance.floatValue>0) //如果有钱就判断是否是同一个ID
                {
                    if ([PTmodel.account_id isEqualToString:model.id]) {
                        ChooseModel * M = [[ChooseModel alloc] init];
                        M.balance = PTmodel.balance;
                        M.iswallet = NO;
                        M.id = PTmodel.id;
                        M.gameplatform = @"";
                        M.account_id = PTmodel.account_id;
                        M.name = PTmodel.name;
                        M.ptRistrict = @"";
                        [self.rightdataArray addObject:M];
                    }
                    
                }else
                {
                    ChooseModel * M = [[ChooseModel alloc] init];
                    M.balance = PTmodel.balance;
                    M.iswallet = NO;
                    M.id = PTmodel.id;
                    M.gameplatform = @"";
                    M.account_id = PTmodel.account_id;
                    M.name = PTmodel.name;
                    M.ptRistrict = @"";
                    [self.rightdataArray addObject:M];
                    
                }
            }
            
        }
 
    }else
    {
        _gameModel = model;
        _type = @"转出";
        for (PTActivityTransferModel * gameModel in self.activityArray)
        {
            if ([model.account_id isEqualToString:gameModel.id]) {
                ChooseModel * M = [[ChooseModel alloc] init];
                M.balance = gameModel.balance;
                M.iswallet = YES;
                M.id = gameModel.id;
                M.gameplatform = gameModel.gameplatform;
                M.account_id = @"";
                M.name = gameModel.name;
                M.ptRistrict = gameModel.ptRistrict;
                [self.rightdataArray addObject:M];
            }
            
        }
    }
    if (_rightdataArray.count>0)
    {
        [self getRigtData:_rightdataArray[0]];
    }else
    {
         [self getbuttonTitleWithactivityWihtName:@"暂无可转账平台"];
        
        
    }
    
}
- (void)getRigtData:(ChooseModel *)model
{
    [self getbuttonTitleWithactivityWihtName:kFormat(@"%@(%@)", model.name,model.balance)];
    if (model.iswallet) {
        _walletModel = model;

    }else
    {
        _gameModel = model;
    }
}
- (void)getbuttonTitleWithactivityWihtName:(NSString *)name
{
    [_rightButton setTitle:name forState:0];
    [_rightButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -_rightButton.imageView.width , 0, _rightButton.imageView.width)];
    [_rightButton setImageEdgeInsets:UIEdgeInsetsMake(0, _rightButton.titleLabel.width + 10, 0, - _rightButton.titleLabel.width)];
}
- (void)getbuttonTitleWithPTTransferWihtName:(NSString *)name
{

    [_leftButton setTitle:name forState:0];
    [_leftButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -_leftButton.imageView.width , 0, _leftButton.imageView.width)];
    [_leftButton setImageEdgeInsets:UIEdgeInsetsMake(0, _leftButton.titleLabel.width + 10, 0, - _leftButton.titleLabel.width)];
    
   
}
- (void)customUI
{
    _dataArray = [NSMutableArray alloc];
    [_leftButton add_BtnClickHandler:^(NSInteger tag) { // 活动账号
        
        [self popViewWithActivity:YES];
    }];
    [_rightButton add_BtnClickHandler:^(NSInteger tag) { // 平台账号
         [self popViewWithActivity:NO];
    }];
    
    KViewRadius(_lijiButton, 2);
    KViewRadius(_yijianButton, 2);
    WeakSelf
    [_lijiButton add_BtnClickHandler:^(NSInteger tag) { //立即转账
        
        if (self.leftdataArray.count==0)
        {
             [JMNotifyView showNotify:@"暂无余额"  isSuccessful:NO];
            return ;
        }else if (self.rightdataArray.count==0)
        {
            [JMNotifyView showNotify:@"暂无可转账平台"  isSuccessful:NO];
            return ;
        }
        
        [self getMoneyTransferWithparameters:@{@"account_id":weakSelf.walletModel.id,@"gameplatform":weakSelf.gameModel.id,@"amount":weakSelf.priceTextField.text,@"type":weakSelf.type}];
        
    }];
    [_yijianButton add_BtnClickHandler:^(NSInteger tag) { //一件转回
         NSMutableString * mustr = [[NSMutableString alloc] init];;
        for (PTTransferModel * mode in self.dataArray) {//平台判断是否有余额
            
            if (mode.balance.floatValue>0.00)
            {
              [mustr appendString:kFormat(@"%@,", mode.id)];
            }
        }
        if (kStringIsEmpty(mustr)) {
            [JMNotifyView showNotify:@"暂无可转回的平台"  isSuccessful:NO];
            return;
        }
        NSRange deleteRange = {[mustr length] - 1, 1 };
        [mustr deleteCharactersInRange:deleteRange];
        [self showLoadingAnimation];
        WeakSelf
        NSString  * moneyTransfer = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"getMoneyTurnBackAll"];
        [[SmileHttpTool sharedInstance] GET :moneyTransfer parameters:@{@"allids":mustr} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
            NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
            if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
                [weakSelf getMobileTransferAccounts];
                
            }else{
                [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
            }
            
        } failure:^(id responseObject, NSError *error) {
            
        }];
        
    }];
    [_activityButton add_BtnClickHandler:^(NSInteger tag) { //刷新活动账号
        [self activitySelected:YES];
    }];
    [_platformButton add_BtnClickHandler:^(NSInteger tag) { //刷新平台账号
        [self activitySelected:NO];
    }];
    [self activitySelected:YES];
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource=self;
    _tableView.showsVerticalScrollIndicator = NO;
    [_tableView registerClass:[EDTransferTableViewCell class] forCellReuseIdentifier:@"EDTransferTableViewCell"];
    [_tableView registerClass:[EDTableViewCell class] forCellReuseIdentifier:@"EDTableViewCell"];
    _tableView.backgroundColor = [UIColor whiteColor];
    
}

- (void)popViewWithActivity:(BOOL)isActivity
{
    
    NSMutableArray * dataArray = [NSMutableArray new];;
    if (isActivity)
    {
        _isLeft = YES;
        for (ChooseModel * modle in _leftdataArray)
        {
            [dataArray addObject:kFormat(@"%@(%@)", modle.name,modle.balance)];
        }
        
    }else
    {
        _isLeft = NO;
        for (ChooseModel * modle in _rightdataArray)
        {
            [dataArray addObject:kFormat(@"%@(%@)", modle.name,modle.balance)];
        }
        
    }
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= MAINCOLOR; //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= MAINCOLOR;
    manager.rightBtnTitleColor=MAINCOLOR;
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    manager.pickerTitleSelectColor = MAINCOLOR;
    WeakSelf
    [CGXPickerView showStringPickerWithTitle:@"账户类型" DataSource:dataArray DefaultSelValue:weakSelf.selectValue IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
        weakSelf.selectValue = selectValue;
        NSLog(@"%d",[selectRow intValue]);
        if (weakSelf.isLeft) {
            NSInteger index =0;
            for (int i =0; i<dataArray.count; i++)
            {
                if ([selectValue isEqualToString:dataArray[i]])
                {
                    index = i;
                }
            }
            ChooseModel * model =  weakSelf.leftdataArray[index];
            [self getLeftdata:model];
            
        }else
        {
            NSInteger index =0;
            for (int i =0; i<dataArray.count; i++)
            {
                if ([selectValue isEqualToString:dataArray[i]])
                {
                    index = i;
                }
            }
            ChooseModel * model =  weakSelf.rightdataArray[index];
            [self getRigtData:model];
            
        }
        
    }];
}
- (void)activitySelected:(BOOL)isselected
{
    _isActivity = isselected;
    if (_isActivity)
    {
        NSString *balance = kFormat(@"余额:%@元", [NSString stringWithFormat:@"%.2f",self.WatchTotalprofit]);
        self.banner.text = balance;
        self.PTTitle.text = @"活动账户";
        self.tableView.tableFooterView = [self Cancellation];
    }else
    {
        NSString *balance = kFormat(@"余额:%@元", [NSString stringWithFormat:@"%.2f",self.PTTotalprofit]);
        self.banner.text = balance;
        self.PTTitle.text = @"平台账户";
         _tableView.tableFooterView = [UIView new];
    }
    _activityImage.hidden = !isselected;
    _activityButton.selected = isselected;
    _platformButton.selected = !isselected;
    _platformImage.hidden = isselected;
    [self.tableView reloadData];
}
- (UIView *)Cancellation
{
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 70)];
    footView.backgroundColor = RGBA(232, 232, 232, 1);
    

    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth/2 - 125/2, 15, 125, 40)];
    [button setTitle:@"一键注销" forState:0];
    [button setTitleColor:WHITECOLOR forState:0];
    button.titleLabel.font = AutoFont(14);
    [button setBackgroundImage:[UIImage imageNamed:@"tab active"] forState:0];
    KViewRadius(button, 2);
    [button add_BtnClickHandler:^(NSInteger tag) { // 一件注销
        
        NSMutableString * mustr = [[NSMutableString alloc] init];;
        for (PTActivityTransferModel * mode in self.activityArray) {//平台判断是否有余额
            
            if (mode.balance.floatValue>0.00)
            {
                [mustr appendString:kFormat(@"%@,", mode.id)];
            }
        }
        
        if (kStringIsEmpty(mustr)) {
            [JMNotifyView showNotify:@"暂无可转注销的账户"  isSuccessful:NO];
            return;
        }
        NSRange deleteRange = {[mustr length] - 1, 1 };
        [mustr deleteCharactersInRange:deleteRange];
        
        [self showLoadingAnimation];
        WeakSelf
        NSString  * moneyTransfer = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"cancellationAccountAll"];
        [[SmileHttpTool sharedInstance] GET :moneyTransfer parameters:@{@"zxidlist":mustr} origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
            NSDictionary * dict = [WJUnit dictionaryWithJsonString:responseObject];
            if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
                [weakSelf getMobileTransferAccounts];
                
            }else{
                [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
            }
            
        } failure:^(id responseObject, NSError *error) {
            
        }];
    }];
    [footView addSubview:button];
    
    return footView;
}
-(void)action_BtnClick:(UIButton *)sender{
   // sender.selected = !sender.selected;
    if (sender.selected ==NO) {
        WEAKSELF
        [[SmileAlert sharedInstance] mazi_alertContent:@"自动转账设置\n\n开启后，进游戏时，余额会自动跟随进入，无需转账，是否开启？" AndBlock:^(BOOL sure) {
            STRONGSELFFor(weakSelf);
            if (sure==YES) {
                 [strongSelf requestSwithOpen:@"开启" sender:sender];
            }
        }];
    }else{
        WEAKSELF
        [[SmileAlert sharedInstance] mazi_alertContent:@"自动转账设置\n\n如果关闭，将需要手动转账，余额不会自动跟随，是否关闭？" AndBlock:^(BOOL sure) {
            STRONGSELFFor(weakSelf);
            if (sure==YES) {
                [self requestSwithOpen:@"关闭" sender:sender];
            }
        }];
        
      
    }

    
}

-(void)requestSwithOpen:(NSString *)setStr sender:(UIButton*)sender{
    //switchAutoTransfer
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"switchAutoTransfer"];
    NSMutableDictionary * jsonDicc = [NSMutableDictionary dictionary];
    [jsonDicc setValue:setStr forKey:@"status"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:jsonDicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            // YYLog(@"loadDailySignPromotion----%@",dict001);
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([setStr isEqualToString:@"开启"]) {
                    sender.selected = YES;
                }else{
                     sender.selected = NO;
                }
              [self getuserInfoDicc];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
      //  [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isActivity) {
       return self.activityArray.count;
    }else
    {
        if (self.dataArray.count%4 >0) {
            return self.dataArray.count/4 +1;
        }else
        {
            return self.dataArray.count/4;
        }
    }
}
#pragma mark -
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isActivity) {
         PTActivityTransferModel *activitymodel = _activityArray[indexPath.row];
        return activitymodel.cellHeight;
    }else
    {
        return 54;
    }
 
}
#pragma mark -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    if (_isActivity)
    {
        EDTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"EDTableViewCell"];
        cell.modle = _activityArray[indexPath.row];
        PTActivityTransferModel *activitymodel = _activityArray[indexPath.row];
        if ([activitymodel.name isEqualToString:@"基本账户"]) {
            if ([[self.userIncoDicc jsonString:@"auto_transfer_switch"] isEqualToString:@"开启"]) {
                cell.action_Btn.selected = YES;
            }else{
                cell.action_Btn.selected = NO;
            }
            [cell.action_Btn addTarget:self action:@selector(action_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            
        }
        return cell;
       
    }else
    {
        EDTransferTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"EDTransferTableViewCell"];
        for (int i =0; i<4; i++)
        {
            PTTransferModel * model = _dataArray[indexPath.row* 4 +i];
            
            [cell getPTTransferModelData:model index:i];
            if (_dataArray.count-1 == indexPath.row* 4 +i )
            {
                break;
            }
        }
        return cell;
    }
    
   
    
}
@end

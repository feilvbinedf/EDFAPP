//
//  EDWithdrawalCenterViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/9.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDWithdrawalCenterViewController.h"
#import "WithdrawalTableHeaderView.h"
#import "TiKuanActionView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "EDBangdingPhoneOrEmailViewCobntroller.h"
#import "EDDBankeCardListViewController.h"
#import "EDNameSettingViewController.h"
#import "EDChangeWithdrawalsCodeViewController.h"
#import "CJBankCardModel.h"
#import "CGXPickerView.h"
#import "TiKuanResultsView.h"

#import "CJItemWelfareModel.h"

@interface EDWithdrawalCenterViewController ()
@property(nonatomic,strong)TPKeyboardAvoidingScrollView * bgSC;
@property(nonatomic,strong)WithdrawalTableHeaderView* tabHeader;
@property(nonatomic,strong)UIScrollView* listBtnView;
@property(nonatomic,strong)NSMutableArray * listBtnArry;
@property(nonatomic,strong)TiKuanActionView* tikuanInfoView;
@property(nonatomic,strong)TiKuanResultsView* tikuan_ResultsView;
@property(nonatomic,strong)CJBankCardModel *sellect_CardModel;
@property(nonatomic,strong)CJBankCardModel *sellect_zhanghuModel;
@property(nonatomic,strong)NSMutableArray * zhang_listArry;
@property(nonatomic,strong)NSMutableArray * youhuiKaListArry;
@property(nonatomic,strong)UIButton * bottomBtn;
@property(nonatomic,strong)UIImageView  * tishiImage;

@property(nonatomic,assign)NSInteger   couponFlag;


@end

@implementation EDWithdrawalCenterViewController

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([AppDelegate shareAppdelegate].index==2) {
        [self.bgSC.mj_header beginRefreshing];
    }
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
      self.tabHeader.frame = CGRectMake(0, 0, kScreenWidth, 135);
      self.tikuanInfoView.frame = CGRectMake(0, self.tabHeader.bottom, kScreenWidth, 270);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.couponFlag = 0;
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor =[UIColor whiteColor];
    //kRGBColor(245, 245, 245);
    self.listBtnArry = [NSMutableArray array];
    self.zhang_listArry = [NSMutableArray array];
    self.youhuiKaListArry = [NSMutableArray array];
    [self creatUI];
    [self configData];
WEAKSELF
        self.bgSC.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            //调用刷新方法
            STRONGSELFFor(weakSelf);
            [strongSelf refushMyInfoData];
            [strongSelf request_UserInfoBangDing];
            [strongSelf requestZhanghuList];
            [strongSelf get_tiKuanTypeRequest];
            [strongSelf User_getAllBankCards];
        }];
        [self.bgSC.mj_header beginRefreshing];
    
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LoginSusess) name:LoginSusessNotificationse object:nil];

}

-(void)LoginSusess{
       self.tabHeader.cashTF.text = @"";
    self.tikuanInfoView.codeTF.text = @"";
        [self.bgSC.mj_header beginRefreshing];
}

-(void)creatUI{
    
    self.bgSC = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight)];
    self.bgSC.backgroundColor = kRGBColor(245, 245, 245);
    self.bgSC.contentSize = CGSizeMake(kScreenWidth, kScreenHeight-kStatusBarAndNavigationBarHeight);
    [self.view addSubview:self.bgSC];
    self.bgSC.backgroundColor = [UIColor whiteColor];
    
    self.tabHeader = [[NSBundle mainBundle]loadNibNamed:@"WithdrawalTableHeaderView" owner:self options:nil].lastObject;
    self.tabHeader.frame = CGRectMake(0, 0, kScreenWidth, 135);
    self.tabHeader.cashTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.bgSC addSubview:self.tabHeader];
    
    self.tikuanInfoView = [[NSBundle mainBundle]loadNibNamed:@"TiKuanActionView" owner:self options:nil].lastObject;
    self.tikuanInfoView.frame = CGRectMake(0, self.tabHeader.bottom, kScreenWidth, 270);
    [self.bgSC addSubview:self.tikuanInfoView];
   self.tikuanInfoView.codeTF.keyboardType = UIKeyboardTypeNumberPad;
    
    self.bottomBtn = [BBControl createButtonWithFrame:CGRectMake(60, self.tikuanInfoView.bottom+15, kScreenWidth-120, 50) target:self SEL:@selector(quKuanBtnClick:) title:@"立即取款"];
    [self.bottomBtn setBackgroundImage: [UIImage imageNamed:@"login_Btn_bg"] forState:0];
    [self.bottomBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [self.bottomBtn setTitleColor:[UIColor whiteColor] forState:0];
    
    self.tishiImage = [BBControl createImageViewFrame:CGRectMake(0, 0, 30, 30) imageName:@"tishiImage"];
    self.tishiImage.contentMode = UIViewContentModeScaleToFill;
    [self.bottomBtn addSubview:self.tishiImage];
    self.tishiImage.hidden = YES;
    [self.bgSC addSubview:self.bottomBtn];
    
    
    self.tikuan_ResultsView = [[NSBundle mainBundle]loadNibNamed:@"TiKuanResultsView" owner:self options:nil].lastObject;
    self.tikuan_ResultsView.frame = CGRectMake(0, 0, kScreenWidth, self.bgSC.height-50);

    [self.bgSC addSubview:self.tikuan_ResultsView];
    self.tikuan_ResultsView.hidden = YES;
    WEAKSELF
    self.tikuan_ResultsView.actionBtnClickBlock = ^(NSDictionary * _Nonnull dataDicc, NSString *typeStr) {
        STRONGSELFFor(weakSelf);
        if ([typeStr isEqualToString:@"activeWithdraw"]) {
            //审核中
            [strongSelf get_tiKuanTypeRequest];
        }
        else if ([typeStr isEqualToString:@"auditWithdraw"]){
            //已审核
             [strongSelf get_tiKuanTypeRequest];
              //   [strongSelf cancel_OrderWith:dataDicc];
        }
        else if ([typeStr isEqualToString:@"passOnceWithdraw"]){
            //已通过
           [strongSelf makeSure_OrderWith:dataDicc];
        }else{
            // refuseWithdraw
           [strongSelf makeDimiss_OrderWith:dataDicc];
            
        }

    };
    
    
    
    
    [self.tikuanInfoView.bangdingHuming add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf no_nameClick:strongSelf.tikuanInfoView.bangdingHuming];
    }];
    
    [self.tikuanInfoView.noCardBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf no_cardClick:strongSelf.tikuanInfoView.noCardBtn];
    }];
    
    
    [self.tikuanInfoView.noCodeBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf no_codeClick:strongSelf.tikuanInfoView.noCodeBtn];
    }];
    
    [self.tikuanInfoView.fogrtBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf bangdingCode];
    }];
    
    [self.tikuanInfoView.cardBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf selectCardList];
    }];
    [self.tikuanInfoView.zhanghuBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf selectZhanghuList];
    }];
    
    
}
-(void)configData{
    if (!kDictIsEmpty([DJLoginHelper sharedInstance].cj_UserDicc)) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[DJLoginHelper sharedInstance].cj_UserDicc];
        self.tabHeader.yueLab.text =[NSString stringWithFormat:@"余额:%@",[dict  jsonString:@"property"]];
    
    }
    
    NSDictionary * dicc = [kUserDefaults objectForKey:@"userBangDingInfo"];
    if (!kDictIsEmpty(dicc)) {
        
        if (kStringIsEmpty([dicc jsonString:@"code"])) {
            self.tikuanInfoView.codeTF.hidden = YES;
            self.tikuanInfoView.noCodeBtn.hidden = NO;
            self.tikuanInfoView.fogrtBtn.hidden = YES;
        }else{
            self.tikuanInfoView.codeTF.hidden = NO;
            self.tikuanInfoView.noCodeBtn.hidden = YES;
            self.tikuanInfoView.fogrtBtn.hidden = NO;
        }
//        if (kStringIsEmpty([dicc jsonString:@"name"])) {
//            self.tikuanInfoView.humingLab.text = @"提款户名:     暂未绑定";
//            self.tikuanInfoView.bangdingHuming.hidden = NO;
//        }else{
//            self.tikuanInfoView.humingLab.text =  [NSString stringWithFormat:@"提款户名:     %@",[dicc jsonString:@"name"]];
//            self.tikuanInfoView.bangdingHuming.hidden = YES;
//        }
        
        
//        NSArray * cordArry = [dicc jsonArray:@"userCards"];
//        if (cordArry.count==0) {
//            self.tikuanInfoView.noCardBtn.hidden = NO;
//        }else{
//            self.tikuanInfoView.noCardBtn.hidden = YES;
//        }
   }
    
}
-(void)User_getAllBankCards{
    [self showLoadingAnimation];
    //User_getAllBankCards
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getAllBankCards"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *dictArry = [dict001 jsonArray:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (dictArry.count>0) {
                [strongSelf.modelsArry removeAllObjects];
                
                for (NSDictionary * diccdata in dictArry) {
                    CJBankCardModel *  queModel = [[CJBankCardModel alloc]initWithDictionary:diccdata error:nil];
                    [strongSelf.modelsArry addObject:queModel];
                }
                CJBankCardModel * fristModel = [strongSelf.modelsArry firstObject];
                strongSelf.sellect_CardModel = fristModel;
                [strongSelf.tikuanInfoView.cardBtn setTitle:fristModel.card forState:0];
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
        if (strongSelf.modelsArry.count==0) {
                             strongSelf.tikuanInfoView.noCardBtn.hidden = NO;
          }else{
                             strongSelf.tikuanInfoView.noCardBtn.hidden = YES;
        }

  
    } failure:^(id responseObject, NSError *error) {
        //NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        
    }];
}

-(void)refushMyInfoData{
    //myUserNavigationDatd
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos2"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
         //   NSLog(@"refushMyInfoData---%@",dict);
            NSString * nameStr =[[dict jsonDict:@"data"]  jsonString:@"name"];
            NSString * fristStr;
            if (!kStringIsEmpty(nameStr)) {
                fristStr = [nameStr substringToIndex:1];
                for (int i = 0; i <nameStr.length-1; i++) {
                    fristStr = [fristStr stringByAppendingString:@"*"];
                }
            }
         
            strongSelf.tikuanInfoView.humingLab.text =  [NSString stringWithFormat:@"提款户名:     %@",fristStr];
            if ([[[dict jsonDict:@"data"]  jsonString:@"name"] isEqualToString:@"false"] || kStringIsEmpty([[dict jsonDict:@"data"]  jsonString:@"name"] )) {
                strongSelf.tikuanInfoView.humingLab.text = @"提款户名:     暂未绑定";
                strongSelf.tikuanInfoView.bangdingHuming.hidden = NO;
            }else{
                 strongSelf.tikuanInfoView.bangdingHuming.hidden = YES;
            }

        }else{
//            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
        [strongSelf.bgSC.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
      // [self refushMyInfoData];
        [self.bgSC.mj_header endRefreshing];
    }];
    
    NSString  * url22= [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_getUserInfos"];
    [[SmileHttpTool sharedInstance] GET :url22 parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            [[DJLoginHelper sharedInstance]save_UserInfo:[dict jsonDict:@"data"]];
          //  NSLog(@"YINetWorkAPIGenerate--User_getUserInfos---%@",dict);
             strongSelf.tabHeader.yueLab.text =[NSString stringWithFormat:@"余额:%@",[[dict jsonDict:@"data"]  jsonString:@"property"]];
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
              [strongSelf.bgSC.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
     //  [self refushMyInfoData];
    }];
    
    
}
-(void)requestZhanghuList{
   // http://localhost:8080/richman/moneyMobileAction!getUserWithDrawAccounts.action
    //Wallt_getUserWithDrawAccounts
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_getUserWithDrawAccounts"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSArray *dictArry = [dict001 jsonArray:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (dictArry.count>0) {
                [strongSelf.zhang_listArry removeAllObjects];
                
                for (NSDictionary * diccdata in dictArry) {
                    CJBankCardModel *  queModel = [[CJBankCardModel alloc]initWithDictionary:diccdata error:nil];
                    [strongSelf.zhang_listArry addObject:queModel];
                }
                CJBankCardModel * fristModel = [strongSelf.zhang_listArry firstObject];
                strongSelf.sellect_zhanghuModel = fristModel;
                [strongSelf.tikuanInfoView.zhanghuBtn setTitle:[NSString stringWithFormat:@"%@(%@元)",fristModel.name,fristModel.cash] forState:0];
            }else{
                [strongSelf.zhang_listArry removeAllObjects];
                 [strongSelf.tikuanInfoView.zhanghuBtn setTitle:@"暂无可提款账户 " forState:0];
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
      [strongSelf.bgSC.mj_header endRefreshing];
        
    } failure:^(id responseObject, NSError *error) {
        //NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
        [self stopLoadingAnimation];
        [WJUnit showMessage:@"请求错误"];
        [strongSelf.bgSC.mj_header endRefreshing];
        
    }];
    
}
-(void)request_UserInfoBangDing{
    //User_GetgetUserBandInfo
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_GetgetUserBandInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary *dict = [dict001 jsonDict:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (!kDictIsEmpty(dict )) {
               // YYLog(@"request_UserInfoBangDing---%@",dict);
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault  setValue:dict forKey:@"userBangDingInfo"];
                [userDefault synchronize ];
                
                if (kStringIsEmpty([dict jsonString:@"code"])) {
                    strongSelf.tikuanInfoView.codeTF.hidden = YES;
                    strongSelf.tikuanInfoView.noCodeBtn.hidden = NO;
                    strongSelf.tikuanInfoView.fogrtBtn.hidden = YES;
                }else{
                    strongSelf.tikuanInfoView.codeTF.hidden = NO;
                    strongSelf.tikuanInfoView.noCodeBtn.hidden = YES;
                    strongSelf.tikuanInfoView.fogrtBtn.hidden = NO;
                }
            }
        }else{
            
        }
             [self.bgSC.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
              [self.bgSC.mj_header endRefreshing];
    }];

}
#pragma mark - 按钮点击
-(void)no_nameClick:(UIButton*)sender{

    EDNameSettingViewController * weichaatVC = [EDNameSettingViewController new];
    weichaatVC.title = @"绑定姓名";
    weichaatVC.showType = 0;
    WEAKSELF
    weichaatVC.dismiss = ^(BOOL isSend) {
        STRONGSELFFor(weakSelf);
        [strongSelf refushMyInfoData];
    };
    [self.navigationController pushViewController:weichaatVC animated:YES];
}
-(void)no_cardClick:(UIButton*)sender{
       [self bangdingCard];
}
-(void)no_codeClick:(UIButton*)sender{
    NSDictionary * dicc = [kUserDefaults objectForKey:@"userBangDingInfo"];
    if (kStringIsEmpty([dicc jsonString:@"phone"]) || [[dicc jsonString:@"phone"] isEqualToString:@"false"]) {
        WEAKSELF
        [[SmileAlert sharedInstance] mazi_alertContent:@"请先手机认证\n再进行密码设置" AndBlock:^(BOOL sure) {
            STRONGSELFFor(weakSelf);
            if (sure==YES) {
                EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
                bangdingVC.title = @"绑定手机";
                bangdingVC.showType = 0;
                WEAKSELF
                bangdingVC.dismiss = ^(BOOL isSend) {
                    STRONGSELFFor(weakSelf);
                    [strongSelf request_UserInfoBangDing];
                    
                    __block EDWithdrawalCenterViewController *weakSelf = self;
                    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                      [weakSelf bangdingCode];
                    });
                  
                };
                [self.navigationController pushViewController:bangdingVC animated:YES];
            }
        }];
        
    }else{
          [self bangdingCode];
    }
    
    
}
#pragma mark - 提款点击
-(void)quKuanBtnClick:(UIButton*)sender{
    
   // [self tikuan_CheckBankInfo];
    
   [self quTiKuanRequest];
    
//    [self showPopAlertViewRequest];
    
}


-(void)showPopAlertViewRequest{
   
    [self showLoadingAnimation];
   NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"userCouponActiongetUserCouponList"];
    NSMutableDictionary * diccc001 = [NSMutableDictionary dictionary];
    [diccc001 setValue:@"tk" forKey:@"type"];
    [diccc001 setValue:@(0) forKey:@"status"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:diccc001 origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
       [strongSelf stopLoadingAnimation];
  NSError *errorData;
   NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
   NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
   NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
   NSLog(@"userCouponActiongetUserCouponList----%@",dict001);
        [strongSelf.youhuiKaListArry removeAllObjects];
     NSArray *  dataArr = [dict001 jsonArray:@"data"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
               
            if (dataArr.count >0) {
                for (int i = 0; i < dataArr.count; i ++) {
                          NSDictionary * modelDicc = dataArr[i];
                          CJItemWelfareModel * modell = [[CJItemWelfareModel alloc]initWithDictionary:modelDicc error:nil];
                         [strongSelf.youhuiKaListArry addObject:modell];
                    
                    
                    if (i == dataArr.count-1) {
                        [strongSelf showPopViewAlert:strongSelf.youhuiKaListArry];
                    }
                }
            }else{
                
              [strongSelf showPopViewAlert:strongSelf.youhuiKaListArry];
            }
      
           }else{
                   dispatch_async(dispatch_get_main_queue(), ^{
                            ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                            [toast showLXAlertView];
                        });
               
           }

        
     //   NSLog(@"userCouponActiongetUserCouponList---%@",dataArr);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        STRONGSELFFor(weakSelf);
         [strongSelf stopLoadingAnimation];
        [strongSelf.ks_TableView.mj_footer endRefreshing];
        [strongSelf.ks_TableView.mj_header endRefreshing];
        //  [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    

}
-(void)showPopViewAlert:(NSArray *)dataArry{
    
    WEAKSELF
    [[SmileAlert sharedInstance] CashCardCenterListPopView_alertContent:dataArry AndBlock:^(BOOL sure, NSDictionary * _Nonnull dataDicc) {
        STRONGSELFFor(weakSelf);
        
        YYLog(@"CashCardCenterListPopView_alertContent-----%@",dataDicc);
        if (!kDictIsEmpty(dataDicc) && sure ==YES) {
            [strongSelf maskeSureTiKuanRequest:YES with:dataDicc];
        }else{
            [strongSelf maskeSureTiKuanRequest:NO with:dataDicc];
        }
        
    }];

}
-(void)quTiKuanRequest{
    //Wallt_submitDrawing
    if (kStringIsEmpty(self.sellect_CardModel.id)) {
        [JMNotifyView showNotify:@"请选择提款卡号" isSuccessful:NO];
        return;
    }
    if (kStringIsEmpty(self.sellect_zhanghuModel.id)) {
        [JMNotifyView showNotify:@"请选择提款账户" isSuccessful:NO];
        return;
    }
    if (kStringIsEmpty(self.tabHeader.cashTF.text)) {
        [JMNotifyView showNotify:@"请输入提款金额" isSuccessful:NO];
        return;
    }
    if ([self isNum:self.tabHeader.cashTF.text]==NO) {
        [JMNotifyView showNotify:@"提款金额必须为数字" isSuccessful:NO];
        return;
    }
    if ([self.tabHeader.cashTF.text integerValue]<100) {
        [JMNotifyView showNotify:@"提款金额不能少于100" isSuccessful:NO];
        return;
    }
    if ([self.tabHeader.cashTF.text integerValue]>[self.sellect_zhanghuModel.cash integerValue]) {
        [JMNotifyView showNotify:@"提款金额不能大于所选账户余额" isSuccessful:NO];
        return;
    }
    
    
    if (kStringIsEmpty(self.tikuanInfoView.codeTF.text)) {
        [JMNotifyView showNotify:@"请输入提款密码" isSuccessful:NO];
        return;
    }
  
    
    
    if (self.couponFlag >0) {
   
        [self showPopAlertViewRequest];
        
    }else{
        
        [self maskeSureTiKuanRequest:NO with:nil];
    }

}
-(void)maskeSureTiKuanRequest:(BOOL)isCard with:(NSDictionary *)diccc{
    
    
     NSInteger  randNum = [self getRandomNumber:100 to:1000];
     NSString * timeStr = [self getNowTimeTimestamp3];
     NSString * cashRecord = [NSString stringWithFormat:@"TK%@%ld",timeStr,randNum];
     
     NSMutableDictionary  * dicc = [NSMutableDictionary dictionary];
     [dicc setValue:self.tabHeader.cashTF.text forKey:@"amount"];
     [dicc setValue:self.tikuanInfoView.codeTF.text forKey:@"code"];
      [dicc setValue:cashRecord forKey:@"no"];
      [dicc setValue:self.sellect_zhanghuModel.id forKey:@"accountId"];
      [dicc setValue:self.sellect_CardModel.id forKey:@"usercardId"];
      [dicc setValue:self.sellect_CardModel.promotion_id forKey:@"promotionId"];
    if (isCard ==YES) {
        [dicc setValue:[diccc jsonString:@"coupon_id"] forKey:@"coupon_id"];
    }
     [self showLoadingAnimation];
      NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_submitDrawing"];
     
     WEAKSELF
     [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
         [self stopLoadingAnimation];
         STRONGSELFFor(weakSelf)
         NSError *errorData;
         NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
         NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
         NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
         if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"提款申请已提交"] andlightStr:@""];
                 [toast showLXAlertView];
                 [strongSelf get_tiKuanTypeRequest];
             });
         }else{
             dispatch_async(dispatch_get_main_queue(), ^{
                 ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                 [toast showLXAlertView];
             });
         }
     } failure:^(id responseObject, NSError *error) {
         [self stopLoadingAnimation];
         NSLog(@"error----%@",error);
         [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
     }];
    
    
    
}
- (BOOL)isNum:(NSString *)checkedNumString {
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}
-(void)tikuan_CheckBankInfo:(NSString*)idStr{
    //Wallt_verificationDrawingInfo
    NSString  * url22= [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_verificationDrawingInfo"];
    NSMutableDictionary * dicc = [NSMutableDictionary dictionary];
    [dicc setValue:idStr forKey:@"cardNo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url22 parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf);
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            NSLog(@"YINetWorkAPIGenerate--Wallt_verificationDrawingInfo---%@",dict);
            
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
        [strongSelf.bgSC.mj_header endRefreshing];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [WJUnit showMessage:@"获取卡号信息失败"];
        // [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}
-(void)makeSureTiKuan:(NSString*)idStr{
    //Wallt_confirmAdoptDrawingNumber
            NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_confirmAdoptDrawingNumber"];
    NSMutableDictionary * dicc = [NSMutableDictionary dictionary];
    [dicc setValue:idStr forKey:@"orderId"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"提款申请已提交"] andlightStr:@""];
                [toast showLXAlertView];
              
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
}
-(void)get_tiKuanTypeRequest{
    //Wallt_getDrawingInfo
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_getDrawingInfo"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:nil origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        [self stopLoadingAnimation];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            NSDictionary *dict = [dict001 jsonDict:@"data"];
            strongSelf.couponFlag = [dict jsonInteger:@"couponFlag"];
            if (strongSelf.couponFlag >0) {
                strongSelf.tishiImage.hidden = NO;
            }else{
                strongSelf.tishiImage.hidden = YES;
            }
            YYLog(@"Wallt_getDrawingInfo----%@",dict);
            if ([dict jsonArray:@"refuseWithdraw"].count ==0 && [dict jsonArray:@"passOnceWithdraw"].count ==0 &&[dict jsonArray:@"activeWithdraw"].count ==0 &&[dict jsonArray:@"auditWithdraw"].count ==0 ) {
                //没有提款
                dispatch_async(dispatch_get_main_queue(), ^{
                    // UI更新代码
                   strongSelf.tikuan_ResultsView.hidden = YES;

                });
            }else{
                   strongSelf.tikuan_ResultsView.hidden = NO;
                if ( [dict jsonArray:@"activeWithdraw"].count >0 || [dict jsonArray:@"auditWithdraw"].count >0) {
                     strongSelf.tikuan_ResultsView.hidden = NO;
                    if ([dict jsonArray:@"activeWithdraw"].count >0) {
                        // [{"id":"asbvasbab","remark":"","no":"TK12345684"}], 审核中的提款
                        [strongSelf.tikuan_ResultsView refushDataWithType:@"activeWithdraw" andDic:[dict jsonArray:@"activeWithdraw"].firstObject];
                    }
                    
                    if ([dict jsonArray:@"auditWithdraw"].count >0) {
                        // [{"id":"asbvasbab","remark":"","no":"TK12345684"}], 已审核的提款
                        [strongSelf.tikuan_ResultsView refushDataWithType:@"auditWithdraw" andDic:[dict jsonArray:@"auditWithdraw"].firstObject];
                    }
                }else{
                      strongSelf.tikuan_ResultsView.hidden = NO;
                    if ([dict jsonArray:@"refuseWithdraw"].count >0) {
                        // [{"id":"asbvasbab","remark":"","no":"TK12345684"}], 已拒绝的提款
                        [strongSelf.tikuan_ResultsView refushDataWithType:@"refuseWithdraw" andDic:[dict jsonArray:@"refuseWithdraw"].firstObject];
                    }
                    
                    if ([dict jsonArray:@"passOnceWithdraw"].count >0) {
                        // [{"id":"asbvasbab","remark":"","no":"TK12345684"}], 已通过的提款
                        [strongSelf.tikuan_ResultsView refushDataWithType:@"passOnceWithdraw" andDic:[dict jsonArray:@"passOnceWithdraw"].firstObject];
                    }
             //   }
                
                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    strongSelf.tikuan_ResultsView.hidden = NO;
//
//                });
                
            }
            
            
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
}
-(void)bangdingCard{
    EDDBankeCardListViewController  * SecurityVC = [EDDBankeCardListViewController new];
    SecurityVC.title = @"我的银行卡";
    [self.navigationController pushViewController:SecurityVC animated:YES];
}

-(void)bangdingCode{
    
    EDChangeWithdrawalsCodeViewController *awalsCodeVC = [EDChangeWithdrawalsCodeViewController new];
    awalsCodeVC.showType =0;
    awalsCodeVC.title = @"设置提款密码";
    awalsCodeVC.dismiss = ^(BOOL isSend) {
        if (isSend==YES) {
            [self request_UserInfoBangDing];
        }
    };
    [self.navigationController pushViewController:awalsCodeVC animated:YES];
}
//取消订单
-(void)cancel_OrderWith:(NSDictionary *)dataDicc{
//Wallt_confirmAdoptDrawingNumber
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_confirmAdoptDrawingNumber"];
    NSMutableDictionary * dicc = [NSMutableDictionary dictionary];
    [dicc setValue:[dataDicc jsonString:@"id"] forKey:@"orderId"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"提款已取消"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf get_tiKuanTypeRequest];
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
    
    

}

-(void)makeDimiss_OrderWith:(NSDictionary *)dataDicc{
    //Wallt_confirmAdoptDrawingNumber
    [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_onfirmRefuseDrawingNumber"];
    NSMutableDictionary * dicc = [NSMutableDictionary dictionary];
    [dicc setValue:[dataDicc jsonString:@"id"] forKey:@"orderId"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"已确认"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf get_tiKuanTypeRequest];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];

}

-(void)makeSure_OrderWith:(NSDictionary *)dataDicc{
    //Wallt_confirmAdoptDrawingNumber
      [self showLoadingAnimation];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"Wallt_confirmAdoptDrawingNumber"];
    NSMutableDictionary * dicc = [NSMutableDictionary dictionary];
    [dicc setValue:[dataDicc jsonString:@"id"] forKey:@"orderId"];
    
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        [self stopLoadingAnimation];
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSString *responseObjectStr =[dict001 jsonString:@"statusCode"];
        if ([responseObjectStr isEqualToString:@"SUCCESS"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1.5 andImageName:@"success" andTitleStr:[NSString stringWithFormat:@"已确认"] andlightStr:@""];
                [toast showLXAlertView];
                [strongSelf get_tiKuanTypeRequest];
                
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                ToastView * toast = [[ToastView alloc] initWithtToastandStayTime:1 andImageName:@"error" andTitleStr:[dict001 jsonString:@"message"] andlightStr:@""];
                [toast showLXAlertView];
            });
        }
    } failure:^(id responseObject, NSError *error) {
        [self stopLoadingAnimation];
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
    }];
    
}

#pragma mark - 选择相关

-(void)selectCardList{
    
    if (self.modelsArry.count==0) {
        
        [self User_getAllBankCards];
        return;
    }
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    NSMutableArray * dataArry = [NSMutableArray array];
    for ( CJBankCardModel* modell in self.modelsArry) {
        [dataArry addObject:modell.card];
    }
    WEAKSELF
    [CGXPickerView showStringPickerWithTitle:@"选择开户行" DataSource:dataArry DefaultSelValue:self.sellect_CardModel.card IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
        NSLog(@"CGXPickerView--%@",selectValue);
        [self.modelsArry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            STRONGSELFFor(weakSelf);
            NSLog(@"%zi %@", idx, obj);
            CJBankCardModel * modell2 = (CJBankCardModel *)obj;
            if ([modell2.card isEqualToString:selectValue]) {
                NSLog(@"------ %zi %@", idx, obj);
                strongSelf.sellect_CardModel = modell2;
                [strongSelf.tikuanInfoView.cardBtn setTitle:modell2.card forState:0];
                *stop = YES;
            }
        }];
        
    }];
    
}

-(void)selectZhanghuList{
    
    if (self.zhang_listArry.count==0) {
        
        [self requestZhanghuList];
        return;
    }
    CGXPickerViewManager *manager = [[CGXPickerViewManager alloc]init];
    manager.titleLabelColor= kRGBColor(91, 91, 91); //kRGBColor(44, 143, 219);
    manager.leftBtnBGColor= [UIColor whiteColor];
    manager.rightBtnBGColor= [UIColor whiteColor];
    manager.leftBtnTitleColor= kRGBColor(161, 161, 161);
    manager.rightBtnTitleColor=kRGBColor(44, 143, 219);
    manager.leftBtnborderColor= [UIColor whiteColor];
    manager.rightBtnborderColor= [UIColor whiteColor];
    NSMutableArray * dataArry = [NSMutableArray array];
    for ( CJBankCardModel* modell in self.zhang_listArry) {
        [dataArry addObject:modell.name];
    }
    WEAKSELF
    [CGXPickerView showStringPickerWithTitle:@"选择开户行" DataSource:dataArry DefaultSelValue:self.sellect_zhanghuModel.card IsAutoSelect:NO Manager:manager ResultBlock:^(id selectValue, id selectRow) {
        NSLog(@"CGXPickerView--%@",selectValue);
        [self.zhang_listArry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            STRONGSELFFor(weakSelf);
            NSLog(@"%zi %@", idx, obj);
            CJBankCardModel * modell2 = (CJBankCardModel *)obj;
            if ([modell2.name isEqualToString:selectValue]) {
                NSLog(@"------ %zi %@", idx, obj);
                strongSelf.sellect_zhanghuModel = modell2;
                [strongSelf.tikuanInfoView.zhanghuBtn setTitle:[NSString stringWithFormat:@"%@(%@元)",modell2.name,modell2.cash] forState:0];
                *stop = YES;
            }
        }];
        
    }];
}

//获取一个随机整数，范围在[from,to），包括from，不包括to
-(int)getRandomNumber:(int)from to:(int)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}
-(NSString *)getNowTimeTimestamp3{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


//    CGFloat  space = 10;
//    CGFloat  btnH = 35;
//    self.listBtnView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.tabHeader.bottom, kScreenWidth, 35*3+45)];
//    for (int i = 0 ; i < 3; i++) {
//        NSInteger index = i % 1;
//        NSInteger page = i / 1;
//        UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        mapBtn.tag = i+100;//
//        if (i==0) {
//            mapBtn.selected = YES;
//        }
//        mapBtn.frame = CGRectMake(index * (kScreenWidth-40 ) + 20, page  * (35 + space)+10, kScreenWidth-40 , btnH);
//        [mapBtn setBackgroundColor:kRGBColor(244, 244, 244) forState:0];
//        [mapBtn setBackgroundColor:kRGBColor(190, 227, 255) forState:UIControlStateSelected];
//        [mapBtn setTitleColor:kRGBColor(91, 91, 91)  forState:0];
//        [mapBtn setTitleColor:EDColor_BaseBlue forState:UIControlStateSelected];
//        [mapBtn setTitle:@"中国人民银行尾号7885" forState:0];
//        [mapBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
//        [mapBtn setTitle:@"中国人民银行尾号7885" forState:UIControlStateSelected];
//        KViewRadius(mapBtn, 4);
//        [mapBtn addTarget:self action:@selector(mapBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//        [self.listBtnArry  addObject:mapBtn];
//        [self.listBtnView addSubview:mapBtn];
//
//
//    }
//    self.listBtnView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:self.listBtnView];


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  TiKuanResultsView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/24.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TiKuanResultsView : UIView

@property (weak, nonatomic) IBOutlet UILabel *orderID;
@property (weak, nonatomic) IBOutlet UILabel *content01;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UILabel *content02;

@property(nonatomic,strong)NSDictionary *itemDataDicc;

@property(nonatomic,copy)NSString *typeStr;

@property (nonatomic, copy) void (^actionBtnClickBlock)(NSDictionary *dataDicc, NSString *typeStr);



-(void)refushDataWithType:(NSString *)type  andDic:(NSDictionary *)dataDicc;

@end

NS_ASSUME_NONNULL_END

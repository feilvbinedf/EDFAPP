//
//  TiKuanActionView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/23.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TiKuanActionView : UIView
@property (weak, nonatomic) IBOutlet UILabel *humingLab;
@property (weak, nonatomic) IBOutlet UIButton *bangdingHuming;

@property (weak, nonatomic) IBOutlet UIButton *zhanghuMoreBtn;

@property (weak, nonatomic) IBOutlet UIButton *zhanghuBtn;
@property (weak, nonatomic) IBOutlet UIImageView *iconxiangxia1;

@property (weak, nonatomic) IBOutlet UIButton *noCardBtn;

@property (weak, nonatomic) IBOutlet UIImageView *xiangxia02;

@property (weak, nonatomic) IBOutlet UIButton *cardBtn;

@property (weak, nonatomic) IBOutlet UIButton *fogrtBtn;

@property (weak, nonatomic) IBOutlet UITextField *codeTF;

@property (weak, nonatomic) IBOutlet UIButton *noCodeBtn;
@end

NS_ASSUME_NONNULL_END

//
//  TiKuanActionView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/23.
//  Copyright © 2019 p. All rights reserved.
//

#import "TiKuanActionView.h"
#import "RestrictionInput.h"
@implementation TiKuanActionView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    KViewBorder(self.zhanghuMoreBtn, EDColor_BaseBlue, 1);
    KViewRadius(self.zhanghuMoreBtn, 4);
    
    KViewRadius(self.fogrtBtn, 4);
    KViewBorder(self.fogrtBtn, EDColor_BaseBlue, 1);
    
    self.bangdingHuming.hidden = YES;
    self.noCardBtn.hidden = YES;
    self.noCodeBtn.hidden = YES;

    [self.codeTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

}
-(void)textFieldDidChange:(UITextField *)textField {
    [RestrictionInput restrictionInputTextField:textField maxNumber:6 showView:self showErrorMessage:@""];
    
}
@end

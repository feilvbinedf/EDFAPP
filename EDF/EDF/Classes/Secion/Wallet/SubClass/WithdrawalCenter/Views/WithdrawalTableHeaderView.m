//
//  WithdrawalTableHeaderView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/9.
//  Copyright © 2019 p. All rights reserved.
//

#import "WithdrawalTableHeaderView.h"
#import "RestrictionInput.h"

@implementation WithdrawalTableHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self.cashTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)textFieldDidChange:(UITextField *)textField {
       [RestrictionInput restrictionInputTextField:textField maxNumber:8 showView:self showErrorMessage:@""];

}


@end

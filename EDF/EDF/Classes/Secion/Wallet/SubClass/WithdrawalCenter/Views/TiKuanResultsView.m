//
//  TiKuanResultsView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/24.
//  Copyright © 2019 p. All rights reserved.
//

#import "TiKuanResultsView.h"

@implementation TiKuanResultsView
-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.content02.hidden = YES;
    self.content02.text= @"";
}

-(void)refushDataWithType:(NSString *)type  andDic:(NSDictionary *)dataDicc{
    self.typeStr = type;
    self.itemDataDicc = [NSDictionary dictionaryWithDictionary:dataDicc];
    self.content01.text = [dataDicc jsonString:@"remark"];
    self.content02.text =[dataDicc jsonString:@"remark2"];
    self.orderID.text =[dataDicc jsonString:@"no"];
     self.content02.hidden = YES;
    if ([type isEqualToString:@"activeWithdraw"]) {
        //审核中
        [self.typeBtn setTitle:@"刷新" forState:0];
    }
    else if ([type isEqualToString:@"auditWithdraw"]){
        //已审核
        [self.typeBtn setTitle:@"刷新" forState:0];
        
    }
    else if ([type isEqualToString:@"passOnceWithdraw"]){
      //已通过
          [self.typeBtn setTitle:@"确认" forState:0];
        
    }else{
       // refuseWithdraw
        self.content02.hidden = NO;
        [self.typeBtn setTitle:@"确认" forState:0];
    }

}

- (IBAction)typeActionClick:(UIButton *)sender {
    
    if (self.actionBtnClickBlock) {
        self.actionBtnClickBlock(self.itemDataDicc, self.typeStr);
    }
    
}




//refuseWithdraw
//
//List<CashRecord>
//
//已拒绝的提款
//如果当前有未确认的提款 只会有一个List存在数据
//passOnceWithdraw
//
//List<CashRecord>
//
//已通过的提款
//如果当前有未确认的提款 只会有一个List存在数据
//activeWithdraw
//
//List<CashRecord>
//
//
//审核中的提款
//如果当前有未确认的提款 只会有一个List存在数据
//auditWithdraw
//
//List<CashRecord>
//
//已审核的提款
//如果当前有未确认的提款 只会有一个List存在数据

@end

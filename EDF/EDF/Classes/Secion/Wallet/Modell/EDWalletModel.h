//
//  EDWalletModel.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/10.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDWalletModel : EDBaseModel

@property(nonatomic,copy)NSString<Optional> * name;
@property(nonatomic,copy)NSString<Optional> * other;
@property(nonatomic,copy)NSString<Optional> * deposittips;
@property(nonatomic,copy)NSString<Optional> * start_date;
@property(nonatomic,copy)NSString<Optional> * end_date;
@property(nonatomic,copy)NSString<Optional> * id; //活动ID
@property(nonatomic,copy)NSString<Optional> * restrict2;
@property(nonatomic,copy)NSString<Optional> * restrict_bank;
@property(nonatomic,copy)NSString<Optional> * restrict_show;
@property(nonatomic,strong)NSNumber *  flowtimes;
@property(nonatomic,strong)NSNumber *  bonus;
@property(nonatomic,strong)NSNumber *  returnmax;
@property(nonatomic,strong)NSNumber *  returnvalue;

@property(nonatomic,assign)BOOL  is_Seclect;


//充值活动
//{
//    deposittips = "";
//    "end_date" = "";
//    flowtimes = 15;
//    id = "yrj19_p1";
//    mark = "";
//    name = "\U5b58100\U900166(\U7535\U5b50)";
//    other = "\U5b58\U6b3e\U7b49\U4e8e:100";
//    parent = "";
//    restrict2 = "select%20*%20from%20(select%20count(*)%20as%20curcount%20from%20promotionrecord%20t%20where%20t.user_id%20=%20'$u$'%20and%20t.promotion_id%20=%20'$p$')where%20curcount%20=%200";
//    "restrict_bank" = "";
//    "restrict_show" = "";
//    returnmax = 66;
//    returnvalue = 66;
//    "start_date" = "";
//
//}
@end

NS_ASSUME_NONNULL_END

//
//  DJLoginHelper.m
//  DJ
//
//  Created by 微笑吧阳光 on 2019/6/10.
//  Copyright © 2019 DJ. All rights reserved.
//

#import "DJLoginHelper.h"
#import "EDLoginMainViewController.h"
#import "JPUSHService.h"
@implementation DJLoginHelper

+(DJLoginHelper*)sharedInstance{
    static DJLoginHelper* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(& onceToken,^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)dj_showLoginVC:(UIViewController*)toViewController{
    
    EDLoginMainViewController * LoginMainVC = [[EDLoginMainViewController alloc] init];
    EDBaseNavigationController * loginNav = [[EDBaseNavigationController alloc]initWithRootViewController:LoginMainVC];
    dispatch_async(dispatch_get_main_queue(), ^{
        [toViewController presentViewController:loginNav animated:YES completion:nil];
    });
    
}

-(void)loginOut{
    
    [kUserDefaults setValue:@"" forKey:@"cj_uuid"];
    [kUserDefaults setValue:@"" forKey:@"cj_useralias"];
    [JPUSHService setAlias:@"88888888" completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
           if (iResCode ==0) {
                 NSLog(@"💗极光注册--loginOut--add_setAlias成功");
            }else{
                NSLog(@"💣极光注册--loginOut--add_setAlias失败");
            }
       } seq:10086];
    
    
}

//是否登陆
-(BOOL)is_Login{
    
    if (kStringIsEmpty([self cj_UserUuid])) {
        return NO;
    }else{
        return YES;
    }
}
//保存
-(void)save_Uuid:(NSString *)uuid{
    
    if (!kStringIsEmpty(uuid)) {
          [kUserDefaults setValue:uuid forKey:@"cj_uuid"];
    }
    

}



-(NSString *)cj_UserUuid{
    NSString * uuidString = [kUserDefaults objectForKey:@"cj_uuid"];
    return uuidString;
}

//获取本地别名
-(NSString *)cj_UserAlias{
    NSString * uuidString = [kUserDefaults objectForKey:@"cj_useralias"];
     return uuidString;
    
}
//保存
-(void)save_UserAlias:(NSString *)Alias{
    if (!kStringIsEmpty(Alias)) {
          [kUserDefaults setValue:Alias forKey:@"cj_useralias"];
        [JPUSHService setAlias:Alias completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            if (iResCode ==0) {
                  NSLog(@"💗极光注册add_setAlias成功----%@",Alias);
             }else{
                 NSLog(@"💣极光注册add_setAlias失败");
             }
        } seq:10086];
      }
    
}


//保存
-(void)save_Token:(NSString *)token{
    
    if (!kStringIsEmpty(token)) {
        [kUserDefaults setValue:token forKey:@"cj_token"];
    }
}

-(NSString *)cj_token{
    NSString * uuidString = [kUserDefaults objectForKey:@"cj_token"];
    return uuidString;
}
//保存用户资料
-(void)save_UserInfo:(NSDictionary *)userInfoDicc{
    
    if (!kDictIsEmpty(userInfoDicc)) {
        [kUserDefaults setValue:userInfoDicc forKey:@"cj_UserInfo"];
    }
 
}
//获取本地UserInfo
-(NSDictionary *)cj_UserDicc{
    NSDictionary * userInfo = [kUserDefaults objectForKey:@"cj_UserInfo"];
    return userInfo;
}
@end

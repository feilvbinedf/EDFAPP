//
//  DJLoginHelper.h
//  DJ
//
//  Created by 微笑吧阳光 on 2019/6/10.
//  Copyright © 2019 DJ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DJLoginHelper : NSObject


#pragma mark -- 单例调用
//创建单例
+ (DJLoginHelper *) sharedInstance;

-(void)loginOut;
//弹出登陆
-(void)dj_showLoginVC:(UIViewController*)toViewController;
//是否登陆
-(BOOL)is_Login;
//保存
-(void)save_Uuid:(NSString *)uuid;

//保存用户资料
-(void)save_UserInfo:(NSDictionary *)userInfoDicc;
//获取本地UID
-(NSString *)cj_UserUuid;

//获取本地别名
-(NSString *)cj_UserAlias;
//保存
-(void)save_UserAlias:(NSString *)Alias;
//获取本地token
-(NSString *)cj_token;
-(void)save_Token:(NSString *)token;
//获取本地UserInfo
-(NSDictionary *)cj_UserDicc;

@end

NS_ASSUME_NONNULL_END

//
//  LoginBottomView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/6.
//  Copyright © 2019 p. All rights reserved.
//

#import "LoginBottomView.h"

@implementation LoginBottomView

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    KViewBorder(self.cancelBtn, [UIColor whiteColor], 1);
    KViewRadius(self.cancelBtn, 3);
    KViewRadius(self.loginBtn, 3);
    
    self.webBtn.hidden = YES;
    KViewRadius(self.yuanB, 3);
    
}


-(void)setIs_Registered:(BOOL)is_Registered{
    
    _is_Registered = is_Registered;
    
    if (is_Registered==YES) {
        
        self.tishilab.text = @"";
        self.webBtn.hidden = YES;
        self.forgetBtn.hidden = YES;
        self.yuanB.hidden = YES;
        self.selectBtn.hidden  =YES;
    }else{
        self.tishilab.text = @"记住密码";
        self.webBtn.hidden = YES;
        self.forgetBtn.hidden = NO;
        self.yuanB.hidden = NO;
        self.selectBtn.hidden  =NO;
        
    }

    
}

@end

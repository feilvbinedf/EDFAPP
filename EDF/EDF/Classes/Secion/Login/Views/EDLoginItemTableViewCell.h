//
//  EDLoginItemTableViewCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/6.

//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EDLoginItemTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textTF;
@property (weak, nonatomic) IBOutlet UIImageView *leftImage;

@end

NS_ASSUME_NONNULL_END

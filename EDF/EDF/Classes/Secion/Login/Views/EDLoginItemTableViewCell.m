//
//  EDLoginItemTableViewCell.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/6.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDLoginItemTableViewCell.h"
#import <objc/runtime.h>
@implementation EDLoginItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(self.textTF, ivar);
    placeholderLabel.textColor = kRGBAColor(255, 255, 255, 0.7);

    //[self.textTF setValue:kRGBAColor(255, 255, 255, 0.7) forKeyPath:@"placeholderLabel.textColor"];
    
    self.textTF.textColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

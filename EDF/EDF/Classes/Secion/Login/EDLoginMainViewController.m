//
//  EDLoginMainViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDLoginMainViewController.h"
#import "LXSegmentBtnView.h"
#import "EDLoginItemTableViewCell.h"
#import "LoginBottomView.h"
#import "RestrictionInput.h"
#import "EDBangdingPhoneOrEmailViewCobntroller.h"
#import "EDChangeWithdrawalsCodeViewController.h"

@interface EDLoginMainViewController ()
<UINavigationControllerDelegate,LXSegmentBtnViewDelegate>
@property (nonatomic , strong) LXSegmentBtnView *segmentView1;
@property (nonatomic , strong) LoginBottomView *tabBottomView;
@property (nonatomic , strong) UIView * headerV;
@property (nonatomic , assign) BOOL  is_Registered;
@property(nonatomic,copy)    NSString  * agent_noStr;

@end

@implementation EDLoginMainViewController
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.headerV.frame = CGRectMake(0, 0, kScreenWidth, kStatusBarAndNavigationBarHeight+40+35+40+30);
    
    self.tabBottomView.frame =CGRectMake(0, 0, kScreenWidth, 250);
    self.ks_TableView.tableFooterView = self.tabBottomView;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 设置导航控制器的代理为self
    self.navigationController.delegate = self;
    
    self.is_Registered =NO;
    [self stepUI];
    
    NSString * contentStr = [self readFromPasteBoard];
    if ([contentStr containsString:@"agent_no"]) {
        NSLog(@"----readFromPasteBoard   ---%@",contentStr);
        NSData *JSONData = [contentStr dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
        if (!kDictIsEmpty(responseJSON)) {
            self.agent_noStr =[responseJSON jsonString:@"agent_no"];
            
        }
    }
    
    
}

-(void)stepUI{
    
    UIImageView  * bgImage = [BBControl createImageViewFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) imageName:@"ic_login_bg"];
    bgImage.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:bgImage];
    UIView * darkV =  [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    darkV.backgroundColor = kRGBAColor(0, 0, 0, 0.5);
    [bgImage addSubview:darkV];
    
    self.headerV  = [BBControl createViewWithFrame:CGRectMake(0, 0, kScreenWidth, kStatusBarAndNavigationBarHeight+40+35+40+30)];
    self.headerV.backgroundColor = [UIColor clearColor];
    
    UIImageView  * bgImage2 = [BBControl createImageViewFrame:CGRectMake(50, kStatusBarAndNavigationBarHeight, kScreenWidth-100, 40) imageName:@"logo_dark"];
    bgImage2.contentMode = UIViewContentModeScaleAspectFit;
    [self.headerV addSubview:bgImage2];
    
    CGFloat btnW = 100;
    self.segmentView1.frame = CGRectMake((self.view.frame.size.width - btnW*2)/2, bgImage2.bottom+35, btnW*2, 40);
    self.segmentView1.btnTitleArray = @[@"登录",@"注册"];
    [self.headerV addSubview:_segmentView1];

    self.tabBottomView = [[NSBundle mainBundle]loadNibNamed:@"LoginBottomView" owner:self options:nil].lastObject;
    self.tabBottomView.frame = CGRectMake(0, 0, kScreenWidth, 250);
    self.tabBottomView.is_Registered = NO;
    self.ks_TableView.tableFooterView = self.tabBottomView;

    self.ks_TableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    self.ks_TableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.ks_TableView];
    
    self.ks_TableView.tableHeaderView = self.headerV;

    //点击事件
    
    WEAKSELF
    [self.tabBottomView.cancelBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf.view endEditing:YES];
       //  [[NSNotificationCenter defaultCenter] postNotificationName:@"registeredSusessNotificationse" object:nil];
        [strongSelf dismissViewControllerAnimated:YES completion:nil];
        
    }];
    

    [self.tabBottomView.loginBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        if (strongSelf.is_Registered==YES) {
               [strongSelf user_ResigerCheakNiCkName];
        }else{
               [strongSelf userLoginRequest];
        }
    }];
    [self.tabBottomView.selectBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        strongSelf.tabBottomView.selectBtn.selected = !strongSelf.tabBottomView.selectBtn.selected ;
    }];
    
    [self.tabBottomView.forgetBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        [strongSelf forgetbtnClick];
        
    }];

}

-(void)forgetbtnClick{
    
    EDBangdingPhoneOrEmailViewCobntroller * bangdingVC = [EDBangdingPhoneOrEmailViewCobntroller new];
    bangdingVC.title = @"验证手机号";
    bangdingVC.showType = 2;
    [self.navigationController pushViewController:bangdingVC animated:YES];
}
#pragma mark 粘贴板 : 读取系统粘贴板
-(NSString *)readFromPasteBoard{
    
    NSString *content = [[UIPasteboard generalPasteboard] string];
    return content;
}

#pragma  mark -----userLoginRequest
-(void)userLoginRequest{
    [self.view endEditing:YES];
    
    EDLoginItemTableViewCell *fristCell   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    EDLoginItemTableViewCell *secCell   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    if (kStringIsEmpty(fristCell.textTF.text)) {
        [JMNotifyView showNotify:@"请输入用户名~" isSuccessful:NO];
        return;
    }
    if (kStringIsEmpty(secCell.textTF.text)) {
        [JMNotifyView showNotify:@"请输入密码~" isSuccessful:NO];
        return;
    }

    if (secCell.textTF.text.length<6 ||secCell.textTF.text.length>16 ) {
        [JMNotifyView showNotify:@"密码长度为6~16个字符~" isSuccessful:NO];
        return;
    }
    [self showLoadingAnimation];
    
    NSString  * loginurl = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"user_loginNnes"];
    NSMutableDictionary * diccc = [NSMutableDictionary dictionary];
    [diccc setValue:fristCell.textTF.text forKey:@"username"];
    [diccc setValue:secCell.textTF.text forKey:@"password"];
     [diccc setValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"macAddress"];
    //loginurl = [loginurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :loginurl parameters:diccc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
           YYLog(@"--user_login--responseObject----%@",dict);
        if ([[dict jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (strongSelf.tabBottomView.selectBtn.selected ==YES) {
                [kUserDefaults setValue:diccc forKey:@"user_loginInfo"];
            }
            NSDictionary * dataDicc = [dict jsonDict:@"data"];
            [JMNotifyView showNotify:@"登陆成功" isSuccessful:YES];
            NSString * cj_Uid = [[dict jsonDict:@"data"] jsonString:@"user_id"];
            [[DJLoginHelper sharedInstance]save_Uuid:cj_Uid];
            NSString * cj_aliess = [dataDicc jsonString:@"alias"];
            [[DJLoginHelper sharedInstance]save_UserAlias:cj_aliess];
            [[NSNotificationCenter defaultCenter] postNotificationName:LoginSusessNotificationse object:nil];
            [AppDelegate shareAppdelegate].index =0;
            
      
            [strongSelf dismissViewControllerAnimated:YES completion:nil];
            
            
            
            
        }else{
              [JMNotifyView showNotify:[dict jsonString:@"message"] isSuccessful:NO];
        }
        [strongSelf stopLoadingAnimation];
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
        [self stopLoadingAnimation];
    }];
    
    
    
    
    
    
}
//- (NSString *)UUID {
////    KeychainItemWrapper *keyChainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"MYAppID" accessGroup:@"com.test.app"];
////    NSString *UUID = [keyChainWrapper objectForKey:(__bridge id)kSecValueData];
////
//    if (UUID == nil || UUID.length == 0) {
//        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
////        [keyChainWrapper setObject:UUID forKey:(__bridge id)kSecValueData];
//    }
//
//    return UUID;
//}
-(void)user_ResigerCheakNiCkName{
 
    [self.view endEditing:YES];
    
    EDLoginItemTableViewCell *fristCell   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    if (kStringIsEmpty(fristCell.textTF.text)) {
        [JMNotifyView showNotify:@"请输入用户名~" isSuccessful:NO];
        return;
    }

    NSInteger chackType = [self checkIsHaveNumAndLetter:fristCell.textTF.text];
    if (chackType==4) {
        [JMNotifyView showNotify:@"用户名只能为字母或数字" isSuccessful:NO];
    }else if (chackType==5){
        [JMNotifyView showNotify:@"用户名长度必须在6~10个字符" isSuccessful:NO];
    }else if (chackType ==6){
        [JMNotifyView showNotify:@"请输入用户名~" isSuccessful:NO];
    }else{
        NSString * subStr = [fristCell.textTF.text substringToIndex:4];
        if ([subStr isEqualToString:@"test"] || [subStr isEqualToString:@"Test"]) {
            [JMNotifyView showNotify:@"用户名开头不能包含“test”~" isSuccessful:NO];
            return;
        }
        
        //User_userRegisterVerificationUsernamee
        [self chack_nameTextTFRequest];

    }

}

-(void)chack_nameTextTFRequest{
    [self showLoadingAnimation];
    EDLoginItemTableViewCell *cell0   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_userRegisterVerificationUsernamee"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:cell0.textTF.text forKey:@"u_name"];
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        if (!kDictIsEmpty(dict001)) {
            if ([[dict001 jsonString:@"data"] isEqualToString:@"NOEXISTENCE"]) {
                [strongSelf cheakPassWord];
            }else{
                [JMNotifyView showNotify:@"该用户名已被使用~"isSuccessful:NO];
                [self stopLoadingAnimation];
            }
            
        }else{
            [JMNotifyView showNotify:@"校验用户名失败，请重试~" isSuccessful:NO];
            [self stopLoadingAnimation];
        }
   
   
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"校验用户名失败，请重试~" isSuccessful:NO];
        [self stopLoadingAnimation];
    }];
}


-(void)cheakPassWord{
    
    EDLoginItemTableViewCell *cell02   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

    // || [self isXiaoShuDian:cell02.textTF.text ] ==NO
    if ([self isBlank:cell02.textTF.text]==YES || [cell02.textTF.text containsString:@"."]) {
        [JMNotifyView showNotify:@"密码不能包含空格和小数点" isSuccessful:NO];
        return;
    }
    
    if ([self judgePassWordLegal:cell02.textTF.text] ==NO ) {
        [JMNotifyView showNotify:@"密码长度为6~16个字符，不能含有空格或小数点，不能是9位以下纯数字" isSuccessful:NO];
        return;
    }
    
    [self cheakSecPassWord02];
}

-(void)cheakSecPassWord02{
    EDLoginItemTableViewCell *cell02   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    EDLoginItemTableViewCell *secCell02   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];

    if (kStringIsEmpty(secCell02.textTF.text)) {
        [JMNotifyView showNotify:@"请输入确认密码" isSuccessful:NO];
        return;
    }
    if (![cell02.textTF.text isEqualToString:secCell02.textTF.text]) {
        [JMNotifyView showNotify:@"两次密码输入不一致~" isSuccessful:NO];
        return;
    }

    [self user_ResigerRerquest];
    
}

-(void)user_ResigerRerquest{
       //User_userRegister
    [self showLoadingAnimation];
    EDLoginItemTableViewCell *fristCell   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    EDLoginItemTableViewCell *secCell   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
      EDLoginItemTableViewCell *secCell02   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    EDLoginItemTableViewCell *secCell03   = [self.ks_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"User_userRegisternee"];
    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
    [dicc setValue:fristCell.textTF.text forKey:@"username"];
    [dicc setValue:secCell.textTF.text forKey:@"password"];
    [dicc setValue:secCell02.textTF.text forKey:@"confirmpassword"];
    [dicc setValue:secCell03.textTF.text forKey:@"iiuv"];
    if (kStringIsEmpty(self.agent_noStr)) {
        [dicc setValue:@"" forKey:@"aff"];
    }else{
        [dicc setValue:self.agent_noStr forKey:@"aff"];
    }
    WEAKSELF
    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
        STRONGSELFFor(weakSelf)
        NSError *errorData;
        NSData * data =[responseObject dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSDictionary *dict001 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
        NSDictionary *dict  =[dict001 jsonDict:@"data"];
        if ([[dict001 jsonString:@"statusCode"] isEqualToString:@"SUCCESS"]) {
            if (!kStringIsEmpty([dict jsonString:@"uid"])) {
                 [JMNotifyView showNotify:@"注册成功"  isSuccessful:YES];
                [[DJLoginHelper sharedInstance]save_Uuid:[dict jsonString:@"uid"]];
                 [[DJLoginHelper sharedInstance]save_UserAlias:[dict jsonString:@"alias"]];
                [[NSNotificationCenter defaultCenter] postNotificationName:LoginSusessNotificationse object:nil];
                [AppDelegate shareAppdelegate].index =0;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"registeredSusessNotificationse" object:nil];
                [strongSelf dismissViewControllerAnimated:YES completion:nil];
            }
        }else{
            [JMNotifyView showNotify:[dict jsonString:@"message"]  isSuccessful:NO];
        }
            [self stopLoadingAnimation];
  
        NSLog(@"💗---LogincconRegApiAction---%@",dict);
        
    } failure:^(id responseObject, NSError *error) {
        NSLog(@"error----%@",error);
        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
        [self stopLoadingAnimation];
    }];
    
    
    
}

#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.is_Registered ==YES) {
        return 4;
    }else{
        return 2;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"EDLoginItemTableViewCell";
    EDLoginItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    NSArray * imageIconArry = @[@"account name",@"login_code",@"login_code",@"ic_reg_iies.png"];
    if (cell == nil) {
        cell = (EDLoginItemTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"EDLoginItemTableViewCell" owner:self options:nil] lastObject];
    }
    cell.leftImage.image = [UIImage imageNamed:imageIconArry[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self cofig_CellText:cell With:indexPath];
    return cell;
    
}

-(void)cofig_CellText:(EDLoginItemTableViewCell*)cell With:(NSIndexPath *)indexPath{

    cell.textTF.keyboardType =UIKeyboardTypeDefault;
    cell.textTF.secureTextEntry = NO;
    cell.textTF.tag = indexPath.row +100;

    [cell.textTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [cell.textTF addTarget:self action:@selector(editingDidEnd:) forControlEvents:UIControlEventEditingDidEnd];
    if (self.is_Registered ==NO) {
        switch (indexPath.row) {
            case 0:
            {
                NSDictionary * user_loginInfo = [kUserDefaults objectForKey:@"user_loginInfo"];
                if (!kDictIsEmpty(user_loginInfo)) {
                               cell.textTF.placeholder =@"请输入用户名";
                            cell.textTF.text = [user_loginInfo jsonString:@"username"];
                }

            }
                break;
            case 1:
            {
                cell.textTF.keyboardType =UIKeyboardTypeNumbersAndPunctuation;
                cell.textTF.secureTextEntry = YES;
                if (@available(iOS 11.0, *)) {
                    cell.textTF.textContentType = UITextContentTypeName;
                }
                cell.textTF.placeholder =@"请输入密码";
                NSDictionary * user_loginInfo = [kUserDefaults objectForKey:@"user_loginInfo"];
                if (!kDictIsEmpty(user_loginInfo)) {
                    cell.textTF.text = [user_loginInfo jsonString:@"password"];
                }
                
                //cell.textTF.text = @"fff021021";
            }
                break;
                
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:
            {
                 cell.textTF.placeholder =@"请输入用户名";
            }
                break;
            case 1:
            {
                cell.textTF.placeholder =@"6-16位字母或数字";
                 cell.textTF.secureTextEntry = YES;
                if (@available(iOS 11.0, *)) {
                    cell.textTF.textContentType = UITextContentTypeName;
                }
                cell.textTF.keyboardType =UIKeyboardTypeNumbersAndPunctuation;
            }
                break;
            case 2:
            {
                cell.textTF.placeholder =@"请再次输入密码";
                cell.textTF.secureTextEntry = YES;
                if (@available(iOS 11.0, *)) {
                    cell.textTF.textContentType = UITextContentTypeName;
                }
                cell.textTF.keyboardType =UIKeyboardTypeNumbersAndPunctuation;
            }
                break;
            case 3:
            {
                cell.textTF.placeholder =@"请输入邀请码(选填)";
                cell.textTF.secureTextEntry = NO;
                cell.textTF.keyboardType =UIKeyboardTypeNumbersAndPunctuation;
            }
                break;
                
            default:
                break;
        }

    }
    

}

#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)textFieldDidChange:(UITextField *)textField {
    switch (textField.tag) {
        case 100:
        {
            [RestrictionInput restrictionInputTextField:textField maxNumber:11 showView:self.view showErrorMessage:@"0~11字符~"];
        }
            break;
        case 101:
        {
            [RestrictionInput restrictionInputTextField:textField maxNumber:16 showView:self.view showErrorMessage:@"0~11字符~"];
        }
            break;
        case 102:
        {
            [RestrictionInput restrictionInputTextField:textField maxNumber:16 showView:self.view showErrorMessage:@"0~11字符~"];
        }
            break;
        case 103:
        {
            [RestrictionInput restrictionInputTextField:textField maxNumber:9 showView:self.view showErrorMessage:@"0~11字符~"];
        }
            break;
            
        default:
            break;
    }
    
}

- (void)editingDidEnd:(UITextField *)textField{
    
    if (self.is_Registered ==YES) {
        
        switch (textField.tag) {
            case 100:
            {
                if (textField.text.length<6) {
                    [JMNotifyView showNotify:@"用户名长度必须在6~10个字符" isSuccessful:NO];
                    return;
                }
                if (!kStringIsEmpty(textField.text)) {
                    
                    NSString * subStr = [textField.text substringToIndex:4];
                    if ([subStr isEqualToString:@"test"] || [subStr isEqualToString:@"Test"]) {
                        [JMNotifyView showNotify:@"用户名开头不能包含“test”~" isSuccessful:NO];
                        return;
                    }
                    
                    NSInteger chackType = [self checkIsHaveNumAndLetter:textField.text];
                    if (chackType==4) {
                        [JMNotifyView showNotify:@"用户名只能为字母或数字" isSuccessful:NO];
                        return;
                    }else if (chackType==5){
                        [JMNotifyView showNotify:@"用户名长度必须在6~10个字符" isSuccessful:NO];
                        return;
                    }else if (chackType ==6){
                        [JMNotifyView showNotify:@"请输入用户名~" isSuccessful:NO];
                        return;
                    }
 
                    //结束编辑后要校验一下用户名是否使用
                    //LoginccCheckUsernameAction
                    NSString  * url = [[YINetWorkAPIGenerate sharedInstance] APINomark:@"LoginccCheckUsernameAction"];
                    NSMutableDictionary  *dicc = [NSMutableDictionary dictionary];
                    [dicc setValue:textField.text forKey:@"u_name"];
                    WEAKSELF
                    [[SmileHttpTool sharedInstance] GET :url parameters:dicc origin:YES success:^(NSInteger statusCode, NSString *message, id responseObject) {
                        STRONGSELFFor(weakSelf)
                        NSError *errorData;
                        if ([responseObject isEqualToString:@"norep"]) {
                            [JMNotifyView showNotify:@"恭喜，当前用户名可使用~" isSuccessful:YES];
                        }else {
                            [JMNotifyView showNotify:@"当前用户名已被使用~" isSuccessful:NO];
                        }
                    } failure:^(id responseObject, NSError *error) {
                        NSLog(@"error----%@",error);
                        [JMNotifyView showNotify:@"网络错误，请重试~" isSuccessful:NO];
                    }];
                }
               
                
            }
                break;
                
            default:
                break;
        }
    }

    
}
#pragma ---LXSegmentBtnViewDelegate---
-(void)lxSegmentView:(LXSegmentBtnView *)segmentView selectIndex:(NSInteger)selectIndex{
    if (segmentView == self.segmentView1) {
        NSLog(@"delegate ,index = %zd" , selectIndex);
        if (selectIndex ==0) {
            self.is_Registered =NO;
        }else{
            self.is_Registered =YES;
        }
        if (self.is_Registered ==YES) {
            [self.tabBottomView.loginBtn setTitle:@"注 册" forState:0];
        }else{
            [self.tabBottomView.loginBtn setTitle:@"登 录" forState:0];
        }
        self.tabBottomView.is_Registered =self.is_Registered;
        [self.ks_TableView reloadData];
    }
}

-(LXSegmentBtnView *)segmentView1{
    if (!_segmentView1) {
        _segmentView1 = [[LXSegmentBtnView alloc] init];
        _segmentView1.delegate = self;
        _segmentView1.btnBackgroundNormalColor = [UIColor clearColor];
        _segmentView1.btnBackgroundSelectColor = kRGBAColor(255, 255, 255, 0.2);
        _segmentView1.btnTitleNormalColor =kRGBAColor(255, 255, 255, 0.5);
        _segmentView1.btnTitleSelectColor = [UIColor whiteColor];
        _segmentView1.titleFont = [UIFont systemFontOfSize:16];
    
    }
    return _segmentView1;
}

#pragma mark - UINavigationControllerDelegate
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
    
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
}

- (void)dealloc {
    self.navigationController.delegate = nil;
}

//直接调用这个方法就行
-(int)checkIsHaveNumAndLetter:(NSString*)password{
    
    if (kStringIsEmpty(password)) {
        
        return 6;
    }
    
    //长度条件
    if (password.length<6 || password.length>11) {
        return 5;
    }
    
    //数字条件
    
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]"options:NSRegularExpressionCaseInsensitive error:nil];
    //符合数字条件的有几个字节
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:password
                                 
                                                                       options:NSMatchingReportProgress
                                 
                                                                         range:NSMakeRange(0, password.length)];
    //英文字条件
    
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]"options:NSRegularExpressionCaseInsensitive error:nil];
    //符合英文字条件的有几个字节
    
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:password options:NSMatchingReportProgress range:NSMakeRange(0, password.length)];
    
    if (tNumMatchCount == password.length) {
        //全部符合数字，表示沒有英文
        return 1;
        
    } else if (tLetterMatchCount == password.length) {
        
        //全部符合英文，表示沒有数字
        return 2;
        
    } else if (tNumMatchCount + tLetterMatchCount == password.length) {
        
        //符合英文和符合数字条件的相加等于密码长度
        return 3;
        
    } else {
        return 4;
        //可能包含标点符号的情況，或是包含非英文的文字，这里再依照需求详细判断想呈现的错误
    }
    
    
    
}
- (BOOL)judgePassWordLegal:(NSString *)pass{
    BOOL result = false;
    if ([pass length] >= PassworldLastlength){
        // 判断长度大于6位后再接着判断是否同时包含数字和字符
        NSString * regex =@"^(?=.*[a-zA-Z])(.{6,16})$";
        //@"^(?=.*[0-9])(?=.*[a-zA-Z])(.{6,16})$";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        result = [pred evaluateWithObject:pass];
        if (result ==NO) {
            return NO;
        }
    }
    NSRange range = [pass rangeOfString:@" "];
    if (range.location != NSNotFound) {
        return NO; //yes代表包含空格
    }else {
        
        NSRange range = [pass rangeOfString:@"."];
        if (range.location != NSNotFound) {
            return NO; //yes
        }else {
            
            return YES; //反之
        }
    }
    return result;
    
}

/**
 * 判断字符串是否包含空格
 */
-(BOOL)isBlank:(NSString *)str{
    NSRange _range = [str rangeOfString:@" "];
    if (_range.location != NSNotFound) {
        //有空格
        // NSLog(@"有空格");
        return YES;
    }else {
        //没有空格
        // NSLog(@"没有空格");
        return NO;
    }
}
/**
 * 判断字符串是否包含空格
 */
-(BOOL)isXiaoShuDian:(NSString *)str{
    NSRange _range = [str rangeOfString:@"."];
    if (_range.location != NSNotFound) {
        //有空格
        // NSLog(@"有小数点");
        return YES;
    }else {
        //没有空格
        // NSLog(@"没有小数点");
        return NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

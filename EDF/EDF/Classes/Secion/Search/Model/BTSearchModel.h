//
//  BTSearchModel.h
//  BreadTrip
//
//  Created by Vivitickey on 2017/4/23.
//  Copyright © 2017年 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSearchModel : NSObject


//名字
@property (nonatomic,copy) NSString *name;


@property (nonatomic,assign) float lat;

@property (nonatomic,assign) float lng;


//url地址
@property (nonatomic,copy) NSString *url;

@end

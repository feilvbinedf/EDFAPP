//
//  HistoryManger.h
//  EDF
//
//  Created by p on 2019/7/10.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^searchBlock)(BOOL isSuccess,NSArray *array);

@interface HistoryManger : NSObject
/*! 保存历史搜索 */
- (void)saveHistory :(NSString *)text;
/*! 读取历史搜索 */
- (NSArray *)readHistory;
/*! 删除历史搜索 */
- (void)deleteHistory:(NSString *)text;
@end


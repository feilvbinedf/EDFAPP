//
//  BTSearchView.h
//  BreadTrip
//
//  Created by Vivitickey on 2017/4/19.
//  Copyright © 2017年 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryManger.h"
@interface BTSearchView : UIScrollView
@property(nonatomic,strong)NSArray * historyArray;
@property(nonatomic,strong)NSMutableArray * hotArray;
@property(nonatomic,strong)NSMutableArray * allArray;
@property(nonatomic,strong)HistoryManger *searchVM;

@property (nonatomic,readwrite)UIView * historicalBGView;
@property (nonatomic,readwrite)UIView * popularBGView;
@property (nonatomic,readwrite)UIView * gamerBGView;
-(void)setupUI;
@end

//
//  EDSearchResultViewController.h
//  EDF
//
//  Created by p on 2019/7/24.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "HistoryManger.h"
@interface EDSearchResultViewController : EDBaseViewController
//名字
@property (nonatomic,copy) NSString *name;
@property(nonatomic,strong)HistoryManger *searchVM;
@end

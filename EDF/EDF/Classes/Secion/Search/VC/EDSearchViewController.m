//
//  EDSearchViewController.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/7/4.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDSearchViewController.h"
#import "BTSearchView.h"
#import "HistoryManger.h"
#import "EDSearchResultViewController.h"
@interface EDSearchViewController ()<UITextFieldDelegate>
{
    UIView *_bgView;
    UITextField *_textField;
}
@property (nonatomic, strong) NSMutableArray * dataArray;//
@property (nonatomic,strong) BTSearchView *searchView;
@property(nonatomic,strong)HistoryManger *searchVM;

@end

@implementation EDSearchViewController
-  (void)viewDidAppear:(BOOL)animated
{
    [_searchView setupUI];
    [_searchView setContentSize:CGSizeMake(UIScreenWidth, _searchView.historicalBGView.height + _searchView.popularBGView.height+ _searchView.gamerBGView.height +40)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = WHITECOLOR;
    _searchVM = [[HistoryManger alloc]init];
    self.navigationItem.title = @"";
    [self setupSearchBar];    
    [self setupSearchView];
}
-(void)setupSearchBar{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 30)];
    //设置圆角效果
    bgView.layer.cornerRadius = 2;
    bgView.layer.masksToBounds = YES;
    
    bgView.backgroundColor = [UIColor whiteColor];
    //输入框
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(bgView.frame) - 20, CGRectGetHeight(bgView.frame))];
    _textField.font = AutoFont(14);
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imageView.image = [UIImage imageNamed:@"search"];
    _textField.leftView = imageView;
    _textField.leftViewMode = UITextFieldViewModeAlways;
    _textField.textColor = RGBA(68, 68, 68, 1);
    //清除按钮
    _textField.clearButtonMode =UITextFieldViewModeWhileEditing;
    
    _textField.delegate = self;
    //键盘属性
    _textField.returnKeyType = UIReturnKeySearch;
    
    //为textField设置属性占位符
    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入游戏名称" attributes:@{NSForegroundColorAttributeName:RGBA(161, 161, 161, 1)}];
    _textField.textAlignment = NSTextAlignmentCenter;


    [bgView addSubview:_textField];
    
    self.navigationItem.titleView = bgView;
    
}

//输入框开始编辑
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    

}


-(void)clickCancel{
    [_textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (!kStringIsEmpty(textField.text)) {
        [_searchVM saveHistory:textField.text];
        [_searchView setupUI];
        [_searchView setContentSize:CGSizeMake(UIScreenWidth, _searchView.historicalBGView.height + _searchView.popularBGView.height)];
        
        EDSearchResultViewController * searchVC = [[EDSearchResultViewController alloc] init];
        searchVC.name = textField.text;
        searchVC.searchVM = _searchVM;
        [[WJUnit currentViewController].navigationController pushViewController:searchVC animated:YES];
    }
  
    [textField resignFirstResponder];
    return YES;
}
-(void)clickRightUserBarButton{
    
   
}

-(void)setupSearchView{
    
    _searchView = [[BTSearchView alloc]initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth,ContentViewHeight)];
    //    获取历史记录
    _searchView.searchVM = _searchVM;
    _searchView.historyArray = [_searchVM readHistory];
    [self.view addSubview:_searchView];
    _searchView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
}


@end

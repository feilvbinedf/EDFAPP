//
//  HomeTableViewCell.m
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "PTPlatformViewController.h"
@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     self.selectionStyle = UITableViewCellAccessoryNone;
    [self.contentView addSubview:self.gameView];
}
- (UIScrollView *)gameView
{
    if (!_gameView) {
        _gameView = [[UIScrollView alloc] initWithFrame:CGRectMake(15, self.gameIcon.bottom+10, UIScreenWidth,0)];
    }
    return _gameView;
}
//循环创建Button
-(void )addButtonWithGameArray:(NSArray *)arr{
    CGFloat h = 0;
    CGFloat w = 0;
    for (UIView *view in _gameView.subviews) {
        if (view.tag >0) {
            [view removeFromSuperview];
        }
        
    }
    _games = arr;
    for (int i = 0; i < arr.count; i++) {
        HomeModel * model = arr[i];
        
        UIButton *button = [[UIButton alloc] init];
        
        [button addTarget:self action:@selector(gameClick:) forControlEvents:UIControlEventTouchUpInside];
        button.tag =  i;
        [button setImage:[UIImage imageNamed:model.image] forState:0 ];
        KViewRadius(button, 2);
        if (arr.count<4) {
            button.frame = CGRectMake(12+ i * (107+8), 6, 107, 50);//重设button的frame
            h = 1 *50 ;
            w = UIScreenWidth ;
        }else
        {
             h = 2 *50 ;
             w = 8 * 120 ;
            int value = i % (2);
            if (value ==0)
            {//0， 2，4 6 8
                 button.frame = CGRectMake(12+ i/2 * (107+8), 6, 107, 50);//重设button的frame
            }else
            {
                //1-1，3-1，5 7
                button.frame = CGRectMake(12+  (i-1)/2 * (107+8), 50+16, 107, 50);//重设button的frame
            }
            
        }
        [_gameView addSubview:button];
        
        if ([model.is_promotion isEqualToString:@"是"]) {
            UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake(button.mj_x-2, button.mj_y-1, 27, 27)];
            icon.image = [UIImage imageNamed:@"huodong001"];
            icon.tag=100;
            //icon.contentMode = UIViewContentModeScaleAspectFit;
            [_gameView addSubview:icon];
        }
    }
    //设置scrollView 滚动
    [_gameView setContentSize:CGSizeMake(w, h)];
    //隐藏滚动条
    _gameView.showsHorizontalScrollIndicator = NO;

    
}

- (void)gameClick:(UIButton *)sender
{
    
    HomeModel * model = _games[sender.tag];
    
    if ([model.type isEqualToString:@"捕鱼达人"] || [model.type isEqualToString:@"真人娱乐"]) {
        if ([model.id isEqualToString:@"o"]) {
            PTPlatformViewController * PTVC = [[PTPlatformViewController alloc] init];
            PTVC.modle = model;
            [[WJUnit currentViewController].navigationController pushViewController :PTVC animated:YES];
        }else
        {
            if (self.getGameUrlBlock) {
                self.getGameUrlBlock(model);
            }
        }
        
    }else
    {
        PTPlatformViewController * PTVC = [[PTPlatformViewController alloc] init];
        PTVC.modle = model;
        [[WJUnit currentViewController].navigationController pushViewController :PTVC animated:YES];
    }
   
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


//
//  HomeTableViewCell.h
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UIImageView *gameIcon;
@property (strong, nonatomic)  UIScrollView *gameView;
@property (strong, nonatomic)  NSArray *games;
-(void )addButtonWithGameArray:(NSArray *)arr;
@property (copy, nonatomic) void(^getGameUrlBlock)(HomeModel * modle);
@end

NS_ASSUME_NONNULL_END

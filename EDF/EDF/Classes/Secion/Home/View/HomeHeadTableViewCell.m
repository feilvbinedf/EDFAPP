//
//  HomeHeadTableViewCell.m
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import "HomeHeadTableViewCell.h"

@implementation HomeHeadTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
 
   
    self.balanceBtn.titleLabel.lineBreakMode = 0;//这句话很重要，不加这句话加上换行符也没用
    self.balanceBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  PTCollectionViewCell.h
//  EDF
//
//  Created by p on 2019/7/9.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PTCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *activeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *gameicon;
@property (weak, nonatomic) IBOutlet UIButton *playBat;
@property (weak, nonatomic) IBOutlet UIButton *collBtn;
@property(nonatomic,strong)PTModel * model;
@property (copy, nonatomic) void(^getGameUrlBlock)(PTModel * modle);
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@end

NS_ASSUME_NONNULL_END

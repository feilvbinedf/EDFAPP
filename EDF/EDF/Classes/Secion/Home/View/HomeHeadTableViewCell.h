//
//  HomeHeadTableViewCell.h
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeHeadTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *balanceBtn;
@property (weak, nonatomic) IBOutlet UIButton *depositBtn;
@property (weak, nonatomic) IBOutlet UIButton *withdrawBtn;
@property (weak, nonatomic) IBOutlet UIButton *transferBtn;
@end

NS_ASSUME_NONNULL_END

//
//  PTCollectionViewCell.m
//  EDF
//
//  Created by p on 2019/7/9.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PTCollectionViewCell.h"

@implementation PTCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gameicon.contentMode             =UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage)];
    [self.gameicon addGestureRecognizer:tapGesture];
    self.gameicon.userInteractionEnabled = YES;
    
}
- (void)clickImage
{
    if (self.getGameUrlBlock) {
        self.getGameUrlBlock(_model);
    }
}
- (void)setModel:(PTModel *)model
{
    _model = model;
    _activeIcon.hidden = YES;
    NSString * str = [NSString stringWithFormat:@"%@%@",PTimageGameURL,model.pic];
    if ([model.is_promotion isEqualToString:@"是"]) {
        _activeIcon.hidden = NO;
    }
    [self.gameicon sd_setImageWithURL:[NSURL URLWithString:str]];
    [self.collBtn setTitle:model.displayname forState:0];
    self.collBtn.selected = model.isusergame;
    [self.collBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.collBtn.imageView.width , 0, self.collBtn.imageView.width)];
    [self.collBtn setImageEdgeInsets:UIEdgeInsetsMake(0, self.collBtn.titleLabel.width , 0, - self.collBtn.titleLabel.width)];
}
@end

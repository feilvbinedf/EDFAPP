//
//  EDHomeTableHeadView.h
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HW3DBannerView.h"
#import "SGAdvertScrollView.h"
NS_ASSUME_NONNULL_BEGIN

@interface EDHomeTableHeadView : UIView<SGAdvertScrollViewDelegate>
@property (nonatomic,strong) HW3DBannerView *scrollView;
@property (strong, nonatomic)  SGAdvertScrollView *advertScrollViewCenter;
@property (nonatomic,strong) UIView     *noticeview;
@property (nonatomic,strong) UIImageView *noticeimageview;
@property (copy, nonatomic) void(^Advertblock)(NSInteger index);
@property (copy, nonatomic) void(^BancelsUrlblock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END

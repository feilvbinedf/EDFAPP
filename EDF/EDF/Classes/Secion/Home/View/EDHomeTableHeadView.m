//
//  EDHomeTableHeadView.m
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDHomeTableHeadView.h"

@implementation EDHomeTableHeadView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self get_up];
    }
    return self;
    
}
- (void)get_up
{
    [self addSubview:self.scrollView];
    
    [self addSubview:self.noticeview];
    [_noticeview addSubview:self.noticeimageview];
    [_noticeview addSubview:self.advertScrollViewCenter];
   
    
}
/// 代理方法
- (void)advertScrollView:(SGAdvertScrollView *)advertScrollView didSelectedItemAtIndex:(NSInteger)index {
    WeakSelf
    if (self.Advertblock) {
        weakSelf.Advertblock(index);
    }
}
- (HW3DBannerView*)scrollView{
    
    if (!_scrollView) {
        _scrollView =  [HW3DBannerView initWithFrame:CGRectMake(0, 0, UIScreenWidth ,  RealValue_H(310 +56)) imageSpacing:10 imageWidth:UIScreenWidth - 60];
        _scrollView.placeHolderImage = [UIImage imageNamed:@"placeHolder"]; // 设置占位图片
        _scrollView.backgroundColor = WHITECOLOR;
        _scrollView.curPageControlColor = TABTITLECOLOR;
        _scrollView.initAlpha = 1; // 设置两边卡片的透明度
        _scrollView.imageRadius = 4; // 设置卡片圆角
        _scrollView.imageHeightPoor = 6; // 设置中间卡片与两边卡片的高度差
        
        WeakSelf
        _scrollView.clickImageBlock = ^(NSInteger currentIndex) { // 点击中间图片的回调
            
            if (weakSelf.BancelsUrlblock) {
                weakSelf.BancelsUrlblock(currentIndex);
            }
            
        };
    }
    return _scrollView;
}


- (UIView *)noticeview
{
    if (!_noticeview) {
        _noticeview = [[UIView alloc] initWithFrame:CGRectMake(15, _scrollView.bottom +8, self.width -30, 26)];
        _noticeview.backgroundColor = RGBA(230, 247, 250, 1);
        KViewRadius(_noticeview, 2);
    }
    return _noticeview;
}
- (UIImageView *)noticeimageview
{
    if (!_noticeimageview) {
        _noticeimageview = [[UIImageView alloc] initWithFrame:CGRectMake(6,26/2 - RealValue_W(24)/2, RealValue_W(28), RealValue_W(24))];
        _noticeimageview.image = [UIImage imageNamed:@"Notice"];
       
    }
    return _noticeimageview;
}
- (SGAdvertScrollView *)advertScrollViewCenter
{
    if (!_advertScrollViewCenter) {
        _advertScrollViewCenter = [[SGAdvertScrollView alloc] initWithFrame:CGRectMake(_noticeimageview.right + RealValue_W(24), 0, _noticeview.width -30, 26)];
        _advertScrollViewCenter.scrollTimeInterval = 2;
        _advertScrollViewCenter.backgroundColor = CLEARCOLOR;
        _advertScrollViewCenter.titleFont = AutoFont(14);
        _advertScrollViewCenter.delegate = self;
    }
    return _advertScrollViewCenter;
}
@end

//
//  HomeModel.h
//  EDF
//
//  Created by p on 2019/7/5.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeModel : NSObject
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *type;
@property(nonatomic,strong)NSString *chargepercent;
@property(nonatomic,strong)NSString *client;
@property(nonatomic,strong)NSString *od;
@property(nonatomic,copy)NSString *is_promotion;
@property(nonatomic,strong)NSString *image;
@end
@interface ClassModel : NSObject
@property(nonatomic,strong)NSString *images;
@property(nonatomic,strong)NSArray *dataArray;
@property(nonatomic,assign)BOOL isOpen;
@property(nonatomic,assign)CGFloat cellHeight;
@end
@interface BannerModel : NSObject
@property(nonatomic,strong)NSArray *viwepagers;
@property(nonatomic,strong)NSArray *notices;
@end

@interface LatelyModel : NSObject
@property(nonatomic,strong)NSString *displayname;
@property(nonatomic,strong)NSString *gamecode;
@property(nonatomic,strong)NSString *gameid;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *isusergame;
@property(nonatomic,strong)NSString *pic;
@property(nonatomic,strong)NSString *plat_id;
@end
NS_ASSUME_NONNULL_END

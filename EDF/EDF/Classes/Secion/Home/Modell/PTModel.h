//
//  PTModel.h
//  EDF
//
//  Created by p on 2019/7/10.
//  Copyright © 2019年 p. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTModel : NSObject
@property(nonatomic,strong)NSString *gameid;
@property(nonatomic,strong)NSString *is_promotion;
@property(nonatomic,assign)BOOL isusergame;//是否收藏
@property(nonatomic,strong)NSString *displayname;//名字
@property(nonatomic,strong)NSString *plat_id;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *pic;
@property(nonatomic,strong)NSString *gamecode;
@property(nonatomic,strong)NSString *orientation;
@property(nonatomic,strong)NSString *istrial;//是否试玩
@end

NS_ASSUME_NONNULL_END

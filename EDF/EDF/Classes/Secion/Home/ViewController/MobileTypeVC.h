//
//  MobileTypeVC.h
//  EDF
//
//  Created by p on 2019/7/9.
//  Copyright © 2019年 p. All rights reserved.
//

#import "EDBaseViewController.h"
#import "HomeModel.h"
typedef enum : NSUInteger {
    HOTType = 0,//推荐游戏
    SAVEType = 1,  //收藏游戏
    LATELYType = 2,//最近游戏
    ALLType = 3,  //所有游戏
}PTType;

@interface MobileTypeVC : EDBaseViewController
@property (nonatomic,readwrite)PTType ptType;
@property (nonatomic,readwrite)HomeModel * modle;

@end


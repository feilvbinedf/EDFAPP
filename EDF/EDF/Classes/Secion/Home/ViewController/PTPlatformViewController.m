//
//  PTPlatformViewController.m
//  EDF
//
//  Created by p on 2019/7/9.
//  Copyright © 2019年 p. All rights reserved.
//

#import "PTPlatformViewController.h"
#import "PTPageView.h"
@interface PTPlatformViewController ()
@property(nonatomic,strong) PTPageView *pageView;
@end

@implementation PTPlatformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _modle.name;
    [self.view addSubview:self.pageView];
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem RightitemWithImageNamed:@"" title:@"搜索" target:self action:@selector(search)];
}
- (void)search
{
    
}
- (PTPageView *)pageView {
    _pageView = [[PTPageView alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight, UIScreenWidth, UIScreenHeight - kStatusBarAndNavigationBarHeight)
                                       withTitles:@[@"推荐游戏",@"收藏游戏",@"最近游戏",@"全部游戏"]
                              withViewControllers:@[@"MobileTypeVC",@"MobileTypeVC",@"MobileTypeVC",@"MobileTypeVC"]
                                   withParameters:nil withModels:_modle];
    
    
    return _pageView;
}

@end

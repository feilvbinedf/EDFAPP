//
//  EDPTGameDetailViewController.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/8/8.
//  Copyright © 2019 p. All rights reserved.
//

#import "EDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EDPTGameDetailViewController : EDBaseViewController
@property (nonatomic, copy) NSString *urlStr;
@property (nonatomic, assign) BOOL allowRotation;//是否允许横屏竖屏
@end

NS_ASSUME_NONNULL_END

//
//  CashCardCenterListPopView.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CashCardCenterListPopView : UIView
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UITableView *listTb;

@property (weak, nonatomic) IBOutlet UIButton *otherBtn;


@property(nonatomic,strong)NSArray * dataArry;


@property (nonatomic, copy) void (^seclectBlock)(BOOL sure, NSDictionary * dataDicc);

@end

NS_ASSUME_NONNULL_END

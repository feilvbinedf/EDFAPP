//
//  PopCenterCardListCell.h
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/26.
//  Copyright © 2019 p. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopCenterCardListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UIButton *useBtn;

@end

NS_ASSUME_NONNULL_END

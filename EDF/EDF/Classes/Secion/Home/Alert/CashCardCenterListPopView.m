//
//  CashCardCenterListPopView.m
//  EDF
//
//  Created by 微笑吧阳光 on 2019/12/26.
//  Copyright © 2019 p. All rights reserved.
//

#import "CashCardCenterListPopView.h"
#import "PopCenterCardListCell.h"
#import "CJItemWelfareModel.h"

@interface CashCardCenterListPopView()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation CashCardCenterListPopView

-(void)awakeFromNib{
    
    
    [super awakeFromNib];
    
    
    [self creatData];
    
    
}

-(void)setDataArry:(NSArray *)dataArry{
    _dataArry = dataArry;

    YYLog(@"setDataArry---%@",dataArry);
    
    [self.listTb reloadData];
    
    
}
-(void)creatData{
    self.listTb.backgroundColor = [UIColor clearColor];
    self.listTb.delegate = self;
    self.listTb.dataSource = self;

    WEAKSELF
    [self.otherBtn add_BtnClickHandler:^(NSInteger tag) {
       STRONGSELFFor(weakSelf);
        if (strongSelf.seclectBlock) {
                  strongSelf.seclectBlock(NO,nil);
              }
    }];
    
}


#pragma mark ----,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"PopCenterCardListCell";
    PopCenterCardListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (PopCenterCardListCell *)[[[NSBundle mainBundle] loadNibNamed:@"PopCenterCardListCell" owner:self options:nil] lastObject];
    }
     CJItemWelfareModel * modell = self.dataArry[indexPath.row];
   // cell.itemModel = modell;
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.nameLab.text = modell.name;
    cell.timeLab.text = modell.expire_txt;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    WEAKSELF
    [cell.useBtn add_BtnClickHandler:^(NSInteger tag) {
        STRONGSELFFor(weakSelf);
        NSMutableDictionary * dataDicc = [NSMutableDictionary dictionary];
        [dataDicc setValue:modell.id forKey:@"coupon_id"];
        if (strongSelf.seclectBlock) {
            strongSelf.seclectBlock(YES,dataDicc);
        }
    }];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


#pragma makr ------UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}



@end

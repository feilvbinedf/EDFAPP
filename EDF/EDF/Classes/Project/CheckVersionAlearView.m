//
//  CheckVersionAlearView.m
//  Calculated
//
//  Created by p on 2018/5/21.
//  Copyright © 2018年 p. All rights reserved.
//

#import "CheckVersionAlearView.h"
#define MainScreenRect [UIScreen mainScreen].bounds
@interface CheckVersionAlearView()
@property (nonatomic,strong)UIView   *bgView1;
@property (nonatomic,strong)UIView   *bgView;
@property (nonatomic,strong)UIImageView  *imageView;
@property (nonatomic,strong)UILabel  *titleLable;
@property (nonatomic,strong)UILabel  *measageLable;
@property (nonatomic,strong)UIButton  *okButton;
@property (nonatomic,strong)UIButton  *cancelButton;
@property (nonatomic,strong)NSString  *titleStr;
@property (nonatomic,strong)NSString  *measage;
@property (nonatomic,assign)NSInteger  state;
@property (nonatomic,strong)NSString  *canceltext;
@property (nonatomic,strong)NSString  *confirmtext;
@property (nonatomic,strong)UIButton  *QcancelButton;
@property (nonatomic,strong)UIImageView *QimageView;
@end
@implementation CheckVersionAlearView
- (void)setNeedsLayout
{
    if ( _state == 2) {
        UIBezierPath * W_bezierPath = [UIBezierPath bezierPath];
        [W_bezierPath moveToPoint:CGPointMake(_bgView.width/2 , _bgView.height - RealValue_W(97))];
        [W_bezierPath addLineToPoint:CGPointMake(_bgView.width/2 , _bgView.height)];
        CAShapeLayer * W_shapeLayer = [CAShapeLayer layer];
        W_shapeLayer.strokeColor = RGBA(216, 216, 216, 1).CGColor;
        W_shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        W_shapeLayer.path = W_bezierPath.CGPath;
        W_shapeLayer.lineWidth = 0.5f;
        [_bgView.layer addSublayer:W_shapeLayer];
        
        UIBezierPath * bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint:CGPointMake(0 , _bgView.height - RealValue_W(97))];
        [bezierPath addLineToPoint:CGPointMake(_bgView.width , _bgView.height - RealValue_W(97))];
        CAShapeLayer * shapeLayer = [CAShapeLayer layer];
        shapeLayer.strokeColor = RGBA(216, 216, 216, 1).CGColor;
        shapeLayer.fillColor  = [UIColor clearColor].CGColor;
        shapeLayer.path = bezierPath.CGPath;
        shapeLayer.lineWidth = 0.5f;
        [_bgView.layer addSublayer:shapeLayer];
    }
    
}
- (instancetype)initWithmessage:(NSString *)message
                       titleStr:(NSString *)titleStr
                        openUrl:(NSString *)url
                        confirm:(NSString *)confirm
                         cancel:(NSString *)cancel
                          state:(NSInteger )state
                  RechargeBlock:(RechargeBlock)block
{
    
    if (self=[super init])
    {
        self.frame=MainScreenRect;
        self.backgroundColor=[UIColor colorWithWhite:.0 alpha:.6];
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
        [self addGestureRecognizer:tap];
        _measage = message;
        _state = state;
        _canceltext = cancel;
        _confirmtext = confirm;
        _titleStr = titleStr;
        [self addSubview:self.bgView1];
        [_bgView1 addSubview:self.bgView];
                
        if (state ==1 )
        {
           
            [_bgView addSubview:self.imageView];
            [_bgView addSubview:self.titleLable];
            [_bgView addSubview:self.measageLable];
            [_bgView addSubview:self.okButton];
            _okButton.frame = CGRectMake(20 , _bgView.height - RealValue_W(74 +40), _bgView.width - 40, RealValue_W(74));
            _okButton.backgroundColor =WHITECOLOR;
            [_okButton setTitleColor:MAINCOLOR forState:UIControlStateNormal];
            _okButton.layer.masksToBounds     =YES;
            _okButton.layer.cornerRadius      =2;
            KViewBorder(_okButton,RGBA(216, 216, 216, 1), 0.5);
            
        }else if (state ==2)
        {
            [_bgView addSubview:self.imageView];
            [_bgView addSubview:self.titleLable];
            [_bgView addSubview:self.measageLable];
            [_bgView addSubview:self.okButton];
            [_bgView addSubview:self.cancelButton];
 
        }else if (state==3) {
            [_bgView addSubview:self.imageView];
            [_bgView addSubview:self.titleLable];
            [_bgView addSubview:self.measageLable];
            [_bgView addSubview:self.cancelButton];
            _cancelButton.frame = CGRectMake(20 , _bgView.height - RealValue_W(74 +40), _bgView.width - 40, RealValue_W(74));
            _cancelButton.backgroundColor = ColorStr(@"#00A7B3");
            [_cancelButton setTitleColor:WHITECOLOR forState:UIControlStateNormal];
            _cancelButton.layer.masksToBounds     =YES;
            _cancelButton.layer.cornerRadius      =2;
            
        }else if (state ==4)
        {
            _bgView.width =  RealValue_W(550);
            _bgView.height = RealValue_W(564);
            [_bgView addSubview:self.QcancelButton];
            [_bgView addSubview:self.QimageView];
            [_bgView addSubview:self.measageLable];
            _measageLable.mj_y = _QimageView.bottom;
        }
        
    }
    
    self.recharBlock=block;
    return self;
}
- (void)setQrcodeUrl:(NSString *)qrcodeUrl
{
    _qrcodeUrl = qrcodeUrl;
    [_QimageView sd_setImageWithURL:[NSURL URLWithString:qrcodeUrl]];
}
- (UIView *)bgView1
{
    if (!_bgView1) {
        _bgView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, RealValue_W(540), RealValue_H(580))];
        _bgView1.center = self.center;
        _bgView1.backgroundColor=[UIColor colorWithWhite:0 alpha:0];

        UIImageView * image =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, RealValue_W(540), RealValue_H(240))];
        image.image = [UIImage imageNamed:@"Version"];
        [_bgView1 addSubview:image];
    }
    return _bgView1;
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, RealValue_H(240), RealValue_W(540), RealValue_H(340))];
       
        _bgView.backgroundColor=WHITECOLOR;
    }
    return _bgView;
}
- (UILabel *)measageLable
{
    if (!_measageLable) {
        _measageLable = [[UILabel alloc] initWithFrame:CGRectMake(RealValue_H(40), RealValue_H(30), _bgView.width - 40, 60)];
        _measageLable.text = _measage;
        _measageLable.textAlignment=NSTextAlignmentCenter;
        _measageLable.font=AutoFont(15);
        _measageLable.textColor = RGBA(68, 68, 68, 1);
        _measageLable.numberOfLines = 0;
        _measageLable.adjustsFontSizeToFitWidth = YES;
    }
    return _measageLable;
}
- (UIButton *)okButton
{
    if (!_okButton) {
        _okButton = [[UIButton alloc] initWithFrame:CGRectMake(_bgView.width/2, _bgView.height - RealValue_W(96), _bgView.width/2, RealValue_W(96))];
        
        [_okButton setTitle:_confirmtext forState:(UIControlStateNormal)];
        [_okButton setTitleColor:MAINCOLOR forState:UIControlStateNormal];
        [_okButton addTarget:self action:@selector(OKclick) forControlEvents:(UIControlEventTouchUpInside)];
        _okButton.layer.masksToBounds     =YES;
        _okButton.layer.cornerRadius      =2;
        _okButton.titleLabel.font = AutoFont(15);
    }
    return _okButton;
}
- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, _bgView.height - RealValue_W(96), _bgView.width/2, RealValue_W(96))];
        
        [_cancelButton setTitle:_canceltext forState:(UIControlStateNormal)];
        [_cancelButton setTitleColor:MAINCOLOR forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelclick) forControlEvents:(UIControlEventTouchUpInside)];
        _cancelButton.titleLabel.font = AutoFont(15);
        
    }
    return _cancelButton;
}
-(void)OKclick{
    
    if (self.recharBlock)
    {
        self.recharBlock();
        
    }
    if (_state == 2)
    {
        [self dismissAlertView];
    }
    
}
- (void)dismiss
{
    if (_state == 3 ||_state == 4)
    {
        [self dismissAlertView];
    }
    
}
-(void)cancelclick
{
    [self dismissAlertView];
}
-(void)dismissAlertView
{
    [self removeFromSuperview];
    
}

-(void)showLXAlertView
{
    [kWindow addSubview:self];
    [self setShowAnimation];
}
-(void)setShowAnimation{
    WeakSelf;
    switch (_animationStyle) {
            
        case LXASAnimationDefault:
        {
            [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [weakSelf.bgView1.layer setValue:@(0) forKeyPath:@"transform.scale"];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.23 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [weakSelf.bgView1.layer setValue:@(1.2) forKeyPath:@"transform.scale"];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.09 delay:0.02 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                        [weakSelf.bgView1.layer setValue:@(.9) forKeyPath:@"transform.scale"];
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.05 delay:0.02 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                            [weakSelf.bgView1.layer setValue:@(1.0) forKeyPath:@"transform.scale"];
                        } completion:^(BOOL finished) {
                            weakSelf.userInteractionEnabled = YES;
                        }];
                    }];
                }];
            }];
        }
            break;
            
        case LXASAnimationLeftShake:{
            
            CGPoint startPoint = CGPointMake(-_bgView1.width, self.center.y);
            weakSelf.bgView1.layer.position=startPoint;
            
            //damping:阻尼，范围0-1，阻尼越接近于0，弹性效果越明显
            //velocity:弹性复位的速度
            [UIView animateWithDuration:.8 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                weakSelf.bgView1.layer.position=self.center;
                
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
            
        case LXASAnimationTopShake:{
            
            CGPoint startPoint = CGPointMake(self.center.x, -_bgView1.frame.size.height);
             _bgView1.layer.position=startPoint;
            
            //damping:阻尼，范围0-1，阻尼越接近于0，弹性效果越明显
            //velocity:弹性复位的速度
            [UIView animateWithDuration:.8 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                weakSelf.bgView1.layer.position=self.center;
                
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
            
        case LXASAnimationNO:{
            
        }
            
            break;
            
        default:
            break;
    }
}
@end

//
//  AppDelegate.h
//  EDF
//
//  Created by p on 2019/7/4.
//  Copyright © 2019年 p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) NSInteger index;
@property (copy, nonatomic) NSString *loveHuodongID;
/**
 * 是否允许转向
 */
@property(nonatomic,assign)BOOL allowRotation;

@property(nonatomic,assign)BOOL is_RegisteredAlert;
@property (strong, nonatomic) NSString * customer_service;
+(AppDelegate *)shareAppdelegate;
@end

